var hideBlock = function ($mainBlock, $hideBlock){
	$mainBlock.on('click', function(e) {
		if ($hideBlock.css('display') != 'block') {
			$mainBlock.toggleClass('active')
			$hideBlock.slideToggle('slow');
			var firstClick = true;
	        $(document).bind('click.myEvent', function(e) {
	            if (!firstClick && $(e.target).closest($hideBlock).length == 0) {
	                $hideBlock.slideUp('slow');
	                $mainBlock.removeClass('active')
	                $(document).unbind('click.myEvent');
	            }
	            firstClick = false;
	        });
		}

		//e.preventDefault();
	})
}

var sidebarShow = function($link){
    $link.on('click', function(){ 
    	var $this = $(this);
    	if($this.next('ul').length){ 
			$this.parent().find('.sidebar_submenu').slideToggle().toggleClass('active');
			return false;
    	}
    })
}

$.fn.equivHeight = function (){
	var $blocks = $(this),
		maxH    = $blocks.eq(0).height(); 			
	$blocks.each(function(){
		maxH = ( $(this).height() > maxH ) ? $(this).height() : maxH + 5 + '%';				
	});
	$blocks.height(maxH); 
}

var showSidebar = function(){
	var $this 		= $(this),
		$sidebar 	= $('.sidebar'),
		$main 		= $('.main'),
		$window 	= $(window);			
	$('.btn-menu').on('click', function(){
		if(($(window).width() > 992) === true){
			if($sidebar.hasClass('active')){
				$sidebar.slideUp('slow');
				$sidebar.removeClass('active');
				$main.animate({
					'margin-left':'0px'
				},200);
			} else{
				$sidebar.addClass('active');
				$sidebar.slideDown('slow');				
				$main.animate({
					'margin-left':'199px'
				},200);
			}	
		} else{
			$sidebar.slideToggle('slow');
		}					
	})
}

var resizeFooter = function(){
	$('.main').removeAttr('style');
	$('.footer').removeAttr('style');
	if(($('.main_block').height() + 40) > $(window).height()){
		$('.main').height($(window).height() - 70 + 'px');
	} else {
			if($('.main').height() < $(window).height()){
			$('.main').height($(window).height() - 70 + 'px');
			$('.footer').css({
				'bottom':'0'
			})
		} else{
			$('.main').removeAttr('style');		
			$('.footer').css({
				'position':'relative'
			})
		}
	}
}

var calendarHeight = function(){
	$('.calendar_body').height($('.web_radar').height() - $('.calendar_title').height() - 84 - $('.summary_content').height() + 'px');
}

var disabledInput = function(){
	var $disabled = $('.form-group').find('[disabled]');
	if($disabled){
		$disabled.closest('.form-group').addClass('disabled');
	}
}

var closeBlock = function($clickBlock, $blockCLose){
	$clickBlock.on('click', function(){
		$(this).closest($blockCLose).slideUp('slow');
	})
}
var showBlock = function($clickBlock, $dadBlock, $blockShow){
	$clickBlock.on('click', function(e){
		e.preventDefault();
		$(this).closest($dadBlock).find($blockShow).slideToggle();
	})
}

var removeMore = function($remove_more){
	$(document).on('click', $remove_more, function(e){
		e.preventDefault();
		$(this).closest('.row').remove();
		resizeFooter();
	})
}

var forBlockFooter = function($click_block){
	$(document).on('click', $click_block, function(){
		resizeFooter();
	})
}

var rating = function(){
	$('.progress-pie-chart').each(function(){
	    var $this 	= $(this),
	        rating 	= parseFloat($this.data('percent')),
	        percent = (rating/5)*100;
	        deg = 360*percent/100;
	    if (percent > 50) {
	        $this.addClass('gt-50');
	    }	   
	    $this.find('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
	    $this.find('.ppc-percents span').html('<cite class="bold">'+ rating +'</cite>');
	});
}

var uploadFile = function($uploadBlock){
	$(this).each(function(){
		$('.fileSelect').on('click', function(e){
			e.preventDefault();
			$(this).closest($uploadBlock).find('.uploadbtn').click();
		})
	})		
}

var socialBorder = function($mainBlock){
	$mainBlock.each(function(){
		var $_icon = $(this).find('i');
		if($_icon.hasClass('i-twitter')){
			$(this).css('border-left', '2px solid #2fabe1');
			$(this).hover(function(){
				$(this).find('i').css('color', '#2fabe1');
			}, function() {
				$_icon.removeAttr('style');
			})
		} else if($_icon.hasClass('i-facebook')){
			$(this).css('border-left', '2px solid #3c5b9b');
			$(this).hover(function(){
				$_icon.css('color', '#3c5b9b');
			}, function() {
				$_icon.removeAttr('style');
			})
		} else if($_icon.hasClass('i-google')){
			$(this).css('border-left', '2px solid #f63e28');
			$(this).hover(function(){
				$_icon.css('color', '#f63e28');
			}, function() {
				$_icon.removeAttr('style');
			})
		} else if($_icon.hasClass('i-linkedin')){
			$(this).css('border-left', '2px solid #0079BA');
			$(this).hover(function(){
				$_icon.css('color', '#0079BA');
			}, function() {
				$_icon.removeAttr('style');
			})
		} else if($_icon.hasClass('i-instagram')){
			$(this).css('border-left', '2px solid #B17D4E');
			$(this).hover(function(){
				$_icon.css('color', '#B17D4E');
			}, function() {
				$_icon.removeAttr('style');
			})
		}		
	})
}