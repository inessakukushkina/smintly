(function ($) {
    $(document).ready(function () {
        /**
         * Contains 'post a link' form / Suggested RSS form / Custom RSS form
         *
         * @type {*|HTMLElement}
         */
        var $container = $('#ajax-container');

        /**
         * Container for file is uploading
         *
         * @type {*|HTMLElement}
         */
        var $file_name = $('#filename');

        /**
         * Form for post to socials
         *
         * @type {*|HTMLElement}
         */
        var $form = $('#post-update-form');

        var $logo = $('#image-designer-logo');

        var $bgImages = $('.image-designer-bg-image');

        var $addSecondaryText = $('#image-designer-add-secondary-text');
        var $secondaryTextBlock = $('#secondary-text-block');

        var $headlineText = $('#image-designer-headline-text');
        var $secondaryText = $('#image-designer-secondary-text');

        var $pickColor = $(".pick-a-color");

        var $bgContrast = $('#image-designer-bg-type-contrast');

        var filters = [
            'contrast',
            'blurred',
            'grayscale'
        ];

        var canvas = new fabric.Canvas('image-designer-canvas');
        var canvasImage = null;
        var canvasBgImage = null;
        var canvasHeaderText = new fabric.Text($headlineText.val(), {
            left: 10,
            top: 10,
            fontSize: 30,
            fill: '#ffffff',
            shadow: 'rgba(0,0,0,0.3) 1px 1px 1px'
        });
        var canvasSecondaryText = new fabric.Text($secondaryText.val(), {
            left: 10,
            top: 100,
            fontSize: 22,
            fill: '#ffffff',
            shadow: 'rgba(0,0,0,0.3) 1px 1px 1px'
        });
        canvas.backgroundColor = 'grey';
        canvas.add(canvasHeaderText);

        $pickColor.pickAColor({
            showHexInput: false
        });

        $pickColor.on('change', function() {
            var $this = $(this);
            if($this.attr('name') == 'headline_color') {
                canvasHeaderText.setColor('#'+$this.val());
            } else {
                canvasSecondaryText.setColor('#'+$this.val());
            }
            canvas.renderAll();
        });

        $headlineText.on('keyup blur', function(){
            var $this = $(this);
            canvasHeaderText.setText($this.val());
            canvas.renderAll();
        });

        $secondaryText.on('keyup blur', function(){
            var $this = $(this);
            canvasSecondaryText.setText($this.val());
            canvas.renderAll();
        });

        $container.on('change', 'input[name="attachment_type"]', function () {
            var $this = $(this);
            var $block = $('#'+$this.val()+'-block');
            if($block.css('display') == 'none') {
                $('.attachment-block').hide(200);
                $block.show(200);
            }
        });

        $addSecondaryText.on('click', function() {
            var $this = $(this);
            if($this.data('added') == 'true') {
                $this.data('added', 'false');
                $this.html('+ Add secondary text');
                $secondaryTextBlock.hide(1000);
                canvas.remove(canvasSecondaryText);
                canvas.renderAll();
            } else {
                $this.data('added', 'true');
                $this.html('- Remove secondary text');
                $secondaryTextBlock.show(1000);
                canvas.add(canvasSecondaryText);
                canvas.renderAll();
            }
            return false;
        });

        $bgImages.on('click', function(){
            var $this = $(this);
            $bgImages.removeClass('active');
            $this.addClass('active');
            fabric.Image.fromURL($this.data('src'), function(img) {
                img.set({width: canvas.width, height: canvas.height, originX: 'left', originY: 'top'});
                canvas.setBackgroundImage(img, function(){});
                canvasBgImage = img;
                applyedFilters = [];
                $('input[name="bg_image_type"]').trigger('change');
            });
        });
        $($bgImages[Math.floor(Math.random() * $bgImages.length)]).trigger( "click" );


        fabric.Image.filters.Blur = fabric.util.createClass({

            type: 'Blur',
            radius: 9,

            applyTo: function(canvasEl) {
                stackBlurImage(canvasEl, this.radius, false);
            }
        });

        fabric.Image.filters.Blur.fromObject = function(object) {
            return new fabric.Image.filters.Redify(object);
        };


        $container.on('change', 'input[name="bg_image_type"]', function() {
            var $this = $(this);
            if(!this.checked) {
                return;
            }
            canvasBgImage.filters[filters.indexOf('contrast')] = ($bgContrast[0].checked) ?
                new fabric.Image.filters.Tint({
                    color: '#000',
                    opacity: 0.5
                }) :
                null;
            switch ($this.val()) {
                case 'normal':
                    canvasBgImage.filters[filters.indexOf('grayscale')] = null;
                    canvasBgImage.filters[filters.indexOf('blurred')] = null;
                    break;
                case 'blurred':
                    canvasBgImage.filters[filters.indexOf('grayscale')] = null;
                    canvasBgImage.filters[filters.indexOf($this.val())] = new fabric.Image.filters.Blur({
                        radius: 13
                    });
                    break;
                case 'grayscale':
                    canvasBgImage.filters[filters.indexOf('blurred')] = null;
                    canvasBgImage.filters[filters.indexOf($this.val())] = new fabric.Image.filters.Grayscale();
                    break;
            }
            canvasBgImage.applyFilters(canvas.renderAll.bind(canvas));
        });

        $bgContrast.on('change', function() {
            $('input[name="bg_image_type"]').trigger('change');
        });

        var $current_loading, $current_button;
        var options = {
            maxFileSize: 10000000,
            dataType: 'json',
            multiple: true,
            done: function (e, data) {
                $current_button.show();
                $current_loading.hide();
                var result = data.result[0];

                if (undefined !== result.error && result.error.length) {
                    $file_name.empty();
                    showFlashErrors(result.error);
                    return;
                }
                var size = data.files[0].size / 1000;
                var type = $(this).attr('id');
                var preview;
                if (type == 'videos') {
                    preview = '<div class="preview" >' +
                    '<img class="img-close" src="' + g_settings.base_url + '/public/images/im_prev_close.png" />' +
                    '<video class="img-preview">' +
                    '<source src="' + result.url + '"/>' +
                    '<span>' + data.files[0].name + '</span>' +

                    '</video></div>';
                } else if(type =='image-designer') {
                    preview = '<img class="img-close logo-close" src="' + g_settings.base_url + '/public/images/im_prev_close.png" />' +
                    '<img class="img-preview logo-preview" src="' + result.url + '" />';
                } else {
                    preview = '<div class="preview" >' +
                    '<img class="img-close" src="' + g_settings.base_url + '/public/images/im_prev_close.png" />' +
                    '<img class="img-preview" src="' + result.url + '" />' +
                    '</div>';
                }
                if (type == 'image-designer') {
                    $logo.show();
                    $logo.find('.preview').html(preview);
                    if(canvasImage === null) {
                        fabric.Image.fromURL(result.url, function(oImg) {
                            canvasImage = oImg;
                            var delta = canvasImage.getWidth() / 128;
                            canvasImage.setWidth(128);
                            canvasImage.setHeight(canvasImage.getHeight() / delta);
                            canvas.add(canvasImage);
                        });
                    } else {
                        canvasImage.setSrc(result.url);
                        canvas.renderAll();
                    }
                } else {
                    $(this).parent().find('.preview').remove();
                    $(this).parent().find('i').hide();
                    $(this).parent().prepend(preview);
                }
                $form.find('input[name=image_name]').val(data.files[0].name).trigger('change');
            },
            start: function () {
                //if ($(this).attr('id')!='gallery') {
                //    $('.file').hide();
                //}
            },
            progress: function (e, data) {
                //$load_div.show();
                //$progress.show();
                //var progress = parseInt(data.loaded / data.total * 100, 10);
                //$bar.css('width', progress + '%');
            },
            fail: function (e, data) {
                $current_button.show();
                $current_loading.hide();
                showFlashErrors('File type not supported. Please try again with a different image');
            },
            add: function (e, data) {
                $current_loading = $(this).parent().find('.loading');
                $current_button = $(this).parent().find('.fileSelect');
                $current_loading.show();
                $current_button.hide();
                if ($(this).attr('id') == 'videos') {
                    acceptFileTypes = /\.(wmv|avi|mpe?g|mp4|webm|3gpp|mpegps|flv)$/;
                } else if($(this).attr('id') == 'image-designer') {
                    acceptFileTypes = /\.(jpeg|jpg|png)$/;
                }  else {
                    acceptFileTypes = /\.(gif|jpeg|jpg|png)$/;
                }
                if (!acceptFileTypes.test(data.files[0].name)) {
                    showFlashErrors('Not allowed file type');
                    $current_button.show();
                    $current_loading.hide();
                    return false;
                } else {
                    data.submit();
                    $file_name.html(data.files[0].name);
                }

            },
            autoUpload: true,
            url: g_settings.base_url + 'social/create/upload_images'
        };

        $(".inputFile").fileupload(options);

        $container.on('click', '.img-close', function () {
            var $self = $(this);
            var parent = $self.parent();
            var img = parent.find('.img-preview');
            var vid = parent.find('video');
            if (vid.length) {
                fname = vid.find('source').attr('src');
            } else {
                fname = img.attr('src');
            }
            var url = g_settings.base_url + 'social/create/upload_images?file=' + fname;
            var $post_id = $('[name="post_id"]');
            if ($post_id.length) {
                url += '&post_id=' + $post_id.val();
            }
            $.ajax({
                url: url,
                type: 'DELETE',
                success: function (success) {
                    if (success) {
                        $file_name.empty();
                        $form.find('input[name=image_name]').val('').trigger('change');
                        var type = $form.find('input[name=image_name]').attr('id');
                        if(!type == 'imange-designer') {
                            parent.parent().find('i').show();
                            parent.remove();
                        } else {
                            parent.html('');
                            $logo.hide();
                            canvas.remove(canvasImage);
                            canvasImage = null;
                        }
                        $(".inputFile").fileupload(options);
                    }
                }
            });
            return false;
        });

    });
})(jQuery);