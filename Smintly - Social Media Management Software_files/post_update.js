(function ($) {
    $(document).ready(function () {

        /**
         * Contains 'post a link' form / Suggested RSS form / Custom RSS form
         *
         * @type {*|HTMLElement}
         */
        var $container = $('#ajax-container');

        /**
         * Used for RSS links -- fill then user click on post radiobutton
         * Contains link for selected post
         * Then we load 'post a link' form - insert it to 'url' field
         *
         * @type {String}
         */
        var selected_link = '';

        /**
         * See 'selected_link' for more comments
         * Then we load 'post a link' form - insert it to 'description' textarea
         *
         * @type {String}
         */
        var selected_description = '';

        /**
         * Container of feed of rss
         *
         * @type {*|HTMLElement}
         */
        var $fcontainer = $('div.feed');

        var $main_block = $('.main_block');

        /**
         * Custom RSS feed block - click on one of the user RSS titles
         * Load selected RSS feed into section right
         */
        $container.on('change', 'select[name="feed"]', function () {
            var $self = $(this);

            wait();
            $.ajax({
                url: g_settings.base_url + 'social/create/post_rss',
                type: 'POST',
                dataType: 'json',
                data: 'feed=' + $self.val(),
                success: function (response) {
                    if (response.success) {
                        $fcontainer.html(response.html);
                        //update selectik items
                        $(".select_block").each(function (indx) {
                            var self = $(this);
                            $(this).ddslick({
                                width: self.data('width') || 174,
                                height: self.data('height') || null
                            })
                        });

                        /*$container.find('input').iCheck({
                         checkboxClass: 'icheckbox_minimal-grey',
                         radioClass: 'iradio_minimal-grey',
                         increaseArea: '20%' // optional
                         });*/

                    }
                    stopWait();
                }
            });
            return false;
        });
        /**
         * Load forms into bottom of the page (post a ling / RSS)
         * Get html from controller
         *
         * @param action
         */
        function load_blocks_html(action) {
            $.ajax({
                url: g_settings.base_url + 'social/create/' + action,
                type: 'POST',
                success: function (html) {

                    $container.html(html);
                    $container.find('.custom-form').checkBo();
                    $container.find('.input_date').datepicker();
                    $container.find('.chosen-select').chosen();
                    rebind_vars($container);

                    //update selectik items
                    $(".select_block").each(function (indx) {
                        var self = $(this);
                        $(this).ddslick({
                            width: self.data('width') || 174,
                            height: self.data('height') || null
                        })
                    });

                    /*$container.find('input').iCheck({
                     checkboxClass: 'icheckbox_minimal-grey',
                     radioClass: 'iradio_minimal-grey',
                     increaseArea: '20%' // optional
                     });*/


                },
                complete: function () {
                    //if we load html after selecting RSS post
                    // we need to insert selected data into inputs
                    if ($container.find('input[name=url]').length) {
                        $container.find('input[name=url]').val(selected_link);
                        $container.find('textarea[name=description]').val(selected_description.trim());
                        $('#attachment').hide();
                    }
                    $('#post-custom-rss-link').attr('id', 'post-button');


                }
            })
        }

        /**
         * Hide schedule date settings (date / time / am \ pm)
         */
        $main_block.on('change', '#immediate-type', function () {
            if ($('#immediate-type').parents('label').hasClass('checked')) {
                $main_block.find('#schedule-settings').hide();
            }
        });

        /**
         * Show schedule date settings (date / time / am \ pm)
         */
        $main_block.on('change', '#schedule-type', function () {
            if ($('#schedule-type').parents('label').hasClass('checked')) {
                $main_block.find('#schedule-settings').show();
            }
        });

        /**
         * Strip HTML and PHP tags from a string
         *
         * @param str
         * @returns {*|XML|string|void}
         */
        function strip_tags(str) {
            return str.replace(/<\/?[^>]+>/gi, '');
        }


        /**
         * Click of some of posts in RSS feed
         * (click on 'radio' to make it checked)
         */
        $main_block.on('change', 'input[name=rss_feed_item]', function () {
            var $self = $(this);
            var $info_container = $self.parents('label');
            selected_link = $info_container.next().next().attr('href');
            selected_description = strip_tags($info_container.text());
        });

        /**
         * Checked RSS post data sended to 'post a link' form
         */
        $main_block.on('click', '#post-custom-rss-link', function () {
            load_blocks_html('get_post_a_link_html');
            return false;
        });

        $main_block.on('click', '#post-button', function () {
            $('#post-update-form').submit();
        });

        /**
         * Send data to posting link
         */
            //Do we need this piece of code???
        $container.on('submit', '#post-update-form', function () {
            var $self = $(this);
            wait();

            $.ajax({
                url: $self.attr('action'),
                data: $self.serialize(),
                type: 'POST',
                dataType: 'JSON',
                success: function (response) {
                    if (response.success) {
                        $self[0].reset();
                        $self.find('span.checkBox, span.radio').css('background-position', '0 0');
                        $self.find('#schedule-settings').hide();
                        $(response.message).insertBefore($('.page-wrapper'));
                    } else {
                        clearErrors();
                        showFormErrors(response.errors);
                    }
                    stopWait();
                }

            });
            return false;
        });

    });
})(jQuery);
