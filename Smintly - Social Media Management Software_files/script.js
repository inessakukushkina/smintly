$(document).ready(function() {	
	hideBlock($('.user-nav_link'), $('.sub_menu'));
	sidebarShow($('.sidebar a'));

	showSidebar();
	$('.custom-form').checkBo();

	hideBlock($('.collapse-button'), $('.head_nav'));

	if(!$('.web_radar').length){
		$('.box_dashboard').attr('style', 'width:100% !important;');
		$('.box_content').addClass('col-lg-3');
	}
	$('.clear').on('click', function(e){
		e.preventDefault();
		$(this).closest('.form-group').find('.form-control').removeAttr('value');
		$(this).remove();
	});

	$('.chosen-single').find('div').addClass('button-select');

	$('.button-select').find('b').addClass('fa').addClass('fa-angle-down');	

	$(".chosen-search").remove();

	$('.cb-remove').on('click', function(){
		$(this).parent().parent().remove();
		//resizeFooter();
	});

	$('.show_block').on('click', function(e){
		e.preventDefault();
		$(this).closest('.form-group').find('.toggle_block').slideToggle();
		//resizeFooter();
	});

	/*$(document).on('click', '.btn-add', function(){
		$(this).closest('.row').find('.insert_block').clone(true)
		.insertAfter($(this).closest('.row').find('.past_block'))
		.removeClass('hidden').removeClass('insert_block');
		//resizeFooter();
	});*/

/*	$(document).on('click', '.more-link', function(){
		$(this).closest('.row').find('.more_block').clone(true).insertBefore($(this).parent()).removeClass('hidden').removeClass('more_block');
		//resizeFooter();
	});*/

	$('.invite_user').on('click', function(e){
		e.preventDefault();
		$(this).closest('.row').next('.row').slideToggle();
	})

	$('.input_date').datepicker();

	/*$('.nav-tabs a').on('click', function (e) {
		e.preventDefault()
		$(this).tab('show');
		//resizeFooter();
	})*/

	rating();
	socialBorder($('.web_radar_content'));
	socialBorder($('.radar_body'));
	socialBorder($('.post_content'));



	//forBlockFooter('.cb-radio');
	//forBlockFooter('[data-toggle]');

	removeMore('.remove_more');

	uploadFile('.well-standart');

	calendarHeight();
	//resizeFooter();
	disabledInput();
	closeBlock($('.close_block'), '.notification');
	closeBlock($('.fa-remove'), '.validate');
    $('.cb-checkbox').on('click', function(){
        var self = $(this);
        var check = self.find('[type="checkbox"]')
        if(self.hasClass('checked')) {
            check.attr('checked','');
        } else {
            check.removeAttr('checked');
        }
    });
}) // close Ready

$(window).resize(function() {
	calendarHeight();
	//resizeFooter();
})

