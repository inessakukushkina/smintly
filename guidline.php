<?php require_once 'inc/modules/header.php';?>
<div class="guide">
	<div class="guide_nav">
		<div class="navbar navbar-fixed-top header">
			<div class="head_control m-r20">
				<div class="logo">
					<a href="">
						<img src="images/logo/logo.png" alt="" class="logo_img">
					</a>
				</div>
				<button class="btn btn-menu">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>	
			<h2 class="blue-color raleway p-t8">Repucaution guidline</h2>
		</div>
	</div>
	<?php require_once 'inc/modules/sidebar-guide.php';?>
	<div class="main p-rl20">
		<h2 id="grid" class="sub-header">Grid system</h2>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>
                        Extra small devices
                        <small>Phones (< 768px)</small>
                    </th>
                    <th>
                        Small devices
                        <small>Tablets (≥ 768px)</small>
                    </th>
                    <th>
                        Medium devices
                        <small>Desktops (>992px)</small>
                    </th>
                    <th>
                        Large devices
                        <small>Desktops (≥ 1200px)</small>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th class="text-nowrap">Grid behavior</th>
                    <td>Horizontal at all times</td>
                    <td colspan="3">Collapsed to start, horizontal above breakpoints</td>
                </tr>
                <tr>
                    <th class="text-nowrap">Container width</th>
                    <td>None (auto)</td>
                    <td>750px</td>
                    <td>970px</td>
                    <td>1170px</td>
                </tr>
                <tr>
                    <th class="text-nowrap">Class prefix</th>
                    <td><code>.col-xs-*</code></td>
                    <td><code>.col-sm-*</code></td>
                    <td><code>.col-md-*</code></td>
                    <td><code>.col-lg-*</code></td>
                </tr>
                <tr>
                    <th class="text-nowrap"># of columns</th>
                    <td colspan="4">12</td>
                </tr>
                <tr>
                    <th class="text-nowrap">Gutter width</th>
                    <td colspan="4">30px (15px on each side of a column)</td>
                </tr>
                <tr>
                    <th class="text-nowrap">Nestable</th>
                    <td colspan="4">Yes</td>
                </tr>
                <tr>
                </tbody>
            </table>
        </div>
        <div class="container-fluid p-0">
            <div class="row m-b15">
                <div class="col-sm-12">
                    <div class="bscol">col-sm-12</div>
                </div>
            </div>
            <div class="row m-b15">
                <div class="col-sm-6"><div class="bscol">col-sm-6</div></div>
                <div class="col-sm-6"><div class="bscol">col-sm-6</div></div>
            </div>
            <div class="row m-b15">
                <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
                <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
                <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
            </div>
            <div class="row m-b15">
                <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
                <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
                <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
                <div class="col-sm-3"><div class="bscol">col-sm-3</div></div>
            </div>
            <div class="row m-b15">
                <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
                <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
                <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
                <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
                <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
                <div class="col-sm-2"><div class="bscol">col-sm-2</div></div>
            </div>
            <div class="row m-b15">
                <div class="col-sm-4"><div class="bscol">col-sm-4</div></div>
                <div class="col-sm-8"><div class="bscol">col-sm-8</div></div>
            </div>
        </div>
            <pre class="prettyprint prettyprinted"><span class="tag">&lt;div</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"container"</span><span class="tag">&gt;</span><span class="pln">
  </span><span class="tag">&lt;div</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"row"</span><span class="tag">&gt;</span><span class="pln">
    </span><span class="tag">&lt;div</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"col-*-*"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/div&gt;</span><span class="pln">
  </span><span class="tag">&lt;/div&gt;</span><span class="pln">
</span><span class="tag">&lt;/div&gt;</span></pre> 

		<h2 id="typography" class="sub-header">Typography</h2>
		<p>HTML headings, <code>&lt;h1&gt;</code> through <code>&lt;h6&gt;</code></p>
        <div class="demo">              	
          	<table class="table">
                <thead>
	                <tr>
	                    <th>Tag</th>
	                    <th>Font size</th>
	                </tr>
                </thead>
                <tbody>
					<tr>
						<td><h1>h1. heading</h1></td>
						<td class="type-info">20px</td>
					</tr>
					<tr>
						<td><h2>h2. heading</h2></td>
						<td class="type-info">18px</td>
					</tr>
					<tr>
						<td><h3>h3. heading</h3></td>
						<td class="type-info">16px</td>
					</tr>
					<tr>
						<td><h4>h4. heading</h4></td>
						<td class="type-info">14px</td>
					</tr>
					<tr>
						<td><h5>h5. heading</h5></td>
						<td class="type-info">12px</td>
					</tr>
					<tr>
						<td><h6>h6. heading</h6></td>
						<td class="type-info">11px</td>
					</tr>
              	</tbody>
          </table>
        </div>

        <pre class="prettyprint prettyprinted"><span class="tag">&lt;h1&gt;</span><span class="pln">...</span><span class="tag">&lt;/h1&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h1"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span><span class="pln">
</span><span class="tag">&lt;h2&gt;</span><span class="pln">...</span><span class="tag">&lt;/h2&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h2"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span><span class="pln">
</span><span class="tag">&lt;h3&gt;</span><span class="pln">...</span><span class="tag">&lt;/h3&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h2"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span><span class="pln">
</span><span class="tag">&lt;h4&gt;</span><span class="pln">...</span><span class="tag">&lt;/h4&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h2"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span><span class="pln">
</span><span class="tag">&lt;h5&gt;</span><span class="pln">...</span><span class="tag">&lt;/h5&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h2"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span><span class="pln">
</span><span class="tag">&lt;h6&gt;</span><span class="pln">...</span><span class="tag">&lt;/h6&gt;</span><span class="pln"> </span><span class="com">&lt;!-- or --&gt;</span><span class="pln"> </span><span class="tag">&lt;TAG</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"h2"</span><span class="tag">&gt;</span><span class="pln">...</span><span class="tag">&lt;/TAG&gt;</span></pre>
        <h3 class="sub-header bold">Font family</h4>
        <table class="table">
	        <thead>
	        	<tr>
	        		<th>Font-family</th>
	        		<th>Class</th>
	        		<th>Variables</th>
	        	</tr>
	        </thead>
	        <tbody>
	        	<tr>
	        		<td>Roboto</td>
	        		<td>
	        			<ul>
	        				<li class="regRoboto">regRoboto</li>
	        				<li class="lightRoboto">lightRoboto</li>
	        			</ul>
	        		</td>
	        		<td>@roboto</td>
	        	</tr>
	        	<tr>
	        		<td>MontSerrat</td>
	        		<td>
	        			<ul>
	        			 	<li class="montSerrat">montSerrat</li>
	        			</ul>
	        		</td>
	        		<td>@montSerrat</td>
	        	</tr>
	        	<tr>
	        		<td>Lato</td>
	        		<td>
	        			<ul>
	        			 	<li class="laTo">laTo</li>
	        			</ul>
	        		</td>
	        		<td>@lato</td>
	        	</tr>
	        	<tr>
	        		<td>Helvetica</td>
	        		<td>
	        			<ul>
	        			 	<li class="helvetica">helvetica</li>
	        			</ul>
	        		</td>
	        		<td>@helvetica</td>
	        	</tr>
	        	<tr>
	        		<td>Raleway</td>
	        		<td>
		        		<ul>
		        			<li class="raleway">raleway</li>
		        		</ul>
	        		</td>
	        		<td>@raleway</td>
	        	</tr>
	        </tbody>        	
        </table>

        <h2 id="header" class="tables">Head Menu</h2>
        <?php require_once 'inc/modules/nav-header-login.php' ?>
        <br/>
       	<?php require_once 'inc/modules/nav-header-notLogin.php' ?>
       	<br/>
       	<?php require_once 'inc/modules/nav-header-sign.php' ?>
        
		<h2 id="icons" class="sub-header">Icons</h2>
		<table class="table icons">
			<thead>
				<th>View</th>
				<th>Name icons</th>
				<th>View</th>
				<th>Name icons</th>
				<th>View</th>
				<th>Name icons</th>
				<th>View</th>
				<th>Name icons</th>
				<th>View</th>
				<th>Name icons</th>
				<th>View</th>
				<th>Name icons</th>
				<th>View</th>
				<th>Name icons</th>
			</thead>
			<tbody>
				<tr>
					<td><i class="ti-home"></i></td>
					<td>ti-home</td>
					<td><i class="ti-world"></i></td>
					<td>ti-world</td>
					<td><i class="ti-comment-alt"></i></td>
					<td>ti-comment-alt</td>
					<td><i class="ti-bar-chart"></i></td>
					<td>ti-bar-chart</td>
					<td><i class="ti-thumb-up"></i></td>
					<td>ti-thumb-up</td>
					<td><i class="ti-stats-up"></i></td>
					<td>ti-stats-up</td>
					<td><i class="ti-user"></i></td>
					<td>ti-user</td>
				</tr>
				<tr>
					<td><i class="ti-settings"></i></td>
					<td>ti-settings</td>
					<td><i class="fa fa-times"></i></td>
					<td>fa fa-times</td>
					<td><i class="ti-file"></i></td>
					<td>ti-file</td>
					<td><i class="fa fa-user"></i></td>
					<td>fa fa-user</td>
					<td><i class="fa fa-thumbs-o-up"></i></td>
					<td>fa fa-thumbs-o-up</td>
					<td><i class="ti-twitter"></i></td>
					<td>ti-twitter</td>
					<td><i class="ti-comment"></i></td>
					<td>ti-comment</td>
				</tr>
				<tr>
					<td><i class="ti-comment"></i></td>
					<td>ti-comment</td>
					<td><i class="ti-bar-chart-alt"></i></td>
					<td>ti-bar-chart-alt</td>
					<td><i class="ti-facebook"></i></td>
					<td>ti-facebook</td>
					<td><i class="ti-twitter"></i></td>
					<td>ti-twitter</td>
					<td><i class="ti-server"></i></td>
					<td>ti-server</td>
					<td><i class="fa fa-reply"></i></td>
					<td>fa fa-reply</td>
					<td><i class="fa fa-share"></i></td>
					<td>fa fa-share</td>
				</tr>	
				<tr>
					<td><i class="fa fa-star"></i></td>
					<td>fa fa-star</td>
					<td><i class="fa fa-twitter-square"></i></td>
					<td>fa fa-twitter-square</td>
					<td><i class="ti-google"></i></td>
					<td>ti-google</td>
					<td><i class="ti-instagram"></i></td>
					<td>ti-instagram</td>
					<td><i class="ti-layers-alt"></i></td>
					<td>ti-layers-alt</td>
					<td><i class="ti-pulse"></i></td>
					<td>ti-pulse</td>
					<td><i class="ti-rss-alt"></i></td>
					<td>ti-rss-alt</td>
				</tr>
				<tr>
					<td><i class="ti-id-badge"></i></td>
					<td>ti-id-badge</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>		
			</tbody>
		</table>
		
		<h2 id="buttons" class="sub-header">Buttons</h2>
		<button class="btn btn-add">+ Add keyword</button>

		<button class="btn btn-save">Save</button>
		<button class="btn btn-facebook">Configure your account</button>
		<button class="btn btn-twitter">Configure your account</button>
		<button class="btn btn-youtube">Configure your account</button>
		<button class="btn btn-linkedin">Configure your account</button>
		<button class="btn btn-google">Configure your account</button>
		<button class="btn btn-instagram">Configure your account</button>
		<br/>
		<br/>
		<button class="btn btn-remove">Remove</button>

		<h2 id="forms" class="sub-header">Forms</h2>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group has-error">
					<input type="text" class="form-control">
					<p class="error_text">
						<i class="fa fa-exclamation-circle"></i>
						Please enter First name
					</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<input type="text" class="form-control" disabled="">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<textarea rows="5" class="form-control"></textarea>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group has-error">
					<textarea rows="5" class="form-control"></textarea>
					<p class="error_text">
						<i class="fa fa-exclamation-circle"></i>
						Please enter text
					</p>
				</div>
			</div>
		</div>

		<h2 id="dropdown" class="sub-header">Dropdown</h2>
		<div class="row">
			<div class="col-sm-6">
				<select class="chosen-select">
					<option value="">One</option>
					<option value="">Two</option>
					<option value="">Three</option>
					<option value="">Four</option>
					<option value="">Five</option>
					<option value="">Six</option>
					<option value="">Seven</option>
					<option value="">Eight</option>
					<option value="">Nine</option>
					<option value="">Ten</option>
				</select>
			</div>
		</div>

		<h2 id="notification" class="sub-header">Notification</h2>
		<div class="notification notify_good">
			<div class="container">
				<p class="notify_text">
					Text notify
					<i class="fa fa-remove close_block"></i>
				</p>				
			</div>			
		</div>
		<div class="notification notify_good">
			<div class="container">
				<p class="notify_text"><i class="fa fa-check"></i>
					Save successful. Your data has been successfully saved.
					<i class="fa fa-remove close_block"></i>
				</p>				
			</div>
		</div>
		<div class="notification notify_bad">
			<div class="container">
				<p class="notify_text">
					<i class="fa fa-exclamation-triangle"></i>
					Warning : Update Auth Security is fail
					<i class="fa fa-remove close_block"></i>
				</p>				
			</div>
		</div>
		<div class="notification notify_wait">
			<div class="container">
				<p class="notify_text">
					Please wait...
				</p>				
			</div>
		</div>
		<div class="notification notify_warning">
			<div class="container">
				<p class="notify_text">
					Authentication is processing...
					<i class="fa fa-remove close_block"></i>
				</p>				
			</div>
		</div>

		<h2 id="checkbo" class="sub-header">Checkbo & Radio</h2>
		<div class="row custom-form">
			<div class="col-sm-6">
				<h4 class="sub-header">Checkbox</h4>
				<div class="row ">
					<div class="col-xs-12">
						<label class="cb-checkbox">
							<input type="checkbox"/>
							Bank
						</label>
					</div>
					<div class="col-xs-12">
						<div class="p-b10">
							<label class="cb-checkbox">
								<input type="checkbox"/>
								Credit card
							</label>
						</div>
					</div>
				</div>
				  <pre class="prettyprint prettyprinted"><span class="tag">&lt;label</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"cb-checkbox"</span><span class="tag">&gt;</span><span class="pln">
</span><span class="tag">&lt;input</span><span class="pln"> </span><span class="atn">type</span><span class="pun">=</span><span class="atv">"checkbox"</span><span class="tag">&gt;</span><span class="pln">
Bank
</span><span class="tag">&lt;/label&gt;</span></pre> 
			</div>
			<div class="col-sm-6">
				<h4 class="sub-header">Radio</h4>
				<div class="row ">
					<div class="col-xs-12">
						<label class="cb-radio">
							<input type="radio"/>
							Bank
						</label>
					</div>
					<div class="col-xs-12">
						<div class="p-b10">
							<label class="cb-radio">
								<input type="radio"/>
								Credit card
							</label>
						</div>
					</div>
				</div>
										  <pre class="prettyprint prettyprinted"><span class="tag">&lt;label</span><span class="pln"> </span><span class="atn">class</span><span class="pun">=</span><span class="atv">"cb-radio"</span><span class="tag">&gt;</span><span class="pln">
</span><span class="tag">&lt;input</span><span class="pln"> </span><span class="atn">type</span><span class="pun">=</span><span class="atv">"radio"</span><span class="tag">&gt;</span><span class="pln">
Bank
</span><span class="tag">&lt;/label&gt;</span></pre> 
			</div>		
		</div>
		<h2 id="modal" class="sub-header">Modal window</h2>

		<a href="" class="link" data-toggle="modal" data-target=".modal">Click to call modal window</a>
		<div class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>				
					</div>
					<div class="modal-body">
						<h4 class="head_tab">Remove feed</h4>
						<p class="black">Do you really want to remove Rss Feed <span class="bold">CB ( http://clickbrain.com/feed )</span> from your feeds list?</p>
					</div>
					<div class="modal-footer clearfix">
						<div class="pull-right">
							<a class="link m-r10" data-dismiss="modal" aria-hidden="true" href="">Close</a>
							<button type="button" class="btn btn-save">Remove</button>
						</div>					
					</div>
				</div>
			</div>
		</div>

		<h2 id="calendar" class="sub-header">Datepicker</h2>
		<div class="row">
			<div class="col-sm-3">
				<div class="form-group date_calendar">
					<input type="text" class="form-control input_date">
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group date_calendar">
					<input type="text" class="form-control input_date">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group date_calendar">
					<input type="text" class="form-control time_date">
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group date_calendar">
					<input type="text" class="form-control time_date">
				</div>
			</div>
		</div>

		<!--<div class="row">
			<div class="col-sm-6">
				<div class="time_date"></div>
			</div>
			<div class="col-sm-6">
				<div class="time_date"></div>
			</div>
		</div>-->

		<h4 class="sub-header">Daterangepicker</h4>
		<div class="date_range">
			<div class="reportrange" >
				<i class="fa fa-calendar"></i>
				<span></span>
				<b class="caret"></b>
			</div>
		</div>

		<h2 id="empty" class="sub-header">Empty Block</h2>
		<div class="row">
			<div class="col-xs-12">
				<p class="large-size text_color">
					No mentioned
				</p>
			</div>
		</div>

		<h2 id="tables" class="sub-header">Responsive Tables</h2>
		<div class="row">
			<div class="col-xs-12 m-t10">
				<table class="responsive-table">
					<thead class="table_head">
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td data-th="First Name">Admin</td>
							<td data-th="Last Name">Admin</td>
							<td data-th="Email">admin@admin.com</td>
						</tr>
						<tr>
							<td data-th="First Name">Admin</td>
							<td data-th="Last Name">Admin</td>
							<td data-th="Email">admin@admin.com</td>
						</tr>
						<tr>
							<td data-th="First Name">Admin</td>
							<td data-th="Last Name">Admin</td>
							<td data-th="Email">admin@admin.com</td>
						</tr>
						<tr>
							<td data-th="First Name">Admin</td>
							<td data-th="Last Name">Admin</td>
							<td data-th="Email">admin@admin.com</td>
						</tr>
						<tr>
							<td data-th="First Name">Admin</td>
							<td data-th="Last Name">Admin</td>
							<td data-th="Email">admin@admin.com</td>
						</tr>
						<tr>
							<td data-th="First Name">Admin</td>
							<td data-th="Last Name">Admin</td>
							<td data-th="Email">admin@admin.com</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>		

		<h2 id="loading" class="sub-header">Loading</h2>	
		<div class="row">
			<div class="col-xs-12 text-center p-tb10">
				<img src="images/loading/loading.gif" alt="">
			</div>
		</div>		

		<h2 id="bread" class="sub-header">Breadcrumbs</h2>	
		<p class="attention bold">На страницах из Меню, например, Web Radar, Review и т.д хлебные крошки не добавлять. Только в пунктах подменю</p>
		<div class="row">
			<div class="col-xs-12">
				<ul class="breadcrumbs">
					<li class="breadcrumbs_item">
						<a href="" class="breadcrumbs_link">Web Radar</a>
					</li>
					<li class="breadcrumbs_item active">
						All mentioned
					</li>
				</ul>
			</div>
		</div>

		<h2 id="web-radar" class="sub-header">Web radar</h2>

		<h4 class="sub-header">Twitter</h4>
		<div class="row">
			<div class="col-xs-12">
				<div class="web_radar m-t20 pull_border">
					<div class="web_radar_content dTable">
						<div class="dRow">
							<div class="dCell cellImg">
								<a href="">
									<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
								</a>
							</div>
							<div class="dCell">
								<p class="web_radar_date">03.27.2015 06.29 am</p>
								<i class="fa fa-reply"></i>
								<i class="fa fa-share"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-twitter-square i-twitter"></i>
								<p class="web_radar_text">
									Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
									Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
									Donec hendrerit iaculis tellus eget consectetur. 
									Vestibulum sed elit id magna mattis viverra ac quis dui. 
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									Duis et elit semper, tempus ante vel, fermentum lectus. 
									Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<h4 class="sub-header">Facebook</h4>
		<div class="row">
			<div class="col-xs-12">
				<div class="web_radar m-t20 pull_border">
					<div class="web_radar_content dTable">
						<div class="dRow">
							<div class="dCell cellImg">
								<a href="">
									<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
								</a>
							</div>
							<div class="dCell">
								<p class="web_radar_date">03.27.2015 06.29 am</p>
								<i class="fa fa-facebook-square i-facebook"></i>
								<p class="web_radar_text">
									Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
								</p>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>

		<h4 class="sub-header">Google</h4>
		<div class="row">
			<div class="col-xs-12">
				<div class="web_radar m-t20 pull_border">
					<div class="web_radar_content dTable">
						<div class="dRow">
							<div class="dCell cellImg">
								<a href="">
									<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
								</a>
							</div>
							<div class="dCell">
								<p class="web_radar_date">03.27.2015 06.29 am</p>
								<i class="fa fa-google-plus-square i-google"></i>
								<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
								<p class="web_radar_text">
									Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
									Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
									Donec hendrerit iaculis tellus eget consectetur. 
									Vestibulum sed elit id magna mattis viverra ac quis dui. 
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									Duis et elit semper, tempus ante vel, fermentum lectus. 
									Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
								</p>
								<a href="">+1</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<h4 class="sub-header">Linkedin</h4>
		<div class="row">
			<div class="col-xs-12">
				<div class="web_radar m-t20 pull_border">
					<div class="web_radar_content dTable">
						<div class="dRow">
							<div class="dCell cellImg">
								<a href="">
									<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
								</a>
							</div>
							<div class="dCell">
								<p class="web_radar_date">03.27.2015 06.29 am</p>
								<i class="fa fa-linkedin-square i-linkedin"></i>
								<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
								<p class="web_radar_text">
									Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
									Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
									Donec hendrerit iaculis tellus eget consectetur. 
									Vestibulum sed elit id magna mattis viverra ac quis dui. 
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									Duis et elit semper, tempus ante vel, fermentum lectus. 
									Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
								</p>
								<div class="clearfix social_like">
									<a href=""><i class="ti-thumb-up"></i></a>
									<a href=""><i class="ti-thumb-down"></i></a>
									<a href="" class="show_comments"><i class="ti-comment-alt"></i></a>
								</div>
								<div class="web_comments">
									<div class="form-group">
										<textarea rows="5" class="form-control" placeholder="Write a comment"></textarea>
									</div>
									<div class="pull-right">
										<button class="btn btn-save">Post comment</button>
									</div>
								</div>									
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<h4 class="sub-header">Instagram</h4>
		<div class="row">
			<div class="col-xs-12">
				<div class="web_radar m-t20 pull_border">
					<div class="web_radar_content dTable">
						<div class="dRow">
							<div class="dCell cellImg">
								<a href="">
									<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
								</a>
							</div>
							<div class="dCell">
								<p class="web_radar_date">03.27.2015 06.29 am</p>
								<i class="fa fa-instagram i-instagram"></i>
								<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
								<p class="web_radar_text">
									Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
									Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
									Donec hendrerit iaculis tellus eget consectetur. 
									Vestibulum sed elit id magna mattis viverra ac quis dui. 
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									Duis et elit semper, tempus ante vel, fermentum lectus. 
									Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
								</p>
								<div class="clearfix social_like">
									<a href=""><i class="ti-thumb-up"></i></a>
									<a href=""><i class="ti-thumb-down"></i></a>
									<a href="" class="show_comments"><i class="ti-comment-alt"></i></a>
								</div>
								<div class="web_comments">
									<div class="row">
										<div class="col-xs-12">
											<div class="comment_block dTable">
												<div class="dRow">
													<div class="dCell cellImg">
														<a href="">
															<img class="comment_avatar" src="images/avatar/avatar1.png" alt="">
														</a>
													</div>
													<div class="dCell">
														<a class="blue_color comment_title">
															jboylion23
														</a>
														<p class="comment_text">Yes!!!</p>
														<p class="comment_date">
															<cite class="gray-color fa fa-clock-o "></cite>Apr 13, 2015 02:35 pm
														</p>
													</div>
												</div>
												<div class="dRow">
													<div class="dCell cellImg">
														<a href="">
															<img class="comment_avatar" src="images/avatar/enot.jpg" alt="">
														</a>
													</div>
													<div class="dCell">
														<a class="blue_color comment_title">
															jboylion23
														</a>
														<p class="comment_text">
															Acquire Facebook followers with the help of our Social marketing program
															Acquire Facebook followers with the help of our Social marketing program
															Acquire Facebook followers with the help of our Social marketing program
															Acquire Facebook followers with the help of our Social marketing program
															Acquire Facebook followers with the help of our Social marketing program
															Acquire Facebook followers with the help of our Social marketing program
															Acquire Facebook followers with the help of our Social marketing program
															Acquire Facebook followers with the help of our Social marketing program
															Acquire Facebook followers with the help of our Social marketing program
															Acquire Facebook followers with the help of our Social marketing program
														</p>
														<p class="comment_date">
															<cite class="gray-color fa fa-clock-o "></cite>Apr 13, 2015 02:35 pm
														</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<div class="form-group">
												<textarea rows="5" class="form-control" placeholder="Write a comment"></textarea>
											</div>
											<div class="pull-right">
												<button class="btn btn-save">Post comment</button>
											</div>
										</div>
									</div>
								</div>									
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<h2 id="pagination" class="sub-header">Pagination</h2>
		<div class="row">
			<div class="col-xs-12">
				<ul class="pagination">
					<li class="pagination_item unactive">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">1</a>
					</li>			
					<li class="pagination_item unactive">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">		
				<ul class="pagination">
					<li class="pagination_item unactive">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">2</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<ul class="pagination">
					<li class="pagination_item">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">2</a>
					</li>
					<li class="pagination_item unactive">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<ul class="pagination">
					<li class="pagination_item">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">2</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">3</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<ul class="pagination">
					<li class="pagination_item">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">2</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">3</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">4</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<ul class="pagination">
					<li class="pagination_item">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">2</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">3</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">4</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">5</a>
					</li>
					<li class="pagination_item unactive">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<ul class="pagination">
					<li class="pagination_item unactive">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">2</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">3</a>
					</li>
					<li class="pagination_item more_pages">
						<a href="" class="pagination_link">...</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">17</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<ul class="pagination">
					<li class="pagination_item">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">2</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">3</a>
					</li>
					<li class="pagination_item more_pages">
						<a href="" class="pagination_link">...</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">17</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<ul class="pagination">
					<li class="pagination_item">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">2</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">3</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">4</a>
					</li>
					<li class="pagination_item more_pages">
						<a href="" class="pagination_link">...</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">17</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<ul class="pagination">
					<li class="pagination_item">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">2</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">3</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">4</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">5</a>
					</li>
					<li class="pagination_item more_pages">
						<a href="" class="pagination_link">...</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">17</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<ul class="pagination">
					<li class="pagination_item">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item more_pages">
						<a href="" class="pagination_link">...</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">5</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">6</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">7</a>
					</li>
					<li class="pagination_item more_pages">
						<a href="" class="pagination_link">...</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">17</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<ul class="pagination">
					<li class="pagination_item">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item more_pages">
						<a href="" class="pagination_link">...</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">6</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">7</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">8</a>
					</li>
					<li class="pagination_item more_pages">
						<a href="" class="pagination_link">...</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">17</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">	
				<ul class="pagination">
					<li class="pagination_item">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">1</a>
					</li>
					<li class="pagination_item more_pages">
						<a href="" class="pagination_link">...</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">15</a>
					</li>
					<li class="pagination_item">
						<a href="" class="pagination_link">16</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">17</a>
					</li>
					<li class="pagination_item unactive">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
		<h2 id="progressbar" class="sub-header">Progressbar</h2>
		<div class="row">
			<div class="col-sm-3">
				<code>data-value="10%"</code>
				<div class="progressBar">
					<div class="progressLine" data-value="10"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<code>data-value="20%"</code>
				<div class="progressBar">
					<div class="progressLine" data-value="20"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<code>data-value="30%"</code>
				<div class="progressBar">
					<div class="progressLine" data-value="30"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<code>data-value="40%"</code>
				<div class="progressBar">
					<div class="progressLine" data-value="40"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<code>data-value="50%"</code>
				<div class="progressBar">
					<div class="progressLine" data-value="50"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<code>data-value="60%"</code>
				<div class="progressBar">
					<div class="progressLine" data-value="60"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<code>data-value="70%"</code>
				<div class="progressBar">
					<div class="progressLine" data-value="70"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<code>data-value="80%"</code>
				<div class="progressBar">
					<div class="progressLine" data-value="80"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<code>data-value="90%"</code>
				<div class="progressBar">
					<div class="progressLine" data-value="90"></div>
				</div>
			</div>
			<div class="col-sm-3">
				<code>data-value="100%"</code>
				<div class="progressBar">
					<div class="progressLine" data-value="100"></div>
				</div>
			</div>
		</div>

		<h2 id="note" class="sub-header">Notification, p.2</h2>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate pink">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note">!</i>
						</div>
						<div class="validateCell">
							<div class="pull-left">
								<p>
									User does not have any Google Analytics account.
								</p>								
							</div>
						</div>
					</div>
				</div>
			</div>											
		</div>
		<br/>

		<div class="row">
			<div class="col-xs-12">
				<div class="validate blue">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note">!</i>
						</div>
						<div class="validateCell">
							<div class="pull-left">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit incidunt harum rem praesentium consequatur cumque aliquid, consectetur quibusdam esse, odit in hic unde corporis minima obcaecati, et laborum facere. Vero.
								</p>								
							</div>
						</div>
					</div>
				</div>
			</div>											
		</div>

		<h2 id="cssHelper" class="sub-header">Css Helper</h2>
        <h3>Margin classes</h3>
        <div class="row">
            <div class="col-xs-12">
                <div class="well well-default-o">
                    <dl class="togglable togglable-list">
                        <dt class="togglable-heading"><strong>All sides:</strong> <code class="small">m-*</code></dt>
                        <dd class="togglable-content">
                                  <span class="label label-default" style="display: inline-block;">m-0</span>
                                  <span class="label label-default" style="display: inline-block;">m-1</span>
                                  <span class="label label-default" style="display: inline-block;">m-2</span>
                                  <span class="label label-default" style="display: inline-block;">m-3</span>
                                  <span class="label label-default" style="display: inline-block;">m-4</span>
                                  <span class="label label-default" style="display: inline-block;">m-5</span>
                                  <span class="label label-default" style="display: inline-block;">m-6</span>
                                  <span class="label label-default" style="display: inline-block;">m-7</span>
                                  <span class="label label-default" style="display: inline-block;">m-8</span>
                                  <span class="label label-default" style="display: inline-block;">m-9</span>
                                  <span class="label label-default" style="display: inline-block;">m-10</span>
                                  <span class="label label-default" style="display: inline-block;">m-11</span>
                                  <span class="label label-default" style="display: inline-block;">m-12</span>
                                  <span class="label label-default" style="display: inline-block;">m-13</span>
                                  <span class="label label-default" style="display: inline-block;">m-14</span>
                                  <span class="label label-default" style="display: inline-block;">m-15</span>
                                  <span class="label label-default" style="display: inline-block;">m-16</span>
                                  <span class="label label-default" style="display: inline-block;">m-17</span>
                                  <span class="label label-default" style="display: inline-block;">m-18</span>
                                  <span class="label label-default" style="display: inline-block;">m-19</span>
                                  <span class="label label-default" style="display: inline-block;">m-20</span>
                                  <span class="label label-default" style="display: inline-block;">m-21</span>
                                  <span class="label label-default" style="display: inline-block;">m-22</span>
                                  <span class="label label-default" style="display: inline-block;">m-23</span>
                                  <span class="label label-default" style="display: inline-block;">m-24</span>
                                  <span class="label label-default" style="display: inline-block;">m-25</span>
                                  <span class="label label-default" style="display: inline-block;">m-26</span>
                                  <span class="label label-default" style="display: inline-block;">m-27</span>
                                  <span class="label label-default" style="display: inline-block;">m-28</span>
                                  <span class="label label-default" style="display: inline-block;">m-29</span>
                                  <span class="label label-default" style="display: inline-block;">m-30</span>
                                  <span class="label label-default" style="display: inline-block;">m-31</span>
                                  <span class="label label-default" style="display: inline-block;">m-32</span>
                                  <span class="label label-default" style="display: inline-block;">m-33</span>
                                  <span class="label label-default" style="display: inline-block;">m-34</span>
                                  <span class="label label-default" style="display: inline-block;">m-35</span>
                                  <span class="label label-default" style="display: inline-block;">m-36</span>
                                  <span class="label label-default" style="display: inline-block;">m-37</span>
                                  <span class="label label-default" style="display: inline-block;">m-38</span>
                                  <span class="label label-default" style="display: inline-block;">m-39</span>
                                  <span class="label label-default" style="display: inline-block;">m-40</span>
                        </dd>
                        <dt class="togglable-heading"><strong>Negative all sides:</strong> <code class="small">m--*</code></dt>
                        <dd class="togglable-content">
                                  <span class="label label-warning" style="display: inline-block;">m--0</span>
                                  <span class="label label-warning" style="display: inline-block;">m--1</span>
                                  <span class="label label-warning" style="display: inline-block;">m--2</span>
                                  <span class="label label-warning" style="display: inline-block;">m--3</span>
                                  <span class="label label-warning" style="display: inline-block;">m--4</span>
                                  <span class="label label-warning" style="display: inline-block;">m--5</span>
                                  <span class="label label-warning" style="display: inline-block;">m--6</span>
                                  <span class="label label-warning" style="display: inline-block;">m--7</span>
                                  <span class="label label-warning" style="display: inline-block;">m--8</span>
                                  <span class="label label-warning" style="display: inline-block;">m--9</span>
                                  <span class="label label-warning" style="display: inline-block;">m--10</span>
                                  <span class="label label-warning" style="display: inline-block;">m--11</span>
                                  <span class="label label-warning" style="display: inline-block;">m--12</span>
                                  <span class="label label-warning" style="display: inline-block;">m--13</span>
                                  <span class="label label-warning" style="display: inline-block;">m--14</span>
                                  <span class="label label-warning" style="display: inline-block;">m--15</span>
                                  <span class="label label-warning" style="display: inline-block;">m--16</span>
                                  <span class="label label-warning" style="display: inline-block;">m--17</span>
                                  <span class="label label-warning" style="display: inline-block;">m--18</span>
                                  <span class="label label-warning" style="display: inline-block;">m--19</span>
                                  <span class="label label-warning" style="display: inline-block;">m--20</span>
                                  <span class="label label-warning" style="display: inline-block;">m--21</span>
                                  <span class="label label-warning" style="display: inline-block;">m--22</span>
                                  <span class="label label-warning" style="display: inline-block;">m--23</span>
                                  <span class="label label-warning" style="display: inline-block;">m--24</span>
                                  <span class="label label-warning" style="display: inline-block;">m--25</span>
                                  <span class="label label-warning" style="display: inline-block;">m--26</span>
                                  <span class="label label-warning" style="display: inline-block;">m--27</span>
                                  <span class="label label-warning" style="display: inline-block;">m--28</span>
                                  <span class="label label-warning" style="display: inline-block;">m--29</span>
                                  <span class="label label-warning" style="display: inline-block;">m--30</span>
                                  <span class="label label-warning" style="display: inline-block;">m--31</span>
                                  <span class="label label-warning" style="display: inline-block;">m--32</span>
                                  <span class="label label-warning" style="display: inline-block;">m--33</span>
                                  <span class="label label-warning" style="display: inline-block;">m--34</span>
                                  <span class="label label-warning" style="display: inline-block;">m--35</span>
                                  <span class="label label-warning" style="display: inline-block;">m--36</span>
                                  <span class="label label-warning" style="display: inline-block;">m--37</span>
                                  <span class="label label-warning" style="display: inline-block;">m--38</span>
                                  <span class="label label-warning" style="display: inline-block;">m--39</span>
                                  <span class="label label-warning" style="display: inline-block;">m--40</span>
                        </dd>
                        <hr class="hr-md">
                        <dt class="togglable-heading"><strong>Top:</strong> <code class="small">m-t*</code></dt>
                        <dd class="togglable-content">
                                <p>Top only:
                                  <span class="label label-default" style="display: inline-block;">m-t0</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t1</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t2</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t3</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t4</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t5</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t6</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t7</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t8</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t9</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t10</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t11</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t12</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t13</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t14</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t15</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t16</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t17</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t18</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t19</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t20</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t21</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t22</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t23</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t24</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t25</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t26</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t27</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t28</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t29</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t30</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t31</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t32</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t33</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t34</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t35</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t36</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t37</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t38</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t39</span> 
                                  <span class="label label-default" style="display: inline-block;">m-t40</span> 
                                </p>
                                <p>Top and right:
                                  <span class="label label-info" style="display: inline-block;">m-tr0</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr1</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr2</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr3</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr4</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr5</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr6</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr7</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr8</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr9</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr10</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr11</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr12</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr13</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr14</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr15</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr16</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr17</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr18</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr19</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr20</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr21</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr22</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr23</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr24</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr25</span>
                                  <span class="label label-info" style="display: inline-block;">m-tr26</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr27</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr28</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr29</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr30</span>
                                  <span class="label label-info" style="display: inline-block;">m-tr31</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr32</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr33</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr34</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr35</span>
                                  <span class="label label-info" style="display: inline-block;">m-tr36</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr37</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr38</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr39</span> 
                                  <span class="label label-info" style="display: inline-block;">m-tr40</span>
                                </p>
                                <p>Top and bottom:
                                  <span class="label label-primary" style="display: inline-block;">m-tb0</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb1</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb2</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb3</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb4</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb5</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb6</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb7</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb8</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb9</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb10</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb11</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb12</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb13</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb14</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb15</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb16</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb17</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb18</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb19</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb20</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb21</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb22</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb23</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb24</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb25</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb26</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb27</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb28</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb29</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb30</span>
                                  <span class="label label-primary" style="display: inline-block;">m-tb31</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb32</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb33</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb34</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb35</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb36</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb37</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb38</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb39</span> 
                                  <span class="label label-primary" style="display: inline-block;">m-tb40</span>
                                </p>
                                <p>Top and left:
                                  <span class="label label-success" style="display: inline-block;">m-tl0</span>
                                  <span class="label label-success" style="display: inline-block;">m-tl1</span>
                                  <span class="label label-success" style="display: inline-block;">m-tl2</span>
                                  <span class="label label-success" style="display: inline-block;">m-tl3</span>
                                  <span class="label label-success" style="display: inline-block;">m-tl4</span>
                                  <span class="label label-success" style="display: inline-block;">m-tl5</span>
                                  <span class="label label-success" style="display: inline-block;">m-tl6</span>
                                  <span class="label label-success" style="display: inline-block;">m-tl7</span>
                                  <span class="label label-success" style="display: inline-block;">m-tl8</span>
                                  <span class="label label-success" style="display: inline-block;">m-tl9</span>
                                  <span class="label label-success" style="display: inline-block;">m-tl10</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl11</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl12</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl13</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl14</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl15</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl16</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl17</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl18</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl19</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl20</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl21</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl22</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl23</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl24</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl25</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl26</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl27</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl28</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl29</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl30</span>
                                  <span class="label label-success" style="display: inline-block;">m-tl31</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl32</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl33</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl34</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl35</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl36</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl37</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl38</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl39</span> 
                                  <span class="label label-success" style="display: inline-block;">m-tl40</span>
                               </p>
                        </dd>
                        <dt class="togglable-heading"><strong>Negative top:</strong> <code class="small">m--t*</code></dt>
                        <dd class="togglable-content">
                            <p>Negative top only:
                              <span class="label label-default" style="display: inline-block;">m--t0</span>
                              <span class="label label-default" style="display: inline-block;">m--t1</span>
                              <span class="label label-default" style="display: inline-block;">m--t2</span>
                              <span class="label label-default" style="display: inline-block;">m--t3</span>
                              <span class="label label-default" style="display: inline-block;">m--t4</span>
                              <span class="label label-default" style="display: inline-block;">m--t5</span>
                              <span class="label label-default" style="display: inline-block;">m--t6</span>
                              <span class="label label-default" style="display: inline-block;">m--t7</span>
                              <span class="label label-default" style="display: inline-block;">m--t8</span>
                              <span class="label label-default" style="display: inline-block;">m--t9</span>
                              <span class="label label-default" style="display: inline-block;">m--t10</span>
                              <span class="label label-default" style="display: inline-block;">m--t11</span>
                              <span class="label label-default" style="display: inline-block;">m--t12</span>
                              <span class="label label-default" style="display: inline-block;">m--t13</span>
                              <span class="label label-default" style="display: inline-block;">m--t14</span>
                              <span class="label label-default" style="display: inline-block;">m--t15</span>
                              <span class="label label-default" style="display: inline-block;">m--t16</span>
                              <span class="label label-default" style="display: inline-block;">m--t17</span>
                              <span class="label label-default" style="display: inline-block;">m--t18</span>
                              <span class="label label-default" style="display: inline-block;">m--t19</span>
                              <span class="label label-default" style="display: inline-block;">m--t20</span>
                              <span class="label label-default" style="display: inline-block;">m--t21</span>
                              <span class="label label-default" style="display: inline-block;">m--t22</span>
                              <span class="label label-default" style="display: inline-block;">m--t23</span>
                              <span class="label label-default" style="display: inline-block;">m--t24</span>
                              <span class="label label-default" style="display: inline-block;">m--t25</span>
                              <span class="label label-default" style="display: inline-block;">m--t26</span>
                              <span class="label label-default" style="display: inline-block;">m--t27</span>
                              <span class="label label-default" style="display: inline-block;">m--t28</span>
                              <span class="label label-default" style="display: inline-block;">m--t29</span>
                              <span class="label label-default" style="display: inline-block;">m--t30</span>
                              <span class="label label-default" style="display: inline-block;">m--t31</span>
                              <span class="label label-default" style="display: inline-block;">m--t32</span>
                              <span class="label label-default" style="display: inline-block;">m--t33</span>
                              <span class="label label-default" style="display: inline-block;">m--t34</span>
                              <span class="label label-default" style="display: inline-block;">m--t35</span>
                              <span class="label label-default" style="display: inline-block;">m--t36</span>
                              <span class="label label-default" style="display: inline-block;">m--t37</span>
                              <span class="label label-default" style="display: inline-block;">m--t38</span>
                              <span class="label label-default" style="display: inline-block;">m--t39</span>
                              <span class="label label-default" style="display: inline-block;">m--t40</span>
                            </p>
                            <p>Negative top and right:
                              <span class="label label-info" style="display: inline-block;">m--tr0</span>
                              <span class="label label-info" style="display: inline-block;">m--tr1</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr2</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr3</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr4</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr5</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr6</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr7</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr8</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr9</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr10</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr11</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr12</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr13</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr14</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr15</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr16</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr17</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr18</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr19</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr20</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr21</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr22</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr23</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr24</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr25</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr26</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr27</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr28</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr29</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr30</span>
                              <span class="label label-info" style="display: inline-block;">m--tr31</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr32</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr33</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr34</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr35</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr36</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr37</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr38</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr39</span> 
                              <span class="label label-info" style="display: inline-block;">m--tr40</span>  
                            </p>
                            <p>Negative top and bottom:
                              <span class="label label-primary" style="display: inline-block;">m--tb0</span>
                              <span class="label label-primary" style="display: inline-block;">m--tb1</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb2</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb3</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb4</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb5</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb6</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb7</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb8</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb9</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb10</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb11</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb12</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb13</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb14</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb15</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb16</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb17</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb18</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb19</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb20</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb21</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb22</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb23</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb24</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb25</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb26</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb27</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb28</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb29</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb30</span>
                              <span class="label label-primary" style="display: inline-block;">m--tb31</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb32</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb33</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb34</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb35</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb36</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb37</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb38</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb39</span> 
                              <span class="label label-primary" style="display: inline-block;">m--tb40</span>
                             </p>
                            <p>Negative top and left:
                              <span class="label label-success" style="display: inline-block;">m--tl0</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl1</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl2</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl3</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl4</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl5</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl6</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl7</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl8</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl9</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl10</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl11</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl12</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl13</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl14</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl15</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl16</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl17</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl18</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl19</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl20</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl21</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl22</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl23</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl24</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl25</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl26</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl27</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl28</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl29</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl30</span>
                              <span class="label label-success" style="display: inline-block;">m--tl31</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl32</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl33</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl34</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl35</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl36</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl37</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl38</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl39</span> 
                              <span class="label label-success" style="display: inline-block;">m--tl40</span>  
                            </p>
                        </dd>
                        <hr class="hr-md">
                        <dt class="togglable-heading"><strong>Right:</strong> <code class="small">m-r*</code></dt>
                        <dd class="togglable-content">
                            <p>Right only:
                                <span class="label label-default" style="display: inline-block;">m-r0</span>
                                <span class="label label-default" style="display: inline-block;">m-r1</span>
                                <span class="label label-default" style="display: inline-block;">m-r2</span>
                                <span class="label label-default" style="display: inline-block;">m-r3</span>
                                <span class="label label-default" style="display: inline-block;">m-r4</span>
                                <span class="label label-default" style="display: inline-block;">m-r5</span>
                                <span class="label label-default" style="display: inline-block;">m-r6</span>
                                <span class="label label-default" style="display: inline-block;">m-r7</span>
                                <span class="label label-default" style="display: inline-block;">m-r8</span>
                                <span class="label label-default" style="display: inline-block;">m-r9</span>
                                <span class="label label-default" style="display: inline-block;">m-r10</span>
                                <span class="label label-default" style="display: inline-block;">m-r11</span>
                                <span class="label label-default" style="display: inline-block;">m-r12</span>
                                <span class="label label-default" style="display: inline-block;">m-r13</span>
                                <span class="label label-default" style="display: inline-block;">m-r14</span>
                                <span class="label label-default" style="display: inline-block;">m-r15</span>
                                <span class="label label-default" style="display: inline-block;">m-r16</span>
                                <span class="label label-default" style="display: inline-block;">m-r17</span>
                                <span class="label label-default" style="display: inline-block;">m-r18</span>
                                <span class="label label-default" style="display: inline-block;">m-r19</span>
                                <span class="label label-default" style="display: inline-block;">m-r20</span>
                                <span class="label label-default" style="display: inline-block;">m-r21</span>
                                <span class="label label-default" style="display: inline-block;">m-r22</span>
                                <span class="label label-default" style="display: inline-block;">m-r23</span>
                                <span class="label label-default" style="display: inline-block;">m-r24</span>
                                <span class="label label-default" style="display: inline-block;">m-r25</span>
                                <span class="label label-default" style="display: inline-block;">m-r26</span>
                                <span class="label label-default" style="display: inline-block;">m-r27</span>
                                <span class="label label-default" style="display: inline-block;">m-r28</span>
                                <span class="label label-default" style="display: inline-block;">m-r29</span>
                                <span class="label label-default" style="display: inline-block;">m-r30</span>
                                <span class="label label-default" style="display: inline-block;">m-r31</span>
                                <span class="label label-default" style="display: inline-block;">m-r32</span>
                                <span class="label label-default" style="display: inline-block;">m-r33</span>
                                <span class="label label-default" style="display: inline-block;">m-r34</span>
                                <span class="label label-default" style="display: inline-block;">m-r35</span>
                                <span class="label label-default" style="display: inline-block;">m-r36</span>
                                <span class="label label-default" style="display: inline-block;">m-r37</span>
                                <span class="label label-default" style="display: inline-block;">m-r38</span>
                                <span class="label label-default" style="display: inline-block;">m-r39</span>
                                <span class="label label-default" style="display: inline-block;">m-r40</span>
                              </p>
                            <p>Right and bottom:
                                <span class="label label-info" style="display: inline-block;">m-rb0</span>
                                <span class="label label-info" style="display: inline-block;">m-rb1</span>
                                <span class="label label-info" style="display: inline-block;">m-rb2</span>
                                <span class="label label-info" style="display: inline-block;">m-rb3</span>
                                <span class="label label-info" style="display: inline-block;">m-rb4</span>
                                <span class="label label-info" style="display: inline-block;">m-rb5</span>
                                <span class="label label-info" style="display: inline-block;">m-rb6</span>
                                <span class="label label-info" style="display: inline-block;">m-rb7</span>
                                <span class="label label-info" style="display: inline-block;">m-rb8</span>
                                <span class="label label-info" style="display: inline-block;">m-rb9</span>
                                <span class="label label-info" style="display: inline-block;">m-rb10</span>
                                <span class="label label-info" style="display: inline-block;">m-rb11</span>
                                <span class="label label-info" style="display: inline-block;">m-rb12</span>
                                <span class="label label-info" style="display: inline-block;">m-rb13</span>
                                <span class="label label-info" style="display: inline-block;">m-rb14</span>
                                <span class="label label-info" style="display: inline-block;">m-rb15</span>
                                <span class="label label-info" style="display: inline-block;">m-rb16</span>
                                <span class="label label-info" style="display: inline-block;">m-rb17</span>
                                <span class="label label-info" style="display: inline-block;">m-rb18</span>
                                <span class="label label-info" style="display: inline-block;">m-rb19</span>
                                <span class="label label-info" style="display: inline-block;">m-rb20</span>
                                <span class="label label-info" style="display: inline-block;">m-rb21</span>
                                <span class="label label-info" style="display: inline-block;">m-rb22</span>
                                <span class="label label-info" style="display: inline-block;">m-rb23</span>
                                <span class="label label-info" style="display: inline-block;">m-rb24</span>
                                <span class="label label-info" style="display: inline-block;">m-rb25</span>
                                <span class="label label-info" style="display: inline-block;">m-rb26</span>
                                <span class="label label-info" style="display: inline-block;">m-rb27</span>
                                <span class="label label-info" style="display: inline-block;">m-rb28</span>
                                <span class="label label-info" style="display: inline-block;">m-rb29</span>
                                <span class="label label-info" style="display: inline-block;">m-rb30</span>
                                <span class="label label-info" style="display: inline-block;">m-rb31</span>
                                <span class="label label-info" style="display: inline-block;">m-rb32</span>
                                <span class="label label-info" style="display: inline-block;">m-rb33</span>
                                <span class="label label-info" style="display: inline-block;">m-rb34</span>
                                <span class="label label-info" style="display: inline-block;">m-rb35</span>
                                <span class="label label-info" style="display: inline-block;">m-rb36</span>
                                <span class="label label-info" style="display: inline-block;">m-rb37</span>
                                <span class="label label-info" style="display: inline-block;">m-rb38</span>
                                <span class="label label-info" style="display: inline-block;">m-rb39</span>
                                <span class="label label-info" style="display: inline-block;">m-rb40</span>
                              </p>
                            <p>Right and left: 
                                <span class="label label-primary" style="display: inline-block;">m-rl0</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl1</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl2</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl3</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl4</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl5</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl6</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl7</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl8</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl9</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl10</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl11</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl12</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl13</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl14</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl15</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl16</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl17</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl18</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl19</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl20</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl21</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl22</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl23</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl24</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl25</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl26</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl27</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl28</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl29</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl30</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl31</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl32</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl33</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl34</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl35</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl36</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl37</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl38</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl39</span>
                                <span class="label label-primary" style="display: inline-block;">m-rl40</span>
                            </p>
                        </dd>
                        <dt class="togglable-heading"><strong>Negative right:</strong> <code class="small">m--r*</code></dt>
                        <dd class="togglable-content">
                            <p>Negative right only:
                                <span class="label label-default" style="display: inline-block;">m--r0</span>
                                <span class="label label-default" style="display: inline-block;">m--r1</span>
                                <span class="label label-default" style="display: inline-block;">m--r2</span>
                                <span class="label label-default" style="display: inline-block;">m--r3</span>
                                <span class="label label-default" style="display: inline-block;">m--r4</span>
                                <span class="label label-default" style="display: inline-block;">m--r5</span>
                                <span class="label label-default" style="display: inline-block;">m--r6</span>
                                <span class="label label-default" style="display: inline-block;">m--r7</span>
                                <span class="label label-default" style="display: inline-block;">m--r8</span>
                                <span class="label label-default" style="display: inline-block;">m--r9</span>
                                <span class="label label-default" style="display: inline-block;">m--r10</span>
                                <span class="label label-default" style="display: inline-block;">m--r11</span>
                                <span class="label label-default" style="display: inline-block;">m--r12</span>
                                <span class="label label-default" style="display: inline-block;">m--r13</span>
                                <span class="label label-default" style="display: inline-block;">m--r14</span>
                                <span class="label label-default" style="display: inline-block;">m--r15</span>
                                <span class="label label-default" style="display: inline-block;">m--r16</span>
                                <span class="label label-default" style="display: inline-block;">m--r17</span>
                                <span class="label label-default" style="display: inline-block;">m--r18</span>
                                <span class="label label-default" style="display: inline-block;">m--r19</span>
                                <span class="label label-default" style="display: inline-block;">m--r20</span>
                                <span class="label label-default" style="display: inline-block;">m--r21</span>
                                <span class="label label-default" style="display: inline-block;">m--r22</span>
                                <span class="label label-default" style="display: inline-block;">m--r23</span>
                                <span class="label label-default" style="display: inline-block;">m--r24</span>
                                <span class="label label-default" style="display: inline-block;">m--r25</span>
                                <span class="label label-default" style="display: inline-block;">m--r26</span>
                                <span class="label label-default" style="display: inline-block;">m--r27</span>
                                <span class="label label-default" style="display: inline-block;">m--r28</span>
                                <span class="label label-default" style="display: inline-block;">m--r29</span>
                                <span class="label label-default" style="display: inline-block;">m--r30</span>
                                <span class="label label-default" style="display: inline-block;">m--r31</span>
                                <span class="label label-default" style="display: inline-block;">m--r32</span>
                                <span class="label label-default" style="display: inline-block;">m--r33</span>
                                <span class="label label-default" style="display: inline-block;">m--r34</span>
                                <span class="label label-default" style="display: inline-block;">m--r35</span>
                                <span class="label label-default" style="display: inline-block;">m--r36</span>
                                <span class="label label-default" style="display: inline-block;">m--r37</span>
                                <span class="label label-default" style="display: inline-block;">m--r38</span>
                                <span class="label label-default" style="display: inline-block;">m--r39</span>
                                <span class="label label-default" style="display: inline-block;">m--r40</span>
                            </p>
                            <p>Negative right and bottom:
                                <span class="label label-info" style="display: inline-block;">m--rb0</span>
                                <span class="label label-info" style="display: inline-block;">m--rb1</span>
                                <span class="label label-info" style="display: inline-block;">m--rb2</span>
                                <span class="label label-info" style="display: inline-block;">m--rb3</span>
                                <span class="label label-info" style="display: inline-block;">m--rb4</span>
                                <span class="label label-info" style="display: inline-block;">m--rb5</span>
                                <span class="label label-info" style="display: inline-block;">m--rb6</span>
                                <span class="label label-info" style="display: inline-block;">m--rb7</span>
                                <span class="label label-info" style="display: inline-block;">m--rb8</span>
                                <span class="label label-info" style="display: inline-block;">m--rb9</span>
                                <span class="label label-info" style="display: inline-block;">m--rb10</span>
                                <span class="label label-info" style="display: inline-block;">m--rb11</span>
                                <span class="label label-info" style="display: inline-block;">m--rb12</span>
                                <span class="label label-info" style="display: inline-block;">m--rb13</span>
                                <span class="label label-info" style="display: inline-block;">m--rb14</span>
                                <span class="label label-info" style="display: inline-block;">m--rb15</span>
                                <span class="label label-info" style="display: inline-block;">m--rb16</span>
                                <span class="label label-info" style="display: inline-block;">m--rb17</span>
                                <span class="label label-info" style="display: inline-block;">m--rb18</span>
                                <span class="label label-info" style="display: inline-block;">m--rb19</span>
                                <span class="label label-info" style="display: inline-block;">m--rb20</span>
                                <span class="label label-info" style="display: inline-block;">m--rb21</span>
                                <span class="label label-info" style="display: inline-block;">m--rb22</span>
                                <span class="label label-info" style="display: inline-block;">m--rb23</span>
                                <span class="label label-info" style="display: inline-block;">m--rb24</span>
                                <span class="label label-info" style="display: inline-block;">m--rb25</span>
                                <span class="label label-info" style="display: inline-block;">m--rb26</span>
                                <span class="label label-info" style="display: inline-block;">m--rb27</span>
                                <span class="label label-info" style="display: inline-block;">m--rb28</span>
                                <span class="label label-info" style="display: inline-block;">m--rb29</span>
                                <span class="label label-info" style="display: inline-block;">m--rb30</span>
                                <span class="label label-info" style="display: inline-block;">m--rb31</span>
                                <span class="label label-info" style="display: inline-block;">m--rb32</span>
                                <span class="label label-info" style="display: inline-block;">m--rb33</span>
                                <span class="label label-info" style="display: inline-block;">m--rb34</span>
                                <span class="label label-info" style="display: inline-block;">m--rb35</span>
                                <span class="label label-info" style="display: inline-block;">m--rb36</span>
                                <span class="label label-info" style="display: inline-block;">m--rb37</span>
                                <span class="label label-info" style="display: inline-block;">m--rb38</span>
                                <span class="label label-info" style="display: inline-block;">m--rb39</span>
                                <span class="label label-info" style="display: inline-block;">m--rb40</span>
                            </p>
                            <p>Negative right and left:         <span class="label label-primary" style="display: inline-block;">m--rl0</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl1</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl2</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl3</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl4</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl5</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl6</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl7</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl8</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl9</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl10</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl11</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl12</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl13</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl14</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl15</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl16</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl17</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl18</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl19</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl20</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl21</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl22</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl23</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl24</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl25</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl26</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl27</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl28</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl29</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl30</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl31</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl32</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl33</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl34</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl35</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl36</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl37</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl38</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl39</span>
                                      <span class="label label-primary" style="display: inline-block;">m--rl40</span>
                              </p>
                        </dd>
                        <hr class="hr-md">
                        <dt class="togglable-heading"><strong>Bottom:</strong> <code class="small">m-b*</code></dt>
                        <dd class="togglable-content">
                            <span class="label label-info" style="display: inline-block;">m-b0</span>
                            <span class="label label-info" style="display: inline-block;">m-b1</span>
                            <span class="label label-info" style="display: inline-block;">m-b2</span>
                            <span class="label label-info" style="display: inline-block;">m-b3</span>
                            <span class="label label-info" style="display: inline-block;">m-b4</span>
                            <span class="label label-info" style="display: inline-block;">m-b5</span>
                            <span class="label label-info" style="display: inline-block;">m-b6</span>
                            <span class="label label-info" style="display: inline-block;">m-b7</span>
                            <span class="label label-info" style="display: inline-block;">m-b8</span>
                            <span class="label label-info" style="display: inline-block;">m-b9</span>
                            <span class="label label-info" style="display: inline-block;">m-b10</span>
                            <span class="label label-info" style="display: inline-block;">m-b11</span>
                            <span class="label label-info" style="display: inline-block;">m-b12</span>
                            <span class="label label-info" style="display: inline-block;">m-b13</span>
                            <span class="label label-info" style="display: inline-block;">m-b14</span>
                            <span class="label label-info" style="display: inline-block;">m-b15</span>
                            <span class="label label-info" style="display: inline-block;">m-b16</span>
                            <span class="label label-info" style="display: inline-block;">m-b17</span>
                            <span class="label label-info" style="display: inline-block;">m-b18</span>
                            <span class="label label-info" style="display: inline-block;">m-b19</span>
                            <span class="label label-info" style="display: inline-block;">m-b20</span>
                            <span class="label label-info" style="display: inline-block;">m-b21</span>
                            <span class="label label-info" style="display: inline-block;">m-b22</span>
                            <span class="label label-info" style="display: inline-block;">m-b23</span>
                            <span class="label label-info" style="display: inline-block;">m-b24</span>
                            <span class="label label-info" style="display: inline-block;">m-b25</span>
                            <span class="label label-info" style="display: inline-block;">m-b26</span>
                            <span class="label label-info" style="display: inline-block;">m-b27</span>
                            <span class="label label-info" style="display: inline-block;">m-b28</span>
                            <span class="label label-info" style="display: inline-block;">m-b29</span>
                            <span class="label label-info" style="display: inline-block;">m-b30</span>
                            <span class="label label-info" style="display: inline-block;">m-b31</span>
                            <span class="label label-info" style="display: inline-block;">m-b32</span>
                            <span class="label label-info" style="display: inline-block;">m-b33</span>
                            <span class="label label-info" style="display: inline-block;">m-b34</span>
                            <span class="label label-info" style="display: inline-block;">m-b35</span>
                            <span class="label label-info" style="display: inline-block;">m-b36</span>
                            <span class="label label-info" style="display: inline-block;">m-b37</span>
                            <span class="label label-info" style="display: inline-block;">m-b38</span>
                            <span class="label label-info" style="display: inline-block;">m-b39</span>
                            <span class="label label-info" style="display: inline-block;">m-b40</span>
                        </dd>
                        <dt class="togglable-heading"><strong>Negative bottom:</strong> <code class="small">m--b*</code></dt>
                        <dd class="togglable-content">
                            <span class="label label-primary" style="display: inline-block;">m--b0</span>
                            <span class="label label-primary" style="display: inline-block;">m--b1</span>
                            <span class="label label-primary" style="display: inline-block;">m--b2</span>
                            <span class="label label-primary" style="display: inline-block;">m--b3</span>
                            <span class="label label-primary" style="display: inline-block;">m--b4</span>
                            <span class="label label-primary" style="display: inline-block;">m--b5</span>
                            <span class="label label-primary" style="display: inline-block;">m--b6</span>
                            <span class="label label-primary" style="display: inline-block;">m--b7</span>
                            <span class="label label-primary" style="display: inline-block;">m--b8</span>
                            <span class="label label-primary" style="display: inline-block;">m--b9</span>
                            <span class="label label-primary" style="display: inline-block;">m--b10</span>
                            <span class="label label-primary" style="display: inline-block;">m--b11</span>
                            <span class="label label-primary" style="display: inline-block;">m--b12</span>
                            <span class="label label-primary" style="display: inline-block;">m--b13</span>
                            <span class="label label-primary" style="display: inline-block;">m--b14</span>
                            <span class="label label-primary" style="display: inline-block;">m--b15</span>
                            <span class="label label-primary" style="display: inline-block;">m--b16</span>
                            <span class="label label-primary" style="display: inline-block;">m--b17</span>
                            <span class="label label-primary" style="display: inline-block;">m--b18</span>
                            <span class="label label-primary" style="display: inline-block;">m--b19</span>
                            <span class="label label-primary" style="display: inline-block;">m--b20</span>
                            <span class="label label-primary" style="display: inline-block;">m--b21</span>
                            <span class="label label-primary" style="display: inline-block;">m--b22</span>
                            <span class="label label-primary" style="display: inline-block;">m--b23</span>
                            <span class="label label-primary" style="display: inline-block;">m--b24</span>
                            <span class="label label-primary" style="display: inline-block;">m--b25</span>
                            <span class="label label-primary" style="display: inline-block;">m--b26</span>
                            <span class="label label-primary" style="display: inline-block;">m--b27</span>
                            <span class="label label-primary" style="display: inline-block;">m--b28</span>
                            <span class="label label-primary" style="display: inline-block;">m--b29</span>
                            <span class="label label-primary" style="display: inline-block;">m--b30</span>
                            <span class="label label-primary" style="display: inline-block;">m--b31</span>
                            <span class="label label-primary" style="display: inline-block;">m--b32</span>
                            <span class="label label-primary" style="display: inline-block;">m--b33</span>
                            <span class="label label-primary" style="display: inline-block;">m--b34</span>
                            <span class="label label-primary" style="display: inline-block;">m--b35</span>
                            <span class="label label-primary" style="display: inline-block;">m--b36</span>
                            <span class="label label-primary" style="display: inline-block;">m--b37</span>
                            <span class="label label-primary" style="display: inline-block;">m--b38</span>
                            <span class="label label-primary" style="display: inline-block;">m--b39</span>
                            <span class="label label-primary" style="display: inline-block;">m--b40</span>
                        </dd>
                        <hr class="hr-md">
                        <dt class="togglable-heading"><strong>Left:</strong> <code class="small">m-l*</code></dt>
                        <dd class="togglable-content">
                            <span class="label label-warning" style="display: inline-block;">m-l0</span>
                            <span class="label label-warning" style="display: inline-block;">m-l1</span>
                            <span class="label label-warning" style="display: inline-block;">m-l2</span>
                            <span class="label label-warning" style="display: inline-block;">m-l3</span>
                            <span class="label label-warning" style="display: inline-block;">m-l4</span>
                            <span class="label label-warning" style="display: inline-block;">m-l5</span>
                            <span class="label label-warning" style="display: inline-block;">m-l6</span>
                            <span class="label label-warning" style="display: inline-block;">m-l7</span>
                            <span class="label label-warning" style="display: inline-block;">m-l8</span>
                            <span class="label label-warning" style="display: inline-block;">m-l9</span>
                            <span class="label label-warning" style="display: inline-block;">m-l10</span>
                            <span class="label label-warning" style="display: inline-block;">m-l11</span>
                            <span class="label label-warning" style="display: inline-block;">m-l12</span>
                            <span class="label label-warning" style="display: inline-block;">m-l13</span>
                            <span class="label label-warning" style="display: inline-block;">m-l14</span>
                            <span class="label label-warning" style="display: inline-block;">m-l15</span>
                            <span class="label label-warning" style="display: inline-block;">m-l16</span>
                            <span class="label label-warning" style="display: inline-block;">m-l17</span>
                            <span class="label label-warning" style="display: inline-block;">m-l18</span>
                            <span class="label label-warning" style="display: inline-block;">m-l19</span>
                            <span class="label label-warning" style="display: inline-block;">m-l20</span>
                            <span class="label label-warning" style="display: inline-block;">m-l21</span>
                            <span class="label label-warning" style="display: inline-block;">m-l22</span>
                            <span class="label label-warning" style="display: inline-block;">m-l23</span>
                            <span class="label label-warning" style="display: inline-block;">m-l24</span>
                            <span class="label label-warning" style="display: inline-block;">m-l25</span>
                            <span class="label label-warning" style="display: inline-block;">m-l26</span>
                            <span class="label label-warning" style="display: inline-block;">m-l27</span>
                            <span class="label label-warning" style="display: inline-block;">m-l28</span>
                            <span class="label label-warning" style="display: inline-block;">m-l29</span>
                            <span class="label label-warning" style="display: inline-block;">m-l30</span>
                            <span class="label label-warning" style="display: inline-block;">m-l31</span>
                            <span class="label label-warning" style="display: inline-block;">m-l32</span>
                            <span class="label label-warning" style="display: inline-block;">m-l33</span>
                            <span class="label label-warning" style="display: inline-block;">m-l34</span>
                            <span class="label label-warning" style="display: inline-block;">m-l35</span>
                            <span class="label label-warning" style="display: inline-block;">m-l36</span>
                            <span class="label label-warning" style="display: inline-block;">m-l37</span>
                            <span class="label label-warning" style="display: inline-block;">m-l38</span>
                            <span class="label label-warning" style="display: inline-block;">m-l39</span>
                            <span class="label label-warning" style="display: inline-block;">m-l40</span>
                        </dd>
                        <dt class="togglable-heading"><strong>Negative left:</strong> <code class="small">m--l*</code></dt>
                        <dd class="togglable-content">
                            <span class="label label-info" style="display: inline-block;">m--l0</span>
                            <span class="label label-info" style="display: inline-block;">m--l1</span>
                            <span class="label label-info" style="display: inline-block;">m--l2</span>
                            <span class="label label-info" style="display: inline-block;">m--l3</span>
                            <span class="label label-info" style="display: inline-block;">m--l4</span>
                            <span class="label label-info" style="display: inline-block;">m--l5</span>
                            <span class="label label-info" style="display: inline-block;">m--l6</span>
                            <span class="label label-info" style="display: inline-block;">m--l7</span>
                            <span class="label label-info" style="display: inline-block;">m--l8</span>
                            <span class="label label-info" style="display: inline-block;">m--l9</span>
                            <span class="label label-info" style="display: inline-block;">m--l10</span>
                            <span class="label label-info" style="display: inline-block;">m--l11</span>
                            <span class="label label-info" style="display: inline-block;">m--l12</span>
                            <span class="label label-info" style="display: inline-block;">m--l13</span>
                            <span class="label label-info" style="display: inline-block;">m--l14</span>
                            <span class="label label-info" style="display: inline-block;">m--l15</span>
                            <span class="label label-info" style="display: inline-block;">m--l16</span>
                            <span class="label label-info" style="display: inline-block;">m--l17</span>
                            <span class="label label-info" style="display: inline-block;">m--l18</span>
                            <span class="label label-info" style="display: inline-block;">m--l19</span>
                            <span class="label label-info" style="display: inline-block;">m--l20</span>
                            <span class="label label-info" style="display: inline-block;">m--l21</span>
                            <span class="label label-info" style="display: inline-block;">m--l22</span>
                            <span class="label label-info" style="display: inline-block;">m--l23</span>
                            <span class="label label-info" style="display: inline-block;">m--l24</span>
                            <span class="label label-info" style="display: inline-block;">m--l25</span>
                            <span class="label label-info" style="display: inline-block;">m--l26</span>
                            <span class="label label-info" style="display: inline-block;">m--l27</span>
                            <span class="label label-info" style="display: inline-block;">m--l28</span>
                            <span class="label label-info" style="display: inline-block;">m--l29</span>
                            <span class="label label-info" style="display: inline-block;">m--l30</span>
                            <span class="label label-info" style="display: inline-block;">m--l31</span>
                            <span class="label label-info" style="display: inline-block;">m--l32</span>
                            <span class="label label-info" style="display: inline-block;">m--l33</span>
                            <span class="label label-info" style="display: inline-block;">m--l34</span>
                            <span class="label label-info" style="display: inline-block;">m--l35</span>
                            <span class="label label-info" style="display: inline-block;">m--l36</span>
                            <span class="label label-info" style="display: inline-block;">m--l37</span>
                            <span class="label label-info" style="display: inline-block;">m--l38</span>
                            <span class="label label-info" style="display: inline-block;">m--l39</span>
                            <span class="label label-info" style="display: inline-block;">m--l40</span>
                         </dd>
                    </dl>
                </div>
            </div>
        </div>

        <h3>Padding classes</h3>
        <div class="row">
            <div class="col-xs-12">
                <div class="well well-default-o">
                    <dl class="togglable togglable-list">
                        <dt class="togglable-heading"><strong>All sides:</strong> <code class="small">p-*</code></dt>
                        <dd class="togglable-content">
                                  <span class="label label-default" style="display: inline-block;">p-0</span>
                                  <span class="label label-default" style="display: inline-block;">p-1</span>
                                  <span class="label label-default" style="display: inline-block;">p-2</span>
                                  <span class="label label-default" style="display: inline-block;">p-3</span>
                                  <span class="label label-default" style="display: inline-block;">p-4</span>
                                  <span class="label label-default" style="display: inline-block;">p-5</span>
                                  <span class="label label-default" style="display: inline-block;">p-6</span>
                                  <span class="label label-default" style="display: inline-block;">p-7</span>
                                  <span class="label label-default" style="display: inline-block;">p-8</span>
                                  <span class="label label-default" style="display: inline-block;">p-9</span>
                                  <span class="label label-default" style="display: inline-block;">p-10</span>
                                  <span class="label label-default" style="display: inline-block;">p-11</span>
                                  <span class="label label-default" style="display: inline-block;">p-12</span>
                                  <span class="label label-default" style="display: inline-block;">p-13</span>
                                  <span class="label label-default" style="display: inline-block;">p-14</span>
                                  <span class="label label-default" style="display: inline-block;">p-15</span>
                                  <span class="label label-default" style="display: inline-block;">p-16</span>
                                  <span class="label label-default" style="display: inline-block;">p-17</span>
                                  <span class="label label-default" style="display: inline-block;">p-18</span>
                                  <span class="label label-default" style="display: inline-block;">p-19</span>
                                  <span class="label label-default" style="display: inline-block;">p-20</span>
                                  <span class="label label-default" style="display: inline-block;">p-21</span>
                                  <span class="label label-default" style="display: inline-block;">p-22</span>
                                  <span class="label label-default" style="display: inline-block;">p-23</span>
                                  <span class="label label-default" style="display: inline-block;">p-24</span>
                                  <span class="label label-default" style="display: inline-block;">p-25</span>
                                  <span class="label label-default" style="display: inline-block;">p-26</span>
                                  <span class="label label-default" style="display: inline-block;">p-27</span>
                                  <span class="label label-default" style="display: inline-block;">p-28</span>
                                  <span class="label label-default" style="display: inline-block;">p-29</span>
                                  <span class="label label-default" style="display: inline-block;">p-30</span>
                                  <span class="label label-default" style="display: inline-block;">p-31</span>
                                  <span class="label label-default" style="display: inline-block;">p-32</span>
                                  <span class="label label-default" style="display: inline-block;">p-33</span>
                                  <span class="label label-default" style="display: inline-block;">p-34</span>
                                  <span class="label label-default" style="display: inline-block;">p-35</span>
                                  <span class="label label-default" style="display: inline-block;">p-36</span>
                                  <span class="label label-default" style="display: inline-block;">p-37</span>
                                  <span class="label label-default" style="display: inline-block;">p-38</span>
                                  <span class="label label-default" style="display: inline-block;">p-39</span>
                                  <span class="label label-default" style="display: inline-block;">p-40</span>
                        </dd>               
                        <hr class="hr-md">
                        <dt class="togglable-heading"><strong>Top:</strong> <code class="small">p-t*</code></dt>
                        <dd class="togglable-content">
                                <p>Top only:
                                  <span class="label label-default" style="display: inline-block;">p-t0</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t1</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t2</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t3</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t4</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t5</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t6</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t7</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t8</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t9</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t10</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t11</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t12</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t13</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t14</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t15</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t16</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t17</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t18</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t19</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t20</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t21</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t22</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t23</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t24</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t25</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t26</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t27</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t28</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t29</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t30</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t31</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t32</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t33</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t34</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t35</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t36</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t37</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t38</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t39</span> 
                                  <span class="label label-default" style="display: inline-block;">p-t40</span> 
                                </p>
                                <p>Top and right:
                                  <span class="label label-info" style="display: inline-block;">p-tr0</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr1</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr2</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr3</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr4</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr5</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr6</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr7</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr8</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr9</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr10</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr11</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr12</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr13</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr14</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr15</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr16</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr17</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr18</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr19</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr20</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr21</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr22</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr23</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr24</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr25</span>
                                  <span class="label label-info" style="display: inline-block;">p-tr26</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr27</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr28</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr29</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr30</span>
                                  <span class="label label-info" style="display: inline-block;">p-tr31</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr32</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr33</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr34</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr35</span>
                                  <span class="label label-info" style="display: inline-block;">p-tr36</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr37</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr38</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr39</span> 
                                  <span class="label label-info" style="display: inline-block;">p-tr40</span>
                                </p>
                                <p>Top and bottom:
                                  <span class="label label-primary" style="display: inline-block;">p-tb0</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb1</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb2</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb3</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb4</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb5</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb6</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb7</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb8</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb9</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb10</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb11</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb12</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb13</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb14</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb15</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb16</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb17</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb18</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb19</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb20</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb21</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb22</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb23</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb24</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb25</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb26</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb27</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb28</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb29</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb30</span>
                                  <span class="label label-primary" style="display: inline-block;">p-tb31</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb32</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb33</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb34</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb35</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb36</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb37</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb38</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb39</span> 
                                  <span class="label label-primary" style="display: inline-block;">p-tb40</span>
                                </p>
                                <p>Top and left:
                                  <span class="label label-success" style="display: inline-block;">p-tl0</span>
                                  <span class="label label-success" style="display: inline-block;">p-tl1</span>
                                  <span class="label label-success" style="display: inline-block;">p-tl2</span>
                                  <span class="label label-success" style="display: inline-block;">p-tl3</span>
                                  <span class="label label-success" style="display: inline-block;">p-tl4</span>
                                  <span class="label label-success" style="display: inline-block;">p-tl5</span>
                                  <span class="label label-success" style="display: inline-block;">p-tl6</span>
                                  <span class="label label-success" style="display: inline-block;">p-tl7</span>
                                  <span class="label label-success" style="display: inline-block;">p-tl8</span>
                                  <span class="label label-success" style="display: inline-block;">p-tl9</span>
                                  <span class="label label-success" style="display: inline-block;">p-tl10</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl11</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl12</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl13</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl14</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl15</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl16</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl17</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl18</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl19</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl20</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl21</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl22</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl23</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl24</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl25</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl26</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl27</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl28</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl29</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl30</span>
                                  <span class="label label-success" style="display: inline-block;">p-tl31</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl32</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl33</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl34</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl35</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl36</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl37</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl38</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl39</span> 
                                  <span class="label label-success" style="display: inline-block;">p-tl40</span>
                               </p>
                        </dd>
                        <hr class="hr-md">
                        <dt class="togglable-heading"><strong>Right:</strong> <code class="small">p-r*</code></dt>
                        <dd class="togglable-content">
                            <p>Right only:
                                <span class="label label-default" style="display: inline-block;">p-r0</span>
                                <span class="label label-default" style="display: inline-block;">p-r1</span>
                                <span class="label label-default" style="display: inline-block;">p-r2</span>
                                <span class="label label-default" style="display: inline-block;">p-r3</span>
                                <span class="label label-default" style="display: inline-block;">p-r4</span>
                                <span class="label label-default" style="display: inline-block;">p-r5</span>
                                <span class="label label-default" style="display: inline-block;">p-r6</span>
                                <span class="label label-default" style="display: inline-block;">p-r7</span>
                                <span class="label label-default" style="display: inline-block;">p-r8</span>
                                <span class="label label-default" style="display: inline-block;">p-r9</span>
                                <span class="label label-default" style="display: inline-block;">p-r10</span>
                                <span class="label label-default" style="display: inline-block;">p-r11</span>
                                <span class="label label-default" style="display: inline-block;">p-r12</span>
                                <span class="label label-default" style="display: inline-block;">p-r13</span>
                                <span class="label label-default" style="display: inline-block;">p-r14</span>
                                <span class="label label-default" style="display: inline-block;">p-r15</span>
                                <span class="label label-default" style="display: inline-block;">p-r16</span>
                                <span class="label label-default" style="display: inline-block;">p-r17</span>
                                <span class="label label-default" style="display: inline-block;">p-r18</span>
                                <span class="label label-default" style="display: inline-block;">p-r19</span>
                                <span class="label label-default" style="display: inline-block;">p-r20</span>
                                <span class="label label-default" style="display: inline-block;">p-r21</span>
                                <span class="label label-default" style="display: inline-block;">p-r22</span>
                                <span class="label label-default" style="display: inline-block;">p-r23</span>
                                <span class="label label-default" style="display: inline-block;">p-r24</span>
                                <span class="label label-default" style="display: inline-block;">p-r25</span>
                                <span class="label label-default" style="display: inline-block;">p-r26</span>
                                <span class="label label-default" style="display: inline-block;">p-r27</span>
                                <span class="label label-default" style="display: inline-block;">p-r28</span>
                                <span class="label label-default" style="display: inline-block;">p-r29</span>
                                <span class="label label-default" style="display: inline-block;">p-r30</span>
                                <span class="label label-default" style="display: inline-block;">p-r31</span>
                                <span class="label label-default" style="display: inline-block;">p-r32</span>
                                <span class="label label-default" style="display: inline-block;">p-r33</span>
                                <span class="label label-default" style="display: inline-block;">p-r34</span>
                                <span class="label label-default" style="display: inline-block;">p-r35</span>
                                <span class="label label-default" style="display: inline-block;">p-r36</span>
                                <span class="label label-default" style="display: inline-block;">p-r37</span>
                                <span class="label label-default" style="display: inline-block;">p-r38</span>
                                <span class="label label-default" style="display: inline-block;">p-r39</span>
                                <span class="label label-default" style="display: inline-block;">p-r40</span>
                              </p>
                            <p>Right and bottom:
                                <span class="label label-info" style="display: inline-block;">p-rb0</span>
                                <span class="label label-info" style="display: inline-block;">p-rb1</span>
                                <span class="label label-info" style="display: inline-block;">p-rb2</span>
                                <span class="label label-info" style="display: inline-block;">p-rb3</span>
                                <span class="label label-info" style="display: inline-block;">p-rb4</span>
                                <span class="label label-info" style="display: inline-block;">p-rb5</span>
                                <span class="label label-info" style="display: inline-block;">p-rb6</span>
                                <span class="label label-info" style="display: inline-block;">p-rb7</span>
                                <span class="label label-info" style="display: inline-block;">p-rb8</span>
                                <span class="label label-info" style="display: inline-block;">p-rb9</span>
                                <span class="label label-info" style="display: inline-block;">p-rb10</span>
                                <span class="label label-info" style="display: inline-block;">p-rb11</span>
                                <span class="label label-info" style="display: inline-block;">p-rb12</span>
                                <span class="label label-info" style="display: inline-block;">p-rb13</span>
                                <span class="label label-info" style="display: inline-block;">p-rb14</span>
                                <span class="label label-info" style="display: inline-block;">p-rb15</span>
                                <span class="label label-info" style="display: inline-block;">p-rb16</span>
                                <span class="label label-info" style="display: inline-block;">p-rb17</span>
                                <span class="label label-info" style="display: inline-block;">p-rb18</span>
                                <span class="label label-info" style="display: inline-block;">p-rb19</span>
                                <span class="label label-info" style="display: inline-block;">p-rb20</span>
                                <span class="label label-info" style="display: inline-block;">p-rb21</span>
                                <span class="label label-info" style="display: inline-block;">p-rb22</span>
                                <span class="label label-info" style="display: inline-block;">p-rb23</span>
                                <span class="label label-info" style="display: inline-block;">p-rb24</span>
                                <span class="label label-info" style="display: inline-block;">p-rb25</span>
                                <span class="label label-info" style="display: inline-block;">p-rb26</span>
                                <span class="label label-info" style="display: inline-block;">p-rb27</span>
                                <span class="label label-info" style="display: inline-block;">p-rb28</span>
                                <span class="label label-info" style="display: inline-block;">p-rb29</span>
                                <span class="label label-info" style="display: inline-block;">p-rb30</span>
                                <span class="label label-info" style="display: inline-block;">p-rb31</span>
                                <span class="label label-info" style="display: inline-block;">p-rb32</span>
                                <span class="label label-info" style="display: inline-block;">p-rb33</span>
                                <span class="label label-info" style="display: inline-block;">p-rb34</span>
                                <span class="label label-info" style="display: inline-block;">p-rb35</span>
                                <span class="label label-info" style="display: inline-block;">p-rb36</span>
                                <span class="label label-info" style="display: inline-block;">p-rb37</span>
                                <span class="label label-info" style="display: inline-block;">p-rb38</span>
                                <span class="label label-info" style="display: inline-block;">p-rb39</span>
                                <span class="label label-info" style="display: inline-block;">p-rb40</span>
                              </p>
                            <p>Right and left: 
                                <span class="label label-primary" style="display: inline-block;">p-rl0</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl1</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl2</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl3</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl4</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl5</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl6</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl7</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl8</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl9</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl10</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl11</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl12</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl13</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl14</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl15</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl16</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl17</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl18</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl19</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl20</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl21</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl22</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl23</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl24</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl25</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl26</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl27</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl28</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl29</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl30</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl31</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl32</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl33</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl34</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl35</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl36</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl37</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl38</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl39</span>
                                <span class="label label-primary" style="display: inline-block;">p-rl40</span>
                            </p>
                        </dd>
                        <hr class="hr-md">
                        <dt class="togglable-heading"><strong>Bottom:</strong> <code class="small">p-b*</code></dt>
                        <dd class="togglable-content">
                            <span class="label label-info" style="display: inline-block;">p-b0</span>
                            <span class="label label-info" style="display: inline-block;">p-b1</span>
                            <span class="label label-info" style="display: inline-block;">p-b2</span>
                            <span class="label label-info" style="display: inline-block;">p-b3</span>
                            <span class="label label-info" style="display: inline-block;">p-b4</span>
                            <span class="label label-info" style="display: inline-block;">p-b5</span>
                            <span class="label label-info" style="display: inline-block;">p-b6</span>
                            <span class="label label-info" style="display: inline-block;">p-b7</span>
                            <span class="label label-info" style="display: inline-block;">p-b8</span>
                            <span class="label label-info" style="display: inline-block;">p-b9</span>
                            <span class="label label-info" style="display: inline-block;">p-b10</span>
                            <span class="label label-info" style="display: inline-block;">p-b11</span>
                            <span class="label label-info" style="display: inline-block;">p-b12</span>
                            <span class="label label-info" style="display: inline-block;">p-b13</span>
                            <span class="label label-info" style="display: inline-block;">p-b14</span>
                            <span class="label label-info" style="display: inline-block;">p-b15</span>
                            <span class="label label-info" style="display: inline-block;">p-b16</span>
                            <span class="label label-info" style="display: inline-block;">p-b17</span>
                            <span class="label label-info" style="display: inline-block;">p-b18</span>
                            <span class="label label-info" style="display: inline-block;">p-b19</span>
                            <span class="label label-info" style="display: inline-block;">p-b20</span>
                            <span class="label label-info" style="display: inline-block;">p-b21</span>
                            <span class="label label-info" style="display: inline-block;">p-b22</span>
                            <span class="label label-info" style="display: inline-block;">p-b23</span>
                            <span class="label label-info" style="display: inline-block;">p-b24</span>
                            <span class="label label-info" style="display: inline-block;">p-b25</span>
                            <span class="label label-info" style="display: inline-block;">p-b26</span>
                            <span class="label label-info" style="display: inline-block;">p-b27</span>
                            <span class="label label-info" style="display: inline-block;">p-b28</span>
                            <span class="label label-info" style="display: inline-block;">p-b29</span>
                            <span class="label label-info" style="display: inline-block;">p-b30</span>
                            <span class="label label-info" style="display: inline-block;">p-b31</span>
                            <span class="label label-info" style="display: inline-block;">p-b32</span>
                            <span class="label label-info" style="display: inline-block;">p-b33</span>
                            <span class="label label-info" style="display: inline-block;">p-b34</span>
                            <span class="label label-info" style="display: inline-block;">p-b35</span>
                            <span class="label label-info" style="display: inline-block;">p-b36</span>
                            <span class="label label-info" style="display: inline-block;">p-b37</span>
                            <span class="label label-info" style="display: inline-block;">p-b38</span>
                            <span class="label label-info" style="display: inline-block;">p-b39</span>
                            <span class="label label-info" style="display: inline-block;">p-b40</span>
                        </dd>
                        <hr class="hr-md">
                        <dt class="togglable-heading"><strong>Left:</strong> <code class="small">p-l*</code></dt>
                        <dd class="togglable-content">
                            <span class="label label-info" style="display: inline-block;">p-l0</span>
                            <span class="label label-info" style="display: inline-block;">p-l1</span>
                            <span class="label label-info" style="display: inline-block;">p-l2</span>
                            <span class="label label-info" style="display: inline-block;">p-l3</span>
                            <span class="label label-info" style="display: inline-block;">p-l4</span>
                            <span class="label label-info" style="display: inline-block;">p-l5</span>
                            <span class="label label-info" style="display: inline-block;">p-l6</span>
                            <span class="label label-info" style="display: inline-block;">p-l7</span>
                            <span class="label label-info" style="display: inline-block;">p-l8</span>
                            <span class="label label-info" style="display: inline-block;">p-l9</span>
                            <span class="label label-info" style="display: inline-block;">p-l10</span>
                            <span class="label label-info" style="display: inline-block;">p-l11</span>
                            <span class="label label-info" style="display: inline-block;">p-l12</span>
                            <span class="label label-info" style="display: inline-block;">p-l13</span>
                            <span class="label label-info" style="display: inline-block;">p-l14</span>
                            <span class="label label-info" style="display: inline-block;">p-l15</span>
                            <span class="label label-info" style="display: inline-block;">p-l16</span>
                            <span class="label label-info" style="display: inline-block;">p-l17</span>
                            <span class="label label-info" style="display: inline-block;">p-l18</span>
                            <span class="label label-info" style="display: inline-block;">p-l19</span>
                            <span class="label label-info" style="display: inline-block;">p-l20</span>
                            <span class="label label-info" style="display: inline-block;">p-l21</span>
                            <span class="label label-info" style="display: inline-block;">p-l22</span>
                            <span class="label label-info" style="display: inline-block;">p-l23</span>
                            <span class="label label-info" style="display: inline-block;">p-l24</span>
                            <span class="label label-info" style="display: inline-block;">p-l25</span>
                            <span class="label label-info" style="display: inline-block;">p-l26</span>
                            <span class="label label-info" style="display: inline-block;">p-l27</span>
                            <span class="label label-info" style="display: inline-block;">p-l28</span>
                            <span class="label label-info" style="display: inline-block;">p-l29</span>
                            <span class="label label-info" style="display: inline-block;">p-l30</span>
                            <span class="label label-info" style="display: inline-block;">p-l31</span>
                            <span class="label label-info" style="display: inline-block;">p-l32</span>
                            <span class="label label-info" style="display: inline-block;">p-l33</span>
                            <span class="label label-info" style="display: inline-block;">p-l34</span>
                            <span class="label label-info" style="display: inline-block;">p-l35</span>
                            <span class="label label-info" style="display: inline-block;">p-l36</span>
                            <span class="label label-info" style="display: inline-block;">p-l37</span>
                            <span class="label label-info" style="display: inline-block;">p-l38</span>
                            <span class="label label-info" style="display: inline-block;">p-l39</span>
                            <span class="label label-info" style="display: inline-block;">p-l40</span>
                        </dd>
                    </dl>
                </div>
            </div>
        </div> 
	</div>
<script>
    $('.sidebar a').on('click', function(){
    	//e.preventDefault();
        $('.sidebar a').removeClass('active');
        $(this).addClass('active');
    });
</script>
<script src="js/prettify.js"></script>
<?php require_once 'inc/modules/footer.php'; ?>