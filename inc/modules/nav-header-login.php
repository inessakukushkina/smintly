<div class="navbar navbar-fixed-top header">
	<div class="logo">
		<a href="" class="logo_link">
			Repucaution
		</a>
	</div>
	<button class="btn btn-menu">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<div class="head_nav">
		<ul class="user-nav">
			<li class="user-nav_item">
				<a href="" class="user-nav_link">					
					Admin
				</a>
				<ul class="sub_menu clearfix">
					<li class="user-nav_item">
						<a class="user-nav_link" href="">Log out</a>
					</li>
				</ul>
			</li>
			<li class="user-nav_item">
				<select class="chosen-select m-t7">
					<option value="">Administrator</option>
					<option value="">Test 1</option>
					<option value="">Test 2</option>
				</select>
			</li>
		</ul>
	</div>
	<i class="fa fa-qrcode collapse-button"></i>	
</div>