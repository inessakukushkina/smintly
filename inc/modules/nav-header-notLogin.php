<div class="navbar navbar-fixed-top header">
	<div class="logo">
		<a href="" class="logo_link">
			Repucaution
		</a>
	</div>
	<button class="btn btn-menu">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<div class="head_nav">
		<ul class="user-nav">
			<li class="user-nav_item">
				<a href="" class="user-nav_link">					
					Sign In
				</a>
			</li>
		</ul>
	</div>
	<i class="fa fa-qrcode collapse-button"></i>	
</div>