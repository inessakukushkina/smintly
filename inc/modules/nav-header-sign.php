<div class="navbar navbar-fixed-top header">
	<div class="logo">
		<a href="" class="logo_link">
			Repucaution
		</a>
	</div>
	<div class="head_nav">
		<ul class="user-nav">
			<li class="user-nav_item">
				<a href="" class="user-nav_link">					
					Admin
				</a>
				<ul class="sub_menu clearfix">
					<li class="user-nav_item">
						<a class="user-nav_link" href="">Log out</a>
					</li>
				</ul>
			</li>
			<li class="user-nav_item sign__drop">
				<!--<div class="dropdown sign__drop m-t15">
					<a data-toggle="dropdown" href="#">English</a>
					<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
						<li><a href="">Russian</a></li>
						<li><a href="">English</a></li>
					</ul>
				</div>-->
				<select class="chosen">
					<option value="">English</option>
					<option value="">Russian</option>
				</select>
			</li>
		</ul>
	</div>
	<i class="fa fa-qrcode collapse-button"></i>	
</div>