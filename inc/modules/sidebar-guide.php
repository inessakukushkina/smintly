<div class="sidebar active">
		<ul class="sidebar_content">
			<li class="sidebar_item">
				<a href="#grid" class="sidebar_link active">Grid system</a>
			</li>
			<li class="sidebar_item">
				<a href="#typography" class="sidebar_link">Typography</a>
			</li>
			<li class="sidebar_item">
				<a href="#header" class="sidebar_link">Header</a>
			</li>
			<li class="sidebar_item">
				<a href="#icons" class="sidebar_link">Icons</a>
			</li>
			<li class="sidebar_item">
				<a href="#buttons" class="sidebar_link">Buttons</a>
			</li>
			<li class="sidebar_item">
				<a href="#forms" class="sidebar_link">Forms</a>
			</li>
			<li class="sidebar_item">
				<a href="#dropdown" class="sidebar_link">Dropdown</a>
			</li>
			<li class="sidebar_item">
				<a href="#notification" class="sidebar_link">Notification</a>
			</li>
			<li class="sidebar_item">
				<a href="#checkbo" class="sidebar_link">Checkbo & Radio</a>
			</li>
			<li class="sidebar_item">
				<a href="#modal" class="sidebar_link">Modal window</a>
			</li>
			<li class="sidebar_item">
				<a href="#calendar" class="sidebar_link">Datepicker</a>
			</li>
			<li class="sidebar_item">
				<a href="#empty" class="sidebar_link">Empty block</a>
			</li>
			<li class="sidebar_item">
				<a href="#tables" class="sidebar_link">Responsive Tables</a>
			</li>
			<li class="sidebar_item">
				<a href="#loading" class="sidebar_link">Loading</a>
			</li>
			<li class="sidebar_item">
				<a href="#bread" class="sidebar_link">Breadcrumbs</a>
			</li>
			<li class="sidebar_item">
				<a href="#web-radar" class="sidebar_link">Web radar</a>
			</li>
			<li class="sidebar_item">
				<a href="#progressbar" class="sidebar_link">Progressbar</a>
			</li>
			<li class="sidebar_item">
				<a href="#note" class="sidebar_link">Notification, p.2</a>
			</li>
			<li class="sidebar_item">
				<a href="#cssHelper" class="sidebar_link">Css Helper</a>
			</li>
		</ul>
	</div>