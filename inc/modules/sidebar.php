<div class="sidebar">
	<ul class="sidebar_content">
		<li class="sidebar_item">
			<a href="" class="sidebar_link active">
				<i class="ti-home"></i>Dashboard
			</a>
		</li>		
		<li class="sidebar_item">
			<a href="" class="sidebar_link">
				<i class="ti-world"></i>
				Web Radar
			</a>
			<ul class="sidebar_submenu">
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">All mentions</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Twitter</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Facebook</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Google Plus</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Instagram</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Influencers watch</a>
				</li>
			</ul>
		</li>
		<li class="sidebar_item">
			<a href="" class="sidebar_link">
				<i class="ti-comment-alt"></i>
				Reviews
			</a>
			<ul class="sidebar_submenu">
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Google Places</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Yelp</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Merchant Circle</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Citysearch</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Yahoo Local</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Insider Pages</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Foursquare</a>
				</li>
			</ul>
		</li>
		<li class="sidebar_item">
			<a href="" class="sidebar_link">
			<i class="ti-bar-chart"></i>
			Google Rank</a>
		</li>
		<li class="sidebar_item">
			<a href="" class="sidebar_link">
				<i class="ti-thumb-up"></i>
				Social Media
			</a>
			<ul class="sidebar_submenu">
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Create</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Scheduled Posts</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Social Activity</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Social Reports</a>
				</li>
			</ul>
		</li>
		<li class="sidebar_item">
			<a href="" class="sidebar_link">
				<i class="ti-stats-up"></i>
				Analytics
			</a>
		</li>
		<li class="sidebar_item">
			<a href="" class="sidebar_link">
				<i class="ti-user"></i>
				CRM
			</a>
			<ul class="sidebar_submenu">
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Add record</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Directory</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Clients Activity</a>
				</li>
			</ul>
		</li>
		<li class="sidebar_item">
			<a href="" class="sidebar_link">
				<i class="ti-settings"></i>
				Settings
			</a>
			<ul class="sidebar_submenu">
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Personal Settings</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Directory Settings</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Google Places Keywords</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Social Media</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Social Mentions Keyword Phrases</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Social Keywords</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Analytics</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Rss</a>
				</li>
				<li class="sidebar_submenu_item">
					<a class="sidebar_submenu_link" href="">Collaboration Team</a>
				</li>
			</ul>
		</li>
	</ul>
</div>