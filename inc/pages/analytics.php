<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Analytics</h1>			
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-xs-12">
			<ul class="nav nav-tabs settings_tab">
				<li class="setting_item active auto">
					<a class="setting_link" href="#web-traffic" data-toggle="tab">
						<i class="ti-user"></i>
						Web Traffic
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#search-traffic" data-toggle="tab">
						<i class="ti-search"></i>
						Search Traffic
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#referral-traffic" data-toggle="tab">
						<i class="ti-link"></i>
						Referral Traffic
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#direct-traffic" data-toggle="tab">
						<i class="ti-direction"></i>
						Direct Traffic
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#adwords-traffic" data-toggle="tab">
						<i class="ti-key"></i>
						Adwords Traffic
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="tab-content settings_content">
				<div class="tab-pane active clearfix" id="web-traffic">
					<div class="row">
						<div class="col-xs-12">
							<div class="date_range">
								<div class="reportrange" >
									<i class="fa fa-calendar"></i>
									<span></span>
									<b class="caret"></b>
								</div>
							</div>
						</div>
					</div>	
					<div class="row">
						<div class="col-sm-4">
							<div class="traffic_block">
								<p class="gray_text">
									<i class="ti-user"></i>
									<span class="gray_text_big">363</span> Visits
								</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="traffic_block">
								<p class="green_text">
									<i class="ti-alarm-clock"></i>
									<span class="green_text_big">360</span> Unique Visitors
								</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="traffic_block">
								<p class="pink_text">
									<i class="ti-time"></i>
									<span class="pink_text_big">0:36:00</span> Average Visit Duration
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-8">
							<div class="traffic_shedule">
								<img src="images/pictures/shedule.png" alt="">
							</div>
						</div>
						<div class="col-sm-4">
							<div class="traffic_block">
								<p class="blue_text">
									<span class="blue_text_big">80%</span> Bounce Rate
								</p>
								<div class="progress-pie" data-percent="60">
									<div class="ppc-progress">
										<div class="ppc-progress-fill"></div>
									</div>
									<div class="ppc-percents">
										<div class="pcc-percents-wrapper"><span>%</span></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="traffic_block">
								<p class="light_blue_text">
									<span class="light_blue_text_big">80%</span> New Visits
								</p>
								<div class="progress-pie" data-percent="40">
									<div class="ppc-progress">
										<div class="ppc-progress-fill"></div>
									</div>
									<div class="ppc-percents">
										<div class="pcc-percents-wrapper"><span>%</span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="search-traffic">
					<div class="row">
						<div class="col-xs-12">
							<div class="date_range">
								<div class="reportrange" >
									<i class="fa fa-calendar"></i>
									<span></span>
									<b class="caret"></b>
								</div>
							</div>
						</div>
					</div>	
					<div class="row">
						<div class="col-xs-12">
							<p class="strong-size text_color m-tb20">Top Keywords</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<table class="responsive-table m-b20 b-Bottom">
								<thead class="table_head">
									<th>Source</th>
									<th>Visits</th>
									<th>Avg. Visit Dur.</th>
									<th>% New Visits</th>
									<th>Bounce Rate</th>
								</thead>
								<tbody>
									<tr>
										<td data-th="Source">Social buttons</td>
										<td data-th="Visits">187</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">Social buttons download</td>
										<td data-th="Visits">39</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">Social buttons</td>
										<td data-th="Visits">187</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">Social buttons download</td>
										<td data-th="Visits">39</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="referral-traffic">
					<div class="row">
						<div class="col-xs-12">
							<div class="date_range">
								<div class="reportrange" >
									<i class="fa fa-calendar"></i>
									<span></span>
									<b class="caret"></b>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="strong-size text_color m-tb20">Top WebSites</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<table class="responsive-table m-b20 b-Bottom">
								<thead class="table_head">
									<th>Source</th>
									<th>Visits</th>
									<th>Avg. Visit Dur.</th>
									<th>% New Visits</th>
									<th>Bounce Rate</th>
								</thead>
								<tbody>
									<tr>
										<td data-th="Source">social-buttons.com</td>
										<td data-th="Visits">187</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">site39.social-buttons.com</td>
										<td data-th="Visits">39</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">social-buttons.com</td>
										<td data-th="Visits">187</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">site39.social-buttons.com</td>
										<td data-th="Visits">39</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="direct-traffic">
					<div class="row">
						<div class="col-xs-12">
							<div class="date_range">
								<div class="reportrange" >
									<i class="fa fa-calendar"></i>
									<span></span>
									<b class="caret"></b>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="strong-size text_color m-tb20">Top Pages</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<table class="responsive-table m-b20 b-Bottom">
								<thead class="table_head">
									<th>Source</th>
									<th>Visits</th>
									<th>Avg. Visit Dur.</th>
									<th>% New Visits</th>
									<th>Bounce Rate</th>
								</thead>
								<tbody>
									<tr>
										<td data-th="Source">social-buttons.com</td>
										<td data-th="Visits">187</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">site39.social-buttons.com</td>
										<td data-th="Visits">39</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">social-buttons.com</td>
										<td data-th="Visits">187</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">site39.social-buttons.com</td>
										<td data-th="Visits">39</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="adwords-traffic">
					<div class="row">
						<div class="col-xs-12">
							<div class="date_range">
								<div class="reportrange" >
									<i class="fa fa-calendar"></i>
									<span></span>
									<b class="caret"></b>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="strong-size text_color m-tb20">Top Keywords</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<table class="responsive-table m-b20 b-Bottom">
								<thead class="table_head">
									<th>Source</th>
									<th>Visits</th>
									<th>Avg. Visit Dur.</th>
									<th>% New Visits</th>
									<th>Bounce Rate</th>
								</thead>
								<tbody>
									<tr>
										<td data-th="Source">Social buttons</td>
										<td data-th="Visits">187</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">Social buttons download</td>
										<td data-th="Visits">39</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">Social buttons</td>
										<td data-th="Visits">187</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>
									<tr>
										<td data-th="Source">Social buttons download</td>
										<td data-th="Visits">39</td>
										<td data-th="Avg. Visit Dur.">0:00:00</td>
										<td data-th="% New Visits">100.00%</td>
										<td data-th="Bounce Rate">100.00%</td>
									</tr>									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>