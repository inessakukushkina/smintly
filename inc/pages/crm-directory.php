<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">CRM</h1>
		</div>
	</div>
</div>
<div class="main_block">	
	<div class="row">
		<div class="col-sm-1 col-md-2 col-lg-1">
			<p class="p-t10 strong-size text_color">Directory</p>
		</div>
		<div class="col-sm-3 ">
			<div class="form-group google_places autocomplete_block">
				<input type="text" class="form-control" placeholder="Search by Name" />
			</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search by Company"/>
			</div>
		</div>
		<div class="col-sm-3">
			<button class="btn btn-save">Search</button>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<table class="responsive-table b-Bottom">
				<thead class="table_head">
					<th>Name</th>
					<th>Company</th>
					<th>Action</th>
				</thead>
				<tbody>
					<tr>
						<td data-th="Name" class="p-b10">
							<a href="" class="link">Steve Wozniak</a>
						</td>
						<td data-th="Company" class="p-b10">Apple</td>
						<td data-th="Action" class="p-b10">
							<a href="" class="link m-r10">edit</a> <a href="" class="link">remove</a>
						</td>
					</tr>
					<tr>
						<td data-th="Name" class="p-b10">
							<a href="" class="link">Steve Wozniak</a>
						</td>
						<td data-th="Company" class="p-b10">Apple</td>
						<td data-th="Action" class="p-b10">
							<a href="" class="link m-r10">edit</a> <a href="" class="link">remove</a>
						</td>					
					</tr>					
				</tbody>
			</table>
		</div>
	</div>
</div>