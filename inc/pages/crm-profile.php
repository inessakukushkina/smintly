<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">CRM</h1>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-sm-1 col-md-2 col-lg-1">
			<p class="p-t10 strong-size text_color">Directory</p>
		</div>
		<div class="col-sm-3 ">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search by Name"/>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search by Company"/>
			</div>
		</div>
		<div class="col-sm-3">
			<button class="btn btn-save">Search</button>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 m-t10">
			<div class="b-Bottom clearfix">
				<p class="text_color strong-size pull-sm-left">Steve Wozniak</p>
				<p class="pull-sm-right"><a href="" class="link m-r10">Edit</a> <a href="" class="link">Remove</a></p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<table class="table crm_profile">
				<tbody>
					<tr>
						<td>
							<p class="gray-color bold">First Name</p>
						</td>
						<td>
							<p>Steve</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="gray-color bold">Last Name</p>
						</td>
						<td>
							<p>Wozniak</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="gray-color bold">Email</p>
						</td>
						<td>
							<p><a href="" class="link">steve@apple.com</a></p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="gray-color bold">Company</p>
						</td>
						<td>
							<p>Apple</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="gray-color bold">Website</p>
						</td>
						<td>
							<p><a href="" class="link">www.apple.com</a></p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="gray-color bold">Notes</p>
						</td>
						<td>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
								A quisquam, saepe voluptate placeat temporibus. 
								Quod, ducimus, ipsum quos inventore ex cum nisi impedit at voluptatum cupiditate consequatur neque eius, excepturi!
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="gray-color bold">Profile link</p>
						</td>
						<td>
							<p class="strong-text">
								<i class="fa fa-facebook-square facebook-color"></i>
								<i class="fa fa-twitter-square twitter-color"></i>
								<i class="fa fa-google-plus-square google-color"></i>
								<i class="fa fa-linkedin linkedin-color"></i>
								<i class="fa fa-instagram instagram-color"></i>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 m-t10">
			<p class="text_color strong-size">Social activity</p>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="nav nav-tabs settings_tab">
				<li class="setting_item active auto">
					<a class="setting_link" href="#all-social" data-toggle="tab">
						<i class="ti-thumb-up"></i>
						All social
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#facebook" data-toggle="tab">
						<i class="ti-facebook"></i>
						Facebook
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#twitter" data-toggle="tab">
						<i class="ti-twitter"></i>
						Twitter
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#google" data-toggle="tab">
						<i class="ti-google"></i>
						Google +
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#linkedin" data-toggle="tab">
						<i class="ti-linkedin"></i>
						Linkedin
					</a>
				</li>	
				<li class="setting_item auto">
					<a class="setting_link" href="#instagram" data-toggle="tab">
						<i class="ti-instagram"></i>
						Instagram
					</a>
				</li>			
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="tab-content settings_content">
				<div class="tab-pane active" id="all-social">
					<div class="web_radar m-t20 pull_border">
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-instagram i-instagram"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<div class="clearfix m-t10">
										<a href="">Unlike</a>
										<a href="" class="m-l10 show_comments">Comments</a>
									</div>
									<div class="web_comments">
										<div class="row">
											<div class="col-xs-12">
												<div class="comment_block dTable">
													<div class="dRow">
														<div class="dCell cellImg">
															<a href="">
																<img class="comment_avatar" src="images/avatar/avatar1.png" alt="">
															</a>
														</div>
														<div class="dCell">
															<a class="blue_color comment_title">
																jboylion23
															</a>
															<p class="comment_text">Yes!!!</p>
															<p class="comment_date">
																<cite class="gray-color fa fa-clock-o "></cite>Apr 13, 2015 02:35 pm
															</p>
														</div>
													</div>
													<div class="dRow">
														<div class="dCell cellImg">
															<a href="">
																<img class="comment_avatar" src="images/avatar/enot.jpg" alt="">
															</a>
														</div>
														<div class="dCell">
															<a class="blue_color comment_title">
																jboylion23
															</a>
															<p class="comment_text">
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
															</p>
															<p class="comment_date">
																<cite class="gray-color fa fa-clock-o "></cite>Apr 13, 2015 02:35 pm
															</p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12">
												<div class="form-group">
													<textarea rows="5" class="form-control" placeholder="Write a comment"></textarea>
												</div>
												<div class="pull-right">
													<button class="btn btn-save">Post comment</button>
												</div>
											</div>
										</div>
									</div>									
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>	
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>		
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>		
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>		
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>		
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>		
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>					
					</div>	
				</div>
				<div class="tab-pane" id="facebook">
					<div class="web_radar m-t20 pull_border">
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>	
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>
					</div>		
				</div>
				<div class="tab-pane" id="twitter">
					<div class="web_radar m-t20 pull_border">
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-reply"></i>
									<i class="fa fa-share"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-twitter-square i-twitter"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-reply"></i>
									<i class="fa fa-share"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-twitter-square i-twitter"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-reply"></i>
									<i class="fa fa-share"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-twitter-square i-twitter"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-reply"></i>
									<i class="fa fa-share"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-twitter-square i-twitter"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-reply"></i>
									<i class="fa fa-share"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-twitter-square i-twitter"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-reply"></i>
									<i class="fa fa-share"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-twitter-square i-twitter"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="tab-pane" id="google">
					<div class="web_radar m-t20 pull_border">
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="linkedin">
					<div class="web_radar m-t20 pull_border">
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-linkedin-square i-linkedin"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<div class="clearfix m-t10">
										<a href=""></i> Unlike</a>
										<a href="" class=" m-l10 show_comments">Comments</a>
									</div>
									<div class="web_comments">
										<div class="form-group">
											<textarea rows="5" class="form-control" placeholder="Write a comment"></textarea>
										</div>
										<div class="pull-right">
											<button class="btn btn-save">Post comment</button>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="instagram">
					<div class="web_radar m-t20 pull_border">
						<div class="web_radar_content dTable">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-instagram i-instagram"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<div class="clearfix m-t10">
										<a href="">Unlike</a>
										<a href="" class="m-l10 show_comments">Comments</a>
									</div>
									<div class="web_comments">
										<div class="row">
											<div class="col-xs-12">
												<div class="comment_block dTable">
													<div class="dRow">
														<div class="dCell cellImg">
															<a href="">
																<img class="comment_avatar" src="images/avatar/avatar1.png" alt="">
															</a>
														</div>
														<div class="dCell">
															<a class="blue_color comment_title">
																jboylion23
															</a>
															<p class="comment_text">Yes!!!</p>
															<p class="comment_date">
																<cite class="gray-color fa fa-clock-o "></cite>Apr 13, 2015 02:35 pm
															</p>
														</div>
													</div>
													<div class="dRow">
														<div class="dCell cellImg">
															<a href="">
																<img class="comment_avatar" src="images/avatar/enot.jpg" alt="">
															</a>
														</div>
														<div class="dCell">
															<a class="blue_color comment_title">
																jboylion23
															</a>
															<p class="comment_text">
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
																Acquire Facebook followers with the help of our Social marketing program
															</p>
															<p class="comment_date">
																<cite class="gray-color fa fa-clock-o "></cite>Apr 13, 2015 02:35 pm
															</p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-12">
												<div class="form-group">
													<textarea rows="5" class="form-control" placeholder="Write a comment"></textarea>
												</div>
												<div class="pull-right">
													<button class="btn btn-save">Post comment</button>
												</div>
											</div>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>