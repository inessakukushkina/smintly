<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Dashboard</h1>
		</div>
	</div>
</div>
<div class="main_block">	
	<div class="row">
		<div class="col-lg-8 box_dashboard">
			<!--<div class="block_content summary_content">
				<h2 class="block_content_title">Summary</h2>
				<div class="block_content_body">
					<div class="row">
						<div class="col-sm-6 col-lg-3 box_content">
							<div class="summary_block">
								<i class="ti-file"></i>
								<p class="summary_text p-t10">
									<span>0</span>
									Review(s)
								</p>
							</div>
						</div>
						<div class="col-sm-6 col-lg-3 box_content">
							<div class="summary_block">
								<i class="fa fa-user"></i>
								<p class="summary_text">
									<span>1364</span>
									Web Traffic
									<small>Last month</small>
								</p>
							</div>
						</div>
						<div class="col-sm-6 col-lg-3 box_content">
							<div class="summary_block">
								<i class="fa fa-thumbs-o-up"></i>
								<p class="summary_text">
									<span>1</span>
									Facebook Like (s)
									<small>Last month</small>
								</p>
							</div>
						</div>
						<div class="col-sm-6 col-lg-3 box_content">
							<div class="summary_block">
								<i class="ti-twitter"></i>
								<p class="summary_text">
									<span>50</span>
									Twitter Follower (s)
									<small>Last month</small>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>-->
			<div class="block_content">
				<h2 class="block_content_title">Trends</h2>
				<div class="block_content_body">					
					<ul class="trends_settings nav nav-tabs">
						<li class="trends_item active">
							<a href="#trends_traffic" class="trends_link" data-toggle="tab">
								<i class="ti-server"></i>
								Web Traffic
								<span class="trends_count">
									1364
								</span>
							</a>
						</li>
						<li class="trends_item">
							<a href="#trends_followers" class="trends_link" data-toggle="tab">
								<i class="ti-twitter-alt"></i>
								Twitter Followers
								<span class="trends_count">
									50
								</span>
							</a>
						</li>
						<li class="trends_item">
							<a href="#trends_facebook" class="trends_link" data-toggle="tab">
								<i class="ti-facebook"></i>
								Facebook Likes
								<span class="trends_count">
									5
								</span>
							</a>
						</li>
						<li class="trends_item">
							<a href="#trends_rank" class="trends_link" data-toggle="tab">
								<i class="ti-bar-chart-alt"></i>
								Google Rank
								<span class="trends_count">
									2
								</span>
							</a>
						</li>
						<li class="trends_item">
							<a href="#trends_review" class="trends_link" data-toggle="tab">
								<i class="ti-comment"></i>
								Review
								<span class="trends_count">
									0
								</span>
							</a>
						</li>												
					</ul>
					<div class="row">
						<div class="col-xs-12">
							<div class="trends_chart">
								<div class="tab-content">
									<div class="tab-pane active" id="trends_traffic">
										<img src="images/pictures/shedule.png" alt="">
									</div>
									<div class="tab-pane" id="trends_followers">
									</div>
									<div class="tab-pane" id="trends_facebook">
									</div>
									<div class="tab-pane" id="trends_rank">
										<img src="images/pictures/shedule.png" alt="">
									</div>
									<div class="tab-pane" id="trends_review">
										<p class="bold p-t40">Nothing happened during last month.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 web_radar">
			<div class="block_content">
				<h2 class="block_content_title">Web radar</h2>
				<div class="block_content_body">
					<div class="radar_content">
						<div class="radar_body">
							<a href="">
								<img class="radar_picture" src="images/avatar/avatar1.png" alt="">
							</a>
							<div class="radar_body_content">								
								<p class="radar_date">03.27.2015 06.29 am</p>
								<i class="fa fa-reply"></i>
								<i class="fa fa-share"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-twitter-square i-twitter"></i>
								<img src="images/avatar/enot.jpg" alt="" class="web_radar_image">
								<p class="radar_text">
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
								</p>
							</div>
						</div>
						<div class="radar_body">
							<a href="">
								<img class="radar_picture" src="images/avatar/avatar1.png" alt="">
							</a>
							<div class="radar_body_content">								
								<p class="radar_date">03.27.2015 06.29 am</p>
								<i class="fa fa-reply"></i>
								<i class="fa fa-share"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-facebook-square i-facebook"></i>
								<p class="radar_text">
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
								</p>
							</div>
						</div>
						<div class="radar_body">
							<a href="">
								<img class="radar_picture" src="images/avatar/avatar1.png" alt="">
							</a>
							<div class="radar_body_content">								
								<p class="radar_date">03.27.2015 06.29 am</p>
								<i class="fa fa-reply"></i>
								<i class="fa fa-share"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-google-plus-square i-google"></i>
								<p class="radar_text">
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
									Acquire Facebook and twitter followers with the help of our Social marketing program http://t.co/jWyEmpPNMF #SMM
								</p>
								<p><a href="">+1</a></p>
							</div>
						</div>							
					</div>
					<div class="block_content_footer">
						<a href="" class="link">View all</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="main_block m-t20">
	<div class="row">
		<div class="col-sm-8">
            <div class="block_content">
                <h2 class="block_content_title post_title">Create new post</h2>
                <!--<div class="row" id="create-social">
				    <div class="col-xs-12">
				        <div class="tab-content settings_content">
				            <div class="p-20">
				                <form action="https://app.smintly.com/social/create/post_create" method="POST" id="post-update-form" autocomplete="off">
				                    <p class="text_color strong-size">Post To</p>
									<div class="row m-b15">
				    					<div class="col-xs-12">
						                    <label class="cb-checkbox regRoboto m-r10 checked">
						                		<span class="cb-inner"><i><input type="checkbox" name="post_to_socials[]" value="twitter" checked="checked"></i></span>
						                		<cite class="ti-twitter"></cite>
						                		Twitter
						                	</label>
						                    <label class="cb-checkbox regRoboto m-r10 checked">
						                		<span class="cb-inner"><i><input type="checkbox" name="post_to_socials[]" value="facebook" checked="checked"></i></span>
						                		<cite class="ti-facebook"></cite>
						                		Facebook
						                	</label>
						                    <p>
						                		Please add <a href="https://app.smintly.com/settings/socialmedia/">Linkedin account.</a>
						            		</p>
						            	</div>
									</div>
									<div class="row">
				                        <div class="col-xs-12 m-t10 col-md-10">
				                            <p class="text_color strong-size">Type a message</p>
				                            <div class="form-group">
				                                <textarea name="description" rows="5" class="form-control"></textarea>
				                                <span class="help-block char-counter"></span>
				                            </div>
				                        </div>
				                    </div>
									<div class="row" id="attachment">
									    <div class="col-xs-12 m-t5">
									        <p class="text_color strong-size">Attach image or video (optional)</p>
									    </div>
									    <div class="col-xs-12">
									        <label class="cb-radio regRoboto m-r10 checked">
									            <span class="cb-inner"><i><input name="attachment_type" value="image-designer" checked="checked" type="radio"></i></span>
									            Image designer
									        </label>
									        <label class="cb-radio regRoboto m-r10">
									            <span class="cb-inner"><i><input name="attachment_type" value="photo" type="radio"></i></span>
									            Photo
									        </label>
									        <label class="cb-radio regRoboto">
									            <span class="cb-inner"><i><input name="attachment_type" value="video" type="radio"></i></span>
									            Video
									        </label>
									    </div>
									    <div class="col-xs-12 attachment-block" id="image-designer-block">
									        <div class="well well-standart well-upload-image">
									            <div class="row">
									                <div class="col-md-6">
									                    <div class="row">
									                        <div class="col-xs-12">
									                            <p class="head_tab">Head text</p>
									                            <div class="row">
									                                <div class="col-md-9">
									                                    <div class="form-group ">
									                                        <select id="headline_font_select" class="chosen-select">
									                                            <option selected="selected" value="Fredoka One">Fredoka One</option>
									                                            <option value="Hammersmith One">Hammersmith One</option>
									                                            <option value="Josefin Slab">Josefin Slab</option>
									                                            <option value="Lato">Lato</option>
									                                            <option value="Merriweather">Merriweather</option>
									                                            <option value="Montserrat">Montserrat</option>
									                                            <option value="Open Sans">Open Sans</option>
									                                            <option value="Roboto">Roboto</option>
									                                            <option value="Satisfy">Satisfy</option>
									                                            <option value="Ubuntu">Ubuntu</option>
									                                        </select>
									                                    </div>
									                                </div>
									                                <div class="col-md-3">
									                                    <div class="form-group pull-md-right">
									                                        <div class="input-group pick-a-color-markup" id="headline_color">
									                                            <div class="input-group pick-a-color-markup" id="headline_color"><input value="ffffff" name="headline_color" class="pick-a-color form-control" type="hidden"><div class="input-group-btn"><button type="button" class="btn btn-default color-dropdown dropdown-toggle no-hex"><span class="color-preview current-color" style="background-color: rgb(255, 255, 255);"></span><span class="caret"></span></button><div class="color-menu dropdown-menu no-hex"><div class="color-menu-tabs"><span class="basicColors-tab tab tab-active"><a>Basic Colors</a></span><span class="savedColors-tab tab"><a>Saved Colors</a></span><span class="advanced-tab tab"><a>Advanced</a></span></div><div class="basicColors-content active-content"><h6 class="color-menu-instructions">Tap spectrum or drag band to change color</h6><ul class="basic-colors-list"><li class="color-item"><a class="white color-link"><span class="color-preview white" style="background-color: rgb(255, 255, 255);"></span><span class="color-label">white</span><span class="color-box spectrum-white"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="red color-link"><span class="color-preview red" style="background-color: rgb(255, 0, 0);"></span><span class="color-label">red</span><span class="color-box spectrum-red"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="orange color-link"><span class="color-preview orange" style="background-color: rgb(255, 102, 0);"></span><span class="color-label">orange</span><span class="color-box spectrum-orange"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="yellow color-link"><span class="color-preview yellow" style="background-color: rgb(255, 255, 0);"></span><span class="color-label">yellow</span><span class="color-box spectrum-yellow"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="green color-link"><span class="color-preview green" style="background-color: rgb(0, 128, 0);"></span><span class="color-label">green</span><span class="color-box spectrum-green"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="blue color-link"><span class="color-preview blue" style="background-color: rgb(0, 0, 255);"></span><span class="color-label">blue</span><span class="color-box spectrum-blue"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="purple color-link"><span class="color-preview purple" style="background-color: rgb(128, 0, 128);"></span><span class="color-label">purple</span><span class="color-box spectrum-purple"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="black color-link"><span class="color-preview black" style="background-color: rgb(0, 0, 0);"></span><span class="color-label">black</span><span class="color-box spectrum-black"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li></ul></div><div class="savedColors-content inactive-content"><ul class="saved-color-col 0"><li class="color-item"><a class="#ffffff"><span class="color-preview" style="background-color: rgb(255, 255, 255);"></span><span class="color-label">#ffffff</span></a></li></ul><ul class="saved-color-col 1"></ul></div><div class="advanced-content inactive-content"><h6 class="advanced-instructions">Tap spectrum or drag band to change color</h6><ul class="advanced-list"><li class="hue-item"><span class="hue-text">Hue: <span class="hue-value">0</span></span><span class="color-box spectrum-hue"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></li><li class="lightness-item"><span class="lightness-text">Lightness: <span class="lightness-value">50%</span></span><span class="color-box spectrum-lightness"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></li><li class="saturation-item"><span class="saturation-text">Saturation: <span class="saturation-value">100%</span></span><span class="color-box spectrum-saturation"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></li><li class="preview-item"><span class="preview-text">Preview</span><span class="color-preview advanced"><button class="color-select btn btn-mini advanced" type="button">Select</button></span></li></ul></div></div></div></div>
									                                        </div>
									                                    </div>
									                                </div>
									                            </div>
									                            <div class="form-group">
									                                <textarea name="headline_text" id="image-designer-headline-text" rows="10" class="form-control"></textarea>
									                            </div>
									                        </div>
									                    </div>
									                    <div class="row">
									                        <div class="col-xs-12" id="secondary-text-block" style="display: none;">
									                            <p class="head_tab">Secondary text</p>
									                            <div class="row">
									                                <div class="col-md-9">
									                                    <div class="form-group ">
									                                        <select id="secondary_font_select" class="chosen-select">
									                                            <option selected="selected" value="Fredoka One">Fredoka One</option>
									                                            <option value="Hammersmith One">Hammersmith One</option>
									                                            <option value="Josefin Slab">Josefin Slab</option>
									                                            <option value="Lato">Lato</option>
									                                            <option value="Merriweather">Merriweather</option>
									                                            <option value="Montserrat">Montserrat</option>
									                                            <option value="Open Sans">Open Sans</option>
									                                            <option value="Roboto">Roboto</option>
									                                            <option value="Satisfy">Satisfy</option>
									                                            <option value="Ubuntu">Ubuntu</option>
									                                        </select>
									                                    </div>
									                                </div>
									                                <div class="col-md-3">
									                                    <div class="form-group pull-md-right">
									                                        <div class="input-group pick-a-color-markup" id="headline_color">
									                                            <div class="input-group pick-a-color-markup" id="secondary_color"><input value="ffffff" name="secondary_color" class="pick-a-color form-control" type="hidden"><div class="input-group-btn"><button type="button" class="btn btn-default color-dropdown dropdown-toggle no-hex"><span class="color-preview current-color" style="background-color: rgb(255, 255, 255);"></span><span class="caret"></span></button><div class="color-menu dropdown-menu no-hex"><div class="color-menu-tabs"><span class="basicColors-tab tab tab-active"><a>Basic Colors</a></span><span class="savedColors-tab tab"><a>Saved Colors</a></span><span class="advanced-tab tab"><a>Advanced</a></span></div><div class="basicColors-content active-content"><h6 class="color-menu-instructions">Tap spectrum or drag band to change color</h6><ul class="basic-colors-list"><li class="color-item"><a class="white color-link"><span class="color-preview white" style="background-color: rgb(255, 255, 255);"></span><span class="color-label">white</span><span class="color-box spectrum-white"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="red color-link"><span class="color-preview red" style="background-color: rgb(255, 0, 0);"></span><span class="color-label">red</span><span class="color-box spectrum-red"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="orange color-link"><span class="color-preview orange" style="background-color: rgb(255, 102, 0);"></span><span class="color-label">orange</span><span class="color-box spectrum-orange"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="yellow color-link"><span class="color-preview yellow" style="background-color: rgb(255, 255, 0);"></span><span class="color-label">yellow</span><span class="color-box spectrum-yellow"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="green color-link"><span class="color-preview green" style="background-color: rgb(0, 128, 0);"></span><span class="color-label">green</span><span class="color-box spectrum-green"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="blue color-link"><span class="color-preview blue" style="background-color: rgb(0, 0, 255);"></span><span class="color-label">blue</span><span class="color-box spectrum-blue"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="purple color-link"><span class="color-preview purple" style="background-color: rgb(128, 0, 128);"></span><span class="color-label">purple</span><span class="color-box spectrum-purple"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li><li class="color-item"><a class="black color-link"><span class="color-preview black" style="background-color: rgb(0, 0, 0);"></span><span class="color-label">black</span><span class="color-box spectrum-black"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></a></li></ul></div><div class="savedColors-content inactive-content"><ul class="saved-color-col 0"><li class="color-item"><a class="#ffffff"><span class="color-preview" style="background-color: rgb(255, 255, 255);"></span><span class="color-label">#ffffff</span></a></li></ul><ul class="saved-color-col 1"></ul></div><div class="advanced-content inactive-content"><h6 class="advanced-instructions">Tap spectrum or drag band to change color</h6><ul class="advanced-list"><li class="hue-item"><span class="hue-text">Hue: <span class="hue-value">0</span></span><span class="color-box spectrum-hue"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></li><li class="lightness-item"><span class="lightness-text">Lightness: <span class="lightness-value">50%</span></span><span class="color-box spectrum-lightness"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></li><li class="saturation-item"><span class="saturation-text">Saturation: <span class="saturation-value">100%</span></span><span class="color-box spectrum-saturation"><span class="highlight-band"><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span><span class="highlight-band-stripe"></span></span></span></li><li class="preview-item"><span class="preview-text">Preview</span><span class="color-preview advanced"><button class="color-select btn btn-mini advanced" type="button">Select</button></span></li></ul></div></div></div></div>
									                                        </div>
									                                    </div>
									                                </div>
									                            </div>
									                            <div class="form-group">
									                                <textarea name="secondary_text" id="image-designer-secondary-text" rows="10" class="form-control"></textarea>
									                            </div>
									                        </div>
									                    </div>
									                    <div id="image-designer-logo" style="display: none;">
									                        <div class="row">
									                            <div class="col-sm-5 preview" id="image-designer-logo">

									                            </div>
									                        </div>
									                    </div>
									                    <div class="row well-standart">
									                        <div class="col-xs-12 text-left">
									                            <div class="progressBar" style="display: none;">
									                                <div class="progressLine" data-value="0"></div>
									                            </div>
									                            <button class="btn btn-add m-b20" id="image-designer-add-secondary-text" data-added="false">+ Add secondary text</button>
									                            <button class="btn btn-save fileSelect m-b20">
									                                + Add logo
									                            </button>
									                            <input class="uploadbtn inputFile" id="image-designer" multiple="" type="file">
									                        </div>
									                    </div>
									                </div>
									                <div class="col-md-6 well-standart">
									                    <div class="canvas-block">
									                        <div class="canvas-container">
									                            <div class="canvas-container" style="width: 512px; height: 256px; position: relative; -webkit-user-select: none;"><canvas id="image-designer-canvas" width="512" height="256" class="lower-canvas" style="position: absolute; width: 512px; height: 256px; left: 0px; top: 0px; -webkit-user-select: none;"></canvas><canvas class="upper-canvas " width="512" height="256" style="position: absolute; width: 512px; height: 256px; left: 0px; top: 0px; -webkit-user-select: none; cursor: default;"></canvas></div>
									                        </div>
									                    </div>
									                    <div class="row">
									                        <div class="col-xs-12 m-tb20">
									                            <label class="cb-radio regRoboto m-r10 checked">
									                                <span class="cb-inner"><i><input name="bg_image_type" value="normal" checked="checked" type="radio"></i></span>
									                                Normal
									                            </label>
									                            <label class="cb-radio regRoboto m-r10">
									                                <span class="cb-inner"><i><input name="bg_image_type" value="blurred" type="radio"></i></span>
									                                Blurred
									                            </label>
									                            <label class="cb-radio regRoboto m-r10">
									                                <span class="cb-inner"><i><input name="bg_image_type" value="grayscale" type="radio"></i></span>
									                                Black &amp; White
									                            </label>
									                            <label class="cb-checkbox regRoboto">
									                                <span class="cb-inner"><i><input name="bg_image_type_contrast" id="image-designer-bg-type-contrast" type="checkbox"></i></span>
									                                Increased Contrast
									                            </label>
									                        </div>
									                    </div>
									                    <div class="clearfix">
									                        <div class="well_photo">
									                            <div class="progressBar" style="display: none;">
									                                <div class="progressLine" data-value="0"></div>
									                            </div>
									                            <button class="btn btn-upload-photo fileSelect">
									                               Upload background image
									                            </button>
									                            <input class="uploadbtn inputFile" id="image-designer-bg" multiple="" type="file">
									                                                            <img class="image-designer-bg-image" src="https://app.smintly.com/public/uploads/image-designer/3.jpg" alt="" data-src="https://app.smintly.com/public/theme/images/image-designer/3.jpg">
									                                                            <img class="image-designer-bg-image" src="https://app.smintly.com/public/uploads/image-designer/2.jpg" alt="" data-src="https://app.smintly.com/public/theme/images/image-designer/2.jpg">
									                                                            <img class="image-designer-bg-image" src="https://app.smintly.com/public/uploads/image-designer/1.jpg" alt="" data-src="https://app.smintly.com/public/theme/images/image-designer/1.jpg">
									                                                            <img class="image-designer-bg-image active" src="https://app.smintly.com/public/uploads/image-designer/5.jpg" alt="" data-src="https://app.smintly.com/public/theme/images/image-designer/5.jpg">
									                                                            <img class="image-designer-bg-image" src="https://app.smintly.com/public/uploads/image-designer/4.jpg" alt="" data-src="https://app.smintly.com/public/theme/images/image-designer/4.jpg">
									                                                    </div>
									                    </div>
									                </div>
									            </div>
									        </div>
									    </div>
									    <div class="col-sm-12 col-md-8 col-lg-6 attachment-block" id="photo-block" style="display: none;">
									        <div class="well well-standart">
									                            <i class="fa fa-image"></i>
									                        <button class="btn-save fileSelect">
									                Upload photo
									            </button>
									            <div class="progressBar" style="display: none;">
									                <div class="progressLine" data-value="0"></div>
									            </div>
									            <input class="form-control uploadbtn inputFile" type="file" multiple="">
									        </div>
									    </div>
									    <div class="col-sm-12 col-md-8 col-lg-6 attachment-block" id="video-block" style="display: none;">
									        <div class="well well-standart">
									            <i class="fa fa-play"></i>
									            <button class="btn-save fileSelect">
									                Upload video
									            </button>
									            <div class="progressBar" style="display: none;">
									                <div class="progressLine" data-value="0"></div>
									            </div>
									            <input class="form-control uploadbtn inputFile" id="videos" type="file" multiple="">
									        </div>
									    </div>
									    <input type="hidden" name="image_name" value="">
									    <input type="hidden" name="image_designer_data" value="">
									</div>
									<div class="row">
				                        <div class="col-sm-6">
				                            <p class="text_color strong-size">Add a link (optional)</p>
				                            <div class="form-group">
				                                <input type="text" name="url" class="form-control" value="">
				                            </div>
				                        </div>
				                    </div>

				                    <div class="row">
									    <div class="col-xs-12 m-t5">
									        <p class="text_color strong-size">Shedule a post</p>
									    </div>
									</div>
									<div class="row custom-form">
									    <div class="col-xs-12">
									        <label class="cb-radio regRoboto m-r10 checked">
									            <span class="cb-inner"><i><input type="radio" id="immediate-type" name="posting_type" value="immediate" checked="checked"></i></span>
									            Immediate
									        </label>
									                        <label class="cb-radio regRoboto">
									            <span class="cb-inner"><i><input type="radio" id="schedule-type" name="posting_type" value="schedule"></i></span>
									            Schedule Date
									        </label>
									    </div>
									</div>                                            
									<div class="row m-t10" id="schedule-settings" style="display: none">
									    <div class="col-sm-3">
									        <div class="form-group date_calendar">
									            <input type="text" class="form-control input_date hasDatepicker" value="07/14/2015" name="schedule_date" id="dp1436888478106">
									        </div>
									    </div>
									    <div class="col-sm-3">
									        <select class="chosen-select" name="hours" data-height="100">
									            <option class="podcast" value="1">1 </option>
									            <option class="podcast" value="2">2 </option>
									            <option class="podcast" value="3">3 </option>
									            <option class="podcast" value="4">4 </option>
									            <option class="podcast" value="5">5 </option>
									            <option class="podcast" value="6">6 </option>
									            <option class="podcast" value="7">7 </option>
									            <option class="podcast" value="8">8 </option>
									            <option class="podcast" value="9">9 </option>
									            <option class="podcast" value="10">10 </option>
									            <option class="podcast" value="11">11 </option>
									            <option class="podcast" value="12">12 </option>
									        </select>
									    </div>
									    <div class="col-sm-3">
									        <select class="chosen-select" name="minutes" data-height="100">
									            <option class="podcast" value="0">0 </option>
									            <option class="podcast" value="5">5 </option>
									            <option class="podcast" value="10">10 </option>
									            <option class="podcast" value="15">15 </option>
									            <option class="podcast" value="20">20 </option>
									            <option class="podcast" value="25">25 </option>
									            <option class="podcast" value="30">30 </option>
									            <option class="podcast" value="35">35 </option>
									            <option class="podcast" value="40">40 </option>
									            <option class="podcast" value="45">45 </option>
									            <option class="podcast" value="50">50 </option>
									            <option class="podcast" value="55">55 </option>
									        </select>
									    </div>
									    <div class="col-sm-3">
									        <select class="chosen-select" name="am_pm">
									            <option class="podcast" value="am">AM</option>
									            <option class="podcast" value="pm">PM</option>
									        </select>
									    </div>
									</div>
				                </form>
				            </div>
				        </div>
				    </div>
				</div>
				<div class="row">
				    <div class="col-xs-12">
				        <div class="pull-right m-t20 m-b40">
				            <button id="post-button" type="button" class="btn btn-save">Post update</button>
				        </div>
				    </div>
				</div>-->
				<div class="p-20">
					<div class="row" id="create-social">
    					<div class="col-xs-12">
			                <form action="https://app.smintly.com/social/create/post_create" method="POST" id="post-update-form" autocomplete="off">
			                    <p class="text_color strong-size">Post an update
									<span class="custom-form is-relative top-5 p-l10">
           	 							<label class="cb-checkbox regRoboto m-r10">
							                <input
							                    type="checkbox"
							                    name="post_to_socials[]"
							                    value="twitter"
							                    checked="checked">
							                <cite class="ti-twitter"></cite>
							                Twitter
							            </label>
            							<label class="cb-checkbox regRoboto m-r10">
							                <input
							                    type="checkbox"
							                    name="post_to_socials[]"
							                    value="facebook"
							                    checked="checked">
							                <cite class="ti-facebook"></cite>
							                Facebook
							            </label>
            							<label class="cb-checkbox regRoboto m-r10">
							                <input
							                    type="checkbox"
							                    name="post_to_socials[]"
							                    value="linkedin"
							                    checked="checked">
							                <cite class="ti-linkedin"></cite>
        									Linkedin
        								</label>
            						</span>
            					</p>
            				</form>
						</div>
					</div>
					<div class="row">
					    <div class="col-xs-12 m-t10">
					        <button class="btn" id="bulk_upload" data-show-modal="">Bulk Upload</button>
					        <input type="file" id="bulk_upload_file" style="display: none;"/>
					        <a class="link" href="https://app.smintly.com//public/theme/files/CSV%20Example.csv">Download example file</a>
					    </div>
					</div>
					<div class="modal fade" id="bulk_upload_modal">
					    <div class="modal-dialog">
					        <div class="modal-content">
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					            </div>
					            <div class="modal-body">
					                <h4 class="head_tab">Things to remember before uploading .csv files</h4>
					                <p>- Please consult the sample of .csv document and check the correct order to arrange the content and set the time</p>
									<p>- Make sure to indicate time in the following order (from left to right): year, month, day, hour, minute</p>
									<p>- Scheduled posts should start from tomorrow (not today)</p>
									<p>- Make sure to indicate url address if schedule post for Linkedin</p>
									<div class="custom-form">
					                    <label class="cb-checkbox regRoboto m-r10">
					                        <input type="checkbox" id="show_bulk_upload_notification">
					                        Don`t show this again.
					                    </label>
					                </div>
					            </div>
					            <div class="modal-footer clearfix">
					                <div class="text-center">
					                    <a class="link m-r10" data-dismiss="modal" aria-hidden="true" href="">Cancel</a>
					                    <a class="btn btn-save" id="bulk_upload_modal_button">Bulk Upload</a>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>          
					<div class="row">
	                    <div class="col-xs-12 m-t10 col-md-10">
                            <p class="text_color strong-size m-t10">Type a message</p>
                            <div class="form-group">
                                <textarea name="description" rows="5" class="text-desciption form-control"></textarea>
                                <span class="help-block char-counter pull-right is-block p-t10">(<b>140</b> | 140)</span>
                            </div>
                        </div>
	                    </div>
		                <style>
						    .preview .img-close {
						        position: absolute;
						        right: 27px;
						        top: 13px;
						        cursor: pointer;
						    }

						    .preview .img-preview {
						        width: 100%;
						        margin-bottom: 10px;
						    }
						    .preview .img-close.logo-close {
						        position: absolute;
						        right: 8px;
						        top: -5px;
						        cursor: pointer;
						    }

						    .preview .img-preview.logo-preview {
						        width: 100%;
						        margin-bottom: 10px;
						    }
						</style>
					<!--<div class="row">
					    <div class="col-xs-12 m-t5 custom-form">
					        <label class="cb-checkbox regRoboto m-r10">
					            <input type="checkbox" id="need_attach">
					            Attach image or video (optional)
					        </label>
					    </div>
					</div>-->
					<div class="row">
	                    <div class="col-sm-10">
	                        <p class="text_color strong-size">Specify a link <small>(required for Linkedin only)</small></p>
	                        <div class="form-group">
	                            <input type="text" name="url" class="form-control" value=""/>
	                        </div>
	                    </div>
	                </div>
					<div class="row" id="attachment">
					    <div class="col-xs-12 custom-form">
					    	<p class="text_color strong-size">Add photo or video</p>
					        <label class="cb-radio regRoboto m-r10" data-show="#image-designer-block" data-hide="#photo-block, #video-block">
					            <input name="attachment_type" value="image-designer" name="attachment-block" type="radio">
					            Image designer
					        </label>
					        <label class="cb-radio regRoboto m-r10" data-show="#photo-block" data-hide="#image-designer-block, #video-block">
					            <input name="attachment_type" value="photo" name="attachment-block" type="radio">
					            Photo
					        </label>
					        <label class="cb-radio regRoboto" data-show="#video-block" data-hide="#image-designer-block, #photo-block">
					            <input name="attachment_type" value="video" name="attachment-block" type="radio">
					            Video
					        </label>
					    </div>
					    <div class="col-xs-12 attachment-block is-hidden designer-block" id="image-designer-block">
					        <div class="well well-standart well-upload-image">
					            <div class="row">
					                <div class="col-md-6">
					                    <div class="row">
					                        <div class="col-xs-12">
					                            <p class="head_tab">Head text</p>
					                            <div class="row">
					                                <div class="col-md-9">
					                                    <div class="form-group ">
					                                        <select id="headline_font_select" class="chosen-select">
					                                            <option selected="selected" value="Fredoka One">Fredoka One</option>
					                                            <option value="Hammersmith One">Hammersmith One</option>
					                                            <option value="Josefin Slab">Josefin Slab</option>
					                                            <option value="Lato">Lato</option>
					                                            <option value="Merriweather">Merriweather</option>
					                                            <option value="Montserrat">Montserrat</option>
					                                            <option value="Open Sans">Open Sans</option>
					                                            <option value="Roboto">Roboto</option>
					                                            <option value="Satisfy">Satisfy</option>
					                                            <option value="Ubuntu">Ubuntu</option>
					                                        </select>
					                                    </div>
					                                </div>
					                                <div class="col-md-3">
					                                    <div class="form-group pull-md-right">
					                                        <div class="input-group pick-a-color-markup" id="headline_color">
					                                            <input value="ffffff" name="headline_color" class="pick-a-color form-control" type="hidden">
					                                        </div>
					                                    </div>
					                                </div>
					                            </div>
					                            <div class="form-group">
					                                <textarea name="headline_text" id="image-designer-headline-text" rows="10" class="form-control"></textarea>
					                            </div>
					                        </div>
					                    </div>
					                    <div class="row">
					                        <div class="col-xs-12" id="secondary-text-block" style="display: none;">
					                            <p class="head_tab">Secondary text</p>
					                            <div class="row">
					                                <div class="col-md-9">
					                                    <div class="form-group ">
					                                        <select id="secondary_font_select" class="chosen-select">
					                                            <option selected="selected" value="Fredoka One">Fredoka One</option>
					                                            <option value="Hammersmith One">Hammersmith One</option>
					                                            <option value="Josefin Slab">Josefin Slab</option>
					                                            <option value="Lato">Lato</option>
					                                            <option value="Merriweather">Merriweather</option>
					                                            <option value="Montserrat">Montserrat</option>
					                                            <option value="Open Sans">Open Sans</option>
					                                            <option value="Roboto">Roboto</option>
					                                            <option value="Satisfy">Satisfy</option>
					                                            <option value="Ubuntu">Ubuntu</option>
					                                        </select>
					                                    </div>
					                                </div>
					                                <div class="col-md-3">
					                                    <div class="form-group pull-md-right">
					                                        <div class="input-group pick-a-color-markup" id="headline_color">
					                                            <input value="ffffff" name="secondary_color" class="pick-a-color form-control" type="hidden">
					                                        </div>
					                                    </div>
					                                </div>
					                            </div>
					                            <div class="form-group">
					                                <textarea name="secondary_text" id="image-designer-secondary-text" rows="10" class="form-control"></textarea>
					                            </div>
					                        </div>
					                    </div>
					                    <div id="image-designer-logo" style="display: none;">
					                        <div class="row">
					                            <div class="col-sm-5 preview" id="image-designer-logo">

					                            </div>
					                        </div>
					                    </div>
					                    <div class="row well-standart">
					                        <div class="col-xs-12 text-left">
					                            <div class="progressBar" style="display: none;">
					                                <div class="progressLine" data-value="0"></div>
					                            </div>
					                            <button class="btn btn-add m-b20" id="image-designer-add-secondary-text" data-added="false">
					                                + Add secondary text                            </button>
					                            <button class="btn btn-save fileSelect m-b20">
					                                + Add logo                            </button>
					                            <input class="uploadbtn inputFile" id="image-designer" multiple="" type="file">
					                        </div>
					                    </div>
					                </div>
					                <div class="col-md-6 well-standart">
					                    <div class="canvas-block">
					                        <div class="canvas-container">
					                            <canvas id="image-designer-canvas" width="512px" height="256px"></canvas>
					                        </div>
					                    </div>
					                    <div class="row">
					                        <div class="col-xs-12 custom-form m-tb20">
					                            <label class="cb-radio regRoboto m-r10 checked">
					                                <input name="bg_image_type" value="normal" checked="checked" type="radio">
					                                Normal                            </label>
					                            <label class="cb-radio regRoboto m-r10">
					                                <input name="bg_image_type" value="blurred" type="radio">
					                                Blurred                            </label>
					                            <label class="cb-radio regRoboto m-r10">
					                                <input name="bg_image_type" value="grayscale" type="radio">
					                                Black &amp; White                            </label>
					                            <label class="cb-checkbox regRoboto">
					                                <input name="bg_image_type_contrast" id="image-designer-bg-type-contrast" type="checkbox">
					                                Increased Contrast                            </label>
					                        </div>
					                    </div>
					                    <div class="clearfix">
					                        <div class="well_photo">
					                            <div class="progressBar" style="display: none;">
					                                <div class="progressLine" data-value="0"></div>
					                            </div>
					                            <button class="btn btn-upload-photo fileSelect">
					                                Upload background image <br/><small>png, jpeg</small>                            </button>
					                            <input class="uploadbtn inputFile" id="image-designer-bg" multiple="" type="file">
					                                                            <img
					                                    class="image-designer-bg-image"
					                                    src="https://app.smintly.com/public/uploads/image-designer/3.jpg"
					                                    alt=""
					                                    data-src="https://app.smintly.com/public/theme/images/image-designer/3.jpg"
					                                    />
					                                                            <img
					                                    class="image-designer-bg-image"
					                                    src="https://app.smintly.com/public/uploads/image-designer/2.jpg"
					                                    alt=""
					                                    data-src="https://app.smintly.com/public/theme/images/image-designer/2.jpg"
					                                    />
					                                                            <img
					                                    class="image-designer-bg-image"
					                                    src="https://app.smintly.com/public/uploads/image-designer/1.jpg"
					                                    alt=""
					                                    data-src="https://app.smintly.com/public/theme/images/image-designer/1.jpg"
					                                    />
					                                                            <img
					                                    class="image-designer-bg-image"
					                                    src="https://app.smintly.com/public/uploads/image-designer/5.jpg"
					                                    alt=""
					                                    data-src="https://app.smintly.com/public/theme/images/image-designer/5.jpg"
					                                    />
					                                                            <img
					                                    class="image-designer-bg-image"
					                                    src="https://app.smintly.com/public/uploads/image-designer/4.jpg"
					                                    alt=""
					                                    data-src="https://app.smintly.com/public/theme/images/image-designer/4.jpg"
					                                    />
					                                                    </div>
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>
					    <div class="col-sm-12 col-md-8 col-lg-6 attachment-block is-hidden media-block" id="photo-block">
					        <div class="well well-standart">
					            <i class="fa fa-image"></i>
					            <button class="btn-save fileSelect">
					                Upload photo
					            </button>
					            <div class="progressBar" style="display: none;">
					                <div class="progressLine" data-value="0"></div>
					            </div>
					            <input class="form-control uploadbtn inputFile" type="file" multiple="">
					        </div>
					    </div>
					    <div class="col-sm-12 col-md-8 col-lg-6 attachment-block is-hidden media-block" id="video-block">
					        <div class="well well-standart">
					            <i class="fa fa-play"></i>
					            <button class="btn-save fileSelect">
					                Upload video
					            </button>
					            <div class="progressBar" style="display: none;">
					                <div class="progressLine" data-value="0"></div>
					            </div>
					            <input class="form-control uploadbtn inputFile" id="videos" type="file" multiple="">
					        </div>
					    </div>
					    <input type="hidden" name="image_name" value="">
					    <input type="hidden" name="image_designer_data" value="">
					</div>				

	                <div class="row">
					    <div class="col-xs-12 m-t5">
					        <p class="text_color strong-size">Post now or later?</p>
					    </div>
					</div>
					<div class="row custom-form">
					    <div class="col-xs-12">
					        <label class="cb-checkbox regRoboto" data-toggle="#schedule-settings">
					            <input type="checkbox" id="schedule-type" name="posting_type" value="schedule" >
					            Schedule post
					        </label>
					    </div>
					</div>                                            
					<div class="row m-t10 is-hidden" id="schedule-settings">
	            		<div class="col-sm-12">
	           				<div class="form-group date_calendar">
	                			<input type="text" class="form-control time_date" value="09/17/2015 11:00 AM" name="schedule_date">
	            			</div>
	        			</div>
	    			</div>
	                <div class="row">
					    <div class="col-xs-12 m-t5 custom-form">
					        <label class="cb-checkbox regRoboto m-r10" data-toggle="#cron">
					            <input type="checkbox" id="need_cron" name="is_cron">
					            Make it a recurring post
					        </label>
					    </div>
					</div>
					<div class="row is-hidden" id="cron">
					    <div class="col-xs-12">
					        <div class="row">
					            <div class="col-xs-12">
					                <div class="btn-group" data-toggle="buttons">
					                    <label class="btn btn-primary">
					                        <input name="cron_day[]"
					                            value="Monday"
					                            type="checkbox"
					                            autocomplete="off"> Monday
					                    </label>
					                    <label class="btn btn-primary">
					                        <input name="cron_day[]"
					                            value="Tuesday"
					                            type="checkbox"
					                            autocomplete="off"> Tuesday
					                    </label>
					                    <label class="btn btn-primary">
					                        <input name="cron_day[]"
					                            value="Wednesday"
					                            type="checkbox"
					                            autocomplete="off"> Wednesday
					                    </label>
					                    <label class="btn btn-primary">
					                        <input name="cron_day[]"
					                            value="Thursday"
					                            type="checkbox"
					                            autocomplete="off"> Thursday
					                    </label>
					                    <label class="btn btn-primary">
					                        <input name="cron_day[]"
					                            value="Friday"
					                            type="checkbox"
					                            autocomplete="off"> Friday
					                    </label>
					                    <label class="btn btn-primary">
					                        <input name="cron_day[]"
					                            value="Saturday"
					                            type="checkbox"
					                            autocomplete="off"> Saturday
					                    </label>
					                    <label class="btn btn-primary">
					                        <input name="cron_day[]"
					                            value="Sunday"
					                            type="checkbox"
					                            autocomplete="off"> Sunday
					                    </label>
					                </div>
					            </div>
					        </div>
					        <div class="row">
					            <div class="col-xs-12">
					                <p id="cron_text"></p>
					                <div id="cron-scheduled-block">
					                </div>
					                <div class="row">
					                    <div class="col-xs-12">
					                        <div class="pull-sm-left">
					                            <a class="btn btn-add m-tb20 m-r20" id="cron-time-add-btn">+ Add time</a>
					                        </div>
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
					<script id="cron-time-template" type="text/x-handlebars-template">
					    <div class="cron-time">
					        <div class="form-group date_calendar">
					            <input type="text" class="form-control time m-r20" value="11:00 AM" name="cron_schedule_time[]">
					            <i class="cb-remove cron-time-delete" style="right: 40px;"></i>
					        </div>
					    </div>
					</script>
					<div class="row">
					    <div class="col-xs-12">
					        <div class="pull-right m-t20 m-b20">
					            <button id="post-button" type="button" class="btn btn-save">Post</button>
					        </div>
					    </div>
					</div>
				</div>
			</div>
        </div>

		<div class="col-md-4">
			<div class="block_content">
				<h2 class="block_content_title calendar_title">Calendar</h2>
				<div class="block_content_body calendar_body">
					<div class="calendar_note clearfix">
						<p class="large-size laTo m-b20">Hold Tight - Madonna</p>
						<p class="regRoboto"><span class="bold">Post on:</span> 02-10-2015 03:50pm</p>
					</div>
					<div class="calendar_note clearfix">
						<p class="large-size laTo m-b20">Hold Tight - Madonna</p>
						<p class="regRoboto"><span class="bold">Post on:</span> 02-10-2015 03:50pm</p>
					</div>
					<div class="calendar_note clearfix">
						<p class="large-size laTo m-b20">Hold Tight - Madonna</p>
						<p class="regRoboto"><span class="bold">Post on:</span> 02-10-2015 03:50pm</p>
					</div>
					<div class="calendar_note clearfix">
						<p class="large-size laTo m-b20">Hold Tight - Madonna</p>
						<p class="regRoboto"><span class="bold">Post on:</span> 02-10-2015 03:50pm</p>
					</div>
					<div class="block_content_footer">
						<a href="" class="link">View all</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="welcome" class="modal welcome-modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>				
			</div>
			<div class="modal-body">
				<h4 class="modal-body__title text-center">Thank you for Registration!</h4>
				<p class="modal-body__text text-center">
					Connect your social media profiles in order<br/>
					to start using the service. 
				</p>
				<div class="text-center">
					<button class="btn btn-modal btn-modal-facebook"><i class="fa fa-facebook"></i> Connect Facebook</button>
					<button class="btn btn-modal btn-modal-twitter"><i class="fa fa-twitter"></i> Connect Twitter</button>
					<button class="btn btn-modal btn-modal-linkedin"><i class="fa fa-linkedin"></i> Connect Linkedin</button>
				</div>
				<p class="black text-center m-tb20">
					or <a href="" class="link">Go to Settings</a>
				</p>
			</div>
		</div>
	</div>
</div>