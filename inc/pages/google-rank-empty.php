<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Google Rank</h1>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-sm-2 col-md-1">
			<p href="" class="blue-color text-size p-t10">Period</p>
		</div>
		<div class="col-sm-4">
			<select class="chosen-select">
				<option value="">All time</option>
				<option value="">1 month</option>
				<option value="">3 months</option>
				<option value="">6 months</option>
				<option value="">1 year</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<table class="responsive-table rank">
				<thead class="table_head">
					<tr>
						<th>Keyword Phrase</th>
						<th>Current Rank</th>
						<th>Rank Change</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="3" class="empty">No mentioned</td>						
					</tr>					
				</tbody>
			</table>
		</div>
	</div>
</div>