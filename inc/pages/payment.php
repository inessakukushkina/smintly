    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="main_block m-t30">
                    <h1 class="head_title">Confirmation</h1>
                    <table class="table crm_profile m-t20 custom-form">
                        <tbody>
                            <tr>
                                <td>
                                    <p class="gray-color bold">First Name</p>
                                </td>
                                <td>
                                    <p>test test</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="gray-color bold">Email:</p>
                                </td>
                                <td>
                                    <p>g.s.xbeer@mail.ru</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="gray-color bold">Subscription Plan:</p>
                                </td>
                                <td>
                                    <p>Enterprise</p>
                                    <label class="cb-radio w-100">
                                        <input type="radio" name=plan_period value="5" checked>
                                        1 Month(s) / $39.00
                                    </label>
                                    <label class="cb-radio w-100">
                                        <input type="radio" name=plan_period value="10" checked>
                                        6 Month(s) / $90.00
                                    </label>
                                    <label class="cb-radio w-100">
                                        <input type="radio" name=plan_period value="11" checked>
                                        12 Month(s) / $160.00
                                    </label>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select name="system" class="chosen-select m-t10">
                                                <option value="3">Stripe</option>
                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <button class="btn btn-save">Pay</button>
                        </div>
                    </div>
                </div> 
            </div> 
        </div> 
    </div> 
</div> 