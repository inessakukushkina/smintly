<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="tarif_plan">
				<h4 class="plan-name">Basic</h4>
				<div class="plan-price">
					<ul class="plan-price_list">
						<li class="plan-price_list_item">
							<h4 class="plan-price_title">
								$4  <span class="price-cents">99</span><span class="price-month"> / month</span>
							</h4>
						</li>
					</ul>
				</div>
				<ul class="tarif_plan_list">
					<li class="tarif_plan_list_item"><span><i class="fa fa-minus attention"></i> Automatic sentiment</span> analysis function</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-minus attention"></i> CRM</span></li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Social media</span> management</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Brand reputation</span> monitoring</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-minus attention"></i> Brand influencers</span> watch</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Social</span> activity</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i>Website traffic</span> monitoring</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-minus attention"></i>Reviews</span> monitoring</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-minus attention"></i>Local search keyword</span> tracking</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-minus attention"></i>24/7</span> Tech support</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-minus attention"></i>Email</span> notifications</li>					
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i>Free</span> updates</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-minus attention"></i>Training</span> and assistance</li>
				</ul>				
				<div class="row">
					<div class="col-xs-12 text-center p-tb20">
						<button class="btn btn-save">Subscribe</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="tarif_plan featured">
				<h4 class="plan-name">Standard</h4>
				<div class="plan-price">
					<ul class="plan-price_list">
						<li class="plan-price_list_item">
							<h4 class="plan-price_title">
								$9 <span class="price-cents">99</span><span class="price-month"> / month</span>
							</h4>
						</li>
					</ul>
				</div>
				<ul class="tarif_plan_list">
					<li class="tarif_plan_list_item"><span><i class="fa fa-minus attention"></i> Automatic sentiment</span> analysis function</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-minus attention"></i> CRM</span></li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Social media</span> management</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Brand reputation</span> monitoring</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Brand influencers</span> watch</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Social</span> activity</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Website traffic</span> monitoring</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Reviews</span> monitoring</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Local search keyword</span> tracking</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-minus attention"></i> 24/7</span> Tech support</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Email</span> notifications</li>					
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Free</span> updates</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Training</span> and assistance</li>
				</ul>					
				<div class="row">
					<div class="col-xs-12 text-center p-tb20">
						<button class="btn btn-save">Subscribe</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="tarif_plan">
				<h4 class="plan-name">Enterprise</h4>
				<div class="plan-price">
					<ul class="plan-price_list">
						<li class="plan-price_list_item">
							<h4 class="plan-price_title">
								$39 <span class="price-cents">99</span><span class="price-month"> / month</span>
							</h4>
						</li>
					</ul>
				</div>
				<ul class="tarif_plan_list">
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Automatic sentiment</span> analysis function</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> CRM</span></li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Social media</span> management</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Brand reputation</span> monitoring</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Brand influencers</span> watch</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Social</span> activity</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Website traffic</span> monitoring</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Reviews</span> monitoring</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Local search keyword</span> tracking</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> 24/7</span> Tech support</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Email</span> notifications</li>					
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Free</span> updates</li>
					<li class="tarif_plan_list_item"><span><i class="fa fa-plus green-color"></i> Training</span> and assistance</li>
				</ul>				
				<div class="row">
					<div class="col-xs-12 text-center p-tb20">
						<button class="btn btn-save">Subscribe</button>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>