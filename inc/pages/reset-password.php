<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="sign_block ">
				<div class="sign_block__head">
					<h2 class="sign_title">Reset password</h2>
				</div>
				<div class="sign_block__body">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="New password">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Confirm password">
							</div>
						</div>				
					</div>
					<div class="row custom-form">
						<div class="col-xs-12">
							<div class="pull-right">
								<button class="btn-save btn-large">Save</button>
							</div>					
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>