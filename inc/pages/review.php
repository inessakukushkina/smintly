<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Review</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Review</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Google places</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-sm-5">
			<p class="strong-size text_color p-t10">All reviews</p>
		</div>
		<div class="col-sm-3">
			<div class="form-group date_calendar">
				<input type="text" class="form-control input_date">
			</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group date_calendar">
				<input type="text" class="form-control input_date">
			</div>
		</div>
		<div class="col-sm-1">
			<div class="pull-sm-right">
				<button class="btn btn-save m-b20">Apply</button>
			</div>
		</div>		
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="pull_border">
				<div class="row">
					<div class="col-sm-3">
						<div class="review review_total">
							<p class="review_text"><span>0</span> Total reviews</p>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="review review_positive">
							<p class="review_text"><span>0</span> Positive</p>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="review review_negative">
							<p class="review_text"><span>0</span> Negative</p>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="review review_neutral">
							<p class="review_text"><span>0</span> Neutral</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row m-t20">
		<div class="col-xs-12">
			<div class="block_content">
				<div class="block_content_body text-center">					
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="row">
								<div class="col-xs-12">
									<p class="text_color large-size m-b20">Overall Sentiment: <span class="bold black strong-size">4 / 5</span></p>
									<div class="rating" data-value="4.5">
										<div class="rating_bar"></div>
									</div>
								</div>
							</div>
							<div class="row rating_block">
								<div class="col-xs-1">
									<p class="text-left rating_text">0</p>
								</div>
								<div class="col-xs-3">
									<p class="text-center rating_text">1</p>
								</div>
								<div class="col-xs-2">
									<p class="text-center rating_text">2</p>
								</div>
								<div class="col-xs-2">
									<p class="text-center rating_text">3</p>
								</div>
								<div class="col-xs-3">
									<p class="text-center rating_text">4</p>
								</div>
								<div class="col-xs-1">
									<p class="text-right rating_text">5</p>
								</div>
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
	
	<div class="row m-t20">
		<div class="col-xs-12">			
			<div class="block_content">				
				<div class="block_content_body">
					<div class="recent_review">
						<div class="clearfix">
							<p class="recent_review_title pull-sm-left">Simon Shoubridge</p>
							<p class="recent_review_date pull-sm-right">2015-02-25</p>
						</div>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star"></i>
						<i class="icon-star"></i>
						<p class="recent_review_text">
							Went in today for cleaning the hygienist was excellent and thorough. 
							Super nice office staff. 
							Dr. Hogg has fixed me up properly when I lost a filling, the work was perfect! 
							I highly recommend them for you and your families dental needs!
						</p>
					</div>
				</div>
			</div>
			<div class="block_content m-t20">	
				<div class="block_content_body">
					<div class="recent_review">
						<div class="clearfix">
							<p class="recent_review_title pull-sm-left">Simon Shoubridge</p>
							<p class="recent_review_date pull-sm-right">2015-02-25</p>
						</div>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star"></i>
						<i class="icon-star"></i>
						<p class="recent_review_text">
							Went in today for cleaning the hygienist was excellent and thorough. 
							Super nice office staff. 
							Dr. Hogg has fixed me up properly when I lost a filling, the work was perfect! 
							I highly recommend them for you and your families dental needs!
						</p>
					</div>
				</div>
			</div>
			<div class="block_content m-t20">	
				<div class="block_content_body">
					<div class="recent_review">
						<div class="clearfix">
							<p class="recent_review_title pull-sm-left">Simon Shoubridge</p>
							<p class="recent_review_date pull-sm-right">2015-02-25</p>
						</div>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star"></i>
						<i class="icon-star"></i>
						<p class="recent_review_text">
							Went in today for cleaning the hygienist was excellent and thorough. 
							Super nice office staff. 
							Dr. Hogg has fixed me up properly when I lost a filling, the work was perfect! 
							I highly recommend them for you and your families dental needs!
						</p>
					</div>
				</div>
			</div>
			<div class="block_content m-t20">	
				<div class="block_content_body">
					<div class="recent_review">
						<div class="clearfix">
							<p class="recent_review_title pull-sm-left">Simon Shoubridge</p>
							<p class="recent_review_date pull-sm-right">2015-02-25</p>
						</div>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star"></i>
						<i class="icon-star"></i>
						<p class="recent_review_text">
							Went in today for cleaning the hygienist was excellent and thorough. 
							Super nice office staff. 
							Dr. Hogg has fixed me up properly when I lost a filling, the work was perfect! 
							I highly recommend them for you and your families dental needs!
						</p>
					</div>
				</div>
			</div>
			<div class="block_content m-t20">	
				<div class="block_content_body">
					<div class="recent_review">
						<div class="clearfix">
							<p class="recent_review_title pull-sm-left">Simon Shoubridge</p>
							<p class="recent_review_date pull-sm-right">2015-02-25</p>
						</div>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star"></i>
						<i class="icon-star"></i>
						<p class="recent_review_text">
							Went in today for cleaning the hygienist was excellent and thorough. 
							Super nice office staff. 
							Dr. Hogg has fixed me up properly when I lost a filling, the work was perfect! 
							I highly recommend them for you and your families dental needs!
						</p>
					</div>
				</div>
			</div>
			<div class="block_content m-t20">	
				<div class="block_content_body">
					<div class="recent_review">
						<div class="clearfix">
							<p class="recent_review_title pull-sm-left">Simon Shoubridge</p>
							<p class="recent_review_date pull-sm-right">2015-02-25</p>
						</div>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star"></i>
						<i class="icon-star"></i>
						<p class="recent_review_text">
							Went in today for cleaning the hygienist was excellent and thorough. 
							Super nice office staff. 
							Dr. Hogg has fixed me up properly when I lost a filling, the work was perfect! 
							I highly recommend them for you and your families dental needs!
						</p>
					</div>
				</div>
			</div>
			<div class="block_content m-t20">	
				<div class="block_content_body">
					<div class="recent_review">
						<div class="clearfix">
							<p class="recent_review_title pull-sm-left">Simon Shoubridge</p>
							<p class="recent_review_date pull-sm-right">2015-02-25</p>
						</div>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star active-rating"></i>
						<i class="icon-star"></i>
						<i class="icon-star"></i>
						<p class="recent_review_text">
							Went in today for cleaning the hygienist was excellent and thorough. 
							Super nice office staff. 
							Dr. Hogg has fixed me up properly when I lost a filling, the work was perfect! 
							I highly recommend them for you and your families dental needs!
						</p>
					</div>
				</div>
			</div>
			<div class="col-xs-12 text-center">
				<img src="images/loading/loading.gif" class="m-tb20" alt="">
			</div>
		</div>
	</div>
</div>