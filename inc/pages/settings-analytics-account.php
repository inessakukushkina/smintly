<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Analytics Details</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Analytics Details</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color strong-size">Google Analytics </p>
				</div>
				<div class="col-sm-6">
					<div class="pull-sm-right">
						<button class="btn btn-add">Logout</button>
					</div>
				</div>						
			</div>
			<table class="table account_analytics m-t20">
				<tbody>
					<tr>
						<td>
							<p class="gray-color bold">Account</p>
						</td>
						<td>
							<p>repucaution</p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="gray-color bold">Webproperty</p>
						</td>
						<td>
							<p><a href="" class="link">repucaution.com</a></p>
						</td>
					</tr>
					<tr>
						<td>
							<p class="gray-color bold">Profile</p>
						</td>
						<td>
							<p>All data on the website</p>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="row">
				<div class="col-xs-12">
					<div class="b-Top clearfix m-b10 p-t20">
						<div class="pull-sm-right">
							<button class="btn btn-save">Change</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>