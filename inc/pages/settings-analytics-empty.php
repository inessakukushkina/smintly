<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Analytics Details</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Analytics Details</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="row">
				<div class="col-xs-12">
					<p class="text_color strong-size">Google Analytics</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="validate pink">
						<div class="validateRow">
							<div class="validateCell">
								<i class="note">!</i>
							</div>
							<div class="validateCell">
								<div class="pull-left">
									<p>
										User does not have any Google Analytics account.
									</p>								
								</div>
							</div>
						</div>
					</div>
				</div>											
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="b-Top clearfix m-b10 m-t20 p-t20">
						<div class="pull-sm-right">
							<button class="btn btn-save">Back</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>