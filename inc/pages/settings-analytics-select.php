<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Analytics Details</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Analytics Details</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color strong-size">Google Analytics </p>
					<p class="black smallText">Select your Google Analytics profile to get statistics</p>
				</div>
				<div class="col-sm-6">
					<div class="pull-sm-right">
						<button class="btn btn-add">Logout</button>
					</div>
				</div>						
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="b-Top clearfix m-b10 m-t20 p-t20">
						<div class="pull-sm-right">
							<button class="btn btn-save">Select</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>