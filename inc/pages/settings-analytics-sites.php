<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Analytics Details</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Analytics Details</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="row">
				<div class="col-xs-12">
					<p class="text_color strong-size">Google Analytics </p>
					<p class="black smallText">Select your Google Analytics profile to get statistics</p>
				</div>						
			</div>
			<div class="row custom-form">
				<div class="col-xs-12">
					<i class="icon-folder"></i>
					<ul class="tree-folder">
						<li class="tree-folder_item">
							<p>
								<span class="tree-folder_item_name">
									repucaution
								</span>
							</p>
							<ul class="tree-folder_item_list">
								<li class="tree-folder_item_list_point">
									<p>
										<span class="tree-folder_item_name">
											repucaution.com
										</span>
									</p>
									<ul class="tree-folder_item_list">
										<li class="tree-folder_item_list_point">
											<p>
												<label class="cb-radio">
													<input type="radio">
													All data on the website
												</label>
											</p>											
										</li>
									</ul>
								</li>
							</ul>
						</li>
						<li class="tree-folder_item">
							<p>
								<span class="tree-folder_item_name">
									smintly
								</span>
							</p>
							<ul class="tree-folder_item_list">
								<li class="tree-folder_item_list_point">
									<p>
										<span class="tree-folder_item_name">
											smintly.com
										</span>
									</p>
									<ul class="tree-folder_item_list">
										<li class="tree-folder_item_list_point">
											<p>
												<label class="cb-radio">
													<input type="radio">
													All data on the website
												</label>
											</p>											
										</li>
									</ul>
								</li>
							</ul>
						</li>
						<li class="tree-folder_item">
							<p>
								<span class="tree-folder_item_name">
									raccoon
								</span>
							</p>
							<ul class="tree-folder_item_list">
								<li class="tree-folder_item_list_point">
									<p>
										<span class="tree-folder_item_name">
											raccoon.com
										</span>
									</p>
									<ul class="tree-folder_item_list">
										<li class="tree-folder_item_list_point">
											<p>
												<label class="cb-radio">
													<input type="radio">
													All data on the website
												</label>
											</p>											
										</li>
									</ul>
								</li>
							</ul>
						</li>
						<li class="tree-folder_item">
							<p>
								<span class="tree-folder_item_name">
									fox
								</span>
							</p>
							<ul class="tree-folder_item_list">
								<li class="tree-folder_item_list_point">
									<p>
										<span class="tree-folder_item_name">
											fox.com
										</span>
									</p>
									<ul class="tree-folder_item_list">
										<li class="tree-folder_item_list_point">
											<p>
												<label class="cb-radio">
													<input type="radio">
													All data on the website
												</label>
											</p>
											<ul class="tree-folder_item_list">
												<li class="tree-folder_item_list_point">
													<p>
														<label class="cb-radio">
															<input type="radio">
															Settings website
														</label>
													</p>											
												</li>
											</ul>										
										</li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="b-Top clearfix m-b10 m-t40 p-t20">
						<div class="pull-sm-right">
							<button class="btn btn-save">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>