<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Directory Settings</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					Directory Settings
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-md-10 col-lg-8 custom-form m-b10">
			<label class="cb-checkbox">
				<input type="checkbox"/>
				I want to receive new reviews by email
			</label>
		</div>
	</div>					
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color strong-size">Google Places (type business name)</p>
					<div class="form-group google_places autocomplete_block">
						<input class="form-control" name='directory'/>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Yelp</p>
					<div class="form-group">
						<input class="form-control m-b10"/>
						<a href="" class="link">Find url</a>	
					</div>												
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Merchant Circle</p>
					<div class="form-group">
						<input class="form-control m-b10"/>
						<a href="" class="link">Find url</a>	
					</div>												
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Citysearch</p>
					<div class="form-group">
						<input class="form-control m-b10"/>
						<a href="" class="link">Find url</a>	
					</div>												
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Yahoo Local</p>
					<div class="form-group">
						<input class="form-control m-b10"/>
						<a href="" class="link">Find url</a>	
					</div>												
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Insider Pages</p>
					<div class="form-group">
						<input class="form-control m-b10"/>
						<a href="" class="link">Find url</a>	
					</div>												
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Foursquare</p>
					<div class="form-group">
						<input class="form-control m-b10"/>
						<a href="" class="link">Find url</a>	
					</div>												
				</div>							
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="b-Top p-tb20 m-t20">
				<button class="btn btn-save pull-right">Save</button>
			</div>			
		</div>
	</div>
</div>