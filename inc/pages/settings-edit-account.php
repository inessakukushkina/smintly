<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Social Media</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Social Media</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Edit account</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-xs-12">
			<h4 class="edit-profile-title"><i class="fa fa-facebook"></i> Alex Jorjik</p>
		</div>
	</div>
	<div class="row custom-form edit_profile">
		<div class="col-sm-6">			
			<div class="choose_account account_facebook">
				<h3 class="choose_account_title">
					<i class="fa fa-facebook"></i> Facebook page
				</h3>
				<ul class="choose_account_list">
					<li class="choose_account_list_item">
						<label class="cb-radio w-100">
							<input type="radio" name="facebook-group">
							<div class="dTable">
								<div class="dRow">
									<div class="dCell">
										<h3 class="choose_account_list_name">Chamomile Chamomile</h3>
									</div>
								</div>										
							</div>												
						</label>									
					</li>
					<li class="choose_account_list_item">
						<label class="cb-radio w-100">
							<input type="radio" name="facebook-group">
							<div class="dTable">
								<div class="dRow">
									<div class="dCell">
										<h3 class="choose_account_list_name">Raccoon Raccoon</h3>
									</div>
								</div>										
							</div>										
						</label>
					</li>
				</ul>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="choose_account account_facebook">
				<h3 class="choose_account_title"><i class="fa fa-facebook"></i> Facebook settings</h3>                                        
                <p class="text_color strong-size m-t20">
                	Max daily count of automatically followed users by search
                </p>
                <div class="form-group">
                    <input type="text" class="form-control">
                </div>                                           
                <label class="cb-checkbox w-100">
                    <input type="checkbox">
                    Automatically follow users by search
                </label>
                <label class="cb-checkbox w-100">
                    <input type="checkbox">                  
                  	Automatically unfollow those who unsubscribed from your account                           
                </label>
                <label class="cb-checkbox w-100">
                    <input type="checkbox">
                    Auto-send welcome message to new followers
                </label>
                <p class="text_color strong-size m-t20">
                	Welcome message text
                </p>
                <div class="form-group">
                    <textarea rows="5" class="form-control"></textarea>
                </div>
                <label class="cb-checkbox w-100">
                    <input type="checkbox">                   
                   	Auto-follow new followers
                </label>
                <label class="cb-checkbox w-100">
                   <input type="checkbox">                   
                  	Auto-retweet
                </label>
                <label class="cb-checkbox w-100">
                    <input type="checkbox">
                    Auto-favourite</h3>
                </label>                                                 
			</div>
		</div>
	</div>
	<div class="row">
        <div class="col-xs-12">
            <div class="b-Top m-t20 p-t20">
                <div class="pull-sm-left p-tb10">
                    <a href="https://app.smintly.com/settings/socialmedia/" class="link ">
                        <i class="fa fa-long-arrow-left blue-color m-r5"></i>
                        Back to Social Media Settings
                    </a>
                </div>
                <div class="pull-sm-right">
                    <button type="submit" class="btn-save m-b20">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>