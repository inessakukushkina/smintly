<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Google Places Keywords</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Google Places Keywords</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-xs-12">
			<p class="black strong-size">Set Company Address for search engine tracking. Start typing your Company Name and autocomplete will show available results.</p>
		</div>
	</div>			
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<p class="text_color strong-size">Company name + address (Google Places)</p>
			<div class="row">
				<div class="col-sm-6">							
					<div class="form-group">
						<input class="form-control m-b10" value="advmapdm"/>
						<i class="fa fa-times clear"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<p class="black strong-size">Choose 10 keyword phrases for search engine tracking</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color strong-size">Keyword phrase 1</p>
					<div class="form-group">
						<input class="form-control m-b10" value="advmapdm"/>
						<i class="fa fa-times clear"></i>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Keyword phrase 2</p>
					<div class="form-group">
						<input class="form-control m-b10" value="advmapdm"/>
						<i class="fa fa-times clear"></i>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Keyword phrase 3</p>
					<div class="form-group">
						<input class="form-control m-b10" value="advmapdm"/>
						<i class="fa fa-times clear"></i>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Keyword phrase 4</p>
					<div class="form-group">
						<input class="form-control m-b10" value="advmapdm"/>
						<i class="fa fa-times clear"></i>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Keyword phrase 5</p>
					<div class="form-group">
						<input class="form-control m-b10" value="advmapdm"/>
						<i class="fa fa-times clear"></i>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Keyword phrase 6</p>
					<div class="form-group">
						<input class="form-control m-b10" value="advmapdm"/>
						<i class="fa fa-times clear"></i>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Keyword phrase 7</p>
					<div class="form-group">
						<input class="form-control m-b10" value="advmapdm"/>
						<i class="fa fa-times clear"></i>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Keyword phrase 8</p>
					<div class="form-group">
						<input class="form-control m-b10" value="advmapdm"/>
						<i class="fa fa-times clear"></i>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Keyword phrase 9</p>
					<div class="form-group">
						<input class="form-control m-b10" value="advmapdm"/>
						<i class="fa fa-times clear"></i>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Keyword phrase 10</p>
					<div class="form-group">
						<input class="form-control m-b10" value="advmapdm"/>
						<i class="fa fa-times clear"></i>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="b-Top p-tb20 m-t20">
				<button class="btn btn-save pull-right">Save</button>
			</div>			
		</div>
	</div>
</div>