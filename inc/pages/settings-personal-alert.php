<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Personal Settings</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Personal Settings</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color strong-size">Email *</p>
					<div class="form-group has-error">
						<input class="form-control"/>
						<p class="error_text">
							<i class="fa fa-exclamation-circle"></i>
							Please enter Email
						</p>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">First Name *</p>
					<div class="form-group has-error">
						<input class="form-control"/>
						<p class="error_text">
							<i class="fa fa-exclamation-circle"></i>
							Please enter First Name
						</p>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Last Name *</p>
					<div class="form-group has-error">
						<input class="form-control"/>
						<p class="error_text">
							<i class="fa fa-exclamation-circle"></i>
							Please enter Last Name
						</p>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Old Password *</p>
					<div class="form-group has-error">
						<input class="form-control"/>
						<p class="error_text">
							<i class="fa fa-exclamation-circle"></i>
							Please enter Old Password
						</p>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">New Password *</p>
					<div class="form-group has-error">
						<input class="form-control"/>
						<p class="error_text">
							<i class="fa fa-exclamation-circle"></i>
							Please enter New Password
						</p>
					</div>
				</div>
				<div class="col-sm-6">
					<p class="text_color strong-size">Confirm New Password *</p>
					<div class="form-group has-error">
						<input class="form-control"/>
						<p class="error_text">
							<i class="fa fa-exclamation-circle"></i>
							Please confirm New Password
						</p>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="b-Top p-tb20 m-t20">
				<button class="btn btn-save pull-right">Save</button>
			</div>			
		</div>
	</div>
</div>