<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Rss</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Rss</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<p class="text_color strong-size">
				Select options
			</p>
			<div class="row custom-form">
				<div class="col-xs-12">
					<label class="cb-radio" data-show=".industry" data-hide=".feeds">
						<input type="radio" name='radio-group'>
						Industry
					</label>
					<div class="row industry is-hidden">
						<div class="col-sm-8">
							<select class="chosen-select">
								<option value="">Apple Top 10 Songs</option>
								<option value="">Forbes Popular Stories</option>
								<option value="">HP News</option>
								<option value="">Oracle Corporate News</option>
							</select>
						</div>
						<div class="col-sm-4">
							<button class="btn btn-save">Save</button>
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<label class="cb-radio" data-show=".feeds" data-hide=".industry">
						<input type="radio" name='radio-group'>
						Custom RSS Feeds
					</label>
					<div class="row feeds is-hidden">
						<div class="col-xs-12">
							<p class="black strong-size">
								Add new RSS Feed
							</p>
						</div>
						<div class="col-sm-5">
							<div class="row hidden more_block">
								<div class="col-xs-12">
									<p class="text_color strong-size">Title
										<a href="" class="pull-right link remove_more">Remove</a>
									</p>
									<div class="form-group">
										<input type="text" class="form-control">
									</div>
									<p class="text_color strong-size">Link Url</p>
									<div class="form-group">
										<input type="text" class="form-control">
									</div>
								</div>
							</div>	
							<div class="row">
								<div class="col-xs-12">
									<p class="text_color strong-size">Title</p>
									<div class="form-group">
										<input type="text" class="form-control">
									</div>
									<p class="text_color strong-size">Link Url</p>
									<div class="form-group">
										<input type="text" class="form-control">
									</div>
								</div>
							</div>									
							<div class="clearfix">
								<button class="btn btn-add pull-left more-link">+ One more</button>
								<button class="btn btn-save pull-right">Add all to feed</button>
							</div>
						</div>	
						<div class="col-xs-12 p-t15">									
							<p class="text_color strong-size">List of RSS</p>
							<p class="black"><span class="bold">CB</span> http://clickbrain.com/feed</p>
							<a href="" class="link" data-toggle="modal" data-target=".modal">Remove</a>
							<p class="black p-t10"><span class="bold">Menshealth</span> http://www.menshealth.com/events-promotions/washpofeed</p>
							<a href="" class="link" data-toggle="modal" data-target=".modal">Remove</a>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<div class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>				
			</div>
			<div class="modal-body">
				<h4 class="head_tab">Remove feed</h4>
				<p class="black">Do you really want to remove Rss Feed <span class="bold">CB ( http://clickbrain.com/feed )</span> from your feeds list?</p>
			</div>
			<div class="modal-footer clearfix">
				<div class="pull-right">
					<a class="link m-r10" data-dismiss="modal" aria-hidden="true" href="">Close</a>
					<button type="button" class="btn btn-save">Remove</button>
				</div>					
			</div>
		</div>
	</div>
</div>