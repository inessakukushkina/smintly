<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Social Mentions Keyword Phrases</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Social Mentions Keyword Phrases</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="row custom-form">
				<div class="col-xs-12">
					<p class="text_color strong-size pull-sm-left p-t5">
						Track mentions of your company in social networks
					</p>					
				</div>
				<div class="col-xs-12">
					<div class="row hidden insert_block">
						<div class="col-xs-12">
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm">
								<i class="cb-remove"></i>
								<div class="clearfix">
									<label class="cb-checkbox text-size pull-sm-left">
										<input type="checkbox">
										Exact
									</label>
									<div class="pull-sm-right">
										<a href="" class="link show_block">Include / Exclude</a>
									</div>
								</div>								
								<div class="toggle_block row">
									<div class="col-sm-6">
										<p class="text_color">Include +</p>
										<div class="form-group">
											<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
										</div>
									</div>
									<div class="col-sm-6">
										<p class="text_color">Exclude -</p>
										<div class="form-group">
											<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 m-t20">
					<div class="row past_block">
						<div class="col-xs-12">
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm">
								<div class="clearfix">
									<label class="cb-checkbox text-size pull-sm-left">
										<input type="checkbox">
										Exact
									</label>
									<div class="pull-sm-right">
										<a href="" class="link show_block">Include / Exclude</a>
									</div>
								</div>								
								<div class="toggle_block row">
									<div class="col-sm-6">
										<p class="text_color">Include +</p>
										<div class="form-group">
											<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
										</div>
									</div>
									<div class="col-sm-6">
										<p class="text_color">Exclude -</p>
										<div class="form-group">
											<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="pull-sm-right">
						<button class="btn btn-add m-tb20 m-r20">+ Add keyword</button>
						<button class="btn btn-save m-tb20 pull-right">Save</button>
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>