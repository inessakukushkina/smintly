<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Social Media</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Social Media</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-xs-12">				
			<div class="row group_account">
				<div class="col-xs-12 m-b20">
					<p class="text_color large-size">Group of account</p>
					<button class="btn btn-add create_account">
						Create a group of accounts
					</button>						
				</div>
			</div>
			<div class="row account_block">
				<div class="col-sm-6 col-lg-4 custom-form m-b20 account_item">
					<div class="row">
						<div class="col-sm-6">
							<p class="text_color large-size">Facebook gateway</p>
							<p class="black smallText">Setup your Facebook account</p>															
						</div>
						<div class="col-sm-6 sm-right">
							<button class="btn btn-facebook">Add your account</button>				
						</div>								
					</div>	
					<div class="row">
						<div class="col-xs-12 m-t20">
							<div class="user_account account_facebook dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>												
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_facebook dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_facebook dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_facebook dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4 custom-form m-b20 account_item">
					<div class="row">
						<div class="col-sm-6 m-b20">
							<p class="text_color large-size">Twitter gateway</p>
							<p class="black smallText">Setup your Twitter account</p>
						</div>
						<div class="col-sm-6 sm-right m-b20">
							<button class="btn btn-twitter">Add your account</button>
						</div>
					</div>	
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_twitter dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_twitter dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_twitter dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4 custom-form m-b20 account_item">
					<div class="row">
						<div class="col-sm-6 m-b20">
							<p class="text_color large-size">Youtube gateway</p>
							<p class="black smallText">Setup your Youtube account</p>
						</div>
						<div class="col-sm-6 sm-right m-b20">
							<button class="btn btn-youtube">Add your account</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_youtube dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_youtube dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_youtube dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>				
				<div class="col-sm-6 col-lg-4 custom-form m-b20 account_item">
					<div class="row">
						<div class="col-sm-6 m-b20">
							<p class="text_color large-size">Instagram gateway</p>
							<p class="black smallText">Setup your Instagram account</p>
						</div>
						<div class="col-sm-6 sm-right m-b20">
							<button class="btn btn-instagram">Add your account</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_instagram dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_instagram dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_instagram dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_instagram dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>	
				<div class="col-sm-6 col-lg-4 custom-form m-b20 account_item">
					<div class="row">
						<div class="col-sm-6 m-b20">
							<p class="text_color large-size">Linkedin gateway</p>
							<p class="black smallText">Setup your Linkedin account</p>
						</div>
						<div class="col-sm-6 sm-right m-b20">
							<button class="btn btn-linkedin">Add your account</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_linkedin dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_linkedin dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>						
				<div class="col-sm-6 col-lg-4 custom-form m-b20 account_item">
					<div class="row">
						<div class="col-sm-6 m-b20">
							<p class="text_color large-size">Google gateway</p>
							<p class="black smallText">Setup your Google account</p>
						</div>
						<div class="col-sm-6 sm-right m-b20">
							<button class="btn btn-linkedin">Add your account</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_google dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_google dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_google dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_google dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>	
			</div>
			<div class="row">
				<div class="col-sm-8">
					<div class="row">
						<div class="col-xs-12">
							<p class="text_color large-size">Timezones</p>
							<p class="black smallText">Select your Time zone</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-9">
							<div class="form-group">
								<select class="chosen-select">
									<option value="">One</option>
									<option value="">Two</option>
									<option value="">Three</option>
									<option value="">Four</option>
									<option value="">Five</option>
									<option value="">Six</option>
									<option value="">Seven</option>
									<option value="">Eight</option>
									<option value="">Nine</option>
									<option value="">Ten</option>							
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<button class="btn btn-save m-b15">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-save m-tb20 pull-right">Save</button>
		</div>
	</div>
</div>

<div class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>				
			</div>
			<div class="modal-body">
				<h4 class="head_tab">Remove feed</h4>
				<p class="black">Do you really want to remove Rss Feed <span class="bold">CB ( http://clickbrain.com/feed )</span> from your feeds list?</p>
			</div>
			<div class="modal-footer clearfix">
				<div class="pull-right">
					<a class="link m-r10" data-dismiss="modal" aria-hidden="true" href="">Close</a>
					<button type="button" class="btn btn-save">Remove</button>
				</div>					
			</div>
		</div>
	</div>
</div>