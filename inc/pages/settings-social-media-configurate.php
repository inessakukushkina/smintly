<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Social Media</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Social Media</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row account_block">
		<div class="col-sm-6 col-lg-4 m-b20 account_item">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color large-size">Facebook gateway</p>
					<p class="black smallText">Setup your Facebook account</p>															
				</div>
				<div class="col-sm-6 sm-right">
					<button class="btn btn-facebook">Add your account</button>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-lg-4 m-b20 account_item">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color large-size">Twitter gateway</p>
					<p class="black smallText">Setup your Twitter account</p>
				</div>
				<div class="col-sm-6 sm-right">
					<button class="btn btn-twitter">Configure your account</button>
				</div>
			</div>
			<div class="row">
                <div class="col-xs-12">
                    <a class="link twitter-btn enter-pin" data-toggle="modal" data-target="#EnterCode" href="#EnterCode">Enter pin code.</a>
                </div>
            </div>
		</div>
		<div class="col-sm-6 col-lg-4 m-b20 account_item">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color large-size">Youtube gateway</p>
					<p class="black smallText">Setup your Youtube account</p>
				</div>
				<div class="col-sm-6 sm-right">
					<button class="btn btn-youtube">Add your account</button>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-lg-4 m-b20 account_item">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color large-size">Instagram gateway</p>
					<p class="black smallText">Setup your Instagram account</p>
				</div>
				<div class="col-sm-6 sm-right">
					<button class="btn btn-instagram">Add your account</button>
				</div>
			</div>
		</div>		
		<div class="col-sm-6 col-lg-4 m-b20 account_item">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color large-size">Linkedin gateway</p>
					<p class="black smallText">Setup your Linkedin account</p>
				</div>
				<div class="col-sm-6 sm-right">
					<button class="btn btn-linkedin">Add your account</button>
				</div>
			</div>
		</div>						
		<div class="col-sm-6 col-lg-4 m-b20 account_item">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color large-size">Google gateway</p>
					<p class="black smallText">Setup your Google account</p>
				</div>
				<div class="col-sm-6 sm-right">
					<button class="btn btn-google">Add your account</button>
				</div>
			</div>
		</div>	
	</div>
	<div class="row">
		<div class="col-sm-8">
			<div class="row">
				<div class="col-xs-12">
					<p class="text_color large-size">Timezones</p>
					<p class="black smallText">Select your Time zone</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-9">
					<div class="form-group">
						<select class="chosen-select">
							<option value="">One</option>
							<option value="">Two</option>
							<option value="">Three</option>
							<option value="">Four</option>
							<option value="">Five</option>
							<option value="">Six</option>
							<option value="">Seven</option>
							<option value="">Eight</option>
							<option value="">Nine</option>
							<option value="">Ten</option>							
						</select>
					</div>
				</div>
				<div class="col-sm-3">
					<button class="btn btn-save m-b15">Save</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="b-Top p-tb20 m-t20">
				<button class="btn btn-save pull-right">Save</button>
			</div>			
		</div>
	</div>
</div>