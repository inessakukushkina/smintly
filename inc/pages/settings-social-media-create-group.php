<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Create a group of accounts</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Social Media</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row custom-form">
		<div class="col-xs-12">
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color strong-size">Name of the group</p>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<p class="text_color strong-size">Description of the group</p>
					<div class="form-group">
						<textarea rows="5" class="form-control"></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<p class="text_color strong-size m-b20">Choose an account for the group</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="choose_account account_facebook">
						<h3 class="choose_account_title"><i class="fa fa-facebook"></i> Facebook</h3>
						<ul class="choose_account_list">
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="facebook-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Chamomile Chamomile</h3>
											</div>
										</div>										
									</div>												
								</label>									
							</li>
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="facebook-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="images/raccoon.jpg" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Raccoon Raccoon</h3>
											</div>
										</div>										
									</div>										
								</label>
							</li>
						</ul>
					</div>
					<div class="choose_account account_youtube">
						<h3 class="choose_account_title"><i class="fa fa-youtube"></i> Youtube</h3>
						<ul class="choose_account_list">
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="youtube-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Chamomile Chamomile</h3>
											</div>
										</div>										
									</div>												
								</label>									
							</li>
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="youtube-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="images/raccoon.jpg" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Raccoon Raccoon</h3>
											</div>
										</div>										
									</div>										
								</label>
							</li>
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="youtube-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Chamomile Chamomile</h3>
											</div>
										</div>										
									</div>												
								</label>									
							</li>
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="youtube-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="images/raccoon.jpg" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Raccoon Raccoon</h3>
											</div>
										</div>										
									</div>										
								</label>
							</li>
						</ul>
					</div>
					<div class="choose_account account_linkedin">
						<h3 class="choose_account_title"><i class="fa fa-linkedin"></i> Linkedin</h3>
						<ul class="choose_account_list">
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="linkedin-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Chamomile Chamomile</h3>
											</div>
										</div>										
									</div>												
								</label>									
							</li>
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="linkedin-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="images/raccoon.jpg" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Raccoon Raccoon</h3>
											</div>
										</div>										
									</div>										
								</label>
							</li>
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="linkedin-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Chamomile Chamomile</h3>
											</div>
										</div>										
									</div>												
								</label>									
							</li>
						</ul>
					</div>					
				</div>
				<div class="col-sm-6">
					<div class="choose_account account_twitter">
						<h3 class="choose_account_title"><i class="fa fa-twitter"></i> Twitter</h3>
						<ul class="choose_account_list">
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="twitter-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Chamomile Chamomile</h3>
											</div>
										</div>										
									</div>												
								</label>									
							</li>
						</ul>
					</div>						
					<div class="choose_account account_instagram">
						<h3 class="choose_account_title"><i class="fa fa-instagram"></i> Instagram</h3>
						<ul class="choose_account_list">
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="instagram-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="images/raccoon.jpg" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Raccoon Raccoon</h3>
											</div>
										</div>										
									</div>										
								</label>
							</li>
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="instagram-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Chamomile Chamomile</h3>
											</div>
										</div>										
									</div>												
								</label>									
							</li>
						</ul>
					</div>
					<div class="choose_account account_google">
						<h3 class="choose_account_title"><i class="fa fa-google-plus"></i> Google</h3>
						<ul class="choose_account_list">
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="google-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="images/raccoon.jpg" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Raccoon Raccoon</h3>
											</div>
										</div>										
									</div>										
								</label>
							</li>
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="google-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Chamomile Chamomile</h3>
											</div>
										</div>										
									</div>												
								</label>									
							</li>
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="google-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="images/raccoon.jpg" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Raccoon Raccoon</h3>
											</div>
										</div>										
									</div>										
								</label>
							</li>
							<li class="choose_account_list_item">
								<label class="cb-radio w-100">
									<input type="radio" name="google-group">
									<div class="dTable">
										<div class="dRow">
											<div class="dCell cellImg">
												<img class="choose_account_list_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
											</div>
											<div class="dCell">
												<h3 class="choose_account_list_name">Chamomile Chamomile</h3>
											</div>
										</div>										
									</div>												
								</label>									
							</li>
						</ul>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="b-Top m-t20 p-t20">
					<div class="pull-sm-left p-tb10">
						<a href="" class="link ">
							<i class="fa fa-long-arrow-left blue-color m-r5"></i>
							Back to Social Media Settings
						</a>
					</div>
					<div class="pull-sm-right">
						<button class="btn-save m-b20">Save</button>
					</div>
					</div>					
				</div>
			</div>
		</div>						
	</div>
</div>