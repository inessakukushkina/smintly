<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Social Media</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Social Media</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-xs-12">
			<p class="text_color large-size">Group of account</p>
			<button class="btn btn-add create_account" data-toggle="modal" data-target="#create_group">Create a group of accounts</button>
			<div class="row m-b20">
				<div class="col-sm-6">
					<div class="group_account_item clearfix dTable">
						<div class="dRow">
							<div class="dCell cellImg">
								<i class="icon-group"></i>
							</div>
							<div class="dCell">
								<h3 class="group_account_title">												
									Raccoon Group
								</h3>
								<small class="group_account_description">Racoon group description</small>
								<div class="group_account_control">
									<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
									<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
								</div>
							</div>
						</div>	
					</div>
					<div class="group_account_item clearfix dTable">
						<div class="dRow">
							<div class="dCell cellImg">
								<i class="icon-group"></i>
							</div>
							<div class="dCell">
								<h3 class="group_account_title">												
									Chamomile Group
								</h3>
								<small class="group_account_description">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									Donec vel neque ultricies, bibendum tellus eu, dictum risus. 
									Praesent luctus lectus vel mauris consequat convallis. 
									Duis ultrices faucibus volutpat. 
									Etiam accumsan, purus vitae bibendum tincidunt, sem leo imperdiet tortor, non porttitor erat nibh quis nisl. 
									Nulla aliquam quis lorem in molestie.
								</small>								
								<div class="group_account_control">
									<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
									<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="group_account_item clearfix dTable">
						<div class="dRow">
							<div class="dCell cellImg">
								<i class="icon-group"></i>
							</div>
							<div class="dCell">
								<h3 class="group_account_title">												
									Chamomile Group
								</h3>
								<small class="group_account_description">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
									Donec vel neque ultricies, bibendum tellus eu, dictum risus. 
									Praesent luctus lectus vel mauris consequat convallis. 
									Duis ultrices faucibus volutpat. 
									Etiam accumsan, purus vitae bibendum tincidunt, sem leo imperdiet tortor, non porttitor erat nibh quis nisl. 
									Nulla aliquam quis lorem in molestie.
								</small>								
								<div class="group_account_control">
									<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
									<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
								</div>
							</div>
						</div>
					</div>
					<div class="group_account_item clearfix dTable">
						<div class="dRow">
							<div class="dCell cellImg">
								<i class="icon-group"></i>
							</div>
							<div class="dCell">
								<h3 class="group_account_title">												
									Raccoon Group
								</h3>
								<small class="group_account_description">
									Maecenas a enim in arcu vulputate euismod id nec lorem. 
									Sed sagittis elementum tortor sit amet tempus. 
									Mauris lacinia congue dui, ac pulvinar elit tempus eget. Sed ut iaculis ante. 
									Mauris scelerisque nunc a nisl sagittis, at vulputate nisi fermentum. 
									Vestibulum mollis ligula nec eros gravida efficitur. 
									Donec faucibus dui sit amet facilisis ullamcorper. 
									Sed est lorem, feugiat ac mi nec, mattis elementum eros.
								</small>
								<div class="group_account_control">
									<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
									<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
			<div class="row account_block">
				<div class="col-sm-6 col-lg-4 m-b20 account_item">
					<div class="row custom-form">
						<div class="col-sm-6">
							<p class="text_color large-size">Facebook gateway</p>
							<p class="black smallText">Setup your Facebook account</p>															
						</div>
						<div class="col-sm-6 sm-right">
							<button class="btn btn-facebook">Add your account</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-t20">
							<div class="user_account account_facebook dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>												
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_facebook dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_facebook dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_facebook dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4 m-b20 account_item">
					<div class="row">
						<div class="col-sm-6">
							<p class="text_color large-size">Twitter gateway</p>
							<p class="black smallText">Setup your Twitter account</p>
							<a href="#EnterCode" data-toggle="modal" data-target="#EnterCode" class="link">Enter your code</a>
						</div>
						<div class="col-sm-6 sm-right">
							<button class="btn btn-twitter">Configure your account</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-t20">
							<div class="user_account account_twitter dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_twitter dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_twitter dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-4 m-b20 account_item">
					<div class="row">
						<div class="col-sm-6">
							<p class="text_color large-size">Youtube gateway</p>
							<p class="black smallText">Setup your Youtube account</p>
						</div>
						<div class="col-sm-6 sm-right">
							<button class="btn btn-youtube">Add your account</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-t20">
							<div class="user_account account_youtube dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_youtube dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_youtube dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-sm-6 col-lg-4 m-b20 account_item">
					<div class="row">
						<div class="col-sm-6">
							<p class="text_color large-size">Instagram gateway</p>
							<p class="black smallText">Setup your Instagram account</p>
						</div>
						<div class="col-sm-6 sm-right">
							<button class="btn btn-instagram">Add your account</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-t20">
							<div class="user_account account_instagram dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_instagram dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_instagram dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_instagram dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>	
				<div class="col-sm-6 col-lg-4 m-b20 account_item">
					<div class="row">
						<div class="col-sm-6">
							<p class="text_color large-size">Linkedin gateway</p>
							<p class="black smallText">Setup your Linkedin account</p>
						</div>
						<div class="col-sm-6 sm-right">
							<button class="btn btn-linkedin">Add your account</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-t20">
							<div class="user_account account_linkedin dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_linkedin dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>						
				<div class="col-sm-6 col-lg-4 m-b20 account_item">
					<div class="row">
						<div class="col-sm-6">
							<p class="text_color large-size">Google gateway</p>
							<p class="black smallText">Setup your Google account</p>
						</div>
						<div class="col-sm-6 sm-right">
							<button class="btn btn-google">Add your account</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-t20">
							<div class="user_account account_google dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_google dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-t20">
							<div class="user_account account_google dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="images/raccoon.jpg" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Racoon Racoon</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="user_account account_google dTable">
								<div class="dRow">
									<div class="dCell cellImg">
										<img class="user_account_image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTntsQ374RrFIo_gNj6CdDXny_7ty_CJ33HTUmdpFINCZDvBNLE3g" alt="">
									</div>
									<div class="dCell">
										<h3 class="user_account_name pull-md-left">Chamomile Chamomile</h3>
										<div class="pull-md-right m-t10">
											<a href="" class="link "><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="" class="remove_link m-l10"><i class="fa fa-remove"></i> Delete</a>
										</div>
									</div>
								</div>										
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>	
	</div>
</div>
<div id="EnterCode" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>				
			</div>
			<div class="modal-body">
				<h4 class="head_tab">Pin code</h4>
				<div class="row">
					<div class="col-sm-8">
						<div class="form-group">
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer clearfix">
				<div class="pull-right">
					<a class="link m-r10" data-dismiss="modal" aria-hidden="true" href="">Close</a>
					<button type="button" class="btn btn-save">Save</button>
				</div>					
			</div>
		</div>
	</div>
</div>