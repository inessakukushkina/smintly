<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Social Media</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Social Media</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-md-10 col-lg-8">
			<div class="row">
				<div class="col-sm-6">
					<div class="row">
						<div class="col-xs-12">
							<p class="text_color strong-size">Keyword phrase 10</p>
							<p class="black smallText">Select your Time zone</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-9">
							<div class="form-group">
								<select class="chosen-select">
									<option value="">One</option>
									<option value="">Two</option>
									<option value="">Three</option>
									<option value="">Four</option>
									<option value="">Five</option>
									<option value="">Six</option>
									<option value="">Seven</option>
									<option value="">Eight</option>
									<option value="">Nine</option>
									<option value="">Ten</option>							
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<button class="btn btn-save m-b15">Save</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="row">
						<div class="col-xs-12">
							<p class="text_color strong-size">Facebook gateway</p>
							<p class="black smallText">Setup your Facebook account</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-9">
							<div class="form-group">
								<select class="chosen-select">
									<option value="">One</option>
									<option value="">Two</option>
									<option value="">Three</option>
									<option value="">Four</option>
									<option value="">Five</option>
									<option value="">Six</option>
									<option value="">Seven</option>
									<option value="">Eight</option>
									<option value="">Nine</option>
									<option value="">Ten</option>							
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<button class="btn btn-save m-b15">Save</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="" class="link">Logout from Facebook</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="row custom-form">
						<div class="col-xs-12">
							<p class="text_color strong-size m-t20">Twitter gateway</p>
							<p class="black smallText">Setup your Twitter account</p>
							<label class="cb-checkbox text-size">
								<input type="checkbox">
								Auto-follow twitter
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="" class="link">Logout from Facebook</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 custom-form">
					<div class="row ">
						<div class="col-xs-12">
							<p class="text_color strong-size m-t20">Youtube gateway</p>
							<p class="black smallText">Setup your Youtube account</p>
							<label class="cb-checkbox text-size">
								<input type="checkbox">
								Auto-follow Youtube
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="" class="link">Logout from Youtube</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 custom-form">
					<p class="text_color strong-size m-t20">Linkedin gateway</p>
					<p class="black smallText">Setup your Linkedin account</p>
					<a href="" class="link">Logout from Linkedin</a>
				</div>
				<div class="col-sm-6 custom-form">
					<p class="text_color strong-size m-t20">Google gateway</p>
					<p class="black smallText">Setup your Google account</p>
					<a href="" class="link">Logout from Google</a>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 m-b20">
					<p class="text_color strong-size m-t20">Instagram gateway</p>
					<p class="black smallText">Setup your Instagram account</p>
					<a href="" class="link">Logout from Instagram</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-save m-tb20 pull-right">Save</button>
		</div>
	</div>
</div>