<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Collaboration Team</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					<a href="" class="breadcrumbs_link">Collaboration Team</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">	
	<div class="row">
		<div class="col-xs-12">
			<a href="" class="link invite_user">Invite</a>
		</div>
	</div>
	<div class="row invite_block m-t20">
		<div class="col-sm-6">
			<div class="input-group form-group">						
				<input type="text" class="form-control" placeholder="Enter email" data-role="tagsinput">
				<span class="input-group-btn">
					<button class="btn btn-save" type="submit">Save</button>
				</span>					
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 m-t10">
			<table class="responsive-table">
				<thead class="table_head">
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td data-th="First Name">Admin</td>
						<td data-th="Last Name">Admin</td>
						<td data-th="Email">admin@admin.com</td>
					</tr>
					<tr>
						<td data-th="First Name">Admin</td>
						<td data-th="Last Name">Admin</td>
						<td data-th="Email">admin@admin.com</td>
					</tr>
					<tr>
						<td data-th="First Name">Admin</td>
						<td data-th="Last Name">Admin</td>
						<td data-th="Email">admin@admin.com</td>
					</tr>
					<tr>
						<td data-th="First Name">Admin</td>
						<td data-th="Last Name">Admin</td>
						<td data-th="Email">admin@admin.com</td>
					</tr>
					<tr>
						<td data-th="First Name">Admin</td>
						<td data-th="Last Name">Admin</td>
						<td data-th="Email">admin@admin.com</td>
					</tr>
					<tr>
						<td data-th="First Name">Admin</td>
						<td data-th="Last Name">Admin</td>
						<td data-th="Email">admin@admin.com</td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>			
</div>