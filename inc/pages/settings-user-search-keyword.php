<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Social User Search Keyword Phrases</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					Social User Search Keyword Phrases
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row custom-form">
		<div class="col-xs-12">
			<label for="" class="cb-checkbox">
				<input type="checkbox">
				Auto-search and follow twitter
			</label>
		</div>
	</div>
	<div class="row custom-form">
		<div class="col-md-10 col-lg-8">			
			<div class="row">
				<div class="col-xs-12">
					<div class="form-group quantity-form m-t20">					
						<p class="text_color strong-size">
							Max daily count of automatically followed users by search
						</p>
						<div class="row">
							<div class="col-sm-2 col-lg-1 quantity_block">
								<input type="text" class="form-control quantity config_input" value=0>
								<i class="fa fa-angle-up decrementBtn"></i>
								<i class="fa fa-angle-down incrementBtn"></i>	
							</div>
						</div>						
					</div>
				</div>
				<div class="col-xs-12">
					<div class="row hidden insert_block">
						<div class="col-xs-12">
							<div class="b-Bottom m-b15">
								<div class="form-group">
									<input class="form-control m-b10" value="advmapdm">
									<i class="cb-remove"></i>
									<div class="clearfix">
										<label class="cb-checkbox text-size pull-sm-left">
											<input type="checkbox">
											Exact
										</label>
										<div class="pull-sm-right">
											<a href="" class="link show_block">Include / Exclude</a>
										</div>
									</div>
									<div class="toggle_block row">
										<div class="col-sm-6 ">
											<p class="text_color">Include +</p>
											<div class="form-group">
												<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
											</div>
										</div>
										<div class="col-sm-6">
											<p class="text_color">Exclude -</p>
											<div class="form-group">
												<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
											</div>
										</div>
									</div>
									<div class="row m-tb10">
										<div class="col-sm-6">
											<a href="" class="link show_followers m-t10">Min / Max Followers</a>
										</div>
										<div class="col-sm-6 sm-right">
											<a href="" class="link show_time m-t10">Follow Time</a>									
										</div>
									</div>
									<div class="followers_block row">
										<div class="col-sm-6">
											<div class="form-group quantity-form">
												<p class="text_color">Min</p>
												<input type="text" class="form-control quantity" value="0">
												<i class="fa fa-angle-up decrementBtn"></i>
												<i class="fa fa-angle-down incrementBtn"></i>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group quantity-form">
												<p class="text_color">Max</p>
												<input type="text" class="form-control quantity" val="0">
												<i class="fa fa-angle-up decrementBtn"></i>
												<i class="fa fa-angle-down incrementBtn"></i>
											</div>
										</div>
									</div>
									<div class="time_block row">
										<div class="col-sm-4">
											<select class="chosen-select">
												<option value="">One</option>
												<option value="">Two</option>
												<option value="">Three</option>
												<option value="">Four</option>
												<option value="">Five</option>
												<option value="">Six</option>
												<option value="">Seven</option>
												<option value="">Eight</option>
												<option value="">Nine</option>
												<option value="">Ten</option>
											</select>
										</div>
										<div class="col-sm-4">
											<select class="chosen-select">
												<option value="">One</option>
												<option value="">Two</option>
												<option value="">Three</option>
												<option value="">Four</option>
												<option value="">Five</option>
												<option value="">Six</option>
												<option value="">Seven</option>
												<option value="">Eight</option>
												<option value="">Nine</option>
												<option value="">Ten</option>
											</select>
										</div>
										<div class="col-sm-4">
											<select class="chosen-select">
												<option value="">One</option>
												<option value="">Two</option>
												<option value="">Three</option>
												<option value="">Four</option>
												<option value="">Five</option>
												<option value="">Six</option>
												<option value="">Seven</option>
												<option value="">Eight</option>
												<option value="">Nine</option>
												<option value="">Ten</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 m-t20">
					<div class="row past_block clone_block">
						<div class="col-xs-12">
							<div class="b-Bottom m-b15">
								<div class="form-group">
								<input class="form-control m-b10" value="advmapdm">
								<div class="clearfix">
									<label class="cb-checkbox text-size pull-sm-left">
										<input type="checkbox">
										Exact
									</label>
									<div class="pull-sm-right">
										<a href="" class="link show_block">Include / Exclude</a>
									</div>
								</div>

								<div class="toggle_block row">
									<div class="col-sm-6">
										<p class="text_color">Include +</p>
										<div class="form-group">
											<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
										</div>
									</div>
									<div class="col-sm-6">
										<p class="text_color">Exclude -</p>
										<div class="form-group">
											<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
										</div>
									</div>
								</div>
								<div class="row m-tb10">
									<div class="col-sm-6">
										<a href="" class="link show_followers m-t10">Min / Max Followers</a>
									</div>
									<div class="col-sm-6 sm-right">
										<a href="" class="link show_time m-t10">Follow Time</a>									
									</div>
								</div>
								<div class="followers_block row">
									<div class="col-sm-6">
										<div class="form-group quantity-form">
											<p class="text_color">Min</p>
											<input type="text" class="form-control quantity" value="0">
											<i class="fa fa-angle-up decrementBtn"></i>
											<i class="fa fa-angle-down incrementBtn"></i>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group quantity-form">
											<p class="text_color">Max</p>
											<input type="text" class="form-control quantity" val="0">
											<i class="fa fa-angle-up decrementBtn"></i>
											<i class="fa fa-angle-down incrementBtn"></i>
										</div>
									</div>
								</div>
								<div class="time_block row">
									<div class="col-sm-4">
										<select class="chosen-select">
											<option value="">One</option>
											<option value="">Two</option>
											<option value="">Three</option>
											<option value="">Four</option>
											<option value="">Five</option>
											<option value="">Six</option>
											<option value="">Seven</option>
											<option value="">Eight</option>
											<option value="">Nine</option>
											<option value="">Ten</option>
										</select>
									</div>
									<div class="col-sm-4">
										<select class="chosen-select">
											<option value="">One</option>
											<option value="">Two</option>
											<option value="">Three</option>
											<option value="">Four</option>
											<option value="">Five</option>
											<option value="">Six</option>
											<option value="">Seven</option>
											<option value="">Eight</option>
											<option value="">Nine</option>
											<option value="">Ten</option>
										</select>
									</div>
									<div class="col-sm-4">
										<select class="chosen-select">
											<option value="">One</option>
											<option value="">Two</option>
											<option value="">Three</option>
											<option value="">Four</option>
											<option value="">Five</option>
											<option value="">Six</option>
											<option value="">Seven</option>
											<option value="">Eight</option>
											<option value="">Nine</option>
											<option value="">Ten</option>
										</select>
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="pull-sm-right">
						<button class="btn btn-add m-tb20 m-r20">+ Add keyword</button>
						<button class="btn btn-save m-tb20 pull-right">Save</button>
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>