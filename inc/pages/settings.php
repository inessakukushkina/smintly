<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Settings</h1>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-xs-12">
			<ul class="nav nav-tabs settings_tab">
				<li class="setting_item active">
					<a class="setting_link" href="#personal_settings" data-toggle="tab">
						<i class="ti-user"></i>
						Personal Settings
					</a>
				</li>
				<li class="setting_item">
					<a class="setting_link" href="#directory_settings" data-toggle="tab">
						<i class="ti-settings"></i>
						Directory Settings
					</a>
				</li>
				<li class="setting_item">
					<a class="setting_link" href="#places_keywords" data-toggle="tab">
						<i class="ti-google"></i>
						Google Places Keywords
					</a>
				</li>
				<li class="setting_item">
					<a class="setting_link" href="#social_media" data-toggle="tab">
						<i class="ti-instagram"></i>
						Social Media
					</a>
				</li>
				<li class="setting_item">
					<a class="setting_link" href="#social_keywords" data-toggle="tab">
						<i class="ti-layers-alt"></i>
						Social Keywords
					</a>
				</li>
				<li class="setting_item">
					<a class="setting_link" href="#analytics" data-toggle="tab">
						<i class="ti-pulse"></i>
						Analytics
					</a>
				</li>
				<li class="setting_item">
					<a class="setting_link" href="#rss" data-toggle="tab">
						<i class="ti-rss-alt"></i>
						Rss
					</a>
				</li>
				<li class="setting_item">
					<a class="setting_link" href="#collaboration" data-toggle="tab">
						<i class="ti-id-badge"></i>
						Collaboration Team
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="tab-content settings_content">
				<div class="tab-pane active" id="personal_settings">
					<h4 class="head_tab">Personal Settings</h4>
					<div class="row">
						<div class="col-md-9 col-lg-8">
							<div class="row">
								<div class="col-sm-4">
									<p class="text_color strong-size">Email *</p>
									<div class="form-group">
										<input class="form-control"/>
									</div>
								</div>
								<div class="col-sm-4">
									<p class="text_color strong-size">First Name *</p>
									<div class="form-group">
										<input class="form-control"/>
									</div>
								</div>
								<div class="col-sm-4">
									<p class="text_color strong-size">Last Name *</p>
									<div class="form-group">
										<input class="form-control"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<p class="text_color strong-size">Old Password *</p>
									<div class="form-group">
										<input class="form-control"/>
									</div>
								</div>
								<div class="col-sm-4">
									<p class="text_color strong-size">New Password *</p>
									<div class="form-group">
										<input class="form-control"/>
									</div>
								</div>
								<div class="col-sm-4">
									<p class="text_color strong-size">Confirm New Password *</p>
									<div class="form-group">
										<input class="form-control"/>
									</div>
								</div>
							</div>	
						</div>
					</div>					
				</div>
				<div class="tab-pane" id="directory_settings">
					<h4 class="head_tab">Directory Settings</h4>
					<div class="row">
						<div class="col-xs-12 custom-form m-b10">
							<label class="cb-checkbox">
								<input type="checkbox"/>
								I want to receive new reviews by email
							</label>
						</div>
					</div>					
					<div class="row">
						<div class="col-sm-4">
							<p class="text_color strong-size">Google Places (type business name)</p>
							<div class="form-group">
								<input class="form-control"/>
							</div>
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Yelp</p>
							<div class="form-group">
								<input class="form-control m-b10"/>
								<a href="" class="link">Find url</a>	
							</div>												
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Merchant Circle</p>
							<div class="form-group">
								<input class="form-control m-b10"/>
								<a href="" class="link">Find url</a>	
							</div>												
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Citysearch</p>
							<div class="form-group">
								<input class="form-control m-b10"/>
								<a href="" class="link">Find url</a>	
							</div>												
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Yahoo Local</p>
							<div class="form-group">
								<input class="form-control m-b10"/>
								<a href="" class="link">Find url</a>	
							</div>												
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Insider Pages</p>
							<div class="form-group">
								<input class="form-control m-b10"/>
								<a href="" class="link">Find url</a>	
							</div>												
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Foursquare</p>
							<div class="form-group">
								<input class="form-control m-b10"/>
								<a href="" class="link">Find url</a>	
							</div>												
						</div>							
					</div>
				</div>
				<div class="tab-pane" id="places_keywords">
					<h4 class="head_tab">Google Places Keywords</h4>
					<p class="black strong-size">Set Company Address for search engine tracking. Start typing your Company Name and autocomplete will show available results.</p>
					<div class="row">
						<div class="col-xs-12">
							<p class="text_color strong-size">Company name + address (Google Places)</p>
						</div>
						<div class="col-sm-4">							
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm"/>
								<i class="fa fa-times clear"></i>
							</div>
						</div>
					</div>
					<p class="black strong-size">Choose 10 keyword phrases for search engine tracking</p>
					<div class="row">
						<div class="col-sm-4">
							<p class="text_color strong-size">Keyword phrase 1</p>
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm"/>
								<i class="fa fa-times clear"></i>
							</div>
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Keyword phrase 2</p>
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm"/>
								<i class="fa fa-times clear"></i>
							</div>
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Keyword phrase 3</p>
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm"/>
								<i class="fa fa-times clear"></i>
							</div>
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Keyword phrase 4</p>
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm"/>
								<i class="fa fa-times clear"></i>
							</div>
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Keyword phrase 5</p>
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm"/>
								<i class="fa fa-times clear"></i>
							</div>
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Keyword phrase 6</p>
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm"/>
								<i class="fa fa-times clear"></i>
							</div>
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Keyword phrase 7</p>
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm"/>
								<i class="fa fa-times clear"></i>
							</div>
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Keyword phrase 8</p>
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm"/>
								<i class="fa fa-times clear"></i>
							</div>
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Keyword phrase 9</p>
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm"/>
								<i class="fa fa-times clear"></i>
							</div>
						</div>
						<div class="col-sm-4">
							<p class="text_color strong-size">Keyword phrase 10</p>
							<div class="form-group">
								<input class="form-control m-b10" value="advmapdm"/>
								<i class="fa fa-times clear"></i>
							</div>
						</div>
					</div>			
				</div>
				<div class="tab-pane" id="social_media">
					<h4 class="head_tab">Social Media</h4>
					<div class="row">
						<div class="col-sm-6">
							<div class="row">
								<div class="col-xs-12">
									<p class="text_color strong-size">Keyword phrase 10</p>
									<p class="black smallText">Select your Time zone</p>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<div class="form-group">
										<select class="chosen-select">
											<option value="">One</option>
											<option value="">Two</option>
											<option value="">Three</option>
											<option value="">Four</option>
											<option value="">Five</option>
											<option value="">Six</option>
											<option value="">Seven</option>
											<option value="">Eight</option>
											<option value="">Nine</option>
											<option value="">Ten</option>							
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<button class="btn btn-save m-b15">Save</button>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-xs-12">
									<p class="text_color strong-size">Facebook gateway</p>
									<p class="black smallText">Setup your Facebook account</p>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<div class="form-group">
										<select class="chosen-select">
											<option value="">One</option>
											<option value="">Two</option>
											<option value="">Three</option>
											<option value="">Four</option>
											<option value="">Five</option>
											<option value="">Six</option>
											<option value="">Seven</option>
											<option value="">Eight</option>
											<option value="">Nine</option>
											<option value="">Ten</option>							
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<button class="btn btn-save m-b15">Save</button>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<a href="" class="link">Logout from Facebook</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="row custom-form">
								<div class="col-xs-12">
									<p class="text_color strong-size m-t20">Twitter gateway</p>
									<p class="black smallText">Setup your Twitter account</p>
									<label class="cb-checkbox text-size">
										<input type="checkbox">
										Auto-follow twitter
									</label>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<a href="" class="link">Logout from Facebook</a>
								</div>
							</div>
						</div>
						<div class="col-sm-6 custom-form">
							<div class="row ">
								<div class="col-xs-12">
									<p class="text_color strong-size m-t20">Youtube gateway</p>
									<p class="black smallText">Setup your Youtube account</p>
									<label class="cb-checkbox text-size">
										<input type="checkbox">
										Auto-follow Youtube
									</label>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<a href="" class="link">Logout from Youtube</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 custom-form">
							<p class="text_color strong-size m-t20">Linkedin gateway</p>
							<p class="black smallText">Setup your Linkedin account</p>
							<a href="" class="link">Logout from Linkedin</a>
						</div>
						<div class="col-sm-6 custom-form">
							<p class="text_color strong-size m-t20">Google gateway</p>
							<p class="black smallText">Setup your Google account</p>
							<a href="" class="link">Logout from Google</a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 m-b20">
							<p class="text_color strong-size m-t20">Instagram gateway</p>
							<p class="black smallText">Setup your Instagram account</p>
							<a href="" class="link">Logout from Instagram</a>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="social_keywords">
					<h4 class="head_tab">Social Mentions Keyword Phrases</h4>
					<div class="row custom-form">
						<div class="col-xs-12">
							<p class="text_color strong-size pull-sm-left p-t5">
								Track mentions of your company in social networks
							</p>
							<button class="btn btn-add pull-sm-right">+ Add keyword</button>
						</div>
						<div class="col-xs-12">
							<div class="row hidden insert_block">
								<div class="col-sm-7">
									<div class="form-group">
										<input class="form-control m-b10" value="advmapdm">
										<i class="cb-remove"></i>
										<div class="clearfix">
											<label class="cb-checkbox text-size pull-sm-left">
												<input type="checkbox">
												Exact
											</label>
											<div class="pull-sm-right">
												<a href="" class="link show_block">Include / Exclude</a>
											</div>
										</div>								
										<div class="toggle_block row">
											<div class="col-sm-6">
												<p class="text_color">Include +</p>
												<div class="form-group">
													<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
												</div>
											</div>
											<div class="col-sm-6">
												<p class="text_color">Exclude -</p>
												<div class="form-group">
													<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 m-t20">
							<div class="row past_block">
								<div class="col-sm-7">
									<div class="form-group">
										<input class="form-control m-b10" value="advmapdm">
										<i class="cb-remove"></i>
										<div class="clearfix">
											<label class="cb-checkbox text-size pull-sm-left">
												<input type="checkbox">
												Exact
											</label>
											<div class="pull-sm-right">
												<a href="" class="link show_block">Include / Exclude</a>
											</div>
										</div>								
										<div class="toggle_block row">
											<div class="col-sm-6">
												<p class="text_color">Include +</p>
												<div class="form-group">
													<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
												</div>
											</div>
											<div class="col-sm-6">
												<p class="text_color">Exclude -</p>
												<div class="form-group">
													<textarea class="form-control" placeholder="Comma-separated words..."></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="analytics">
					<h4 class="head_tab">Analytics Details</h4>					
					<div class="row">
						<div class="col-sm-6">
							<p class="text_color strong-size">Google Analytics </p>
							<p class="black smallText">Select your Google Analytics profile to get statistics</p>
						</div>
						<div class="col-sm-6">
							<div class="pull-sm-right">
								<button class="btn btn-add">Logout</button>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top clearfix m-b10 m-t20 p-t20">
								<div class="pull-sm-right">
									<button class="btn btn-save">Select</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="rss">
					<h4 class="head_tab">Rss</h4>
					<p class="text_color strong-size">
						Select options
					</p>
					<div class="row custom-form">
						<div class="col-xs-12">
							<label class="cb-radio" data-show=".industry" data-hide=".feeds">
								<input type="radio" name='radio-group'>
								Industry
							</label>
							<div class="row industry is-hidden">
								<div class="col-sm-5">
									<select class="chosen-select">
										<option value="">Apple Top 10 Songs</option>
										<option value="">Forbes Popular Stories</option>
										<option value="">HP News</option>
										<option value="">Oracle Corporate News</option>
									</select>
								</div>
								<div class="col-sm-4">
									<button class="btn btn-save">Save</button>
								</div>
							</div>
						</div>
						<div class="col-xs-12">
							<label class="cb-radio" data-show=".feeds" data-hide=".industry">
								<input type="radio" name='radio-group'>
								Custom RSS Feeds
							</label>
							<div class="row feeds is-hidden">
								<div class="col-xs-12">
									<p class="black strong-size">
										Add new RSS Feed
									</p>
								</div>
								<div class="col-sm-5">
									<div class="row hidden more_block">
										<div class="col-xs-12">
											<p class="text_color strong-size">Title
												<a href="" class="pull-right link remove_more">Remove</a>
											</p>
											<div class="form-group">
												<input type="text" class="form-control">
											</div>
											<p class="text_color strong-size">Link Url</p>
											<div class="form-group">
												<input type="text" class="form-control">
											</div>
										</div>
									</div>	
									<div class="row">
										<div class="col-xs-12">
											<p class="text_color strong-size">Title</p>
											<div class="form-group">
												<input type="text" class="form-control">
											</div>
											<p class="text_color strong-size">Link Url</p>
											<div class="form-group">
												<input type="text" class="form-control">
											</div>
										</div>
									</div>									
									<div class="clearfix">
										<button class="btn btn-add pull-left more-link">+ One more</button>
										<button class="btn btn-save pull-right">Add all to feed</button>
									</div>
								</div>	
								<div class="col-xs-12 p-t15">									
									<p class="text_color strong-size">List of RSS</p>
									<p class="black"><span class="bold">CB</span> http://clickbrain.com/feed</p>
									<a href="" class="link" data-toggle="modal" data-target=".modal">Remove</a>
									<p class="black p-t10"><span class="bold">Menshealth</span> http://www.menshealth.com/events-promotions/washpofeed</p>
									<a href="" class="link" data-toggle="modal" data-target=".modal">Remove</a>
								</div>						
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="collaboration">
					<h4 class="head_tab">Collaboration Team</h4>
					<div class="row">
						<div class="col-xs-12">
							<a href="" class="link invite_user">Invite</a>
						</div>
					</div>
					<div class="row invite_block m-t20">
						<div class="col-xs-12">
							<div class="input-group form-group">						
								<input type="text" class="form-control" placeholder="Enter email" data-role="tagsinput">
								<span class="input-group-btn">
									<button class="btn btn-save" type="submit">Save</button>
								</span>					
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-t10">
							<p class="large-size text_color">
								No users
							</p>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<button class="btn btn-save m-tb20 pull-right">Save</button>
		</div>
	</div>
</div>

<div class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>				
			</div>
			<div class="modal-body">
				<h4 class="head_tab">Remove feed</h4>
				<p class="black">Do you really want to remove Rss Feed <span class="bold">CB ( http://clickbrain.com/feed )</span> from your feeds list?</p>
			</div>
			<div class="modal-footer clearfix">
				<div class="pull-right">
					<a class="link m-r10" data-dismiss="modal" aria-hidden="true" href="">Close</a>
					<button type="button" class="btn btn-save">Remove</button>
				</div>					
			</div>
		</div>
	</div>
</div>