<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Shedule a post</h1>
			<div class="row">
				<div class="col-xs-12">
					<ul class="breadcrumbs">
						<li class="breadcrumbs_item">
							<a href="" class="breadcrumbs_link">Social Media</a>
						</li>
						<li class="breadcrumbs_item active">
							<a href="" class="breadcrumbs_link">Scheduled Posts</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="main_block">
	<div class="row">
		<div class="col-xs-12">
			<div class="web_radar m-t20 pull_border">
				<div class="post_content dTable">
					<div class="dRow">
						<div class="dCell">
							<div class="clearfix">
								<p class="pull-sm-left">
									<span class="post_date">Post On:</span> 03.27.2015 06.29 am
								</p>
								<p class="pull-sm-right">
									<a class="link m-r10" href="">Edit</a><a class="link" href="">Remove</a>
								</p>
							</div>							
							<p class="web_radar_text ">
								Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
								Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
								Donec hendrerit iaculis tellus eget consectetur. 
								Vestibulum sed elit id magna mattis viverra ac quis dui. 
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
								Duis et elit semper, tempus ante vel, fermentum lectus. 
								Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
							</p>
							<div class="clearfix">
								<p class="pull-sm-left m-b0"><span class="post_date">To:</span> Twitter</p>
								<i class="pull-sm-right fa fa-twitter-square i-twitter"></i>
							</div>							
						</div>
					</div>
				</div>
				<div class="post_content dTable">
					<div class="dRow">
						<div class="dCell">
							<div class="clearfix">
								<p class="pull-sm-left">
									<span class="post_date">Post On:</span> 03.27.2015 06.29 am
								</p>
								<p class="pull-sm-right">
									<a class="link m-r10" href="">Edit</a><a class="link" href="">Remove</a>
								</p>
							</div>							
							<p class="web_radar_text ">
								Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
								Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
								Donec hendrerit iaculis tellus eget consectetur. 
								Vestibulum sed elit id magna mattis viverra ac quis dui. 
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
								Duis et elit semper, tempus ante vel, fermentum lectus. 
								Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
							</p>
							<div class="clearfix">
								<p class="pull-sm-left m-b0"><span class="post_date">To:</span> Facebook</p>
								<i class="pull-sm-right fa fa-facebook-square i-facebook"></i>
							</div>							
						</div>
					</div>
				</div>
				<div class="post_content dTable">
					<div class="dRow">
						<div class="dCell">
							<div class="clearfix">
								<p class="pull-sm-left">
									<span class="post_date">Post On:</span> 03.27.2015 06.29 am
								</p>
								<p class="pull-sm-right">
									<a class="link m-r10" href="">Edit</a><a class="link" href="">Remove</a>
								</p>
							</div>							
							<p class="web_radar_text ">
								Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
								Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
								Donec hendrerit iaculis tellus eget consectetur. 
								Vestibulum sed elit id magna mattis viverra ac quis dui. 
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
								Duis et elit semper, tempus ante vel, fermentum lectus. 
								Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
							</p>
							<div class="clearfix">
								<p class="pull-sm-left m-b0"><span class="post_date">To:</span> Google</p>
								<i class="pull-sm-right fa fa-google-plus-square i-google"></i>
							</div>							
						</div>
					</div>
				</div>
				<div class="post_content dTable">
					<div class="dRow">
						<div class="dCell">
							<div class="clearfix">
								<p class="pull-sm-left">
									<span class="post_date">Post On:</span> 03.27.2015 06.29 am
								</p>
								<p class="pull-sm-right">
									<a class="link m-r10" href="">Edit</a><a class="link" href="">Remove</a>
								</p>
							</div>							
							<p class="web_radar_text ">
								Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
								Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
								Donec hendrerit iaculis tellus eget consectetur. 
								Vestibulum sed elit id magna mattis viverra ac quis dui. 
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
								Duis et elit semper, tempus ante vel, fermentum lectus. 
								Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
							</p>
							<div class="clearfix">
								<p class="pull-sm-left m-b0"><span class="post_date">To:</span> Linkedin</p>
								<i class="pull-sm-right fa fa-linkedin-square i-linkedin"></i>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right">
				<ul class="pagination">
					<li class="pagination_item unactive">
						<a href="" class="pagination_link">Previous</a>
					</li>
					<li class="pagination_item active">
						<a href="" class="pagination_link">1</a>
					</li>			
					<li class="pagination_item unactive">
						<a href="" class="pagination_link">Next</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>