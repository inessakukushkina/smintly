<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="sign_block ">
				<div class="sign_block__head">
					<h2 class="sign_title">Login</h2>
				</div>
				<div class="sign_block__body">
					<div class="row">
						<div class="col-lg-6 lg-half text-center ">
							<button class="btn btn-facebook m-b20 w-100">Login with Facebook</button>
						</div>
						<div class="col-lg-6 lg-half text-center">
							<button class="btn btn-google m-b20 w-100">Login with Google</button>						
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Email">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Password">
							</div>
						</div>				
					</div>
					<div class="row custom-form">
						<div class="col-xs-12">
							<div class="pull-left">
								<label class="cb-checkbox text_color">
									<input type="checkbox">
									Remember me
								</label>
								<a href="" class="link">Forgot password?</a>
							</div>
							<div class="pull-right">
								<button class="btn-save btn-large">Next</button>
							</div>					
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>