<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="sign_block">
				<div class="sign_block__head">
					<h2 class="sign_title">Get Free Account</h2>
					<h4 class="sign_subtitle">Please fill the form to continue</h4>
				</div>
				<div class="sign_block__body">
					<div class="row">
						<div class="col-lg-6 lg-half text-center">
							<button class="btn btn-facebook m-b20 w-100">Sign Up with Facebook</button>
						</div>
						<div class="col-lg-6 lg-half text-center">
							<button class="btn btn-google m-b20 w-100">Sign Up with Google</button>						
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<p class="sign_block__or">or</p>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="First Name">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Last Name">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Email">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Password">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Confirm password">
							</div>	
						</div>
					</div>					
					<div class="row custom-form">
						<div class="col-xs-12">
							<div class="pull-left">
								<label class="cb-checkbox text_color p-t10">
									<input type="checkbox">
									Agree to <a href="" class="link">Terms and Conditions</a>
								</label>						
							</div>
							<div class="pull-right">
								<button class="btn-save btn-large">Next</button>
							</div>					
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>