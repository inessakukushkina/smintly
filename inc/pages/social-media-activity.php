<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Create Social Media</h1>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-xs-12">
			<ul class="nav nav-tabs settings_tab">
				<li class="setting_item active auto">
					<a class="setting_link" href="#facebook" data-toggle="tab">
						<i class="ti-facebook"></i>
						Facebook
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#twitter" data-toggle="tab">
						<i class="ti-twitter"></i>
						Twitter
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#google" data-toggle="tab">
						<i class="ti-google"></i>
						Google +
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#linkedin" data-toggle="tab">
						<i class="ti-linkedin"></i>
						Linkedin
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#youtube" data-toggle="tab">
						<i class="ti-youtube"></i>
						Youtube
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="tab-content settings_content">
		<div class="tab-pane active" id="facebook">			
			<div class="row">
				<div class="col-xs-12">
					<div class="web_radar m-t20 pull_border">
						<div class="web_radar_content dTable web_facebook">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>	
						<div class="web_radar_content dTable web_facebook">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>		
						<div class="web_radar_content dTable web_facebook">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>		
						<div class="web_radar_content dTable web_facebook">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>		
						<div class="web_radar_content dTable web_facebook">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>		
						<div class="web_radar_content dTable web_facebook">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>		
						<div class="web_radar_content dTable web_facebook">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-facebook-square i-facebook"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
									</p>
								</div>
							</div>
						</div>					
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<ul class="pagination pull-right">
						<li class="pagination_item unactive">
							<a href="" class="pagination_link">Previous</a>
						</li>
						<li class="pagination_item active">
							<a href="" class="pagination_link">1</a>
						</li>			
						<li class="pagination_item unactive">
							<a href="" class="pagination_link">Next</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="tab-pane" id="twitter">	
			<div class="row">
				<div class="col-xs-12">
					<ul class="nav nav-tabs settings_tab">
						<li class="setting_item active auto">
							<a class="setting_link" href="#main-feed" data-toggle="tab">
								<i class="ti-folder"></i>
								Main Feed
							</a>
						</li>
						<li class="setting_item auto">
							<a class="setting_link" href="#mentions" data-toggle="tab">
								<i class="ti-book"></i>
								Mentions
							</a>
						</li>
						<li class="setting_item auto">
							<a class="setting_link" href="#send-tweets" data-toggle="tab">
								<i class="ti-new-window"></i>
								Sent Tweets
							</a>
						</li>			
					</ul>
				</div>
			</div>
			<div class="tab-content settings_content m-b20">
				<div class="tab-pane active" id="main-feed">			
					<div class="row">
						<div class="col-xs-12">
							<div class="web_radar m-t20 pull_border">
								<div class="web_radar_content dTable web_twitter">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable web_twitter">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable web_twitter">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable web_twitter">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable web_twitter">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane" id="mentions">			
					<div class="row">
						<div class="col-xs-12">
							<div class="web_radar m-t20 pull_border">
								<div class="web_radar_content dTable">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane" id="send-tweets">			
					<div class="row">
						<div class="col-xs-12">
							<div class="web_radar m-t20 pull_border">
								<div class="web_radar_content dTable">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
								<div class="web_radar_content dTable">
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date">03.27.2015 06.29 am</p>
											<i class="fa fa-reply"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-twitter-square i-twitter"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane" id="google">
			<div class="row">
				<div class="col-xs-12">
					<div class="web_radar m-t20 pull_border">
						<div class="web_radar_content dTable web_google">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable web_google">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>	
						<div class="web_radar_content dTable web_google">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>	
						<div class="web_radar_content dTable web_google">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>	
						<div class="web_radar_content dTable web_google">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>	
						<div class="web_radar_content dTable web_google">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>	
						<div class="web_radar_content dTable web_google">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-google-plus-square i-google"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<a href="">+1</a>
								</div>
							</div>
						</div>		
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane" id="linkedin">	
			<div class="row">
				<div class="col-xs-12">
					<div class="web_radar m-t20 pull_border">
						<div class="web_radar_content dTable web_linkedin">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-linkedin-square i-linkedin"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<div class="clearfix social_like">
										<a href=""><i class="ti-thumb-up"></i></a>
										<a href=""><i class="ti-thumb-down"></i></a>
										<a href="" class="show_comments"><i class="ti-comment-alt"></i></a>
									</div>
									<div class="web_comments">
										<div class="form-group">
											<textarea rows="5" class="form-control" placeholder="Write a comment"></textarea>
										</div>
										<div class="pull-right">
											<button class="btn btn-save">Post comment</button>
										</div>
									</div>									
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable web_linkedin">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-linkedin-square i-linkedin"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<div class="clearfix social_like">
										<a href=""><i class="ti-thumb-up"></i></a>
										<a href=""><i class="ti-thumb-down"></i></a>
										<a href="" class="show_comments"><i class="ti-comment-alt"></i></a>
									</div>
									<div class="web_comments">
										<div class="form-group">
											<textarea rows="5" class="form-control" placeholder="Write a comment"></textarea>
										</div>
										<div class="pull-right">
											<button class="btn btn-save">Post comment</button>
										</div>
									</div>									
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable web_linkedin">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-linkedin-square i-linkedin"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<div class="clearfix social_like">
										<a href=""><i class="ti-thumb-up"></i></a>
										<a href=""><i class="ti-thumb-down"></i></a>
										<a href="" class="show_comments"><i class="ti-comment-alt"></i></a>
									</div>
									<div class="web_comments">
										<div class="form-group">
											<textarea rows="5" class="form-control" placeholder="Write a comment"></textarea>
										</div>
										<div class="pull-right">
											<button class="btn btn-save">Post comment</button>
										</div>
									</div>									
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable web_linkedin">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-linkedin-square i-linkedin"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<div class="clearfix social_like">
										<a href=""><i class="ti-thumb-up"></i></a>
										<a href=""><i class="ti-thumb-down"></i></a>
										<a href="" class="show_comments"><i class="ti-comment-alt"></i></a>
									</div>
									<div class="web_comments">
										<div class="form-group">
											<textarea rows="5" class="form-control" placeholder="Write a comment"></textarea>
										</div>
										<div class="pull-right">
											<button class="btn btn-save">Post comment</button>
										</div>
									</div>									
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable web_linkedin">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-linkedin-square i-linkedin"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<div class="clearfix social_like">
										<a href=""><i class="ti-thumb-up"></i></a>
										<a href=""><i class="ti-thumb-down"></i></a>
										<a href="" class="show_comments"><i class="ti-comment-alt"></i></a>
									</div>
									<div class="web_comments">
										<div class="form-group">
											<textarea rows="5" class="form-control" placeholder="Write a comment"></textarea>
										</div>
										<div class="pull-right">
											<button class="btn btn-save">Post comment</button>
										</div>
									</div>									
								</div>
							</div>
						</div>
						<div class="web_radar_content dTable web_linkedin">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date">03.27.2015 06.29 am</p>
									<i class="fa fa-linkedin-square i-linkedin"></i>
									<img src="images/avatar/enot.jpg" class="web_radar_image" alt="">
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<div class="clearfix social_like">
										<a href=""><i class="ti-thumb-up"></i></a>
										<a href=""><i class="ti-thumb-down"></i></a>
										<a href="" class="show_comments"><i class="ti-comment-alt"></i></a>
									</div>
									<div class="web_comments">
										<div class="form-group">
											<textarea rows="5" class="form-control" placeholder="Write a comment"></textarea>
										</div>
										<div class="pull-right">
											<button class="btn btn-save">Post comment</button>
										</div>
									</div>									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>

		<div class="tab-pane" id="youtube">
			<div class="row">
				<div class="col-xs-12">
					<div class="web_radar m-t20 pull_border">
						<div class="web_radar_content dTable web_youtube">
							<div class="dRow">
								<div class="dCell cellImg">
									<a href="">
										<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
									</a>
								</div>
								<div class="dCell">
									<p class="web_radar_date"><a href="">Ilya</a> year ago</p>
									<i class="fa fa-youtube-square i-youtube"></i>
									<p class="web_radar_text">
										Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
										Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
										Donec hendrerit iaculis tellus eget consectetur. 
										Vestibulum sed elit id magna mattis viverra ac quis dui. 
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
										Duis et elit semper, tempus ante vel, fermentum lectus. 
										Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
									</p>
									<div class="clearfix social_like m-b20">
										<a href=""><i class="ti-thumb-up"></i></a>
										<a href=""><i class="ti-thumb-down"></i></a>
										<a href="" class="show_comments"><i class="ti-comment-alt"></i></a>
										<a href=""><i class="ti-close"></i></a>
									</div>
									<div class="dRow">
										<div class="dCell cellImg">
											<a href="">
												<img class="web_radar_picture" src="images/avatar/avatar1.png" alt="">
											</a>
										</div>
										<div class="dCell">
											<p class="web_radar_date"><a href="">Ilya</a> year ago</p>
											<i class="fa fa-youtube-square i-youtube"></i>
											<p class="web_radar_text">
												Acquire Facebook followers with the help of our Social marketing program <a href="">http://t.co/jWyEmpPNMF</a> <a href="">#SMM</a>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer et sem mi. 
												Nunc at risus diam. Donec tincidunt porttitor metus at varius. 
												Donec hendrerit iaculis tellus eget consectetur. 
												Vestibulum sed elit id magna mattis viverra ac quis dui. 
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
												Duis et elit semper, tempus ante vel, fermentum lectus. 
												Quisque porttitor ipsum eros, non sodales turpis cursus nec. 
											</p>
											<div class="clearfix social_like m-b20">
												<a href=""><i class="ti-thumb-up"></i></a>
												<a href=""><i class="ti-thumb-down"></i></a>
												<a href="" class="show_comments"><i class="ti-comment-alt"></i></a>
												<a href=""><i class="ti-close"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>