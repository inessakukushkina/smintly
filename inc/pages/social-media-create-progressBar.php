<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Create Social Media</h1>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-xs-12">
			<ul class="nav nav-tabs settings_tab">
				<li class="setting_item active auto">
					<a class="setting_link" href="#post_updates" data-toggle="tab">
						<i class="ti-export"></i>
						Post updates
					</a>
				</li>
				<li class="setting_item auto">
					<a class="setting_link" href="#post_rss" data-toggle="tab">
						<i class="ti-rss-alt"></i>
						Post RSS
					</a>
				</li>				
			</ul>
		</div>
	</div>
	<div class="row ">
		<div class="col-xs-12">
			<div class="tab-content settings_content">
				<div class="tab-pane active" id="post_updates">
					<p class="text_color strong-size">Post To</p>
					<div class="row">
						<div class="col-xs-12 custom-form">
							<label class="cb-checkbox regRoboto m-r10">
								<input type="checkbox">
								<cite class="icon-small-group"></cite>
								Raccoon Group
							</label>
							<label class="cb-checkbox regRoboto m-r10">
								<input type="checkbox">
								<cite class="icon-small-group"></cite>
								Chamomile group
							</label>
							<label class="cb-checkbox regRoboto">
								<input type="checkbox">
								<cite class="icon-small-group"></cite>
								Raccoon Group
							</label>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12 m-t10 col-lg-6 col-md-8">
							<p class="text_color strong-size">Status update message</p>
							<div class="form-group">
								<textarea rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-t5">
							<p class="text_color strong-size">Image / Video</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-4 col-lg-3">

						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-4 col-lg-3">
							<div class="well well-standart">
								<i class="fa fa-image"></i>
								<button class="btn-save fileSelect">
									Upload photo
								</button>
								<input class="form-control uploadbtn" type="file" multiple="">
							</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3">
							<div class="well well-standart">
								<i class="fa fa-play"></i>
								<button class="btn-save fileSelect">
									Upload video
								</button>
								<input class="form-control uploadbtn" type="file" multiple="">
							</div>
						</div>
					</div>
					.row>.col-sm-6 col-md-4 col-lg-3
					<div class="row">
						<div class="col-sm-6">
							<p class="text_color strong-size">Url</p>
							<div class="form-group">
								<input type="text" class="form-control"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-t5">
							<p class="text_color strong-size">When to post</p>							
						</div>
					</div>
					<div class="row custom-form">
						<div class="col-xs-12">
							<label class="cb-radio regRoboto m-r10">
								<input type="radio" name="radio-group">
								Immediate
							</label>
							<label class="cb-radio regRoboto">
								<input type="radio" name="radio-group">
								Schedule Date
							</label>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="post_rss">
					<p class="text_color strong-size">Post To</p>
					<div class="row">
						<div class="col-xs-12">
							<label class="cb-checkbox regRoboto m-r10">
								<input type="checkbox">
								<cite class="ti-facebook blue-color"></cite>
								Facebook
							</label>
							<label class="cb-checkbox regRoboto m-r10">
								<input type="checkbox">
								<cite class="ti-twitter blue-color"></cite>
								Twitter
							</label>
							<label class="cb-checkbox regRoboto">
								<input type="checkbox">
								<cite class="ti-linkedin blue-color"></cite>
								Linkedin
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-t5">
							<p class="text_color strong-size">When to post</p>							
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<label class="cb-radio regRoboto m-r10">
								<input type="radio" name="when-post">
								Immediate
							</label>
							<label class="cb-radio regRoboto">
								<input type="radio" name="when-post">
								Schedule Date
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-7">
							<p class="text_color strong-size p-t10">Select feed</p>	
						</div>
						<div class="col-sm-5 m-b20">
							<select class="chosen-select">
								<option value="">Apple Top 10 Songs</option>
								<option value="">CB</option>
								<option value="">Menhealth</option>
							</select>
						</div>
						<div class="col-xs-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="feed">
										<div class="feed_block">
											<div class="clearfix">
												<label class="cb-radio regRoboto w-100">
													<input type="radio" name="radio-song">
													Love Me Like You Do (From "Fifty Shades of Grey") - Ellie Goulding
												</label>
												<img class="feed_picture" src="images/avatar/enot.jpg"/>	
												<a href="" class="link">https://itunes.apple.com/us/album/love-me-like-you...</a>
												<p class="m-t10">
													Love Me Like You Do (From "Fifty Shades of Grey") 
													Fifty Shades of Grey (Original Motion Picture Soundtrack) 
													Ellie Goulding 
													Genre: Soundtrack 
													Price: $1.29
													Release Date: February 10, 2015 ©
													℗ 2015 Universal Studios and Republic Records a division of UMG Recordings, Inc.
												</p>
												<a href="" class="link">iTunes Store</a>
											</div>
											
										</div>
										<div class="feed_block">
											<div class="clearfix">
												<label class="cb-radio regRoboto w-100">
													<input type="radio" name="radio-song">
													See You Again (feat. Charlie Puth) - Wiz Khalifa
												</label>
												<img class="feed_picture" src="images/avatar/enot.jpg"/>	
												<a href="" class="link">https://itunes.apple.com/us/album/love-me-like-you...</a>
												<p class="m-t10">
													See You Again (feat. Charlie Puth) Furious 7 (Original Motion Picture Soundtrack) Wiz Khalifa Genre: Hip-Hop/Rap Price: $1.29 Release Date: March 17, 2015 © ℗ This compilation 2015 Universal Studios and Atlantic Recording Corporation for the United States and WEA International Inc. for the world outside of the United States.
												</p>
												<a href="" class="link">iTunes Store</a>
											</div>
										</div>
										<div class="feed_block">
											<div class="clearfix">
												<label class="cb-radio regRoboto w-100">
													<input type="radio" name="radio-song">
													See You Again (feat. Charlie Puth) - Wiz Khalifa
												</label>
												<img class="feed_picture" src="images/avatar/enot.jpg"/>	
												<a href="" class="link">https://itunes.apple.com/us/album/love-me-like-you...</a>
												<p class="m-t10">
													See You Again (feat. Charlie Puth) Furious 7 (Original Motion Picture Soundtrack) Wiz Khalifa Genre: Hip-Hop/Rap Price: $1.29 Release Date: March 17, 2015 © ℗ This compilation 2015 Universal Studios and Atlantic Recording Corporation for the United States and WEA International Inc. for the world outside of the United States.
												</p>
												<a href="" class="link">iTunes Store</a>
											</div>
										</div>
										<div class="feed_block">
											<div class="clearfix">
												<label class="cb-radio regRoboto w-100">
													<input type="radio" name="radio-song">
													See You Again (feat. Charlie Puth) - Wiz Khalifa
												</label>
												<img class="feed_picture" src="images/avatar/enot.jpg"/>	
												<a href="" class="link">https://itunes.apple.com/us/album/love-me-like-you...</a>
												<p class="m-t10">
													See You Again (feat. Charlie Puth) Furious 7 (Original Motion Picture Soundtrack) Wiz Khalifa Genre: Hip-Hop/Rap Price: $1.29 Release Date: March 17, 2015 © ℗ This compilation 2015 Universal Studios and Atlantic Recording Corporation for the United States and WEA International Inc. for the world outside of the United States.
												</p>
												<a href="" class="link">iTunes Store</a>
											</div>
										</div>
										<div class="feed_block">
											<div class="clearfix">
												<label class="cb-radio regRoboto w-100">
													<input type="radio" name="radio-song">
													See You Again (feat. Charlie Puth) - Wiz Khalifa
												</label>
												<img class="feed_picture" src="images/avatar/enot.jpg"/>	
												<a href="" class="link">https://itunes.apple.com/us/album/love-me-like-you...</a>
												<p class="m-t10">
													See You Again (feat. Charlie Puth) Furious 7 (Original Motion Picture Soundtrack) Wiz Khalifa Genre: Hip-Hop/Rap Price: $1.29 Release Date: March 17, 2015 © ℗ This compilation 2015 Universal Studios and Atlantic Recording Corporation for the United States and WEA International Inc. for the world outside of the United States.
												</p>
												<a href="" class="link">iTunes Store</a>
											</div>
										</div>
										<div class="feed_block">
											<div class="clearfix">
												<label class="cb-radio regRoboto w-100">
													<input type="radio" name="radio-song">
													See You Again (feat. Charlie Puth) - Wiz Khalifa
												</label>
												<img class="feed_picture" src="images/avatar/enot.jpg"/>	
												<a href="" class="link">https://itunes.apple.com/us/album/love-me-like-you...</a>
												<p class="m-t10">
													See You Again (feat. Charlie Puth) Furious 7 (Original Motion Picture Soundtrack) Wiz Khalifa Genre: Hip-Hop/Rap Price: $1.29 Release Date: March 17, 2015 © ℗ This compilation 2015 Universal Studios and Atlantic Recording Corporation for the United States and WEA International Inc. for the world outside of the United States.
												</p>
												<a href="" class="link">iTunes Store</a>
											</div>
										</div>
										<div class="feed_block">
											<div class="clearfix">
												<label class="cb-radio regRoboto w-100">
													<input type="radio" name="radio-song">
													See You Again (feat. Charlie Puth) - Wiz Khalifa
												</label>
												<img class="feed_picture" src="images/avatar/enot.jpg"/>	
												<a href="" class="link">https://itunes.apple.com/us/album/love-me-like-you...</a>
												<p class="m-t10">
													See You Again (feat. Charlie Puth) Furious 7 (Original Motion Picture Soundtrack) Wiz Khalifa Genre: Hip-Hop/Rap Price: $1.29 Release Date: March 17, 2015 © ℗ This compilation 2015 Universal Studios and Atlantic Recording Corporation for the United States and WEA International Inc. for the world outside of the United States.
												</p>
												<a href="" class="link">iTunes Store</a>
											</div>
										</div>
										<div class="feed_block">
											<div class="clearfix">
												<label class="cb-radio regRoboto w-100">
													<input type="radio" name="radio-song">
													See You Again (feat. Charlie Puth) - Wiz Khalifa
												</label>
												<img class="feed_picture" src="images/avatar/enot.jpg"/>	
												<a href="" class="link">https://itunes.apple.com/us/album/love-me-like-you...</a>
												<p class="m-t10">
													See You Again (feat. Charlie Puth) Furious 7 (Original Motion Picture Soundtrack) Wiz Khalifa Genre: Hip-Hop/Rap Price: $1.29 Release Date: March 17, 2015 © ℗ This compilation 2015 Universal Studios and Atlantic Recording Corporation for the United States and WEA International Inc. for the world outside of the United States.
												</p>
												<a href="" class="link">iTunes Store</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="pull-right m-t20 m-b40">
				<button class="btn btn-save">Post update</button>
			</div>
		</div>
	</div>
</div>