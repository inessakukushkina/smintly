<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Social reports</h1>
			<div class="row">
				<div class="col-xs-12">
					<ul class="breadcrumbs">
						<li class="breadcrumbs_item">
							<a href="" class="breadcrumbs_link">Social Media</a>
						</li>
						<li class="breadcrumbs_item active">
							<a href="" class="breadcrumbs_link">Social reports</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-sm-6">
			<div class="block_content m-b20">
				<div class="row">
					<div class="col-xs-12 clearfix ">
						<h2 class="block_content_title pull-sm-left w-100">
							<span class="pull-sm-left">
								<i class="ti-facebook"></i> Facebook
							</span>
							<span class="pull-sm-right">
								<span class="likes_count"><span>1</span> Likes</span>								
							</span>
						</h2>
					</div>
				</div>				
				<div class="block_content_body">
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group date_calendar">
								<input type="text" class="form-control input_date">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group date_calendar">
								<input type="text" class="form-control input_date">
							</div>
						</div>
						<div class="col-sm-3">
							<button class="btn btn-save">Apply</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-b10">
							<span class="link">Trending likes</span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="shedule_report">
								<img src="images/pictures/shedule.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="block_content m-b20">	
				<div class="row">
					<div class="col-xs-12 clearfix ">
						<h2 class="block_content_title pull-sm-left w-100">
							<span class="pull-sm-left">
								<i class="ti-twitter"></i> Twitter
							</span>
							<span class="pull-sm-right">
								<span class="likes_count"><span>55</span> Followers</span>								
							</span>
						</h2>
					</div>
				</div>	
				<div class="block_content_body">
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group date_calendar">
								<input type="text" class="form-control input_date">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group date_calendar">
								<input type="text" class="form-control input_date">
							</div>
						</div>
						<div class="col-sm-3">
							<button class="btn btn-save">Apply</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-b10">
							<span class="link">Trending followers</span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="shedule_report">
								<img src="images/pictures/shedule.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="block_content m-b20">
				<div class="row">
					<div class="col-xs-12 clearfix">
						<h2 class="block_content_title pull-sm-left w-100">
							<span class="pull-sm-left">
								<i class="ti-google"></i> Google
							</span>
							<span class="pull-sm-right">
								<span class="likes_count"><span>10</span> Friends</span>								
							</span>
						</h2>
					</div>
				</div>
				<div class="block_content_body">
					<div class="row">						
						<div class="col-sm-3">
							<div class="form-group date_calendar">
								<input type="text" class="form-control input_date">
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group date_calendar">
								<input type="text" class="form-control input_date">
							</div>
						</div>
						<div class="col-sm-3">
							<button class="btn btn-save">Apply</button>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 m-b10">
							<span class="link">Trending friends</span>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="shedule_report">
								<img src="images/pictures/shedule.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>