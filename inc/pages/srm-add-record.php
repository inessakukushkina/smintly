<div class="p-rl30 p-tb20">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="head_title">CRM</h1>
        </div>
    </div>
</div>
<div class="main_block">
    <div class="row">
        <div class="col-xs-12">
            <p class="black large-size">Add record</p>
        </div>
    </div>
    <form action="" method="POST">
        <div class="row">
            <div class="col-md-10 col-lg-8">
                 <div class="row">
                    <div class="col-sm-6">
                        <p class="text_color strong-size">First Name *</p>
                        <div class="form-group">
                            <input class="form-control" value="" name="firstname"/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <p class="text_color strong-size">Last Name *</p>
                        <div class="form-group">
                            <input class="form-control" value="" name="lastname"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <p class="text_color strong-size">Email *</p>
                        <div class="form-group">
                            <input class="form-control" name="email" value=""/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <p class="text_color strong-size">Phone</p>
                        <div class="form-group">
                            <input class="form-control" type="text" name="phone" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <p class="text_color strong-size">Company</p>
                        <div class="form-group">
                            <input class="form-control" type="text" name="company" value=""/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <p class="text_color strong-size">Address</p>
                        <div class="form-group">
                            <input class="form-control" type="text" name="address" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <p class="text_color strong-size">Website</p>
                        <div class="form-group">
                            <input class="form-control" type="text" name="website" value=""/>
                            <p class="error_text">
                                <i class="fa fa-exclamation-circle"></i>
                                The website address is not valid
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <p class="text_color strong-size">Notes</p>
                        <div class="form-group">
                            <input class="form-control" type="text" name="notes" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <p class="text_color strong-size">Facebook profile link</p>
                        <div class="form-group">
                            <input class="form-control" type="text" name="facebook_link" value=""/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <p class="text_color strong-size">Twitter profile link</p>
                        <div class="form-group">
                            <input class="form-control" type="text" name="twitter_link" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <p class="text_color strong-size">Instagram profile link</p>
                        <div class="form-group">
                            <input class="form-control" type="text" name="instagram_link" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button class="btn btn-save m-tb20 pull-right">Save</button>
                    </div>
                </div>                
            </div>
        </div>
    </form>
</div>