<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Subscriptions settings</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<ul class="breadcrumbs">
				<li class="breadcrumbs_item">
					<a href="" class="breadcrumbs_link">Settings</a>
				</li>
				<li class="breadcrumbs_item active">
					Subscriptions settings
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-xs-12">
			<a href="" class="link">Create new payment</a>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<table class="responsive-table m-t20">
				<thead class="table_head">
					<tr>
						<th>Subscription</th>
						<th>Start</th>
						<th>End</th>
						<th>Cost</th>
						<th>Currency</th>
					</tr>
				</thead>
	            <tbody>               
                    <tr id="1" class="row-error">
                        <td data-th="Subscription">Trial</td>
                        <td data-th="Start">2014-07-14</td>
                        <td data-th="End">2014-08-14</td>
                        <td data-th="Cost">1.00</td>
                        <td data-th="Currency">USD</td>
                    </tr>
                    <tr id="7" class="row-error">
                        <td data-th="Subscription">Enterprise</td>
                        <td data-th="Start">2014-11-01</td>
                        <td data-th="End">2015-04-30</td>
                        <td data-th="Cost">29.00</td>
                        <td data-th="Currency">USD</td>
                    </tr>
                    <tr id="28" class="row-error">
                        <td data-th="Subscription">Enterprise</td>
                        <td data-th="Start">2014-09-17</td>
                        <td data-th="End">2014-10-17</td>
                        <td data-th="Cost">299.00</td>
                        <td data-th="Currency">USD</td>
                    </tr>
                    <tr id="61" class="row-success">
                        <td data-th="Subscription">Enterprise</td>
                        <td data-th="Start">2015-05-14</td>
                        <td data-th="End">2016-05-14</td>
                        <td data-th="Cost">160.00</td>
                        <td data-th="Currency">USD</td>
                    </tr>
                </tbody>
            </table>
		</div>
	</div>
</div>