<div class="p-rl30 p-tb20">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="head_title">Web Radar</h1>
		</div>
	</div>
</div>
<div class="main_block">
	<div class="row">
		<div class="col-sm-8">
			<div class="date_range p-tb10">
				<div class="reportrange" >
					<i class="fa fa-calendar"></i>
					<span></span>
					<b class="caret"></b>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<select class="chosen-select">
				<option value="">All keywords</option>
				<option value="">paris</option>
				<option value="">Social marketing</option>
				<option value="">Social marketing software</option>
				<option value="">Your keyword</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<p class="large-size m-t20 p-b10 b-Bottom text_color">
				No mentioned
			</p>	
		</div>
	</div>
</div>