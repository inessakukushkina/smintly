<?php
	$_GET['id'] = isset($_GET['id']) ? $_GET['id'] : 'dashboard';
	$id = $_GET['id'];
?>
<?php require_once 'inc/config/all.php'; ?>
<?php require_once 'inc/modules/header.php';?>
<?php 
	if($id == 'sign-in' || $id == 'sign-up' || $id == 'forgot-password' || $id == 'reset-password' || $id == 'payment'){
		require_once 'inc/modules/nav-header-sign.php';
	} else{
		require_once 'inc/modules/nav-header-login.php';
	}
	?>
<?php
	if ($id == 'settings-notification'){
		echo '<div class="notification notify_good">
			<div class="container">
				<p class="notify_text"><i class="fa fa-check"></i>
					Save successful. Your data has been successfully saved.
					<i class="fa fa-remove close_block"></i>
				</p>				
			</div>
		</div>';
	}
	if($id == 'settings-wait'){
		echo '<script>
			$("html").addClass("shadow-lock");
			</script>
			<div class="notification notify_wait">
			<div class="container">
				<p class="notify_text">
					Please wait...
				</p>				
			</div>
		</div>';
	}
?>
<div class="page-wrapper">
	<div class="wrapper">		
		<?php
			if($id !== 'sign-in' && $id !== 'sign-up' && $id !== 'forgot-password' && $id !== 'reset-password' && $id !== 'plans' && $id !== 'payment'){
				require_once 'inc/modules/sidebar.php';
				echo '<div class="main">';
			} else {
				echo '<div class="main sign_in">';
			}	
		?>
		
			<?php 
				require_once "inc/pages/" . $id . ".php"; 
			?>
			<?php
				/*if($id !== 'sign-in' && $id !== 'sign-up' && $id !== 'forgot-password' && $id !== 'reset-password'){
					echo'<footer class="footer">
						<p class="footer_text">&copy; 2015  <a href="" class="bold link">Repucaution</a>. All rights reserved</p>
					</footer>';
				}*/
			?>
		</div>			
	</div>
</div>

<?php require_once 'inc/modules/footer.php';?>