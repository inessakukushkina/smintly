var hideBlock = function ($mainBlock, $hideBlock){
	$mainBlock.on('click', function(e) {
		if ($hideBlock.css('display') != 'block') {
			$mainBlock.toggleClass('active')
			$hideBlock.slideToggle('slow');
			var firstClick = true;
	        $(document).bind('click.myEvent', function(e) {
	            if (!firstClick && $(e.target).closest($hideBlock).length == 0) {
	                $hideBlock.slideUp('slow');
	                $mainBlock.removeClass('active')
	                $(document).unbind('click.myEvent');
	            }
	            firstClick = false;
	        });
		}
		e.preventDefault();
	})
}

var sidebarShow = function($link){
	//$link.closest('li').find('ul').removeClass('active');
    $link.on('click', function(){ 
    	var $this = $(this);
    	if($this.next('ul').length){      		
    		var $item 		= $this.closest('li'),    			
    			$parentItem = $item.parent();
    			$item_link  = $parentItem.find('a'),
    			selfClick 	= $item.find('ul:first').is(':visible');
				if(!selfClick) {
					$parentItem.find('> li ul:visible').slideToggle();
					//$item.removeClass('active');
					$item_link.removeClass('active');
				}
				$item.find('ul:first').slideToggle();
				//$item.toggleClass('active');

				$this.toggleClass('active');
				//$item.find('ul:first').slideToggle();
				return false;
    	}
    });
}

$.fn.equivHeight = function (){
	var $blocks = $(this),
		maxH    = $blocks.eq(0).height(); 			
	$blocks.each(function(){
		maxH = ( $(this).height() > maxH ) ? $(this).height() : maxH + 5 + '%';				
	});
	$blocks.height(maxH); 
}

var showSidebar = function(){
	var $this 		= $(this),
		$sidebar 	= $('.sidebar'),
		$main 		= $('.main'),
		$window 	= $(window);			
	$('.btn-menu').on('click', function(){
		/*if(($(window).width() > 992) === true){
			if($sidebar.hasClass('active')){
				$sidebar.slideUp('slow');
				$sidebar.removeClass('active');
				$main.animate({
					'margin-left':'0px'
				},200);
			} else{
				$sidebar.addClass('active');				
				$sidebar.slideDown('slow');				
				$main.animate({
					'margin-left':'249px'
				},200);
			}	
		} else{*/
			$sidebar.slideToggle('slow');
		//}					
	})
}

var resizeFooter = function(){
	$('.main').removeAttr('style');
	$('.footer').removeAttr('style');
	if(($('.main_block').height() + 40) > $(window).height()){
		$('.main').height($(window).height() - 70 + 'px');
	} else {
			if($('.main').height() < $(window).height()){
			$('.main').height($(window).height() - 70 + 'px');
			$('.footer').css({
				'bottom':'0'
			})
		} else{
			$('.main').removeAttr('style');		
			$('.footer').css({
				'position':'relative'
			})
		}
	}
}

var radarContent = function(){
	$('.radar_content').height($('.box_dashboard').height() - $('.web_radar').find('.block_content_title').height() - $('.web_radar').find('.block_content_footer').height() - 43 + 'px'); 
}

var disabledInput = function(){
	var $disabled = $('.form-group').find('[disabled]');
	if($disabled){
		$disabled.closest('.form-group').addClass('disabled');
	}
}

var closeBlock = function($clickBlock, $blockCLose){
	$clickBlock.on('click', function(){
		$(this).closest($blockCLose).slideUp('slow');
	})
}
var showBlock = function($clickBlock, $dadBlock, $blockShow){
	$clickBlock.on('click', function(e){
		e.preventDefault();
		$(this).closest($dadBlock).find($blockShow).slideToggle();
	})
}

var removeMore = function($remove_more){
	$(document).on('click', $remove_more, function(e){
		e.preventDefault();
		$(this).closest('.row').remove();
		resizeFooter();
	})
}

var forBlockFooter = function($click_block){
	$(document).on('click', $click_block, function(){
		resizeFooter();
	})
}

var rating = function(){
	$('.progress-pie-chart').each(function(){
	    var $this 	= $(this),
	        rating 	= parseFloat($this.data('percent')),
	        percent = (rating/5)*100;
	        deg = 360*percent/100;
	    if (percent > 50) {
	        $this.addClass('gt-50');
	    }	   
	    $this.find('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
	    $this.find('.ppc-percents span').html('<cite class="bold">'+ rating +'</cite>');
	});
}

var uploadFile = function($uploadBlock){
	$(this).each(function(){
		$('.fileSelect').on('click', function(e){
			e.preventDefault();
			$(this).closest($uploadBlock).find('.uploadbtn').click();
		})
	})		
}

var socialBorder = function($mainBlock){
	$mainBlock.each(function(){
		var $_this = $(this),
			$_icon = $_this.find('i');
		if($_icon.hasClass('i-twitter')){
			$_this.css('border-left', '2px solid #2fabe1');
			$_this.hover(function(){
				$_this.find('.i-twitter').css('color', '#2fabe1');
			}, function() {
				$_this.find('.i-twitter').removeAttr('style');
			})
		} else if($_icon.hasClass('i-facebook')){
			$_this.css('border-left', '2px solid #3c5b9b');
			$_this.hover(function(){
				$_this.find('.i-facebook').css('color', '#3c5b9b');
			}, function() {
				$_this.find('.i-facebook').removeAttr('style');
			})
		} else if($_icon.hasClass('i-google')){
			$_this.css('border-left', '2px solid #f63e28');
			$_this.hover(function(){
				$_this.find('.i-google').css('color', '#f63e28');
			}, function() {
				$_this.find('.i-google').removeAttr('style');
			})
		} else if($_icon.hasClass('i-linkedin')){
			$_this.css('border-left', '2px solid #0079BA');
			$_this.hover(function(){
				$_this.find('.i-linkedin').css('color', '#0079BA');
			}, function() {
				$_this.find('.i-linkedin').removeAttr('style');
			})
		} else if($_icon.hasClass('i-instagram')){
			$_this.css('border-left', '2px solid #B17D4E');
			$_this.hover(function(){
				$_this.find('.i-instagram').css('color', '#B17D4E');
			}, function() {
				$_this.find('.i-instagram').removeAttr('style');
			})
		} else if($_icon.hasClass('i-youtube')){
			$_this.css('border-left', '2px solid #e62117');
			$_this.hover(function(){
				$_this.find('.i-youtube').css('color', '#e62117');
			}, function() {
				$_this.find('.i-youtube').removeAttr('style');
			})
		}
	})
}

var masonryBlock = function(){
	var $container = $('.account_block');
	$container.imagesLoaded( function() {
		$container.masonry({
			itemSelector: '.account_item',
			columnWidth: '.account_item',
			transitionDuration: 0
		});
	});
}

var chooseAccountGroup = function($item, $dadBlock){
	$item.find('label').on('click', function(e){		
		e.preventDefault();
		var $this = $(this);
		$this.closest($dadBlock).find($item).removeClass('account_checked');
		$this.parent().addClass('account_checked');
	})
}

var showKeyword = function($clickBlock, $slideBlock){
	$clickBlock.on('click', function(e){
		e.preventDefault();
		$(this).closest('.form-group').find($slideBlock).slideToggle();
	})
}

CountGoods = {
	numericValidationString: /^[0-9]+$/
};

function ValidateNumeric(input){
	return CountGoods.numericValidationString.test(input);
}
var quantityInput = function($quantityBlock){
	var $incriments = $quantityBlock.find('.incrementBtn');
	var $decrements = $quantityBlock.find('.decrementBtn');
    $incriments.on('click', function () {  
    	var	$quantity = $(this).closest('.form-group').find('.quantity');
        if (ValidateNumeric($quantity.val())) {
            var number = parseInt($quantity.val());
            number--;
            if (number < 0) number = 0;
            $quantity.val(number);
        }
        else $quantity.val(0);

        return false;
    });

 	$decrements.on('click', function () {
 		var $quantity = $(this).closest('.form-group').find('.quantity');
        if (ValidateNumeric($quantity.val())) {
            var number = parseInt($quantity.val());
            number++;
            if (number < 0) number = 0;
            $quantity.val(number);
        }
        else $quantity.val(0);

        return false;
    });
};

var pieChart = function(){
	$('.progress-pie').each(function(){
	    var $ppc = $(this),
	        percent = parseFloat($ppc.data('percent')),
	        deg = 360*percent/100;
	    if (percent > 50) {
	        $ppc.addClass('gt-50');
	    }   

	    $ppc.find('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
	    $ppc.find('.ppc-percents span').html('<cite>'+ percent +'</cite>'+' %');
	})
}