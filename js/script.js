$(document).ready(function() {	
	hideBlock($('.user-nav_link'), $('.sub_menu'));
	sidebarShow($('.sidebar a'));

	showSidebar();
	$('.custom-form').checkBo();

	hideBlock($('.collapse-button'), $('.head_nav'));

	if(!$('.web_radar').length){
		$('.box_dashboard').attr('style', 'width:100% !important;');
		$('.box_content').addClass('col-lg-3');
	}
	$('.clear').on('click', function(e){
		e.preventDefault();
		$(this).closest('.form-group').find('.form-control').removeAttr('value');
		$(this).remove();
	});

	$('.chosen').chosen({
		disable_search_threshold: 1,
		no_results_text: "Nothing found!"
	});

	$('.chosen-single').find('div').addClass('button-select');

	$('.button-select').find('b').addClass('fa').addClass('fa-angle-down');	

	$(".chosen-search").remove();

	$('.cb-remove').on('click', function(){
		$(this).parent().parent().remove();
	});	

	showKeyword($('.show_block'), '.toggle_block');
	showKeyword($('.show_followers'), '.followers_block');
	showKeyword($('.show_time'), '.time_block');

	quantityInput($('.clone_block'));
	quantityInput($('.quantity_block'));
	
	$(document).on('click', '.btn-add', function(){
		$('.clone_block').removeClass('clone_block');
		$(this).closest('.row').prev('.row').find('.insert_block').clone(true)
		.insertAfter($(this).closest('.row').prev('.row').find('.past_block'))
		.removeClass('hidden').removeClass('insert_block').addClass('clone_block');
		
		quantityInput($(this).closest('.row').prev('.row').find('.clone_block'));
		/*$('.row').find('select').each(function(){
			$(this).removeClass('chosen-select').css('display','block').next().remove();			
			$(this).addClass('chosen-select');
		});*/
	});	

	$(document).on('click', '.more-link', function(){
		$(this).closest('.row').find('.more_block').clone(true).insertBefore($(this).parent()).removeClass('hidden').removeClass('more_block');
	});

	$('.invite_user').on('click', function(e){
		e.preventDefault();
		$(this).closest('.row').next('.row').slideToggle();
	})

	$('.input_date').datepicker();

	$('#welcome').modal();

	$('.time_date').datetimepicker({
		//inline: true,
		sideBySide: true,
        icons: {
           	time: 'ti-time',
            date: 'ti-timer',
            up: 'ti-angle-up',
            down: 'ti-angle-down',
            previous: '',
            next: '',
            today: 'ti-calendar',
            clear: 'ti-trash',
            close: 'ti-close'
        },
	});

	$('.nav-tabs a').on('click', function (e) {
		e.preventDefault()
		$(this).tab('show');		
	})

	var availableTags = [
      "ActionScript",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme"
    ];
	$(".google_places input").autocomplete({
        source: availableTags,
        appendTo: '.autocomplete_block'
    });

	rating();
	pieChart();

	//socialBorder($('.web_radar_content'));
	socialBorder($('.radar_body'));
	socialBorder($('.post_content'));

	showBlock($('.show_comments'), '.web_radar_content', '.web_comments');

	removeMore('.remove_more');

	uploadFile('.well-standart');
	uploadFile('.well_photo');

	disabledInput();
	closeBlock($('.close_block'), '.notification');
	closeBlock($('.fa-remove'), '.validate');

	masonryBlock();
	chooseAccountGroup($('.choose_account_list_item'), '.choose_account_list')

	$("[name='website']").on('keyup',function() {
	    var $_this = $(this);
	    var myVariable = $_this.val();
	    if(!/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(myVariable)){
	        $_this.closest('.form-group').addClass('has-error');
	    } else {	        
	        $_this.closest('.form-group').removeClass('has-error');
	    }
	});

	//var description = document.getElementsByClassName('text-desciption');

	/*function SelectClass(myClass){
		return document.getElementsByClassName(myClass);
	}*/
	/*var selectors = {
		selectClass: null,
		getClass: function(myClass){
			this.selectClass = document.getElementsByClassName(myClass);
			return this;
		}
	}
	selectors.getClass('text-desciption').addEventListener("keyup", function(){
	//SelectClass('text-desciption').addEventListener("keyup", function(){
		var len = this.value.length;
		var counter = SelectClass('char-counter');
		const count = 140;
	    if (len >= count) {
	      val.value = val.value.substring(0, count);
	    } else {
	      counter[0].innerHTML = '(<strong>' + (count - len) + '</strong> | ' + count + ')';
	    }
	})*/

	$('.text-desciption').on('keyup', function(){
		var len = this.value.length;
		var counter = document.getElementsByClassName('char-counter');
		const count = 140;
	    if (len >= count) {
	      val.value = val.value.substring(0, count);
	    } else {
	      counter[0].innerHTML = '(<strong>' + (count - len) + '</strong> | ' + count + ')';
	    }
	});
}) // close Ready