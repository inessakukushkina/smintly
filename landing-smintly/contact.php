<?php
function validateAndSend() {
    $errors = array();

    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
//    if(empty($name)) {
//        $errors['name'] = 'Can`t be blank';
//    }
//    if(empty($message)) {
//        $errors['message'] = 'Can`t be blank';
//    }
//    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
//        $errors['email'] = 'Invalid email';
//    }

    if(!empty($errors)) {
        echo json_encode(array(
            'success' => false,
            'errors' => $errors
        ));
    } else {
        $success = send($name, $email, $message);
        echo json_encode(array(
            'success' => $success,
            'errors' => ($success) ? '' : array('message' => 'Error while sending email.')
        ));
    }
}
function send($name, $email, $message) {
    $to      = 'hello@smintly.com';
    $subject = 'From smintly.com';
    $headers = "From: {$name}<{$email}> \r\n" .
        'Reply-To: poul.shumskiy@gmail.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    return mail($to, $subject, $message, $headers);
}

validateAndSend();
?>