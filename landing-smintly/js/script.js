$(window).scroll(function(){
    var $window_top = $(window).scrollTop();
    if( $window_top > $(".header").height()){
        $(".header").addClass("is-scrolling");
    } else {
        $(".header").removeClass("is-scrolling");
    }

});

$(window).load(function() {
    $('.banner').nivoSlider({
        effect: 'fade',
        directionNav: false,
        prevText: '',
    	nextText: '', 
    });    
   
});
var fitHeight = function () {
    $('.fit-height').css('max-height', window.innerHeight);
};

var btnAvatar = function(){	
	var $_btn 	= $('[data-role]');
	var $src 	= $('.testimonials__content__picture');
	$_btn.each(function(i){
		var $_this 	= $(this);
		var $_index = $_this.parent().index() + 1;
		$_this.attr('style', 'background-image:url(' + $src.eq(i).attr('src') + ')');		
	})
};

var hideBlock = function ($mainBlock, $hide){	
	$mainBlock.on('click', function(e) {
		var $_this = $(this);
		var $_blockHide = $_this.parent().find($hide);
		$('.nav_point').slideUp('slow');
		if ($_blockHide.css('display') != 'block') {
			$_blockHide.slideDown('slow');
			var firstClick = true;
	        $(document).bind('click.myEvent', function(e) {
	            if (!firstClick && $(e.target).closest($_blockHide).length == 0) {
	                $_blockHide.slideUp('slow');
	                $(document).unbind('click.myEvent');
	            }
	            firstClick = false;
	        });
		} else{
			$_blockHide.slideUp('slow');
		}		
		e.preventDefault();
	})
};

function PlanContent(){
    var plan = document.querySelectorAll(".tafif__plan__content");
    var condition = document.querySelectorAll(".tafif__plan__condition");
    for (var i = 1; i < 45; i++) {
        if(i<15){
            plan[i].style.height = condition[i].offsetHeight + 'px';
        } else if(i>15 && i<30){
            plan[i].style.height = condition[i - 15].offsetHeight + 'px';
        } else if(i>30 && i<45){
            plan[i].style.height = condition[i - 30].offsetHeight + 'px';
        }
    }        
}

$(document).ready(function(){
	$('.testimonials').slick({
	  dots: true,
	  arrows: false,
	  speed: 500,
	  infinite:false
	});

	//fitHeight();
	btnAvatar();
	hideBlock($('.btn-menu'), '.nav');
	hideBlock($('.btn-sign'), '.sign__');
	$('.scroll').on('click', function () { 
		var elementClick = $(this).attr("href")
        var destination = $(elementClick).offset().top;
        $("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 800);        
      	return false;
	});

    var $form = $('#contact-form');
    $form.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The name is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9 ]+$/,
                        message: 'The name can only consist of alphabetical, number and spaces'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            message: {
                validators: {
                    notEmpty: {
                        message: 'The message is required and can\'t be empty'
                    }
                }
            }
        }
    }).on('success.form.bv', function(e) {
        // Prevent form submission
        e.preventDefault();
        // Get the form instance
        var $form = $(e.target);
        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');
        // Use Ajax to submit form data
        $.post($form.attr('action'), $form.serialize(), function(result) {
            $('#contact-us').after('<p><strong>Your message successfully sent.</strong></p>');
            $form[0].reset();
        }, 'json');
    });

    PlanContent();
});

window.onresize = function(event) {
    PlanContent();
};