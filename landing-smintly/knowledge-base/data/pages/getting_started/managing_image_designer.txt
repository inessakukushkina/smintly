  * **How to use Image designer?**  



  * **Image designer** makes your posts unique. You can choose a photo from the gallery or download yours. There is an option to type text on the image. You can choose font type and color.
 

 
  * 1. Select **Social Media** on the **Dashboard** and click **Create**.

{{:wiki:10.png|}}

  * 2. Type your **Head text** and choose font. There is an option to add logo and **secondary text** too.

{{:wiki:рол.png|}}

  * 3. Choose the type of the image. You can also upload an image from your computer and make the same changes

{{:wiki:ало.png|}}

  * 4. Click **Post update** or [[getting_started:schedule_a_post|Schedule post]] -> Post update.