  * **How to share a link?**



  * 1. Select **Social Media** on the **Dashboard** and click **Create**.

{{:wiki:10.png|}}

  * 2. Insert the link you want to share here.

{{:wiki:df6d654c80c263ebe1ad075c0b2dc4f5.png|}}
