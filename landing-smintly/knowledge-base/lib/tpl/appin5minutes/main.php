<?php
/**
 * DokuWiki Default Template 2012
 *
 * @link     http://dokuwiki.org/template
 * @author   Anika Henke <anika@selfthinker.org>
 * @author   Clarence Lee <clarencedglee@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
header('X-UA-Compatible: IE=edge,chrome=1');

$hasSidebar = page_findnearest($conf['sidebar']);
$showSidebar = $hasSidebar && ($ACT=='show');
?><!DOCTYPE html>
<html style="height: 100%" lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="utf-8" />
    <title><?php echo tpl_pagetitle(null, true) == 'start' ?
            'Appin5minutes | Mobile applications for small business' :
            ucfirst(str_replace('_', ' ', tpl_pagetitle(null, true))); ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>

    <link href="http://appin5minutes.com/module/main/css/dop_style.css" media="screen" rel="stylesheet" type="text/css">
    <link href="http://appin5minutes.com/module/main/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <link href="http://appin5minutes.com/module/main/css/ui/themes/base/jquery-ui.css" media="screen" rel="stylesheet" type="text/css">
    <link href="http://appin5minutes.com/module/main/css/screen.css" media="all" rel="stylesheet" type="text/css">
    <link href="http://appin5minutes.com/module/main/css/royalslider.css" media="all" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic&amp;subset=latin,cyrillic-ext" media="screen" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&amp;subset=latin,cyrillic-ext" media="screen" rel="stylesheet" type="text/css">
    <link href="http://appin5minutes.com/module/core/css/parsley.css" media="screen" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="http://appin5minutes.com/module/core/js/jquery.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jquery.royalslider.custom.min.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jquery.backgroundSize.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jsrespond.min.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/english.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jsabalert.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jsabpopup.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jsmain.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/site_filesjsab.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jscommon.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jsadditional-methods.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/main/js/jsab-additional-methods.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/core/js/parsley.min.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/core/js/parsley.i18n.ru.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/core/js/parsley.locale.js"></script>
    <script type="text/javascript" src="http://appin5minutes.com/module/core/js/form.js"></script>
    <script type="text/javascript">
        (function(window) {
            window.appin5minutesGlobal = window.appin5minutesGlobal || {};
            window.appin5minutesGlobal = {"url":{"base":"\/"},"locale":"en"};
        })(this);
    </script>
</head>
<style>
    .editbutton_section{display: none;}

    html {
        position: relative;
        min-height: 100%;
    }


    .f-f {
        position: fixed;
        bottom: 0;
        width: 100%;
        height: 75px;
    }

    body > header {
        width: 100%;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 100;
        background: #FFF;
        -webkit-box-shadow: 0 2px 7px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 0 2px 7px rgba(0, 0, 0, .15);
        -o-box-shadow: 0 2px 7px rgba(0, 0, 0, .15);
        -ms-box-shadow: 0 2px 7px rgba(0, 0, 0, .15);
        box-shadow: 0 2px 7px rgba(0, 0, 0, 0.15);
    }
	.lower-block-footer {
	background-color: #535455;
	padding: 20px 0;
	color: #ccc;
}
.lower-block-footer a {
	color: #F68F1E;
}
.lower-block-footer .bullet-list {
	list-style: disc outside;
	padding: 0 0 0 20px;
	color: #F68F1E;
}
.lower-block-footer .bullet-list small { color: #ccc; }
.footer-block {
	padding: 20px;
	background-color: #434445;
}
.footer-block p > small { color: #A1A2A2;  }
.t-4 { top: 4px; }
</style>
<body style="height: 100%" class="own-column lightbox-processed tableHeader-processed">
<?php include('tpl_header.php') ?>
<div class="wraper" style="padding-top: 130px">

    <div id="dokuwiki__site">
        <div id="dokuwiki__top" class="<?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'showSidebar' : ''; ?> <?php echo ($hasSidebar) ? 'hasSidebar' : ''; ?>">
        <div class="wrapper group">
            <?php if($showSidebar && $conf['start'] != $ID){ ?>

                <div id="dokuwiki__aside"><div class="pad include group">
                    <!--<h3 class="toggle"><?php /*echo $lang['sidebar'] */?></h3>-->
                    <div class="content">
                        <?php tpl_flush() ?>
                        <?php tpl_includeFile('sidebarheader.html') ?>
                        <?php tpl_include_page($conf['sidebar'], 1, 1) ?>
                        <?php tpl_includeFile('sidebarfooter.html') ?>
                    </div>
                </div></div>
            <?php } else { ?>
            <style>#dokuwiki__content > .pad { margin-left: 0 !important;}</style>
            <?php } ?>
            <div id="dokuwiki__content"><div class="pad group">
                    <!-- BREADCRUMBS -->
                    <?php if(($conf['breadcrumbs'] || $conf['youarehere']) && $conf['start'] != $ID): ?>
                        <div class="" style="margin-left: 10px; position: relative">
                            <?php if($conf['youarehere']): ?>
                                <div class="youarehere"><?php tpl_youarehere() ?></div>
                            <?php endif ?>
                            <?php //if($conf['breadcrumbs']): ?>
                                <div class="trace"><?php tpl_breadcrumbs() ?></div>
                            <?php //endif ?>
                            <small class="line-through"></small>
                        </div>
                    <?php endif ?>
                <div class="page group">
                    <?php tpl_flush() ?>
                    <?php tpl_includeFile('pageheader.html') ?>
                    <?php tpl_content() ?>
                    <?php tpl_includeFile('pagefooter.html') ?>
                </div>

                <?php tpl_flush() ?>
            </div></div>

            <hr class="a11y" />

            <?php if (!empty($_SERVER['REMOTE_USER'])) {?>
            <div id="dokuwiki__pagetools">



                <h3 class="a11y"><?php echo $lang['page_tools']; ?></h3>
                <div class="tools">
                    <ul>
                        <?php
                            $data = array(
                                'view'  => 'main',
                                'items' => array(
                                    'edit'      => tpl_action('edit',      1, 'li', 1, '<span>', '</span>'),
                                    'revert'    => tpl_action('revert',    1, 'li', 1, '<span>', '</span>'),
                                    'revisions' => tpl_action('revisions', 1, 'li', 1, '<span>', '</span>'),
                                    'backlink'  => tpl_action('backlink',  1, 'li', 1, '<span>', '</span>'),
                                    'subscribe' => tpl_action('subscribe', 1, 'li', 1, '<span>', '</span>'),
                                    'top'       => tpl_action('top',       1, 'li', 1, '<span>', '</span>')
                                )
                            );

                            $evt = new Doku_Event('TEMPLATE_PAGETOOLS_DISPLAY', $data);
                            if($evt->advise_before()){
                                foreach($evt->data['items'] as $k => $html) echo $html;
                            }
                            $evt->advise_after();
                            unset($data);
                            unset($evt);
                        ?>
                    </ul>
                </div>
            </div>
            <?php }?>
        </div>

        <?php // include('tpl_footer.php') ?>
    </div></div>


</div>
<div class="clear"></div>
<footer class="three-cols">
    <div class="wrapper">
        <div class="container">

            <div class="item" id="more">
                <h2>Navigation</h2>
                <ul>
                    <li><a href="https://appin5minutes.com/about">About us</a></li>
                    <li><a href="http://blog.appin5minutes.com/">Blog</a></li><!--
                    <li><a href="">Become an affiliate</a></li>
                    <li><a href="">License agreement</a></li>
                    <li><a href="">Services</a></li>-->
                </ul>
            </div>
            <div class="item">
                <h2>Follow us</h2>
                <ul class="social-butt">
                    <li><a class="twitter" target="_blank" href="https://twitter.com/appin5minutes"></a></li>
                    <li><a class="fb" target="_blank" href="https://www.facebook.com/appin5minutes"></a></li>
                    <!--<li><a class="vk" href=""></a></li>-->
                </ul>
            </div>
            <div class="item contact" id="contact">
                <script>
                    $(function(){
                        $('#ik-form-contact').on('submit', function(){
                            var $this = $(this);
                            $this.ikBlockSendAjaxBlock({
                                iconLoad: true,
                                loadBlock: $this.parent(),
                                success: function(data){
                                    $this.ikBlockClearInput();
                                }
                            });
                            return false;
                        });
                    });
                </script>
                <form method="POST" id="ik-form-contact" action="https://appin5minutes.com/enroll" novalidate="novalidate">
                    <h2>Contact us</h2>
                    <ul>
                        <li class="field fit">
                            <input type="text" name="name" placeholder="Name">
                        </li>
                        <li class="field fit">
                            <input type="email" name="email" class="email" placeholder="Email">
                        </li>
                        <li class="field fit">
                            <input type="text" name="phone" placeholder="Phone number">
                        </li>
                        <li class="field message">
                            <textarea name="message" placeholder="Message"></textarea>
                        </li>
                        <li class="submit">
                            <input name="oggetto" type="hidden" value="Contact Form">
                            <button class="button" type="submit"><span>Send</span></button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        <!-- .container -->
    </div>
</footer>

<section id="bottom">
    <div class="wrapper clear">
        <p class="copy">©  2010 - 2014 All rights reserved</p>
        <div class="clear"></div>
        <hr>
        <p><small></small></p>
    </div>
</section>
<!--/ Modal -->
</body>
</html>
