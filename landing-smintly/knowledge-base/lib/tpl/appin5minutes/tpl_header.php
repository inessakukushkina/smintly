<?php
/**
 * Template header, included in the main and detail files
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
?>
<header>
    <div class="wrapper">
        <section id="main" class="" style="top: -44px;">
            <div id="center">
                <a href="/" id="logo">
                    <img src="https://appin5minutes.com/module/main/images/logo1.png" alt="Appin5minutes">
                </a>
                <nav id="main-nav">
                    <ul>
                        <li><a href="https://appin5minutes.com/">Home</a></li>
                        <li><a href="https://appin5minutes.com/features">Features</a></li>
                        <!--<li><a href="">Benefits</a></li>-->
                        <!--<li><a href="">Business cases</a></li>-->
                        <!--<li><a href="">Pricing</a></li>-->
                        <li><a href="https://appin5minutes.com/about">About us</a></li>
                        <li><a class="app-btn set-icon left" href="https://appin5minutes.com/sign-in"><i class="appi-signup"></i> Sign in</a></li>
                    </ul>
                </nav>
            </div>
        </section>
        <a class="button-mobile-menu"><i class="icon-reorder"></i></a> </div>
</header>
<!-- Modal -->


    <?php
    if (!empty($_SERVER['REMOTE_USER'])) {
        echo '<ul style="position: absolute;"><li class="user">';
        tpl_userinfo();
        tpl_action('admin', 1, 'li');
        tpl_action('profile', 1, 'li');
        tpl_action('register', 1, 'li');
        tpl_action('login', 1, 'li');
        echo '</li></ul>';
    }

    ?>


<?php if ($conf['useacl']): ?>
<div id="dokuwiki__usertools">
    <h3 class="a11y"><?php echo $lang['user_tools']; ?></h3>

</div>
<?php endif ?>


<!--
<div id="dokuwiki__header">
    <div class="pad group">

    <?php tpl_includeFile('header.html') ?>

    <div class="headings group">
        <ul class="a11y skip">
            <li><a href="#dokuwiki__content"><?php /*echo $lang['skip_to_content']; */?></a></li>
        </ul>

        <h1><?php
/*            $logoSize = array();
            $logo = tpl_getMediaFile(array(':wiki:logo.png', ':logo.png', 'images/logo.png'), false, $logoSize);

            tpl_link(
                wl(),
                '<img src="'.$logo.'" '.$logoSize[3].' alt="" /> <span>'.$conf['title'].'</span>',
                'accesskey="h" title="[H]"'
            );
        */?></h1>
        <?php /*if ($conf['tagline']): */?>
            <p class="claim"><?php /*echo $conf['tagline']; */?></p>
        <?php /*endif */?>
    </div>

    <div class="tools group">
        <?php /*if ($conf['useacl']): */?>
            <div id="dokuwiki__usertools">
                <h3 class="a11y"><?php /*echo $lang['user_tools']; */?></h3>
                <ul>
                    <?php
/*                        if (!empty($_SERVER['REMOTE_USER'])) {
                            echo '<li class="user">';
                            tpl_userinfo();
                            echo '</li>';
                        }
                        tpl_action('admin', 1, 'li');
                        tpl_action('profile', 1, 'li');
                        tpl_action('register', 1, 'li');
                        tpl_action('login', 1, 'li');
                    */?>
                </ul>
            </div>
        <?php /*endif */?>

        <div id="dokuwiki__sitetools">
            <h3 class="a11y"><?php /*echo $lang['site_tools']; */?></h3>
            <?php /*tpl_searchform(); */?>
            <div class="mobileTools">
                <?php /*tpl_actiondropdown($lang['tools']); */?>
            </div>
            <ul>
                <?php
/*                    tpl_action('recent', 1, 'li');
                    tpl_action('media', 1, 'li');
                    tpl_action('index', 1, 'li');
                */?>
            </ul>
        </div>

    </div>

    <?php /*if($conf['breadcrumbs'] || $conf['youarehere']): */?>
        <div class="breadcrumbs">
            <?php /*if($conf['youarehere']): */?>
                <div class="youarehere"><?php /*tpl_youarehere() */?></div>
            <?php /*endif */?>
            <?php /*if($conf['breadcrumbs']): */?>
                <div class="trace"><?php /*tpl_breadcrumbs() */?></div>
            <?php /*endif */?>
        </div>
    <?php //endif ?>

    <?php //html_msgarea() ?>

    <hr class="a11y" />
    </div>
</div>
-->