<?php
/**
 * Template footer, included in the main and detail files
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
?>

    <div id="contact" class="section dark container-full">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center bottom-margin top-margin">
                <div class="col-md-4">
                    <i class="li_mail"></i>
                    <strong>Email</strong>
                    <a class="dark-link" href="mailto:hello@smintly.com?subject=Smintly">hello@smintly.com</a>
                </div>
                <div class="col-md-4">
                    <i class="li_location"></i>
                    <strong>Address</strong>
                    123 Wild Horse Valley Drive, Novato, CA 94947
                </div>
                <div class="col-md-4">
                    <i>
                        <img src="http://smintly.com/img/skype.png" alt="Skype">
                    </i>
                    <strong>Skype</strong>
                    <a class="dark-link" href="skype:the_last_available_nickname?call">the_last_available_nickname</a>
                </div>

            </div>
        </div>
        <div id="contact" class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
                <form method="post" id="contactus">
                    <p>Contact us</p>
                    <!-- Name Error messages -->
                    <div id="name-error" class="error">Please enter your name.</div>
                    <input name="name" placeholder="Name" class="form-control fielderror" type="text" id="name">
                    <!-- Email messages -->
                    <div id="email-error" class="error">Please enter your valid E-mail ID.</div>
                    <input name="email" placeholder="Email" class="form-control fielderror" type="text" id="email">
                    <!-- Subject Error messages -->
                    <div id="subject-error" class="error">Please enter the subject.</div>
                    <input name="subject" placeholder="subject" class="form-control fielderror" type="text" id="subject">
                    <!-- Message Error messages -->
                    <div id="message-error" class="error">Please enter your message.</div>
                    <textarea name="message" placeholder="Message" id="message" rows="4" class="form-control fielderror"></textarea>
                    <div id="mail-success" class="success">Your message has been sent successfully.</div>
                    <div id="mail-fail" class="error"> Sorry, error occured this time sending your message.</div>
                    <input type="submit" value="Send Message" class="form-control" id="send-message">
                </form>
                <p class="notification"></p>
            </div>
        </div>
    </div>

    <div id="footer" class="minisection dark container-full">
        <div class="row" id="newsletter">
            <div class="col-md-4 text-left">
                <p class="footer-text">Copyright ©2014 Smintly.com - Brand reputation software</p>
            </div>
            <div class="col-md-8 text-left">

            </div>

        </div>
    </div>



    <!-- =========##[ JAVASCRIPTS ]=========##-->
    <!-- Placed at the end of the document so the pages load faster -->

    <!--<script src="js/validation.js"></script>-->
    <script>
        $('#contactus').on('submit', function () {
            var
                name = $('[name=name]').val(),
                email = $('[name=email]').val(),
                subject = $('[name=subject]').val(),
                message = $('[name=message]').val();

            if ($('#contactus input[type="text"]').val() == "" || $("#contactus textarea").val() == "") {
                $('#contactus input[type="text"],#contactus textarea').addClass('fielderror');
            } else {
                $.ajax({
                    type: "POST",
                    url: "http://smintly.com/functions.php",
                    data: {
                        name: name,
                        email: email,
                        subject: subject,
                        message: message
                    }
                }).success(function (html) {
                    console.log('success');
                    $('.notification').html('Your message has been sent successfuly, we\'ll feed you back as soon as we can. Thank you').fadeIn();

                }).error(function () {
                    console.log('error');
                    //$('h3.notification').html('Oops something went wrong!');
                });


                $('#contactus').slideUp().fadeOut();
            }
            return false;
        });
        $('#contactus input[type="text"],#contactus textarea').on('click', function () {
            $('#contactus input[type="text"],#contactus textarea').removeClass('fielderror');
        });
    </script>

<?php


tpl_includeFile('footer.html');
