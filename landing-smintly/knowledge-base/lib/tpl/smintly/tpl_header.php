<?php
/**
 * Template header, included in the main and detail files
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
?>

<!-- =========[ TOP NAVBAR ]=========-->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <!-- toggle button in non-desktop devices for showing menu -->
    <a class="collapsed navicon-button hidden-lg open" data-toggle="collapse" data-target=".navbar-collapse" href="#">
        <div class="navicon"></div>
    </a>
    <a id="navicon-button" class="navicon-button expand visible-lg" href="http://demo.genixtheme.com/landix/withoutslider/index.html#">
        <div class="navicon"></div>
    </a>

    <!-- menu -->
    <div id="menu">
        <a class="navbar-brand" href="#">
            <img src="http://smintly.com/img/brand-logo.png" alt="logo"></a>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav sf-menu" id="topnav">
                <li><a href="http://smintly.com/#home">Home</a></li>
                <li><a href="http://smintly.com/#showcase1">About</a></li>
                <li><a href="http://smintly.com/#features">Features</a></li>
                <li><a href="http://smintly.com/#application-preview">Preview</a></li>
                <li><a href="http://smintly.com/#priceplans">Plans</a></li>
                <li><a class="just-link" href="/knowledge-base">FAQ</a></li>
                <li><a href="http://smintly.com/#contact ">Contact </a></li>
                <li><a class="just-link" href="http://app.smintly.com">Sign In</a></li>
            </ul>
        </div>
    </div>
    <!-- end of menu -->
</nav>
<!-- =========[ END OF TOP NAVBAR ]=========-->
    <?php
    if (!empty($_SERVER['REMOTE_USER'])) {
        echo '<ul style="position: absolute;"><li class="user">';
        tpl_userinfo();
        tpl_action('admin', 1, 'li');
        tpl_action('profile', 1, 'li');
        tpl_action('register', 1, 'li');
        tpl_action('login', 1, 'li');
        echo '</li></ul>';
    }

    ?>


<?php if ($conf['useacl']): ?>
<div id="dokuwiki__usertools">
    <h3 class="a11y"><?php echo $lang['user_tools']; ?></h3>

</div>
<?php endif ?>


<!--
<div id="dokuwiki__header">
    <div class="pad group">

    <?php tpl_includeFile('header.html') ?>

    <div class="headings group">
        <ul class="a11y skip">
            <li><a href="#dokuwiki__content"><?php /*echo $lang['skip_to_content']; */?></a></li>
        </ul>

        <h1><?php
/*            $logoSize = array();
            $logo = tpl_getMediaFile(array(':wiki:logo.png', ':logo.png', 'images/logo.png'), false, $logoSize);

            tpl_link(
                wl(),
                '<img src="'.$logo.'" '.$logoSize[3].' alt="" /> <span>'.$conf['title'].'</span>',
                'accesskey="h" title="[H]"'
            );
        */?></h1>
        <?php /*if ($conf['tagline']): */?>
            <p class="claim"><?php /*echo $conf['tagline']; */?></p>
        <?php /*endif */?>
    </div>

    <div class="tools group">
        <?php /*if ($conf['useacl']): */?>
            <div id="dokuwiki__usertools">
                <h3 class="a11y"><?php /*echo $lang['user_tools']; */?></h3>
                <ul>
                    <?php
/*                        if (!empty($_SERVER['REMOTE_USER'])) {
                            echo '<li class="user">';
                            tpl_userinfo();
                            echo '</li>';
                        }
                        tpl_action('admin', 1, 'li');
                        tpl_action('profile', 1, 'li');
                        tpl_action('register', 1, 'li');
                        tpl_action('login', 1, 'li');
                    */?>
                </ul>
            </div>
        <?php /*endif */?>

        <div id="dokuwiki__sitetools">
            <h3 class="a11y"><?php /*echo $lang['site_tools']; */?></h3>
            <?php /*tpl_searchform(); */?>
            <div class="mobileTools">
                <?php /*tpl_actiondropdown($lang['tools']); */?>
            </div>
            <ul>
                <?php
/*                    tpl_action('recent', 1, 'li');
                    tpl_action('media', 1, 'li');
                    tpl_action('index', 1, 'li');
                */?>
            </ul>
        </div>

    </div>

    <?php /*if($conf['breadcrumbs'] || $conf['youarehere']): */?>
        <div class="breadcrumbs">
            <?php /*if($conf['youarehere']): */?>
                <div class="youarehere"><?php /*tpl_youarehere() */?></div>
            <?php /*endif */?>
            <?php /*if($conf['breadcrumbs']): */?>
                <div class="trace"><?php /*tpl_breadcrumbs() */?></div>
            <?php /*endif */?>
        </div>
    <?php //endif ?>

    <?php //html_msgarea() ?>

    <hr class="a11y" />
    </div>
</div>
-->