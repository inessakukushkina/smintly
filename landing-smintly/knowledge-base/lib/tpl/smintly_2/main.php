<?php
/**
 * DokuWiki Default Template 2012
 *
 * @link     http://dokuwiki.org/template
 * @author   Anika Henke <anika@selfthinker.org>
 * @author   Clarence Lee <clarencedglee@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
header('X-UA-Compatible: IE=edge,chrome=1');

$hasSidebar = page_findnearest($conf['sidebar']);
$showSidebar = $hasSidebar && ($ACT=='show');
?><!DOCTYPE html>
<html style="height: 100%" lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="utf-8" />
    <title><?php echo tpl_pagetitle(null, true) == 'start' ?
            'Social Media Management & Online Reputation Monitoring' :
            ucfirst(str_replace(':', ': ', (str_replace('_', ' ', tpl_pagetitle(null, true))))); ?> [<?php echo strip_tags($conf['title']) ?>]</title>


    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>

    <?php tpl_includeFile('meta.html') ?>

    <!-- =========[ STYLES ]=========-->
    <link href="http://smintly.com/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="http://smintly.com/css/flexslider.css" rel="stylesheet">
    <link href="http://smintly.com/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://smintly.com/css/superfish.css" rel="stylesheet">
    <link href="http://smintly.com/css/linecons.css" rel="stylesheet">
    <link href="http://smintly.com/css/style.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600" rel="stylesheet">
    <link href="http://smintly.com/css/skin2.css" rel="stylesheet">


    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://smintly.com/js/html5shiv.js"></script>
    <script src="http://smintly.com/js/respond.min.js"></script>
    <![endif]-->
    <!--[if IE 7]>
    <link rel="stylesheet" href="http://smintly.com/css/font-awesome-ie7.min.css">
    <![endif]-->

    <!-- an alternative access to jquery library when CDN is not available-->
    <script type="text/javascript">
        window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
            _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
            $.src="//v2.zopim.com/?3BCJPZit3RKtBKKE39KSemavr4vLkQ97";z.t=+new Date;$.
                type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
    </script>
    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-48438195-1', 'smintly.com');
        ga('send', 'pageview');
    </script>
    <script src="http://smintly.com/js/bootstrap.min.js"></script>
    <script src="http://smintly.com/js/jquery.easing.1.3.min.js"></script>
    <script src="http://smintly.com/js/jquery.placeholder.js"></script>
    <script src="http://smintly.com/js/jquery.nicescroll.min.js"></script>
    <script src="http://smintly.com/js/jquery.prettyPhoto.js"></script>
    <script src="http://smintly.com/js/jquery.flexslider-min.js"></script>
    <script src="http://smintly.com/js/hoverIntent.js"></script>
    <script src="http://smintly.com/js/superfish.js"></script>
    <script src="http://smintly.com/js/jquery.visible.min.js"></script>
    <script src="http://smintly.com/js/svg_fallback.js"></script>
    <script src="http://smintly.com/js/app.js"></script>
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://smintly.com/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://smintly.com/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://smintly.com/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="http://smintly.com/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="http://smintly.com/favicon.png">
</head>
<style>
    .editbutton_section{display: none;}
.level1{
    list-style: none;
}
    html {
        position: relative;
        min-height: 100%;
    }


    .f-f {
        position: fixed;
        bottom: 0;
        width: 100%;
        height: 75px;
    }

    body > header {
        width: 100%;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 100;
        background: #FFF;
        -webkit-box-shadow: 0 2px 7px rgba(0, 0, 0, 0.15);
        -moz-box-shadow: 0 2px 7px rgba(0, 0, 0, .15);
        -o-box-shadow: 0 2px 7px rgba(0, 0, 0, .15);
        -ms-box-shadow: 0 2px 7px rgba(0, 0, 0, .15);
        box-shadow: 0 2px 7px rgba(0, 0, 0, 0.15);
    }
	.lower-block-footer {
	background-color: #535455;
	padding: 20px 0;
	color: #ccc;
}
.lower-block-footer a {
	color: #F68F1E;
}
.lower-block-footer .bullet-list {
	list-style: disc outside;
	padding: 0 0 0 20px;
	color: #F68F1E;
}
.lower-block-footer .bullet-list small { color: #ccc; }
.footer-block {
	padding: 20px;
	background-color: #434445;
}
.footer-block p > small { color: #A1A2A2;  }
.t-4 { top: 4px; }
    #wiki__text{
        min-height: 350px;
    }
#dokuwiki__content .page.group{
    border: none;
}
.page.group a.media img{
    max-width: 790px;
}
.footer{
    bottom: 0;
    width: 100%;
    left: 0;
}
</style>
<body data-spy="scroll" data-target=".nav-collapse" data-offset="100" class=" svg svg" style="height: 100%" class="own-column lightbox-processed tableHeader-processed">


<div class="content">
<?php include('tpl_header.php') ?>
<div class="wraper" style="padding-top: 130px; padding-bottom: 60px; min-height:100%;">

    <div id="dokuwiki__site">
        <div id="dokuwiki__top" class="<?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'showSidebar' : ''; ?> <?php echo ($hasSidebar) ? 'hasSidebar' : ''; ?>">
        <div class="wrapper group">
            <?php if($showSidebar && $conf['start'] != $ID){ ?>

                <div id="dokuwiki__aside"><div class="pad include group">
                    <!--<h3 class="toggle"><?php /*echo $lang['sidebar'] */?></h3>-->
                    <div class="content">
                        <?php tpl_flush() ?>
                        <?php tpl_includeFile('sidebarheader.html') ?>
                        <?php tpl_include_page($conf['sidebar'], 1, 1) ?>
                        <?php tpl_includeFile('sidebarfooter.html') ?>
                    </div>
                </div></div>
            <?php } else { ?>
            <style>#dokuwiki__content > .pad { margin-left: 0 !important;}</style>
            <?php } ?>
            <div id="dokuwiki__content"><div class="pad group">
                    <!-- BREADCRUMBS -->
                    <?php if(($conf['breadcrumbs'] || $conf['youarehere']) && $conf['start'] != $ID): ?>
                        <div class="" style="margin-left: 10px; position: relative">
                            <?php //if($conf['youarehere']): ?>
                                <div class="youarehere"><?php tpl_youarehere() ?></div>
                            <?php //endif ?>
                            <?php //if($conf['breadcrumbs']): ?>
                                <div class="trace"><?php // tpl_breadcrumbs() ?></div>
                            <?php //endif ?>
                            <small class="line-through"></small>
                        </div>
                    <?php endif ?>
                <div class="page group">
                    <?php tpl_flush() ?>
                    <?php tpl_includeFile('pageheader.html') ?>
                    <?php tpl_content() ?>
                    <?php tpl_includeFile('pagefooter.html') ?>
                </div>

                <?php tpl_flush() ?>
            </div></div>

            <hr class="a11y" />

            <?php if (!empty($_SERVER['REMOTE_USER'])) {?>
            <div id="dokuwiki__pagetools">



                <h3 class="a11y"><?php echo $lang['page_tools']; ?></h3>
                <div class="tools">
                    <ul>
                        <?php
                            $data = array(
                                'view'  => 'main',
                                'items' => array(
                                    'edit'      => tpl_action('edit',      1, 'li', 1, '<span>', '</span>'),
                                    'revert'    => tpl_action('revert',    1, 'li', 1, '<span>', '</span>'),
                                    'revisions' => tpl_action('revisions', 1, 'li', 1, '<span>', '</span>'),
                                    'backlink'  => tpl_action('backlink',  1, 'li', 1, '<span>', '</span>'),
                                    'subscribe' => tpl_action('subscribe', 1, 'li', 1, '<span>', '</span>'),
                                    'top'       => tpl_action('top',       1, 'li', 1, '<span>', '</span>')
                                )
                            );

                            $evt = new Doku_Event('TEMPLATE_PAGETOOLS_DISPLAY', $data);
                            if($evt->advise_before()){
                                foreach($evt->data['items'] as $k => $html) echo $html;
                            }
                            $evt->advise_after();
                            unset($data);
                            unset($evt);
                        ?>
                    </ul>
                </div>
            </div>
            <?php }?>
        </div>


    </div></div>


</div>
<div class="clear"></div>
<?php  include('tpl_footer.php') ?>

</div>
</body>
</html>
