<?php
/**
 * Template footer, included in the main and detail files
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
?>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p class="footer__text">
                        © Copyright 2015 Smintly.com - Brand reputation software
                    </p>
                </div>
            </div>
        </div>
    </footer>

<?php


tpl_includeFile('footer.html');
