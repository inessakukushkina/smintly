<?php
/**
 * Template header, included in the main and detail files
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
?>

<!-- =========[ TOP NAVBAR ]=========-->
<header class="navbar navbar-fixed-top header">
    <div class="container">
        <button class="btn btn-menu">
            <span class="cb-bar"></span>
            <span class="cb-bar"></span>
            <span class="cb-bar"></span>
            <span class="cb-bar"></span>
        </button>
        <a href="/" class="logo">
            smintly
        </a>
        <nav class="nav nav_point">
            <ul class="nav__list">
                <li class="nav__list__item">
                    <a href="/#home" class="nav__list__item__link scroll">
                        Home
                    </a>
                </li>
                <li class="nav__list__item">
                    <a href="/#features" class="nav__list__item__link scroll">
                        Features
                    </a>
                </li>
                <li class="nav__list__item">
                    <a href="/#plans" class="nav__list__item__link scroll">
                        Plans
                    </a>
                </li>
                <li class="nav__list__item">
                    <a href="/knowledge-base/getting_started" class="nav__list__item__link scroll">
                        Help
                    </a>
                </li>
                <li class="nav__list__item">
                    <a href="/#contact" class="nav__list__item__link scroll">
                        Contact
                    </a>
                </li>
            </ul>
        </nav>
        <div class="pull-right">
            <div class="contact__">
                <p class="contact__text">
                    (302) 504 41 99
                </p>
            </div>
            <div class="sign__ nav_point">
                <ul class="sign__list">
                    <li class="sign__list__item">
                        <a href="https://app.smintly.com/auth/login" class="sign__list__item__link">
                            Log In
                        </a>
                    </li>
                    <li class="sign__list__item">
                        <a href="/#plans" class="sign__list__item__link sign__link">
                            Sign up
                        </a>
                    </li>
                </ul>
            </div>
            <button class="btn btn-sign ti-view-grid pull-right"></button>
        </div>
    </div>
    <!-- end of menu -->
</header>
<!-- =========[ END OF TOP NAVBAR ]=========-->
    <?php
    if (!empty($_SERVER['REMOTE_USER'])) {
        echo '<ul style="position: absolute;"><li class="user">';
        tpl_userinfo();
        tpl_action('admin', 1, 'li');
        tpl_action('profile', 1, 'li');
        tpl_action('register', 1, 'li');
        tpl_action('login', 1, 'li');
        echo '</li></ul>';
    }

    ?>


<?php if ($conf['useacl']): ?>
<div id="dokuwiki__usertools">
    <h3 class="a11y"><?php echo $lang['user_tools']; ?></h3>

</div>
<?php endif ?>


<!--
<div id="dokuwiki__header">
    <div class="pad group">

    <?php tpl_includeFile('header.html') ?>

    <div class="headings group">
        <ul class="a11y skip">
            <li><a href="#dokuwiki__content"><?php /*echo $lang['skip_to_content']; */?></a></li>
        </ul>

        <h1><?php
/*            $logoSize = array();
            $logo = tpl_getMediaFile(array(':wiki:logo.png', ':logo.png', 'images/logo.png'), false, $logoSize);

            tpl_link(
                wl(),
                '<img src="'.$logo.'" '.$logoSize[3].' alt="" /> <span>'.$conf['title'].'</span>',
                'accesskey="h" title="[H]"'
            );
        */?></h1>
        <?php /*if ($conf['tagline']): */?>
            <p class="claim"><?php /*echo $conf['tagline']; */?></p>
        <?php /*endif */?>
    </div>

    <div class="tools group">
        <?php /*if ($conf['useacl']): */?>
            <div id="dokuwiki__usertools">
                <h3 class="a11y"><?php /*echo $lang['user_tools']; */?></h3>
                <ul>
                    <?php
/*                        if (!empty($_SERVER['REMOTE_USER'])) {
                            echo '<li class="user">';
                            tpl_userinfo();
                            echo '</li>';
                        }
                        tpl_action('admin', 1, 'li');
                        tpl_action('profile', 1, 'li');
                        tpl_action('register', 1, 'li');
                        tpl_action('login', 1, 'li');
                    */?>
                </ul>
            </div>
        <?php /*endif */?>

        <div id="dokuwiki__sitetools">
            <h3 class="a11y"><?php /*echo $lang['site_tools']; */?></h3>
            <?php /*tpl_searchform(); */?>
            <div class="mobileTools">
                <?php /*tpl_actiondropdown($lang['tools']); */?>
            </div>
            <ul>
                <?php
/*                    tpl_action('recent', 1, 'li');
                    tpl_action('media', 1, 'li');
                    tpl_action('index', 1, 'li');
                */?>
            </ul>
        </div>

    </div>

    <?php /*if($conf['breadcrumbs'] || $conf['youarehere']): */?>
        <div class="breadcrumbs">
            <?php /*if($conf['youarehere']): */?>
                <div class="youarehere"><?php /*tpl_youarehere() */?></div>
            <?php /*endif */?>
            <?php /*if($conf['breadcrumbs']): */?>
                <div class="trace"><?php /*tpl_breadcrumbs() */?></div>
            <?php /*endif */?>
        </div>
    <?php //endif ?>

    <?php //html_msgarea() ?>

    <hr class="a11y" />
    </div>
</div>
-->