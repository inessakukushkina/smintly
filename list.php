<?php require_once 'inc/modules/header.php';?>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<h3 class="text-center blue-color">Repucaution list pages</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<a href="guidline.php" class="link">Guidline</a>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<h4 class="text-center">Pages count: <span></span></h4>
		</div>
	</div>
	<div class="row list">
		<div class="col-sm-4">
			<h4>Main menu</h4>
			<ul>				
				<li>Settings
					<ul>
						<li><a href="index.php?id=settings">Settings</a> <span class="attention">MAIN PAGE</span></li>
					</ul>
					<ul>
						<li><a href="index.php?id=settings-personal">Personal page</a>
							<ul>
								<li><a href="index.php?id=settings-personal-alert">Alert</a></li>
							</ul>
						</li>
						<li><a href="index.php?id=settings-directory">Directory page</a></li>
						<li><a href="index.php?id=settings-google-keywords">Google Places Keywords</a>
							<ul>
								<li><a href="index.php?id=settings-google-keywords-alert">Alert</a></li>
							</ul>
						</li>
						<li>Social Media
							<ul>
								<li><a href="index.php?id=settings-social-media-configurate">Configure your account</a></li>
								<li><a href="index.php?id=settings-social-media-account">Account</a></li>
								<li><a href="index.php?id=settings-social-media-create-group">Create Group</a></li>
								<li><a href="index.php?id=settings-social-media-group">Group</a></li>
								<li><a href="index.php?id=settings-edit-account">Edit account</a></li>
							</ul>
						</li>
						<li><a href="index.php?id=settings-user-search-keyword">User Search Keywords</a></li>
						<li><a href="index.php?id=settings-social-keywords">Social Keywords</a></li>
						<li>Analytics
							<ul>
								<li><a href="index.php?id=settings-analytics-select">Select</a></li>
								<li><a href="index.php?id=settings-analytics-empty">Empty</a></li>
								<li><a href="index.php?id=settings-analytics-loading">Loading</a></li>
								<li><a href="index.php?id=settings-analytics-account">Analitics account</a></li>
								<li><a href="index.php?id=settings-analytics-sites">Choose site</a></li>
							</ul>
						</li>	
						<li><a href="index.php?id=settings-rss">Rss</a></li>	
						<li>Collaboration Team
							<ul>
								<li><a href="index.php?id=settings-team-empty">Empty</a></li>
								<li><a href="index.php?id=settings-team-users">Users</a></li>
							</ul>
						</li>	
						<li><a href="index.php?id=substructions">Subscriptions settings</a></li>
						<br/>
						<br/>
						<br/>
						<li><a href="index.php?id=plans">Tarif plans</a></li>
						<li><a href="index.php?id=settings-notification">Notification</a></li>
						<li><a href="index.php?id=settings-wait">Waiting</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="col-sm-4">
			<h4>Sidebar</h4>
			<ul>
				<li><a href="index.php?id=dashboard">Dashboard</a></li>
				<li><a href="index.php?id=web-radar">Web Radar</a>
					<ul>
						<li><a href="index.php?id=web-radar-empty">Empty</a></li>
					</ul>
				</li>
				<li><a href="index.php?id=review">Review</a>
					<ul>
						<li><a href="index.php?id=review-empty">Empty</a></li>
					</ul>
				</li>
				<li>CRM
					<ul>
						<li><a href="index.php?id=crm-activity">Client activity</a></li>
						<li><a href="index.php?id=crm-directory">Directory</a></li>
						<li><a href="index.php?id=crm-profile">Profile</a></li>
						<li><a href="index.php?id=srm-add-record">Add record</a></li>
					</ul>
				</li>
				<li><a href="index.php?id=google-rank">Google rank</a>
					<ul>
						<li><a href="index.php?id=google-rank-empty">Empty</a></li>
					</ul>
				</li>
				<li>Social Media
					<ul>
						<li><a href="index.php?id=social-media-create">Create</a></li>
						<li><a href="index.php?id=social-media-activity">Social Activity</a></li>
						<li><a href="index.php?id=shedule-post">Shelude Post</a></li>
						<li><a href="index.php?id=social-report">Social Reports</a></li>
						<li><a href="index.php?id=analytics">Analytics</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="col-sm-4">
			<h4>Sign In</h4>
			<ul>
				<li><a href="index.php?id=sign-in">Sign In</a></li>
				<li><a href="index.php?id=sign-up">Sign Up</a></li>
				<li><a href="index.php?id=forgot-password">Forgot password</a></li>
				<li><a href="index.php?id=reset-password">Reset password</a></li>
			</ul>
		</div>
	</div>
</div>
<script>
	var $count = $('body').find('li a').length;
		$('body').find('h4 span').text($count);
</script>