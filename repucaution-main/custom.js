/*! 

 *  Written for 	  : http://repucaution.com/

 *  Author      	  : @IkantamCorp - @ElmahdiMahmoud

 *  Updated     	  : 21.11.13

 *  Further changes   : @ElmahdiMahmoud

 */


$(window).load(function() {
    $('.main__banner').nivoSlider({
        effect: 'fade',
        directionNav: true,
        controlNav:true,
        prevText: '',
        nextText: '', 
    });    
   
});
(function($) {

    $(document).ready(function() {



        // open mobile menu

        $(".navbar-toggle").on("click", function(){

            $(".navbar-collapse").slideToggle();

        });



        //set class to IE10

        if (document.body.style.msTouchAction != undefined) {

            document.documentElement.className += "ie10";

        }

        //fixed header

        $(window).scroll(function() {

            var y = $(this).scrollTop(),

                navDocs = $('.docs-nav'),

                header = $('body > header'),

                docSide = $('.span9.docs');



            if (y >= 50) {

                header.addClass('fixed');

                navDocs.addClass('fixed');

                docSide.addClass('push-left');

            } else {

                header.removeClass('fixed');

                navDocs.removeClass('fixed');

                docSide.removeClass('push-left');

            }

        });

        //featured slider

        var

            current = 0,

            $slideImgf = $('.slider > div:first'),

            slideCount = $('.slider > div').length,

            slideImg = $('.slider > div'),

            $bullets = $('<a>').attr("href", "#"),

            figure = $('.slider-wrapper figure'),

            firstFig = $('.slider-wrapper figure:first');



        firstFig.show();



        //show first image slide

        $slideImgf.css({

            display: 'block'

        });



        $('.slider-navigation a').on('click', function() {

            if ($(this).hasClass('active')) return false;

            var _this = $(this),

                _index = _this.index();

            $("a.active").removeClass("active");

            $(this).addClass("active");

            var tabName = $(this).find('i').data('tab');

            showSliderTab(tabName);

            $(slideImg).fadeOut(800).eq(_index).fadeIn(1600);

            $(figure).hide().eq(_index).show();

            return false;

        });



        function showSliderTab(tabName)

        {

            var tab = $('div.featr-'+tabName);

            if (!tab.hasClass('active')) {

                $('[class^="featr-"]').each(function(){

                    $(this).removeClass('active');

                });

                tab.addClass('active');

            }

        }



        $('.controls button').on('click', function() {

            var $this = $(this),

                index = $this.index();



            var $shown = slideImg.not(':hidden'),

                shownIndex = $shown.index(),

                nextIndex,

                minIndex = 0,

                maxIndex = slideImg.children().length - 1;



            var $visible = figure.not(':hidden'),

                visibleIndex = $visible.index(),

                nxtIndex,

                indexMin = 0,

                indexMax = figure.children().length - 1;





            if ($this.hasClass('prev')) {

                nextIndex = shownIndex - 1;

                nxtIndex = visibleIndex - 1;

            } else {

                nextIndex = shownIndex + 1;

                nxtIndex = visibleIndex + 1;

            }

            if (nextIndex < minIndex) {

                nextIndex = maxIndex;

            }

            if (nextIndex > maxIndex) {

                nextIndex = minIndex;

            }



            if (nxtIndex < indexMin) {

                nxtIndex = indexMax;

            }

            if (nxtIndex > indexMax) {

                nxtIndex = indexMin;

            }





            $shown.fadeOut(800);

            $visible.hide();

            $('a.active').removeClass("active");

            $('.slider-navigation a').eq(nextIndex).addClass("active");



            $(slideImg[nextIndex]).fadeIn(1600);

            $(figure[nxtIndex]).show();

        });



        $('a[href=#top]').on('click', function() {

            $('body,html').animate({

                scrollTop: 0

            }, 1000);

            return false;

        });

        $('nav li a').on('click', function() {



            var distance = 120;

            $('html,body').animate({

                scrollTop: $(this.hash).offset().top - distance

            }, 1000);



            return false;

        });



        $('.docs-nav a').on('click', function() {



            var distance = 120;

            $('html,body').animate({

                scrollTop: $(this.hash).offset().top + 60 - distance

            }, 1000);

            $('.docs-nav .nav-list > li > a.active').removeClass('active');

            $(this).addClass('active');

            return false;

        });





        $('.getModal').on('click', function() {

            $('#myModal').modal();

            return false;

        });





        $('#contactForm').on('submit', function() {

            var $this = $(this);



            var

                fname = $this.find('[name=firstName]').val(),

                lname = $this.find('[name=lastName]').val(),

                email = $this.find('[name=email]').val(),

                message = $this.find('[name=message]').val();



            if ($('#contactForm input[type="text"]').val() == "" || $('#contactForm input[type="email"]').val() == "" || $("#contactForm textarea").val() == "") {

                $('#contactForm input[type="text"],#contactForm input[type="email"],#contactForm textarea').addClass('fielderror');

            } else {

                $.ajax({

                    type: "POST",

                    url: "functions.php",

                    data: {

                        fname: fname,

                        lname: lname,

                        email: email,

                        message: message

                    }

                }).success(function(html) {

                    $('h3.notification').html('Your message has been sent successfuly, we\'ll feed you back as soon as we can.<br/\> Thank you').addClass('successmsg');

                }).error(function() {

                    $('h3.notification').html('Oops something went wrong!').addClass('errormsg');

                });

                $('#contactForm .control-group').hide('slow');

            }

            return false;

        });

        $('#contactForm input[type="text"],#contactForm textarea').on('click', function() {

            $('#contactForm input[type="text"],#contactForm textarea').removeClass('fielderror');

        });

        $('#contactusForm').on('submit', function() {

            var

                fname = $('[name=urName]').val(),

                emailAddress = $('[name=emailAddress]').val(),

                message = $('[name=urMsg]').val();



            if ($('#contactusForm input[type="text"]').val() == "" || $('#contactusForm input[type="email"]').val() == "" || $("#contactusForm textarea").val() == "") {

                $('#contactusForm input[type="text"],#contactusForm input[type="email"],#contactusForm textarea').addClass('fielderror');

            } else {

                $.ajax({

                    type: "POST",

                    url: "functions.php",

                    data: {

                        urName: fname,

                        emailAddress: emailAddress,

                        urMsg: message

                    },

                }).success(function(html) {

                    $('h3.notification').html('Your message has been sent successfuly, we\'ll feed you back as soon as we can.<br/\> Thank you').addClass('successmsg');

                }).error(function() {

                    $('h3.notification').html('Oops something went wrong!').addClass('errormsg');

                });

                $('#contactusForm .control-group').hide('slow');

            }

            return false;

        });



        $('#contactusForm input[type="text"],#contactusForm textarea').on('click', function() {

            $('#contactusForm input[type="text"],#contactusForm textarea').removeClass('fielderror');

        });

    }); //end doc ready





    $('#form_subscribe').submit(function(e) {

        e.preventDefault();

        var $this = $(this),

        email = $('#input_subscribe_email').val()

            , $notification = $this.find('h5')

        ;



        $notification.empty();



        $.post("functions.php",

            {

                email: email,

                action: 'popup'

            },

            null,

            'json'

        ).success(function(data) {

                console.log(data);

                if (!data.result && data.error) {

                    $notification.html(data.error).addClass('errormsg');

                }

                if (data.result && data.url) {

                    $('#subox')

                        .modal('hide')

                        .on('hidden', function () {

                            $('body').empty();

                        window.location.href = data.url;

                    });

                }



            }).error(function() {

                $notification.html('Oops something went wrong!').addClass('errormsg');

            });



    });





})(jQuery);





(function($) {

    $.fn.slider = function(options) {



        var settings = $.extend({

            'speed': 300

        }, options);

        var isA = false;

        return this.each(function() {



            var

                sliderWrapper = $(this),

                sliderList = sliderWrapper.children('ul'),

                sliderItem = sliderList.children('li'),

                buttons = sliderWrapper.children('button'),

                sliderItew = sliderWrapper.width(),

                slides = sliderItem.length,



                animateSlider = function(direction) {

                    sliderList.stop(true, true).animate({

                        "margin-left": direction + "=" + sliderItew

                    }, settings.speed, null, function() {

                        isA = false;

                    });

                },



                isAtStart = function() {

                    return parseInt(sliderList.css('margin-left'), 10) === 0;

                },



                isAtEnd = function() {

                    var

                        imageWidth = sliderItem.first().width(),

                        imageCount = sliderItem.length,

                        maxMargin = -1 * (imageWidth * (imageCount - 1));

                    return parseInt(sliderList.css('margin-left'), 10) === maxMargin;

                };



            (slides < 1 || slides === 1) ? buttons.hide() : buttons.show();



            sliderList.css({ width: slides * sliderItew });

            sliderItem.css({ width: sliderItew });



            buttons.on('click', function() {

                var

                    _this = $(this),

                    isBackBtn = _this.hasClass('prev');



                if ((isBackBtn && isAtStart()) || (!isBackBtn && isAtEnd())) {

                    return;

                }

                if (!isA) {

                    isA = true;

                    animateSlider((isBackBtn ? '+' : '-'));

                }



            });



        });



    };



    $("#slider-1").slider({

        'speed': 800

    });



    $('.accordion h3').on('click', function() {

        var $this = $(this);

        $this.next('p').toggle();

    });



})(jQuery);