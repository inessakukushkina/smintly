<!DOCTYPE html>
<!--[if lt IE 9]><script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<!--[if IE 8 ]><html class="ie8"><![endif]-->
<html>
<head>
	<title>FAQ - RepuCaution</title>
	<meta name="google-site-verification" content="yINKC1HicZWAdtt6aEgq7gRXaJO_aF_DMcgYZcn6k4E" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Earn money with us! Join RepuCaution software affiliate programm and get up to 30% sales commission.">
	<meta name="keywords" content="Software, affiliate, comission" />
	<meta name="author" content="RepuCaution LLC">

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="css/master.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css">
	<!-- css -->
	
	<!-- favicon -->
	<link rel="icon" type="image/png" href="favicon.png">
	<!--[if IE]><link rel="shortcut icon" href="favicon.ico"/><![endif]-->
	<!--/ favicon -->
</head>
<body>

<?php include 'includes/header.php'; ?>
<?php include 'includes/modules/modal.php'; ?>
<section class="container posts">
	<h1>FAQ</h1>
	
	<!-- post -->
	<article class="post clearfix accordion">
		<div class="post-content">
        <div class="panel">
        <h3>Where can I learn more about Repucaution's functionality?</h3>
            <p>
                Please check <a href="/knowledge-base">Knowledge Base</a>.
            </p>
        </div>
        <div class="panel">
		<h3>Is it a script that I can install on my own hosting?</h3>
			<p>
				Yes, Repucaution is delivered as .zip archive with the source code and Installation instruction. You can easily launch it on your server and use without limitations. Check <a href="/installation">Installation</a> instruction page for more info about the server requirements.
			</p>
		</div>	
		
		<div class="panel">
		<h3>Is the Software open-source? What technologies does it use?</h3>
			<p>
				The software is 100% open source, it is built on PHP using CodeIgniter framework and utilizes MySQL database.
			</p>
		</div>	
				
		<div class="panel">	
		<h3>Does it only work with 1 business or can I set up multiple businesses?</h3>
			<p>
				You can add as many accounts as you want. There is no limitation. 
			</p>
		</div>	

		<div class="panel">	
		<h3>Will you be updating, adding any new functionality to the script? Would you be charging for it or will it be free upgrades?</h3>
			<p>
				We release new features and improvements every few weeks. All the updates are completely free of charge. Please look at <a href="/release">release log</a> and upcoming features pages for more info about the subject.
			</p>
		</div>	

		<div class="panel">	
		<h3>Can we change the UI?</h3>
			<p>
				Absolutely. You may customize it as you want or ask use for customization service. Look at <a href="/services">Services</a> page for more information.
			</p>
		</div>	

		<div class="panel">	
		<h3>Can I sell the software as SaaS for my clients?</h3>
			<p>
				Of course you can launch it as SaaS solution and charge your clients a subscription fee. Repucaution goes with Paypal subscription module so you can set up your own SaaS in 1 hour.
			</p>	
		</div>	

		<div class="panel">	
		<h3>Can I re-sell the software copies?</h3>
			<p>
                However there is an option to buy a special Developer's License with extended permissions, it allows distributing the source code. Contact us for more information.
			</p>
		</div>
		<!--
		<div class="panel">	
		<h3>How can I buy it?</h3>
			<p>
				All orders processed manually at this time. Please contact us using contact form on the site or via skype. After we clarify all details you will receive an invoice by email and after the payment is complete we will send you the software. 
			</p>	
		</div>	
		-->

		</div>
	</article>
	<!--/ post -->
	

	
</section>

<?php include 'includes/footer.php'; ?>
<?php include 'includes/overall/footer.php'; ?>