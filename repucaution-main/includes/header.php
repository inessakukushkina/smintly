	<!-- header -->
	<header>
		<!-- container -->
		<div class="container">
			<!-- brand logo/slogan -->
			<a href="http://repucaution.com" class="brand">
				<img src="images/form-logo.png" alt="Brand Reputation Monitoring and Social CRM " />
			</a>
			<!--/ brand logo/slogan -->

			<!-- navigation menu -->
			<nav class="pull-right top-55">
	          	<button type="button" class="navbar-toggle">
	            	<span class="icon-bar"></span>
	            	<span class="icon-bar"></span>
	            	<span class="icon-bar"></span>
	          	</button>

				<ul class="inline navbar-collapse">
				
					<li><a href="https://app.smintly.com/auth/register/6">Demo</a></li>
					<li><a href="http://scripts.ikantam.com/social-media-marketing-software.html" target="_blank">Download</a></li>
					
					<li><a href="http://repucaution.com#features">Features</a></li>
                    <!--<li><a href="http://repucaution.com#convenience">For clients</a></li>-->
                    <!--<li><a href="http://repucaution.com#support">Support</a></li>-->
                    <li><a href="http://repucaution.com/installation">Installation</a></li>
                    <li><a href="http://repucaution.com/services">Services</a></li>
                    <li><a href="http://repucaution.com/faq">FAQ</a></li>
                    <li><a href="http://repucaution.com/release">Releases</a></li>
                    <li><a href="http://repucaution.com/blog">Blog</a></li>
                    
                    <!--
					<li><a href="http://repucaution.com/knowledge-base/">Knowledge base</a></li>
					<li><a href="http://repucaution.com/affiliate">Affiliate</a></li>
					-->
          			<li><h4 class="text-orange" style="margin-top: 0;">+302 504 4199</h4></li>
                    <li><a href="#myModal" class="btn orange getModal">Contact</a></li>
				</ul>

                <div class="push-right get-odal-box">
                	<a href="#myModal" class="btn grey getModal">Contact us</a>
					<a href="#top"><i class="icon-top-arr"></i></a>
                </div>
			</nav>
			<!--/ navigation menu -->
		</div>
		<!--/ container -->
	</header>
	<!--/ header -->
	
	<div class="clear"></div>