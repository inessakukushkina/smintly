	<!-- banner -->
	<section class="banner">
		<img src="images/brand_reputation_monitoring_software.jpg" alt="Reputation script" class="intro large" />
		<div class="container clearfix">
			<img src="images/manage_social_media.png" alt="Social media script" class="intro small" />
			<div class="row-fluid">
			<div class="banner-text span6">
				<h2>
					<strong>RepuCaution -</strong> 
					Hosted software solution for social media channels management and brand reputation monitoring
				</h2>
            </div>
            </div>
		</div>		
	</section>
	<!--/ banner -->
	
	<article class="intro-offer">
	<h3>
		<strong>$399</strong>
		<small>for the hosted license</small>
	</h3>
	<h4>NO EXTRA FEES, UNLIMITED USAGE</h4>
	
	<a href="http://siteplo.com/gp/dashboard" target="_blank" class="btn orange brdr-btm btn-large">
		View Demo
	</a>
	<hr class="sharp" />
	</article>