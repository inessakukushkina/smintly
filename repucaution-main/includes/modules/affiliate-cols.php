	<!-- features -->
	<section class="container features inner-t" id="features">

	<div class="row-fluid">
  		<div class="span4">
  			<img src="images/Getsy-high-commision.png" alt="High affiliate sales commission" class="text-center" />
  			<h3>High Commission</h3>
  			<p>
  				We earn sales by your affiliate links. Accordingly, we would like to give you an extra motivation to join our affiliate program and to promote our products as much as they could be promoted. That's why we offer you 30% commission off every real sale made through your links.
  			</p>
  		</div>
  		<div class="span4">
  			<img src="images/Getsy-tracking-panel.png" alt="Online affiliate tracking dashboard" class="text-center" />
  			<h3>Online Tracking Panel</h3>
  			<p>
  				In order to simplify the using of our affiliate program all the sales and income you made as well as links and banners that you use, are easy of access when you log in to your affiliate account.
  			</p>
  		</div>
  		<div class="span4">
  			<img src="images/Getsy-more.png" alt="More info" class="text-center" />
  			<h3>Do you want to know more?</h3>
  			<p>
  			   If you have extra questions about our affiliate program you can read more detailed information here or contact us.
  			</p>
  		</div>
	</div>
	</section>
	<!--/ features -->	
	
	<div class="clear"></div>

	<div class="text-center">
		<a class="btn orange outline btn-large btn-market" href="http://magento.ikantam.com/store/affiliate-program" target="_blank">
			SIGN UP NOW
		</a>
	</div>
	
	<div class="clear"></div>
	<hr class="sharp" />
	
	<section class="container features">
	<div class="row-fluid">
  		<div class="span12">
		<div class="lower-form">
		<?php //include 'functions.php'; ?>
  		<h3>Contact Us</h3>
	    <form id="contactusForm" class="contactForm" method="post">
	    	<div class="control-group">
	    		<div class="controls">
	    		<input type="text" placeholder="Your Name" name="urName"/>
	    		</div>
	    	</div>
			
	    	<div class="control-group">
	    		<div class="controls">
	    		<input type="email" placeholder="Email Address" name="emailAddress"  />
	    		</div>
	    	</div>
			
	    	<div class="control-group">
	    		<div class="controls">
	    		<textarea placeholder="Your message" name="urMsg"></textarea>
	    		</div>
	    	</div>
			
	    	<div class="control-group">
	    		<div>
	    		<button type="submit" class="btn orange pull-right">Send Message</button>
	    		</div>
	    	</div>
			<h3 class="notification"></h3>
	    </form>
  		</div>
		
  		</div>
	</div>
	</section>
	
	
	<div class="clear"></div>
	<hr class="sharp" />
	