	<!-- features -->
	<section class="container features inner-tb" id="features">

        <div class="featr-def active">
            <h2 class="text-center default">Track and manage social media accounts of your business</h2>
            <p class="text-center inner-b">
				RepuCaution allows to track brand mentions on Twitter and Google Plus, manage business pages: update statuses, post images and videos, schedule updates to Facebook fanpages, Twitter and Linkedin accounts, Telegram channels. Software can also track new reviews received for your business in popular local directories.
			</p>
        </div>
        <div class="featr-crm">
            <h2 class="text-center">Social CRM - Track what your leads say</h2>
            <p class="text-center inner-b">
				You can deploy RepuCaution as SaaS on your server to charge your users subscription fee (with PayPal, Srtipe or Authorize.net) or make it a free service. You can easily create pricing plans, define each plan pricing and functionality.
			</p>
        </div>



		<!-- slider-wrapper -->
		<div class="slider-wrapper clearfix">
		<!-- slider -->
		<div class="slider">
			<div>
				<img src="img/screen0.png" alt="telegram posting"/>
			</div>
			<div>
				<img src="img/screen5.png" alt="facebook twitter management"/>
			</div>
			<div>
			 	<img src="img/screen3.png" alt="track business reviews" />
			</div>
			<div>
			 	<img src="img/screen1.png" alt="social crm" />
			</div>
			<div>
				<img src="img/screen2.png" alt="customers feedback tracking" />
			</div>
			<div>
				<img src="img/screen4.png" alt="business reputation monitoring" />
			</div>
		</div>
		<!--/ slider -->
		<div class="middle-block clearfix">
		<!-- slider-navigation -->
		<div class="slider-navigation pull-left">
		    <a class="active" href="#"><i class="slidericon-cloud" data-tab="def"></i></a>
            <a href="#"><i class="slidericon-crm" data-tab="crm"></i></a>
		    <a href="#"><i class="slidericon-users" data-tab="def"></i></a>
		    <a href="#"><i class="slidericon-charts" data-tab="def"></i></a>
		    <a href="#"><i class="slidericon-charts" data-tab="def"></i></a>

		</div>
		<!--/ slider-navigation -->
		<!-- slider figure -->
		<div class="figure pull-left span5">
			<figure>
				<figcaption>
					<h3 class="text-orange">Schedule and post updates to Telegram channels</h3>
					<p>
						Use Repucaution to manage your Telegram channels. You can create, schedule and post messages to Telelgram channels. Add images, videos and links to interesting articles to make sure your channel is always in trend.
					</p>
				</figcaption>
			</figure>
			<figure>
				<figcaption>
					<h3 class="text-orange">Track social media activity</h3>
					<p> 
						Manage your online brand reputation with our script by tracking mentions in social networks
					</p>
				</figcaption>
			</figure>
            <figure>
                <figcaption>
                    <h3 class="text-orange">Customer Relatioship Management</h3>
                    <p>
                        CRM with all common social networks integrated. Effective comminication and engagement.
                    </p>
                </figcaption>
            </figure>
			<figure>
				<figcaption>
					<h3 class="text-orange">Monitor reviews of your business</h3>
					<p> 
						Get notified about new reviews of your business posted in one of local business directories
					</p>
				</figcaption>
			</figure>
			<figure>
				<figcaption>
					<h3 class="text-orange">Manage your business pages</h3>
					<p> 
						RepuCaution software allows you to manage and update social media channels by posting immediate or scheduled text updates along with media content
					</p>
				</figcaption>
			</figure>
			<figure>
				<figcaption>
					<h3 class="text-orange">Track website traffic</h3>
					<p> 
						With our script you can track positions of your keywords in google and monitor traffic received on your website
					</p>
				</figcaption>
			</figure>

		</div>
		<!-- slider figure -->
		</div>
		<!-- controls -->
		<div class="controls">
		  <button class="prev"><i class="icon-prev-arr"></i></button>
		  <button class="next"><i class="icon-next-arr"></i></button>
		</div>
		<!--/ controls -->
		</div>
		<!--/ slider-wrapper -->

	<div class="clear"></div>

	<div class="row-fluid">
  		<div class="span4">
  			<img src="images/social_media_management.png" alt="Social Media Management" class="text-center" />
  			<h3>Track mentions of you business</h3>
  			<p>
  				Effectively track mentions of your brand and participate in conversations to get strong online reputation. Keep your fan pages up to date to get more likes and subscriptions - everything can be done at one place by purchasing RepuCaution script
  			</p>
  		</div>
  		<div class="span4">
  			<img src="images/reviews_tracking_script.png" alt="Track reviews in Yelp" class="text-center" />
  			<h3>Monitor business reviews</h3>
  			<p>
  				You can monitor reviews of your business posted at popular local directories.
  			</p>
  		</div>
  		<div class="span4">
  			<img src="images/track_reputation.png" alt="Track Brand Reputation" class="text-center" />
  			<h3>Improve your SEO</h3>
  			<p>
  			   With RepuCaution online software you can track position of your keywords in Google SERP and analyze effectiveness of SEO campaign
  			</p>
  		</div>
	</div>
	</section>
	<!--/ features -->	
	<div class="clear"></div>