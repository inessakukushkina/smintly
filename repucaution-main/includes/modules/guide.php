<div class="container">
    <h2>User's Guide</h2>
    <div class="guide-item">
        <h5><strong>Free / Paid registration</strong></h5>
        <p>
            To sign up for the service you need to indicate first and last name, email and select a password. Free trial plans provide you with an opportunity to start using the service at once. Paid plans require additional fees.
        </p>
    </div>
    <div class="guide-item">
        <h5><strong>Pricing plans configuration</strong></h5>
        <p>
            Sign-up plans configuration is set up on Admin Dashboard in section "Plans management"
            You are able to create, edit and delete plans. Each plan contains name, features, price and time period. It is necessary to set up payment system first and activate the plans to make them visible on frontend.

        </p>
    </div>
    <div class="guide-item">
        <h5><strong>Supported payment systems</strong></h5>
        <p>
            Repucauiton supports PayPal, credit cards.
            New payment options can be integrated if required.
        </p>
    </div>

    <div class="guide-item">
        <h5><strong>Web radar</strong></h5>
        <p>
            Web radar is a tool used for tracking keywords included in Settings section. You set a period of time to check keywords mentioned and get the complete report. In addition, the feature "Influencer's watch" is available where mentions from the most important individuals are traced.
        </p>
    </div>
    <div class="guide-item">
        <h5><strong>Social CRM</strong></h5>
        <p>
            Repucaution is a tool that allows you to manage social networks and collect all relevant info from social media sources. It is possible to make instant and scheduled posts, look through social media update feeds, check reports.

        </p>
    </div>
    <div class="guide-item">
        <h5><strong>Reviews</strong></h5>
        <p>
            In "Review" section you can check all relevant reviews from local directories. Select a directory and a time interval and see positive, negative, neutral reviews with a total sentiment for them.
            Reviews are added from directories you indicate in settings (Yelp, Google Places, Merchant Circle, etc.).

        </p>
    </div>
    <div class="guide-item">
        <h5><strong>Google rank</strong></h5>
        <p>
            Google rank tab displays keyword phrase position and its changes over a certain time interval.
        </p>
    </div>
    <div class="guide-item">
        <h5><strong>Instant social updates</strong></h5>
        <p>
            Update intervals are set on the server (cron job).<br>

            On default the intervals to collect data are the following:<br>

            radar - every 10 min<br>

            reviews - once a day<br>

            activities  - every 10 min<br>

            social reports - each hour<br>

            reviews notifications - every hour<br>

        </p>
    </div>
    <div class="guide-item">
        <h5><strong>Social activity</strong></h5>
        <p>
            The dashboard provides a snapshot of social activity from Twitter, Facebook, LinkedIn and Google + within the set period of time.

        </p>
    </div>
    <div class="guide-item">
        <h5><strong>Website traffic</strong></h5>
        <p>
            The script calculates web, search, referral, direct and adwords traffic. The information is presented in graphs with such values as number of visits, percentage of new visits, average visit duration and bounce rate.
        </p>
    </div>
</div>