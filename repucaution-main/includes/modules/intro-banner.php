	<!-- banner -->
	<!--<section class="banner hlb">
		<!--<img src="images/brand_reputation_monitoring_software.jpg" alt="Reputation script" class="intro large" />
		<div class="container clearfix">			
			<div class="row-fluid">
			<div class="banner-text span6">
				<h2>
					<strong>RepuCaution -</strong> 
					Hosted software solution for social media channels management and brand reputation monitoring
				</h2>
            </div>
            </div>
		</div>		
	</section>-->
	<!--/ banner -->
    <div class="nivoBanner">
        <div class="nivoSlider main__banner">
            <img class="nivoSlider_picture" src="img/slide4.jpg" title="#caption_text_one"/>
            <img class="nivoSlider_picture" src="img/slide1.jpg" title="#caption_text_two"/>
            <img class="nivoSlider_picture" src="img/slide2.jpg" title="#caption_text_three"/>
            <img class="nivoSlider_picture" src="img/slide3.jpg" title="#caption_text_four"/>
        </div>
        <div id="caption_text_one" class="nivo-html-caption">
            <div class="container top-middle">
                <h2 class="text__title">
                    Open-source social media script
                </h2>
                <p class="text__subtitle">
                    Downloadable social media marketing solution with scheduled posting, social media monitoring, website traffic analysis, social CRM and other useful features.
                </p>
            </div>
        </div>
        <div id="caption_text_two" class="nivo-html-caption">
            <div class="container top-middle">
                <h2 class="text__title">
                    Craft great images for posting 
                    using Social Media Images Designer!
                </h2>
                <p class="text__subtitle">
                    Customize your posts: select background images, edit font type and color, add logos. 
                </p>
                 <p class="text__subtitle">
                    Make your posts look really awesome.
                </p>
            </div>
        </div>
        <div id="caption_text_three" class="nivo-html-caption">
            <div class="container top-middle">
                <h2 class="text__title">
                    Manage multiple profiles 
                    under one account!
                </h2>
                <p class="text__subtitle">
                    Create several profiles and add social mediachannels to them. 
                </p> 
                <p class="text__subtitle">
                    Monitor, manage and create content for multiple profiles which  include sets of social media channels. 
                </p>
            </div>
        </div>
        <div id="caption_text_four" class="nivo-html-caption">
            <div class="container top-middle">
                <h2 class="text__title">
                    Increase the number of Twiiter followers with no spam!
                </h2>
                <p class="text__subtitle">
                   Use Twitter marketing automation tools to simplify interaction with people you follow and your followers. 
                </p>
                 <p class="text__subtitle">
                   Add multiple search criteria, create follow /unfollow rules, craft auto-send welcome messages and auto-retweets.
                </p>
            </div>
        </div>
    </div>	

<?php include 'includes/modules/solution-section.php'; ?>

	<article class="intro-offer">
        <!--<h3 style="color: #f68f1e !important;">$799</h3>
        <h4><span>NO EXTRA FEES, UNLIMITED USAGE</span> <small class="line-through"></small></h4>-->
	<!--http://scripts.ikantam.com/social-media-marketing-software.html-->
	
        <!--<a href="http://ec2-23-21-183-240.compute-1.amazonaws.com/gp/" target="_blank" class="btn orange brdr-btm btn-large"> -->
	<div class="text-center">	
		<a href="https://app.smintly.com/auth/register/6" class="btn orange brdr-btm btn-large">
			Try Demo
		</a>
		
		<a href="http://scripts.ikantam.com/social-media-marketing-software.html" target="_blank" style="background-color:green"class="btn orange brdr-btm btn-large">
			Buy Now
		</a>
		<!--
		<br />
		<br />
		<a href="https://www.jvzoo.com/b/0/100207/1">
			<img src="http://i.jvzoo.com/0/100207/1" alt="Repucaution - social media management/ online reputation monitoring script" border="0" />
		</a>

	</div>
	
	<p>
	<small>
		Software is delivered as .zip archive with the code and installation instruction. After purchase you can install it on your server, use and modify as you want. There are no restrictions. You will also receive software updates from us by email that will include new features and improvements.
	</small>
	</p>
	
	<hr class="sharp" />-->
	</article>
	
	<!--
<div id="subscribe-popup">
    <div class="subscribe-overlay"></div>
    <div class="subscribe-window">
        <div class="close-butt"></div>
        <div class="popup-content">
            <p class="popup-text">
                Be the first to know about new Repucaution features & specials. Add your email to get insider updates.
            </p>
            <p class="error"></p>
            <form id="form_subscribe">
                <div class="control-group">
                    <div class="contrl">
                        <input type="email" id="input_subscribe_email" name="email" placeholder="Your e-mail" class="pull-left">
                        <input type="submit" id="button_subscribe" class="yellow-butt pull-left" value="Submit" />
                    </div>
                </div>
                <div class="clear"></div>
            </form>
        </div>
    </div>
</div>
-->

<?php include 'includes/modules/modal.php'; ?>