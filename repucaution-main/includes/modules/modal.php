<!-- Modal -->
<div id="subox" class="modal modal-subscribe hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <button class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-heading">Live Demo Access</h4>

    <div class="modal-body">
        <!--
        <p class="error"></p>
        <form id="form_subscribe" autocomplete="off">
            <div class="control-group">
            <h5>To get Demo access please specify your email address.</h5>
                <div class="controls">
                    <input type="email" id="input_subscribe_email" name="email" placeholder="Your e-mail" />
                    <p class="cols">
                        <input type="submit" id="button_subscribe" class="btn orange" value="SUBMIT" />
                    </p>
                </div>
            </div>
            <div class="clear"></div>
        </form>-->
        <div id="mc_embed_signup">
            <form action="//repucaution.us9.list-manage.com/subscribe/post?u=9e5b691dd9fd7b6d2524fdb5a&amp;id=f986b8c273" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate  autocomplete="off">
                <div class="control-group">
                    <h5>To get Demo access please specify your email address.</h5>
                    <p>Once you confirm your e-mail, we will send the demo link to your e-mail box.</p>
                    <div class="controls">

                        <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Your e-mail" required />
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;"><input type="text" name="b_9e5b691dd9fd7b6d2524fdb5a_f986b8c273" tabindex="-1" value=""></div>
                        <div class="clear"></div>
                        <p class="cols">
                            <input type="submit" style="width: 120px !important;"  value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn orange button" />
                        </p>
                    </div>
                </div>
                <div class="clear"></div>
            </form>
        </div>

    </div>
</div>