	<!-- banner -->
	<section class="banner support__banner" id="support">		
		<header class="text-center v-mid">
			<h2>RepuCaution - the ultimate software solution to track and manage online presence of a business</h2>
			<p class="drop-shadow">
				Open source software solution developed to make social media management and brand reputation monitoring fast and effective.
			</p>
		</header>
	</section>
	<!--/ banner -->
	<div class="clear"></div>