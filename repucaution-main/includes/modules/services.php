	<!-- row-fluid -->
	<div class="row-fluid bg-grey inner-tb sections" id="convenience">
		<!-- container -->
		<div class="container">
			<header class="text-center">
			<h2>
				<!-- At this moment we provide the following services to all our clients: -->
				Full stack of services for your business
			</h2>
			<p>
			Our team provides all services related to web development, UI design and technical support. 
			You will never be left alone with a problem that may occur during software operation or customization, 
			we are online 24/7 to help you with it.
			</p>
			
			<p>
			Our professional engineers and UX specialists are always ready for a new challenge, 
			either you need a new function, unique and custom interface, 
			attractive landing page or advanced back end architecture - just drop us a message and we will deliver it to you. 
			</p>
				
			</header>
			
			<div class="span4">
				<ul>
				<li>
					<p>
					  Software installation and configuration - $210
					</p>
				</li>
				<li>
					<p>
					  Software upgrade installation - $40
					</p>
				</li>
				<li>
					<p>
						Custom logo creation - from $270
					</p>
				</li>
				<li>
					<p>
						Custom user interface creation - from $420
					</p>
				</li>
				<li>
					<p>
						Additional data source integration - from $150
					</p>
				</li>
				<li>
					<p>
						Additional payment gateway integration - from $200
					</p>
				</li>
				<li>
					<p>
						Adding a new language - from $160
					</p>
				</li>
				</ul>
			</div>
			<div class="span7 text-center">
				<!-- <br><br><br><br><br><br> -->
				<h3>
					Feel free to contact us if you are interested in one of the services listed or need other work done. We are always ready for new challenges! 
				</h3>
				<div class="lower-form">
  		<h3>Contact Us</h3>
	    <form id="contactusForm" class="contactForm" method="post">
	    	<div class="control-group">
	    		<div class="controls">
	    		<input type="text" placeholder="Your Name" name="urName"/>
	    		</div>
	    	</div>
			
	    	<div class="control-group">
	    		<div class="controls">
	    		<input type="email" placeholder="Email Address" name="emailAddress"  />
	    		</div>
	    	</div>
			
	    	<div class="control-group">
	    		<div class="controls">
	    		<textarea placeholder="Your message" name="urMsg"></textarea>
	    		</div>
	    	</div>
			
	    	<div class="control-group">
	    		<div>
	    		<button type="submit" class="btn orange pull-right">Send Message</button>
	    		</div>
	    	</div>
			<h3 class="notification"></h3>
	    </form>
			</div>
			
			</div>
		</div>
		<!--/ container -->
	</div>
	<!--/ row-fluid -->

	<div class="clear"></div>