<!--solution section-->
<div class="container solution-section">
	<div class="solution">
		<div class="top">
			<div class="title pull-left">Cloud Solution</div>
				<p class="description">
					Social media management is now available in the cloud. Get started today to get more<br/>
					followers, likes and customers from social networks. No technical skills are required. 
				</p>
			</div>
		<div class="price pull-right">
			<span class="solution-price-free">FREE</span><br/>
			<span class="solution-price-free-cloud">for 30 days</span><br/>
			<a href="https://app.smintly.com/auth/register/6" class="btn orange brdr-btm btn-large">
				Try Now
			</a>
		</div>
	</div>
	<div class="solution">
		<div class="top">
			<div class="title pull-left">Hosted Solution</div>
				<p class="description">
					Perfect solution for social media marketing agencies and those who want to provide extra<br/>
					tools for customers. Easy to install and maintain, regular updates are available.
				</p>
			</div>
			<div class="price pull-right">
				<span class="solution-price-free">$799</span>
				<br/>
				<br>
				<a href="http://scripts.ikantam.com/social-media-marketing-software.html" class="btn orange brdr-btm btn-large">
					Buy Now
				</a>
			</div>
		</div>
	</div><!--solution section end-->