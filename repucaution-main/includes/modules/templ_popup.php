<div id="subscribe-popup">
    <div class="subscribe-overlay"></div>
    <div class="subscribe-window">
        <div class="close-butt"></div>
        <div class="popup-content">
            <p class="popup-text">
                Be the first to know about new Repucaution features & specials. Add your email to get insider updates.
            </p>
            <p class="error"></p>
            <form id="form_subscribe">
                <div class="control-group">
                    <div class="contrl">
                        <input type="email" id="input_subscribe_email" name="email" placeholder="Your e-mail" class="pull-left">
                        <input type="submit" id="button_subscribe" class="yellow-butt pull-left" value="Submit" />
                    </div>
                </div>
                <div class="clear"></div>
            </form>
        </div>
    </div>
</div>
