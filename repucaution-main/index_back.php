<?php include 'includes/overall/head.php'; ?>
<?php include 'includes/header.php'; ?>

<?php include 'includes/modules/intro-banner.php'; ?>
<?php include 'includes/modules/features.php'; ?>
<?php include 'includes/modules/convenience.php'; ?>
<?php include 'includes/modules/secondary-banner.php'; ?>
<div id="wrap"></div>
	<!-- row-fluid -->
	<div class="row-fluid bg-grey inner-tb sections">
		<!-- container -->
		<div class="container">
			<header class="text-center">
			<h2>
				Testimonials
			</h2>
			<p>
				What our clients say
			</p>	
			</header>
			
<div class="sliderContainer">
<div id="slider-1">
    <ul>
        <!-- slide item -->
        <li>
            <div class="itemContainer clearfix">
                <div class="slideThumb">
                    <img src="img/lilly.png" alt="smintly" style="margin-right:30px"/>
                </div>

                <div class="slideContent">
                    <p>
                      " I was very impressed by this tool. It helped me to save my time a lot. 
                      Can't recommend highly enough! 
                      The team is also very helpful, thanks for a great work."
                    </p>
                    <p><b>- Lilly Fellington, Marketing Consultant,</b> Los Angeles</p>
                </div>
            </div>
        </li>
        <!--/ slide item -->
		
		<!-- slide item
        <li>   
          <div class="itemContainer clearfix">
          <div class="slideThumb"> 
            <img src="images/jmbrandwatch.png" alt="jmbrandwatch" />
          </div>
		  
            <div class="slideContent">
              <p>
				JM Brand Watch is another social media marketing online tools powered by Repucaution software. We are glad to see there are many startups user our software to launch business and give their clients ultimately new experience. 
              </p>
              <p>&#8212; Website: http://jmbrandwatch.com/</p>
            </div>
            </div>
        </li>
		slide item -->
        <!-- slide item -->
        <li>
            <div class="itemContainer clearfix">
                <div class="slideThumb">
                    <img src="img/mark.png" alt="wuggled" style="margin-right:30px"/>
                </div>

                <div class="slideContent">
                    <p>
                      " I am glad that I found Repucaution. 
                      I have been spending so much time posting and analyzing my social media presence. 
                      Now, the only thing I need is several clicks within the tool. 
                      Moreover, other features are very useful too. 
                      Lucky to have such tool! "
                    </p>
                    <p><b>- Mark Roopert, Executive manager,</b> Chicago</p>
                </div>
            </div>
        </li>
        <!--/ slide item -->


        <!-- slide item -->
        <li>   
          <div class="itemContainer clearfix">
          <div class="slideThumb">
            <img src="img/jen.png" alt="jmbrandwatch" />
          </div>
		  
            <div class="slideContent">
              <p>
                " The quality of this tool is excellent and it is matched by outstanding customer service. 
                I am pleased by all the features of Repucaution. 
                Especially by Keywords search and of course the ability of management the entire social accounts from one place. "
              </p>
              <p><b>- Jen Mayple, Social Media Manager,</b> Washington DC</p>
            </div>
            </div>
        </li>
		<!--/ slide item -->
	
		<!-- slide item
        <li>   
          <div class="itemContainer clearfix">
          <div class="slideThumb"> 
            <img src="images/sociable.png" alt="Sociable" />
          </div>
          
            <div class="slideContent">
              <p>
               Sociable.io is another great social media management tool built with our software. It has all the code features of the script and some extra functions developed for it. 
               Tool is paid and utilize Paypal as default payment option.
              </p>
              <p>&#8212; Website: sociable.io</p>
            </div>
            </div>
        </li>
		slide item -->
		
		<!-- slide item
        <li>   
          <div class="itemContainer clearfix">
          <div class="slideThumb"> 
            <img src="images/chillidrop.png" alt="Chillidrop" />
          </div>
		  
            <div class="slideContent">
              <p>
				Chillidrop - the next big software application powered by
				repucaution software. Our team created a custom flat dynamic interface
				for it to meet client's requirements. Now the app is running as
				separate SaaS solution and has its own team of developers. Client was
				happy to save a lot of time and money by using our software to lauch
				own startup.
              </p>
              <p>&#8212; Website: chillidrop.com</p>
            </div>
            </div>
        </li>
		slide item -->
		
		<!-- slide item -->
        <li>   
          <div class="itemContainer clearfix">
          <div class="slideThumb"> 
            <img src="img/michael.png" alt="Drivesales" />
          </div>
		  
            <div class="slideContent">
              <p>
                " It's always nice to find something that helps you to work more efficiently. 
                Repucaution is one of these things. 
                All the functionality and features are very useful. 
                I am very pleased with the work of the tool and the team that helped me with customization. "
              </p>
              <p><b>- Michael Moore, Business Developer,</b> New York</p>
            </div>
            </div>
        </li>
		<!--/ slide item -->

		
    </ul>
  
    <button class="prev"></button>
    <button class="next"></button>
  
</div>
</div>
			
	</div>
		<!--/ container -->
	</div>
	<!--/ row-fluid -->

	<div class="clear"></div>

<?php include 'includes/modules/features-bottom.php'; ?>
<?php include 'includes/footer.php'; ?>
<?php include 'includes/overall/footer.php'; ?>