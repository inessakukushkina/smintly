<!DOCTYPE html>
<!--[if lt IE 9]><script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<!--[if IE 8 ]><html class="ie8"><![endif]-->
<html>
<head>
	<title>Installation instruction - RepuCaution</title>
	<meta name="google-site-verification" content="yINKC1HicZWAdtt6aEgq7gRXaJO_aF_DMcgYZcn6k4E" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Earn money with us! Join RepuCaution software affiliate programm and get up to 30% sales commission.">
	<meta name="keywords" content="Software, affiliate, comission" />
	<meta name="author" content="RepuCaution LLC">

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="css/master.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css">
	<!-- css -->
	
	<!-- favicon -->
	<link rel="icon" type="image/png" href="favicon.png">
	<!--[if IE]><link rel="shortcut icon" href="favicon.ico"/><![endif]-->
	<!--/ favicon -->
</head>
<body>

<?php include 'includes/header.php'; ?>

<section class="container posts">
	<h1>Installation instruction</h1>
	<div class="row">
	
	<!-- sidebar -->
	<div class="span3 docs-nav">
        <ul class="nav nav-list">
				<li><a class="active" href="#requirements"><i class="icon-link"></i> Requirements</a></li>
				<li><a href="#files"><i class="icon-link"></i> Files</a></li>
				<li><a href="#database"><i class="icon-link"></i> Database</a></li>
				<li><a href="#general"><i class="icon-link"></i> General</a></li>
				<li><a href="#superadmin"><i class="icon-link"></i> Superadmin</a></li>
				<li><a href="#apis"><i class="icon-link"></i> APIs</a></li>
				<li><a href="#foursquare"><i class="icon-link"></i> Foursquare</a></li>
				<li><a href="#linkedin"><i class="icon-link"></i> Linkedin</a></li>
				<li><a href="#facebook"><i class="icon-link"></i> Facebook</a></li>
				<li><a href="#twitter"><i class="icon-link"></i> Twitter</a></li>
				<li><a href="#google"><i class="icon-link"></i> Google</a></li>
				<li><a href="#install-task-manager"><i class="icon-link"></i> Install Task Manager</a></li>
				<li><a href="#uninstall-task-manager"><i class="icon-link"></i> Uninstall Task Manager</a></li>
				<li><a href="#updating-application"><i class="icon-link"></i> Updating Application</a></li>
				<li><a href="#problems"><i class="icon-link"></i> Issues</a></li>
			</ul>
      </div>
	  
	  <div class="span9 docs m-tb30">
	  
	    <!-- [ Requirements ] -->
		<section class="docs-section">
			<h2 id="requirements">Requirements</h2>
			
			<ol>
				<li>PHP version >=5.3.0</li>
				<li>
					Operation system that supports the installation of ActiveMQ task manager, supervisord daemon manager, crontab. The installation of the task manager was tested on Ubuntu 12.04 (as it is rather popular on AWS), but it should work also on 10.04 and greater. For now, if you have another OS, you have to install task manager manually, because we don't have detailed instructions yet.
				</li>
			</ol>
		</section>
		
		<!-- [ Files ] -->
		<section class="docs-section">
			<h2 id="files">Files</h2>
			
			<ol>
				<li>
					Copy the files to a directory under your web-root, so the files and folder "application", 	"system", "mq_installer", "public", "index.php" and ".htaccess" must be in it.
					
					<img src="images/docs/1.png" alt="Files" />	
				</li>
				<li>
					Open an application in your browser, you sholud see a database error (that's because we have not configured it yet).
					
					<img src="images/docs/2.png" alt="Files" />	
				</li>
			</ol>
		</section>
		
		<!-- [ Database ] -->
		<section class="docs-section">
			<h2 id="database">Database</h2>
			<ol>
				<li>
					In your PhpMyAdmin create new database (or you might use any other ways to create database). You can choose any available name, I will use the name "repucaution" to explain the next steps.
				</li>
				<li>
					In the file <code>"application/config/database.php"</code> find and set name of the database, that you've just created <code>$db['default']['database'] = 'repucaution';</code>
				</li>
				<li>
					In the file "application/config/database.php" find and set username and password, that you are using to connect to your databases(or to log in PhpMyAdmin):
					
					<p>
					<code>$db['default']['username'] = �your-db-username�;</code>
					<br>
					<code>$db['default']['password'] = 'your-db-password';</code>
					</p>
					
					<img src="images/docs/3.png" alt="Database" />	
					
					Save the file.
				</li>
				<li>
					Go to url "sitename.com/migrate" (where "sitename.com" is a url of the application). 
					On this step database will be filled with tables. You should see a blank screen after operation is completed. To check if everything is ok, you can open your database in PhpMyAdmin.
					
					<img src="images/docs/4.png" alt="Database" />	
					
					<div class="alert">
					  * If url "sitename.com/migrate" is not available, use "sitename.com/index.php/migrate". That also means that you need to configure ".htaccess" file (from "application" folder) to hide "index.php" uri part from url
					</div>
				</li>
				<li>
					If you want to use prefixes for tables in the database:
					- In the file <code>"application/config/database.php"</code> fill in the prefix you want (I will use "rep_" for example)
					<code>$db['default']['dbprefix'] = 'rep_';</code>
					- In the file <code>"application/config/datamapper.php"</code> set the same prefix:
					<code>$config['prefix'] = 'rep_';</code>
					
					<img src="images/docs/5.png" alt="Database" />	
					
					<p>
					That�s better to remove the old tables, but anyway everything will work.
					Like in step 4, go to url "sitename.com/migrate" (where "sitename.com" is a url of the application).
					</p>
					
					<p>
						The tables will have prefix now.
					</p>

					<img src="images/docs/6.png" alt="Database" />	
				</li>
			</ol>
		</section>
		
		<!-- [ General ] -->
		<section class="docs-section">
			<h2 id="general">General</h2>
			
			<ol>
				<li>
					To configure site title edit file <code>"application/config/template.php"</code> and set your title <code>$config['OCU_site_name'] = "Custom title";</code>
					
					<img src="images/docs/7.png" alt="General" />	
					
					Now you can see your custom title.
					
					<img src="images/docs/8.png" alt="General" />	
				</li>
				
			</ol>
		</section>
		
		<!-- [ Superadmin ] -->
		<section class="docs-section">
			<h2 id="superadmin">Superadmin</h2>
			<ol>
				<li>
					To log in application as super admin you have to go to "sitename.com/auth/login" page. 
					Use the following parameters to log in:
						
					<div class="alert alert-info">
						Login: super@admin.com
						<br>
						Password: password
					</div>
					
					<img src="images/docs/9.png" alt="Superadmin" />	
				</li>
				<li>
					Error message  <span class="error-msg">Please fill in all API keys, otherwise users won't have access to some features</span> means that you have some API keys not set.
				</li>
				<li>
					Error message  means <span class="error-msg">Cache directory is not writable or does not exist ( "application/cache" )</span> means that cache directory is not writable which is required. To make it writable use your console, and run command:
					<br>
					<code>sudo chmod 777 path/to/project/application/cache -R</code>
					<p>
						How to get into console you can see in the 
						<span class="lrg-text">INSTALL TASK MANAGER</span> section.
					</p>
				</li>
				
			</ol>
		</section>
		
		<!-- [ APIS ] -->
		<section class="docs-section">
			<h2 id="apis">APIS</h2>
			<ol>
				<li>
					To set up the keys for various API systems you must log in application as superadmin. How to log in is described in <span class="lrg-text">SUPERADMIN</span> section. 
					There is a form for API keys.
					
					<img src="images/docs/10.png" alt="APIS" />	
				</li>
			</ol>
		</section>
		
		<!-- [ Foursquare ] -->
		<section class="docs-section">
			<h3 id="foursquare">Foursquare</h3>
			<ol>
				<li>
					Register in 
					<a href="https://foursquare.com" target="_blank">
						https://foursquare.com
					</a>					
				</li>
				<li>
					Go to 
					<a href="https://foursquare.com/developers/apps" target="_blank">
						https://foursquare.com/developers/apps
					</a>
					<img src="images/docs/Foursquare/fq-1.png" alt="Foursquare" />	
				</li>
				<li>
					Fill fields of form
					<img src="images/docs/Foursquare/fq-2.png" alt="Foursquare" />	
				</li>
				<li>
					Insert your client id and client secret
					<img src="images/docs/Foursquare/fq-3.png" alt="Foursquare" />	
				</li>
			</ol>
		</section>
		
		<!-- [ Linkedin ] -->
		<section class="docs-section">
			<h3 id="linkedin">Linkedin</h3>
			<ol>
				<li>
					Register in 
					<a href="https://developer.linkedin.com/" target="_blank">
						https://developer.linkedin.com/
					</a>					
				</li>
				<li>
					Go to 
					<a href="https://www.linkedin.com/secure/developer" target="_blank">
						https://www.linkedin.com/secure/developer
					</a>
					<img src="images/docs/Linkedin/linkd-1.png" alt="Linkedin" />	
				</li>
				<li>
					Fill fields of form
					<img src="images/docs/Linkedin/linkd-2.png" alt="Linkedin" />	
					<img src="images/docs/Linkedin/linkd-3.png" alt="Linkedin" />	
				</li>
			</ol>
		</section>
		
		<!-- [ Facebook ] -->
		<section class="docs-section">
			<h3 id="facebook">Facebook</h3>
			<ol>
				<li>
					Sign in 
					<a href="https://developers.facebook.com/" target="_blank">
						https://developers.facebook.com/
					</a>					
				</li>
				<li>
					Go to applications->create new app 
					<img src="images/docs/Facebook/fb-1.png" alt="Facebook" />	
				</li>
				<li>
					Fill fields
					<img src="images/docs/Facebook/fb-2.png" alt="Facebook" />	
				</li>
				<li>
					Get params of app 
					<img src="images/docs/Facebook/fb-3.png" alt="Facebook" />	
				</li>
			</ol>
		</section>
		
		<!-- [ Twitter ] -->
		<section class="docs-section">
			<h3 id="twitter">Twitter</h3>
			<ol>
				<li>
					Register as a developer, if you are not registered, and go to 
					<a href="https://dev.twitter.com/apps" target="_blank">
						https://dev.twitter.com/apps
					</a>					
				</li>
				<li>
					Create new application
					<img src="images/docs/15.png" alt="Twitter" />	
				</li>
				<li>
					Set your website (<u>http://sitename.com</u>) and callback url (<u>http://sitename.com/settings/socialmedia/twitter_callback</u>), save changes.

					<img src="images/docs/16.png" alt="Twitter" />	
				</li>
				<li>
					When application has been created, go to the "Settings" tab

					<img src="images/docs/17.png" alt="Twitter" />	
					
					and scroll down to "Application Type" section. Select "Read and Write", then save changes.
					
					<img src="images/docs/18.png" alt="Twitter" />	
				</li>
				<li>
					go to "Details" tab, and keep refreshing the page until "Access Level" becomes "Read and Write".

					<img src="images/docs/19.png" alt="Twitter" />	
				</li>
				<li>
					Copy "Consumer key" and "Consumer secret" to superadmin API keys form.

					<img src="images/docs/20.png" alt="Twitter" />	
				</li>
			</ol>
		</section>
		
		<!-- [ Google ] -->
		<section class="docs-section">
			<h3 id="google">Google</h3>
			<ol>
				<li>
					Sign in 
					<a href="https://developers.google.com" target="_blank">
						https://developers.google.com
					</a>
				</li>
				<li>
					Go to 
					<a href="https://appengine.google.com/start" target="_blank">
						https://appengine.google.com/start
					</a>
					<img src="images/docs/Google/goo-1.png" alt="Google" />	
				</li>
				<li>
					Create new application
					<img src="images/docs/Google/goo-2.png" alt="Google" />		
				</li>
				<li>
					<img src="images/docs/Google/goo-3.png" alt="Google" />		
					<img src="images/docs/Google/goo-4.png" alt="Google" />		
				</li>
				<li>
					Choose APIs
					<img src="images/docs/Google/goo-5.png" alt="Google" />		
				</li>
				<li>
					Create new client id and new server key
					<img src="images/docs/Google/goo-6.png" alt="Google" />		
				</li>
				<li>
					Get your api keys
					<img src="images/docs/Google/goo-7.png" alt="Google" />		
				</li>
			</ol>
		</section>
	  
		<!-- [ Install Task Manager ] -->
		<section class="docs-section">
			<h3 id="install-task-manager">Install Task Manager</h3>
			<p>
				Steps below describe how to install task manager on Ubuntu 12.04, but it should work well on 10.04 and later.
			</p>
			<ol>
				<li>
					To install task manager you have to use console(terminal) on your server as root user. Most of the times you should connect to server with SSH (using PuTTy or any other program). For example, if you have your server installed on Amazon Web Services (AWS), it has an explanation how to connect. Select your instance and press "Connect" button.
					
					<img src="images/docs/29.png" alt="Install Task Manager" />	
					
					Then use provided instructions to connect. (you should have received *.pem key, when you were installing your instance)
					
					<img src="images/docs/30.png" alt="Install Task Manager" />	
				</li>
				
				<li>
					After successful connection you should see a console (something like one shown on the pic below)
					
					<img src="images/docs/31.png" alt="Install Task Manager" />	
					
					Suppose that you already have your version of the Repucation script installed on the server. For example, mine is located in "/var/www/gp" folder. By the way, script has "mq_installer" folder, where all the task manager installation files are stored.
					
					<br>
					
					Using <code>cd</code< command change current directory to project's "mq_installer" folder. Write in your console:
					<code>cd path/to/repucaution/mq_installer</code>
					
					<img src="images/docs/32.png" alt="Install Task Manager" />	
				</li>
				
				<li>
					Now you are in "mq_installer" folder. To make installation script executable run command in your console
					<code>sudo chmod +x install_mq.sh</code>

					<p>
					To start task manager installation run command in your console
					<code>sudo ./install_mq.sh</code>
					</p>
					
					<p>
					Task manager installation script will be now running. It will install ActiveMQ task manager and it�s dependencies, supervisord daemon manager (this manager will be constantly executing php script to perform task manager tasks), it will install (if not installed) and set up cron, so every minute, every ten minutes, every hour and every day it will be executing php script adding tasks to task manager. All these programs should work even after server restart.
					</p>
				</li>
				
				<li>
					To find out the installation was successful you should see a similar lines in your console like there are on the pic
					
					<img src="images/docs/33.png" alt="Install Task Manager" />	
				</li>
				
				<li>
					Now we have to edit task manager's configuration file, that should have a path <code>"/etc/activemq/instances-available/ik-script/activemq.xml"</code> . Ubuntu has it's own console-built-in text redactor. We will use it to edit this config file. In your console run command
					<code>sudo vim /etc/activemq/instances-available/ik-script/activemq.xml</code>
					
					<p>You should see the contents of the config file.</p>		
					
					<img src="images/docs/34.png" alt="Install Task Manager" />	
					
					Use arrows to navigate, go down to the lines 
					<div class="clear"></div>
		
					<pre><ul><li>&lt;transportConnectors&gt;</li><li>&nbsp;&nbsp; &lt;transportConnector name="openwire" uri="tcp://127.0.0.1:61616"/&gt;</li><li>&lt;/transportConnectors&gt;</li></ul></pre>
					
					which are located practically near the end of the file.
					
					<p>
						Press <code>"i"</code> on your keybord to activate <code>"edit"</code> mode in text redactor. After the line <code><transportConnector name="openwire" uri="tcp://127.0.0.1:61616"/></code>, add one more line:
							<br>
						<code>&lt;transportConnector name="stomp" uri="stomp://127.0.0.1:61613"/&gt;</code>.
					</p>
					
					After editing lines it should look like 
					
					<pre><ul><li>&lt;transportConnectors&gt;</li><li>&nbsp;&nbsp; &lt;transportConnector name="openwire" uri="tcp://127.0.0.1:61616"/&gt;</li><li>&nbsp;&nbsp; &lt;transportConnector name="stomp" uri="stomp://127.0.0.1:61613"/&gt;</li><li>&lt;/transportConnectors&gt;</li></ul></pre>

					<p>
						Press "ESC" on your keyboard to exit "edit" mode, then type in <code>":x"</code> and press ENTER. File will be closed with the changes saved. 
					</p>				

					<p>
						You can open a file again to be sure that changes have taken place. (console command <code>sudo vim /etc/activemq/instances-available/ik-script/activemq.xml</code> and then type <code>":q"</code> + Enter to exit) 

					</p>				
					
					<img src="images/docs/35.png" alt="Install Task Manager" />	
				</li>

				<li>
					After that we have to restart task manager to apply changes. To do that run the following command in console
					<code>sudo chmod +x restart_mq.sh && sudo ./restart_mq.sh</code>
					
					<img src="images/docs/36.png" alt="Install Task Manager" />	
					
					Task manager should now work and gather data for Repucaution.
				</li>
				
			</ol>
		</section>

		<!-- [ Uninstall Task Manager ] -->
		<section class="docs-section">
			<h3 id="uninstall-task-manager">Uninstall Task Manager</h3>
					
					<p>
						Perform steps 1 and 2 from the <span class="lrg-text">INSTALL TASK MANAGER</span> section.
						Then in you console run a command:
						<code>sudo chmod +x clear_mq.sh && sudo ./clear_mq.sh</code>
					</p>
					
					<p>
						It will remove all system commands invoked constantly while a task manager was working i.e. supervisord daemon will not longer running, cron task will not be called. But all the software will stay installed.
					</p>
					
					<p>
						To make task manager running again just repeat all the steps from 
						<span class="lrg-text">INSTALL TASK MANAGER</span> section.
					</p>
	  </section>
		
		<!-- [ Updating Application ] -->
		<section class="docs-section">
			<h3 id="updating-application">Updating Application</h3>
			<ol>
				<li>
					Every time you receive a new version of the application, do migrations.
					Go to url "sitename.com/migrate" (where "sitename.com" is a url of the application). 
					On this step database will be filled with tables. You should see a blank screen after operation is completed. To check if everything is ok, you can open your database in PhpMyAdmin.
					<p>
					Detailed in <span class="lrg-text">DATABASE</span> section, (4).
					</p>
				</li>
				<li>
					Check "application/config/database.php" for your database connections settings not rewritten. Detailed in
				</li>
			</ol>
	  </section>
	  
		<!-- [ Problems ] -->
		<section class="docs-section">
			<h3 id="problems">Problems</h3>
			<ol>
				<li>
					When logging in as superadmin you get into ordinary user dashboard, try to go to url:
					<p><u>http://sitename.com/migrate/index/59</u></p>
					then
					<p><u>http://sitename.com/migrate</u></p>
					and then try to log in again
				</li>
			</ol>
	  </section>
	
	
	
	</div>

	
	
</section>

<?php include 'includes/footer.php'; ?>
<?php include 'includes/overall/footer.php'; ?>