<?php
/**
 * Template header, included in the main and detail files
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
?>
<header>
    <!-- container -->
    <div class="container">
        <!-- brand logo/slogan -->
        <a href="/" class="brand">
            <img src="http://repucaution.com/images/form-logo.png" alt="Etsy clone script">
        </a>
        <!--/ brand logo/slogan -->

        <!-- navigation menu -->
        <nav class="pull-right top-55">
            <button type="button" class="navbar-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
            </button>

            <ul class="inline navbar-collapse">
				<li data-toggle="modal" data-target="#subox"><a href="#">Demo</a></li>
				<!--<li><a href="http://scripts.ikantam.com/social-media-marketing-software.html" target="_blank">Buy</a></li>-->
                <li><a href="http://scripts.ikantam.com/social-media-marketing-software.html" target="_blank">Download</a></li>
				
                <li><a href="http://repucaution.com/#features">Features</a></li>
                <!--<li><a href="http://repucaution.com/#convenience">FOR CLIENTS</a></li>-->
                <!--<li><a href="http://repucaution.com/#support">Support</a></li>-->
                <li><a href="http://repucaution.com/installation">INSTALLATION</a></li>
                <li><a href="http://repucaution.com/services">SERVICES</a></li>
                <li><a href="http://repucaution.com/faq">FAQ</a></li>
				<li><a href="http://repucaution.com/release">Releases</a></li>
                <li><a href="http://repucaution.com/blog">Blog</a></li>
                <!--<li><a href="http://repucaution.com/affiliate">AFFILIATE</a></li>-->
                <li><h4 class="text-orange" style="margin-top: 0;">+302 504 4199</h4></li>
                <li><a href="#myModal" class="btn orange getModal">Contact</a></li>
            </ul>

            <div class="push-right get-odal-box">
                <a href="#myModal" class="btn grey getModal">Contact us</a>
                <a href="#top"><i class="icon-top-arr"></i></a>
            </div>
        </nav>
        <!--/ navigation menu -->
    </div>
    <!--/ container -->
    <script>
        /*$(function(){
            $('.getModal').on('click', function() {
                $('#myModal').show();
                return false;
            });
        });*/
    </script>
</header>
<!-- Modal -->


    <?php
    if (!empty($_SERVER['REMOTE_USER'])) {
        echo '<ul style="position: absolute;"><li class="user">';
        tpl_userinfo();
        tpl_action('admin', 1, 'li');
        tpl_action('profile', 1, 'li');
        tpl_action('register', 1, 'li');
        tpl_action('login', 1, 'li');
        echo '</li></ul>';
    }

    ?>


<?php if ($conf['useacl']): ?>
<div id="dokuwiki__usertools">
    <h3 class="a11y"><?php echo $lang['user_tools']; ?></h3>

</div>
<?php endif ?>


<!--
<div id="dokuwiki__header">
    <div class="pad group">

    <?php tpl_includeFile('header.html') ?>

    <div class="headings group">
        <ul class="a11y skip">
            <li><a href="#dokuwiki__content"><?php /*echo $lang['skip_to_content']; */?></a></li>
        </ul>

        <h1><?php
/*            $logoSize = array();
            $logo = tpl_getMediaFile(array(':wiki:logo.png', ':logo.png', 'images/logo.png'), false, $logoSize);

            tpl_link(
                wl(),
                '<img src="'.$logo.'" '.$logoSize[3].' alt="" /> <span>'.$conf['title'].'</span>',
                'accesskey="h" title="[H]"'
            );
        */?></h1>
        <?php /*if ($conf['tagline']): */?>
            <p class="claim"><?php /*echo $conf['tagline']; */?></p>
        <?php /*endif */?>
    </div>

    <div class="tools group">
        <?php /*if ($conf['useacl']): */?>
            <div id="dokuwiki__usertools">
                <h3 class="a11y"><?php /*echo $lang['user_tools']; */?></h3>
                <ul>
                    <?php
/*                        if (!empty($_SERVER['REMOTE_USER'])) {
                            echo '<li class="user">';
                            tpl_userinfo();
                            echo '</li>';
                        }
                        tpl_action('admin', 1, 'li');
                        tpl_action('profile', 1, 'li');
                        tpl_action('register', 1, 'li');
                        tpl_action('login', 1, 'li');
                    */?>
                </ul>
            </div>
        <?php /*endif */?>

        <div id="dokuwiki__sitetools">
            <h3 class="a11y"><?php /*echo $lang['site_tools']; */?></h3>
            <?php /*tpl_searchform(); */?>
            <div class="mobileTools">
                <?php /*tpl_actiondropdown($lang['tools']); */?>
            </div>
            <ul>
                <?php
/*                    tpl_action('recent', 1, 'li');
                    tpl_action('media', 1, 'li');
                    tpl_action('index', 1, 'li');
                */?>
            </ul>
        </div>

    </div>

    <?php /*if($conf['breadcrumbs'] || $conf['youarehere']): */?>
        <div class="breadcrumbs">
            <?php /*if($conf['youarehere']): */?>
                <div class="youarehere"><?php /*tpl_youarehere() */?></div>
            <?php /*endif */?>
            <?php /*if($conf['breadcrumbs']): */?>
                <div class="trace"><?php /*tpl_breadcrumbs() */?></div>
            <?php /*endif */?>
        </div>
    <?php //endif ?>

    <?php //html_msgarea() ?>

    <hr class="a11y" />
    </div>
</div>
-->


	<!-- Modal -->
	<div id="subox" class="modal modal-subscribe hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<button class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-heading">Live Demo Access</h4>
		
	  <div class="modal-body">
            <!--
            <p class="error"></p>
			<form id="form_subscribe" autocomplete="off">
                <div class="control-group">
				<h5>To get Demo access please specify your email address.</h5>
                    <div class="controls">
                        <input type="email" id="input_subscribe_email" name="email" placeholder="Your e-mail" />
						<p class="cols">
							<input type="submit" id="button_subscribe" class="btn orange" value="SUBMIT" />
						</p>
                    </div>
                </div>
                <div class="clear"></div>
            </form>-->
			<div id="mc_embed_signup">
			<form action="//repucaution.us9.list-manage.com/subscribe/post?u=9e5b691dd9fd7b6d2524fdb5a&amp;id=f986b8c273" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate  autocomplete="off">
                <div class="control-group">
				<h5>To get Demo access please specify your email address.</h5>
				<p>Once you confirm your e-mail, we will send the demo link to your e-mail box.</p>
                    <div class="controls">
					
					<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Your e-mail" required />
					<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;"><input type="text" name="b_9e5b691dd9fd7b6d2524fdb5a_f986b8c273" tabindex="-1" value=""></div>
					<div class="clear"></div>
					<p class="cols">
					<input type="submit" style="width: 120px !important;" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn orange button" />
					</p>
                    </div>
                </div>
                <div class="clear"></div>
            </form>
			</div>
			
	  </div>
	</div>