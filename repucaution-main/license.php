<!DOCTYPE html>
<!--[if lt IE 9]><script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<!--[if IE 8 ]><html class="ie8"><![endif]-->
<html>
<head>
	<title>License Agreement - RepuCaution</title>
	<meta name="google-site-verification" content="yINKC1HicZWAdtt6aEgq7gRXaJO_aF_DMcgYZcn6k4E" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Earn money with us! Join RepuCaution software affiliate programm and get up to 30% sales commission.">
	<meta name="keywords" content="Software, affiliate, comission" />
	<meta name="author" content="RepuCaution LLC">

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="css/master.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css">
	<!-- css -->
	
	<!-- favicon -->
	<link rel="icon" type="image/png" href="favicon.png">
	<!--[if IE]><link rel="shortcut icon" href="favicon.ico"/><![endif]-->
	<!--/ favicon -->
</head>
<body>

<?php include 'includes/header.php'; ?>

<section class="container posts">
	<h1>License Agreement</h1>
	
	<!-- post -->
	<article class="post clearfix">
		
		<div class="post-content">
	
	<p>
		By purchasing the software you agree to all provisions of the Agreement that apply to Standard Single Domain Licence (License). 
	</p>
	<p>
		Repucaution grants non-exclusive licence to use the software on a single top level domain (URL) and unlimited number of subdomains and/or folders.
	</p>
	<p>
	</p>









	<h3>1. Granted Permissions</h3>
	<h5>License holder allowed:</h5>
<p>
- to customize functions the Software to suit business needs. No limitations can be applied
</p>
<p>
- to customize User Interface of the Software
</p>
<p>
- to lauch public SaaS solution to get revenue, to use the software for internal purposes without limitations except to the extent not permitted in this Agreement
</p>
<p>
- to use the Software under own Trademark or Business Name
	</p>
	<h5>License holder is nor permitted:</h5>
	<p>
		- to distribute the Software as source code even if customizations applied
<p>
</p>
- to share the Source Code even if customizations applied
<p>
</p>
- distribute or share individual copies of files, libraries, or other programming material in the Software package
<p>
</p>
- modify the software to function in more than instance or location (URL, domain, subdomain, etc.) from a single set of source program files unless each location is separately licensed
<p>
</p>
- use the Software in such as way as to condone or encourage terrorism, promote or provide pirated Software, or any other form of illegal or damaging activity
	</p>
	









	<h3>2. Refund Policy</h3>
	<p>
		All sales are funal and no refunds can be made except crucial bugs in the Software that make it non-functional and can't be fixed within 3 days. Bugs related to Client's hosting environment, Software misuse or other actions and tools that a not related to Software code can't be considered and reaasons for refund.
	</p>
	





	<h3>3. Title</h3>
	<p>
		Title, ownership rights, and intellectual property rights in the Software shall remain with Repucaution.
	</p>
	<p>
		The Software is protected by copyright laws and treaties. Title and related rights in the content generated through the Software is the property of the applicable content owner and may be protected by applicable law.
	</p>
	







	<h3>4. Termination</h3>
	<p>
		This Agreement will terminate automatically upon failure to comply with the limitations described herein or on written notice from an authorized representative of Repucaution.
	</p>
	<p>
		On termination, you must destroy all copies of the Software within 48 hours. Termination of the license due to noncompliance will not result in any refunds of license fees.
	</p>
	




</div>
	</article>
	<!--/ post -->
	

	
</section>

<?php include 'includes/footer.php'; ?>
<?php include 'includes/overall/footer.php'; ?>