<!DOCTYPE html>
<!--[if lt IE 9]><script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<!--[if IE 8 ]><html class="ie8"><![endif]-->
<html>
<head>
    <title>Installation instruction - RepuCaution</title>
    <meta name="google-site-verification" content="yINKC1HicZWAdtt6aEgq7gRXaJO_aF_DMcgYZcn6k4E" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="Earn money with us! Join RepuCaution software affiliate programm and get up to 30% sales commission.">
    <meta name="keywords" content="Software, affiliate, comission" />
    <meta name="author" content="RepuCaution LLC">

    <!-- css -->
    <link rel="stylesheet" type="text/css" href="css/master.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css">
    <!-- css -->

    <!-- favicon -->
    <link rel="icon" type="image/png" href="favicon.png">
    <!--[if IE]><link rel="shortcut icon" href="favicon.ico"/><![endif]-->
    <!--/ favicon -->
</head>
<body>


<div style="margin-left: 100px">
    <div class="feature-text">

        <h5>Introduction</h5>

        <p>

            Repucaution is committed to protecting the privacy of all individuals who:

        <ul>
            <li>visit application's website located at http://repucaution.com/, which includes all

                subdomains, present and future (the "Website");</li>
            <li>use the mobile applications that Repucaution makes available from time to time (the

                "Applications");
                and </li>
            <li> use the application</li>

        </ul>

        </p>
        <p>
            To make this policy easier to read, we call the Website, the Applications and the Platforms

            together the "Services".
        </p>
        <p>    It is application’s policy to respect your privacy and the privacy of all users of the Services. This

            Privacy Policy has been established to help you understand our commitment to protecting your

            privacy and personal data, and the steps we take to ensure it. By visiting and/or using any of the

            Services, you agree to be bound by the terms of the present Privacy Policy (the "Privacy

            Policy"). Where the present Privacy Policy refers to application, it may refer to the Services or to

            Repucaution, depending on the context.
        </p>
        <p>
            Application reserves the right, at any time, to modify or replace the Privacy Policy. Please check

            the Privacy Policy periodically for changes, though we will also notify you via email or other

            direct electronic communication method of any changes that, in our sole discretion, materially

            impact your use of the Services or the treatment of your Personal Information. Your use of the

            Services following the posting of any changes to the Privacy Policy constitutes acceptance of

            those changes.
        </p>

    </div>

    <div class="feature-text">
        <h5>About this Privacy Policy</h5>
        <p>
            This Privacy Policy covers the treatment of personally identifiable information ("Personal

            Information") and other non-identifiable information gathered by Repucaution when you are

            using or accessing the Services.

        </p>

        <p>
            This Privacy Policy does not apply to the practices of any third-party services such as Facebook

            or Twitter that you elect to access through the Platforms (the "Supported Platforms") or to any

            applications developed by third parties that Repucaution does not own or control ("Third- Party

            Apps") or to any third parties who use the Repucaution Application Programming Interface

            (API) to perform any function related to the Platforms. As an example, the Privacy Policy does

            not cover any information or other content you can view via the Services on Supported Platforms

            (but which was not posted there using the Services) or information you provide to Third-Party

            Apps accessed via the Services. While we attempt to facilitate access only to those Supported

            Platforms and Third-Party Apps that share our respect for your privacy, we cannot take

            responsibility for the content or privacy policies of any Supported Platforms or Third-Party

            Apps. We encourage you to carefully review the privacy policies of any Supported Platforms or

            Third-Party Apps you access via the Services.
        </p>
    </div>
    <div class="feature-text">
        <h5>Personal information we may collect and use</h5>
        <p>
            When you first register for a Repucaution account we collect some personal information about

            you such as:
        <ul>
            <li>your full name, username, and email address</li>
            <li>your password, in connection with your account and your log-in facility</li>
            <li>a unique Repucaution user ID (an alphanumeric string) which is assigned to you upon

                registration</li>
            <li>other optional information as part of your account profile</li>
        </ul>
        </p>

        <p>
            As a registered user of Repucaution, we may also collect the following:
        <ul>
            <li>information you post in the form of messages, conversations, or contributions to

                discussions</li>
            <li>information we may receive relating to communications you send us, such as queries or

                comments concerning our services</li>
            <li>information relating to your real time location, but only where you enable your computer or

                mobile device to send us location information or when posting through the Supported Platforms</li>

        </ul>
        </p>

        <p>
            Repucaution uses the collected information for the following general purposes:
        <ul>
            <li>to identify you when you log in to your account</li>
            <li>to enable us to provide the Services to you</li>
            <li>to analyze the Website , Services and our visitors, including research into our user demographics
                and user behaviour in order to improve our content and Services</li>

        </ul>
        </p>

    </div>



</div>
</body>

</html>