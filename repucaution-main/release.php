<?php
$releases = array(
    array(
        'version' => '1.7',
        'date' => 'April 6, 2016',
        'description' => 'The new version improves user experience.<br/><br/>Now it is possible to use the full potential of <a href="http://repucaution.com/knowledge-base/social_media_management:content_calendar_-_user_s_guide">content calendar</a> to manage all scheduled posts in a more efficient way.<br/><br/> <a href="http://repucaution.com/knowledge-base/brand_reputation_monitoring:analytics_-_user_s_guide">Piwik analytics platform</a> enables users to monitor their web traffic.<br/><br/> Integration with <a href="http://repucaution.com/knowledge-base/social_media_management:telegram_channels_-_user_s_guide">Telegram</a>  messaging app makes it possible to post content to its public channels.'
    ),
    array(
        'version' => '1.6',
        'date' => 'September 21, 2015',
        'description' => 'We continue improving Repucaution. Version 1.6 includes several great enhancements.<br/><br/>Posting tool supports <a href="http://repucaution.com/knowledge-base/social_media_management:automation_tools_-_user_s_guide">bulk upload</a> (.csv files) and <a href=" http://repucaution.com/knowledge-base/social_media_management:automation_tools_-_user_s_guide">recurring posts</a> now.  It makes remote posting and scheduling even more effective and easier.<br/><br/>Twitter marketing tools provide a great <a href="http://repucaution.com/knowledge-base/twitter_marketing_tools:automation_features_-_user_s_guide">account analysis</a> and let the user set <a href="http://repucaution.com/knowledge-base/twitter_marketing_tools:automation_features_-_user_s_guide">spam account filters</a> in order to increase search criteria efficiency.<br/><br/>The software has become <a href="http://repucaution.com/knowledge-base/general:new_language">truly multilingual</a>. It`s much easier for developers to work on localization files.<br/><br/>From now on user authorization via Google and Facebook is available.'
    ),
    array(
        'version' => '1.5',
        'date' => 'July 14, 2015',
        'description' => 'The release includes new responsive dashboard UI, additional features and tools.<br/><br/> The dashboard layout has been completely redesigned. Now it is smooth and clear which makes user navigation totally enjoyable. And it is responsive.<br/><br/> <a href="http://repucaution.com/knowledge-base/social_media_management:image_post_designer_-_user_s_guide">A brand new image post designer</a> allows to create engaging images for posting.<br/><br/> <a href="http://repucaution.com/knowledge-base/multiple_profiles:profiles_management_-_user_s_guide">Multiple profile</a> functionality is now available. The user can create multiple profiles (the admin defines if the plan comes with multiple profiles functionality or not) with different social media channels connected - a must feature for those social media experts and agencies that work with customer`s projects. To see necessary information for dashboard sections, it is enough to switch from one profile to another.<br/><br/> The software offers <a href="http://repucaution.com/knowledge-base/twitter_marketing_tools:automation_features_-_user_s_guide">extended automation functionality for Twitter</a>: it is possible to follow new users by search criteria, set follow / unfollow rules, enable auto-retweet or auto-favourite features and more.<br/><br/> The plan payment logic for Stripe has been rebuilt either. Now recurrent payment flow is enabled to avoid the necessity to sign up each time anew as from now on plans are prolonged automatically.'
    ),
    array(
        'version' => '1.4',
        'date' => 'March 25, 2015',
        'description' => 'New version of the script brings more possibilities to social media marketers. We have integrated <a href="http://repucaution.com/knowledge-base/social_crm:web_radar_-_user_s_guide">Instagram</a> API to allow you to track mentions and hashtags containing the keywords you specified. It can be your brand or business name, for example.<br/><br/>Another feature is <a href="http://repucaution.com/knowledge-base/social_crm:web_radar_-_user_s_guide">auto-follow</a> system for Twitter, you can now get more targeted Twitter followers by enabling this function, it will search for brand influencers across the network and automatically follow these accounts.<br/><br/>Finally you have now an option to add <a href="http://repucaution.com/knowledge-base/super_admin:super_admin_functionality_-_user_s_guide"> private plans</a> not published to your public plan page.',
    ),
    array(
        'version' => '1.3',
        'date' => 'December 18, 2014',
        'description' => 'The service has become more efficient in contact, account and plans management. Now Repucaution software is integrated with <a href="http://repucaution.com/knowledge-base/super_admin:super_admin_functionality_-_user_s_guide">Mailchimp</a> which allows to export contact information to service lists. Users are encouraged to invite <a href="http://repucaution.com/knowledge-base/general:collaboration_team_-_user_s_guide">collaboration team members</a> to get help with accounts monitoring. New features for plans management sections were added including <a href="http://repucaution.com/knowledge-base/super_admin:super_admin_functionality_-_user_s_guide">free trial plans</a> per month or per year. ',
    ),
    array(
        'version' => '1.2',
        'date' => 'November 4, 2014',
        'description' => "Our social media marketing service becomes bigger and software now includes <a href='http://repucaution.com/knowledge-base/social_crm:social_crm_-_user_s_guide'>social CRM</a> feature that allows adding customers to the system, track their activity in social networks and engage. Use this business approach if you want to improve your brand image and provide special offers to clients.<br/> The social CRM also enables tracking the company's activity. Instagram network has been finally integrated, you may start using it under CRM section. ",
    ),
    array(
        'version' => '1.10',
        'date' => 'June 6, 2014',
        'description' => 'Added new payment systems: Stripe, Authorize.net in addition to paypal. Added new and more advanced pricing plans configurator. System updated with numerous improvements and some bug fixes.',
    ),
    array(
        'version' => '1.06',
        'date' => 'March 15, 2014',
        'description' => 'Repucaution software moved on the next level with new release. We added some ultimate features to improve brand reputation management functionality. One of the most important features is <a href="http://repucaution.com/knowledge-base/social_crm:web_radar_-_user_s_guide">Influencers watch</a> - intelligent algorithm that detects brand influencers among other people that mention your brand name in their conversation. Track and engage with them to protect your business.',
    ),
    array(
        'version' => '1.05',
        'date' => 'January 2, 2014',
        'description' => 'Software got a number of speed and performance improvements, bug fixes and new features: Linkedin integration (remote status update, stats) and Foursquare API integration to track check ins and <a href="http://repucaution.com/knowledge-base/brand_reputation_monitoring:reviews_-_user_s_guide">reviews</a>.',
    ),
    array(
        'version' => '1.04',
        'date' => 'November 29, 2013',
        'description' => 'Google plus has been finally integrated and it is now possible to monitor mentions of your brand name or any other keyword in this network.',
    ),
    array(
        'version' => '1.03',
        'date' => 'October 10, 2013',
        'description' => 'Social posting application got complete social media <a href="http://repucaution.com/knowledge-base/social_crm:web_radar_-_user_s_guide">keyword monitoring functionality</a> allows to track mentions of you, your brand, trademark or business name in social networks and engage with those who influent reputation of your business. There are advanced keyword settings available to let you search for exact mentions or specific sentiments. Multiple keyword profiles available for all users.',
    ),
    array(
        'version' => '1.02',
        'date' => 'August 4, 2013',
        'description' => 'Software how has improved site administrator dashboard with advanced settings, software subscription module with configurable pricing plan and Paypal payments gateway integrated. Its possible now to configure the script and make it either free or paid right through the admin dashboard.',
    ),

    /*The service has become more efficient in contact, account and plans management.
Now Repucaution software is integrated with Mailchimp which allows to export contact information to service lists.
Users are encouraged to invite collaboration team members to get help with accounts monitoring.
New features for plans management sections were added including free trial plans per month or per year. */
);

?><!DOCTYPE html>
<!--[if lt IE 9]><script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<!--[if IE 8 ]><html class="ie8"><![endif]-->
<html>
<head>
	<title>Releases - RepuCaution</title>
	<meta name="google-site-verification" content="yINKC1HicZWAdtt6aEgq7gRXaJO_aF_DMcgYZcn6k4E" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="description" content="Earn money with us! Join RepuCaution software affiliate programm and get up to 30% sales commission.">
	<meta name="keywords" content="Software, affiliate, comission" />
	<meta name="author" content="RepuCaution LLC">

	<!-- css -->
	<link rel="stylesheet" type="text/css" href="css/master.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css">
	<!-- css -->
	
	<!-- favicon -->
	<link rel="icon" type="image/png" href="favicon.png">
	<!--[if IE]><link rel="shortcut icon" href="favicon.ico"/><![endif]-->
	<!--/ favicon -->
</head>
<body>

<?php include 'includes/header.php'; ?>
<?php include 'includes/modules/modal.php'; ?>
<section class="container posts">
	<h1>Release Notes</h1>
	<?php foreach($releases as $release):?>
        <!-- note -->
        <article class="post clearfix">
            <small class="meta-date pull-left"><?php echo $release['date'];?></small>
            <div class="post-content">
                <h2><a><?php echo $release['version'];?></a></h2>
                <p><?php echo $release['description'];?></p>
            </div>
        </article>
        <!--/ note -->
	<?php endforeach;?>

</section>

<?php include 'includes/footer.php'; ?>
<?php include 'includes/overall/footer.php'; ?>