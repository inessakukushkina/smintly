<?php

require_once('aws/aws-autoloader.php');

use Aws\Common\Aws;

// Create a service builder using a configuration file
$aws = Aws::factory('config.json');

// Get the client from the builder by namespace
$client = $aws->get('Ses');

$result = $client->sendEmail(array(
    // Source is required
    'Source' => 'v.o.v.a@tut.by',
    // Destination is required
    'Destination' => array(
        'ToAddresses' => array('v.o.v.a@tut.by'),
        //'CcAddresses' => array(),
        //'BccAddresses' => array(),
    ),
    // Message is required
    'Message' => array(
        // Subject is required
        'Subject' => array(
            // Data is required
            'Data' => 'Hello from repucaution.com!',
            'Charset' => 'utf-8',
        ),
        // Body is required
        'Body' => array(
            'Text' => array(
                // Data is required
                'Data' => 'This is the message body.',
                'Charset' => 'utf-8',
            ),
            'Html' => array(
                // Data is required
                'Data' => 'This is the message body.',
                'Charset' => 'utf-8',
            ),
        ),
    ),
    //'ReplyToAddresses' => array(),
    //'ReturnPath' => '',
));
